---
layout: page
title: A propos
permalink: /about/
---

Pour les FAQ, c'est par [ici](/pages/faq/), et vous avez quelques conseils d'outils par [ici](https://raindrop.io/ChezIceman/les-outils-d-iceman-38913607/sort=title&perpage=30&page=0)  tandis que la revue de presse évolue chaque jour [ici](https://raindrop.io/ChezIceman/revue-de-medias-29066144/theme=dark&sort=title&perpage=50&page=0).

Quelques articles sont in <a href="{{ site.baseurl }}/pages/english">English</a>

Ceci est mon blog mais me sert aussi à me présenter

### Professionnellement

* Plus de 20 ans d’expérience dans l’automobile, en Recherche et Développement en abordant la qualité, la maintenance, la métrologie (création et gestion d'un laboratoire niveau ISO 17025).
* Expérience dans un laboratoire d’analyse chimique lié au nucléaire.


### Sur le web

* Rédacteur dans un webzine culturel
* Blogueur sur spip, dotclear, wordpress (autohébergé ou non)… puis Jekyll.
* Création et gestion des sites Au Joyeux Végé et Histozic.fr.
* Community manager sur divers forums.

### Mes autres activités
* Utilisation de Linux depuis 2000, à 100% depuis 2016.
* Bénévolat dans des refuges de protection animale, organisation de collectes.
* Rédaction d'un roman ([epub](https://e.pcloud.link/publink/show?code=XZqR17ZOj7UJpj6QjLAKMT5leHPUH1mERxy) ou [pdf](https://e.pcloud.link/publink/show?code=XZ6R17ZClwKjkFcEGJTEvXAFNsbbFGR3jAy))
* d'une petite BD en collaboration avec Odysseus ([pdf](https://e.pcloud.link/publink/show?code=XZiRt7ZJ493bWWgb0RkRM6IhoyaSBJ0lS9V))
* d'une nouvelle ([epub](https://e.pcloud.link/publink/show?code=XZOR17ZEonKToOqr0plRCE6CGnYNhgPphJk) ou [pdf](https://e.pcloud.link/publink/show?code=XZIR17Z3YAu3mhmWc8JNxU3XYWULyhwPg0X))
* et de la poésie (haïkus en version [epub](https://e.pcloud.link/publink/show?code=XZNR17Zc4J21j0tp90eVN8yee0ijb9vJLB7), haïkus illustrés par [Péha](https://lesptitsdessinsdepeha.wordpress.com/2016/04/04/haikus-1ere-partie/) en [pdf](https://e.pcloud.link/publink/show?code=XZAR17ZxwMBeb8PkPfWXHGk77QInkKdvwgy), poèmes : [Vol 1](https://e.pcloud.link/publink/show?code=XZVL17ZM3Giu8AY1rbi1H7iqcU2RJSXWiuV))
* Création musicale (un album de musique électronique [sur Bandcamp](https://iceman75.bandcamp.com/releases))
* Et les vidéos sur la [chaîne Peertube](https://videos.pair2jeux.tube/accounts/cheziceman/video-channels). 
* Et enfin, le [Portfolio est ici](/pages/portfolio/) avec différents thèmes.

### Où je suis allé

La carte est [ici](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/voyages.jpg)