

Ceci est un miroir des meilleurs articles de mon blog principal, [Cheziceman.fr](https://cheziceman.fr), pour ne plus avoir ni publicité, ni cookie. Mais aussi la base de test pour ses évolutions

Le contenu est publié sous licence creative commons. La base a été complètement cassée donc je ne la cite plus.

## Mes principales modifications

* Le meta name keywords a été ajouté
* Le meta name image a été ajouté 
* Un sitemap automatique a été ajouté
* Ajout des boutons de partage sans réseaux sociaux privateurs, dont un pour mastodon sans JavaScript 
* Des menus thématiques basés sur "category" ont été créés dans les Pages
* la limitation de 1000 articles a été contournée en créant des répertoires par année.
* Adaptation à la résolution du terminal ou du navigateur utilisé
* Adaptation au mode dark du navigateur
* Simplification de la feuille de style