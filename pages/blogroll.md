---
layout: default
title: Et pour quelques blogs de plus
---

Quelques liens essentiels, indispensables… bref, on se demande à quoi ça sert puisque vous les connaissez : (l’ordre est aléatoire)

**Généralistes**

* [https://alias.erdorin.org](https://alias.erdorin.org) : Blog à part, troisième époque, le blog de Stephane Gallay, progressif devant l’éternel.
* [https://www.parigotmanchot.fr/](http://www.parigotmanchot.fr/) : Le blog d’un parigot devenu manchot dans son cher bourg.
* [https://www.bloglibre.net/](https://www.bloglibre.net/) : Le blog libre continue de vivre, avec cascador, damien, augier, etc…
* [https://chezmo.wordpress.com](https://chezmo.wordpress.com) : Le Bar à BD de MO, le site de chronique BD que je préfère.
* [https://odysseuslibre.be/site](https://odysseuslibre.be/site) : Le petit monde d’Odysseus est libre, en images et musiques.
* [http://huld.re/journal/](http://huld.re/journal/) : Le journal de bord f6k, libriste et humain.
* [https://sima78.chispa.fr](https://sima78.chispa.fr) : Le blog de SIMA78, encore un blog libriste mais pas que.
* [https://lord.re/](https://lord.re/) : Le blog de Lord avec du cinéma et de la technique.

**Techniques**

* [https://arpinux.org/home/](http://arpinux.org/home/) : Le blog d’Arpinux, celui qui a réussi à mettre la Debian dans sa main
* [https://leblogdolivyeahh.wordpress.com/](https://leblogdolivyeahh.wordpress.com/) : Le blog d’Olivyeahh, sur GNU/Linux essentiellement.
* [http://leblogdericgranier.org/](http://leblogdericgranier.org/) : Le Blog d’Eric Granier, logiciel libre, cinéma