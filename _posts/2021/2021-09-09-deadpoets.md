---
layout: post
title: Cinéma - Le Cercle des Poêtes disparus de Peter Weir (1989)
category: cinema
tags:  cinéma, poésie, 1980s, cinémathèque idéale, philosophie
---

**Dans la liste des films que je revois régulièrement sans lassitude, il y a ce film de Peter Weir qui a marqué ma génération.**

Et pourtant je n'étais pas allé le voir à sa sortie, comme si je ne me sentais pas prêt, ou que je voulais ma différence. Je ne sais plus vraiment, mais il me marque d'autant plus qu'il y a Robin Williams, un des plus grands acteurs selon moi. 

*En 1959, aux États-Unis, Todd Anderson, un garçon timide, est envoyé dans la prestigieuse académie de Welton, dans l'État du Vermont, réputée pour être l'une des plus fermées et austères du pays et où son frère aîné a suivi de brillantes études. Il y fait la rencontre d'un professeur de lettres anglaises aux pratiques pédagogiques plutôt originales, M. Keating, qui encourage le refus du conformisme, l'épanouissement des personnalités par la vie en poésie, l'art de profiter de l'instant présent ("Carpe Diem") et le goût de la liberté.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/deadpoets.jpg)

C'est un film générationnel qui arrive à la fin des années 80, juste avant ces années 90 désenchantées. Il y a ceux qui n'ont retenu que le carpe diem, un peu trop réducteur. J'en ai une interprétation toute personnel. D'ailleurs, le fait d'avoir un groupe de personnages renforce l'appropriation que le spectateur peut en avoir. Le scénario est effectivement remarquable et détonne dans ce maelström de film hollywoodiens, surtout par sa fin.

Mais il ne faudrait pas oublier qu'un film est un tout. Il y a aussi la mise en image qui est très esthétique, avec des plans de coupe qui n'en sont pas vraiment, apportant quelques moments de grâce. Il y a la musique, entre des emprunts à Beethoven, et le thème merveilleux et émouvant de Maurice Jarre. Il y a le jeu incroyable de Robin Williams dont le regard est d'une puissance et d'une profondeur inégalée. Il fallait aussi de jeunes acteurs à son niveau, avec le directeur Peter Weir derrière. Et forcément, l'histoire touche... ou pas.

Dans la scène finale, il y a ceux qui se lèvent et les autres. J'ai parfois l'impression que le monde est ainsi fait. Paradoxalement, beaucoup n'ont peut-être vu qu'une partie infime de ce que voulait dire le film, un peu comme s'ils suivaient la meute à l'époque. Il faut sortir de l'émotion du moment, des discussions de groupes avec leurs leaders pour trouver sa propre voie avec ce film. Il faut se lever et monter sur la table et le regarder à nouveau. Je me surprend à trouver à chaque fois de nouveaux éléments, volontaires ou pas, seul Peter Weir pourrait le dire. 

Accessoirement, le film permet aussi de découvrir la poésie et la littérature américaine et britannique. Bon, en Version Française c'est assez inégale mais en version original c'est bien mieux. Pour ma dernière vision, j'ai mis les sous-titres en anglais. Il me manquait quelques expressions, je dois dire mais on entre bien plus dans la poésie des mots. Dans une période où l'on reparle enfin de poésie, voilà un film qui garde toute sa force malgré le poids des ans.

En video : [la bande annonce ![video](/images/youtube.png)](https://www.youtube.com/watch?v=nGf392QDZKs){:target="_blank"}

> "The Cat sat on the mat"