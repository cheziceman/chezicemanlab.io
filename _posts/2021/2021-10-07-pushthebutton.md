---
layout: post
title: Musique - The Chemical Brothers - Push the Button (2005)
category: musique
tags: musique, big béat, techno, 2000s, hiphop
---

**Pour compléter la période Big Beat de la fin des années 90, il faut parler forcément d'un des initiateurs du style : The Chemical Brothers. Mais pourquoi ne pas aborder autre chose que les très recommandables trois premiers albums ?**

Je suis donc allé poser mes oreilles sur ce 5ème opus qui amorce une nouvelle évolution du duo. Évidemment, le Big Beat n'était déjà plus en 2005 et la pop s'abreuvait de dance et d'électro plutôt que des sons plus brutaux des prédécesseurs. Le Rock n'avait plus l'influence passée et nous entrons dans le règne du Hip-hop, du rap mainstream. Les frangins chimiques ont bien compris ça...

![cover](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/pushtebutton.jpg)
 
Ca frappe fort avec "Galvanize", sa sonorité arabisante en sample, son flow, mais quelque part, on ne reconnaît plus le style Chemical Brothers malgré l'inventivité de la production. Q-Tip assure sur les vocals. Forcément, je m'attends à une suite dans la même veine electro-hip-hop. On reconnaît déjà plus le duo sur "The Boxer" avec le travail sur les rythmiques tout en restant accessible. On ne change pas une équipe qui gagne. Ca envoie sur les dancefloors! Pourtant les morceaux sont longs, pas du tout calibrés radio. Prenez "Believe" et ses 7 minutes... Et pourtant je défie de ne pas bouger son cul et ses jambes sur ce titre imparable. La rythmique est soutenue par Kele Okereke de *Bloc Party*. 

Évidemment, si on n'aime pas les variations autour d'un thème, c'est raté avec les frangins. On a même droit à des titres plus Ambient, comme ce "Hold tight London" qui remplit bien son rôle. Je lui préfère nettement "Come inside", plus dans les habitudes du groupe. Revoilà enfin du gros son, un beat assassin. Le morceau sert effectivement de parfaite base à des battles de danse. C'est toujours l'univers de prédilection du groupe et "The Big Jump" est là pour ça aussi avec son beat, ses breaks, sa rythmique efficace. On peut par contre être surpris par "Left Right" qui paraît basique ... Cette fois c'est Anwar Superstar qui s'occupe du flow sur un vrai morceau hip-hop.

Finalement, entre les deux, le cœur balance mais il y a un point commun encore, c'est la danse. Ce sont toujours des morceaux dansants... Un peu moins pour "Close your eyes" qui garde un côté ambient très présent sur une mélodie pop. En effet, c'est le groupe *The Magic Numbers* qui fait le job, l'arrangement étant plus "chimique". Pas forcément ce que j'attends. Mon truc c'est déjà plus "Shake break bounce", très caliente, même si je trouve que le titre ne décolle pas vraiment. J'aime beaucoup les samples de "Marvo Ging" même si j'aurai bien vu leur ami "Fatboy slim" aux manettes, ou même les *Basement Jaxx*. On reste bien dans la même école. On termine sur un titre plus planant et je reste interrogatif.

L'album montre bien que The Chemical Brothers sait encore produire de bons titres, nous étonner mais il n'y a pas l'homogénéité passée. J'y ressens une hésitation à vraiment basculer sur autre chose, quitte à perdre son âme. Les critiques furent bonne car justement on y trouve quand même son compte. Le Big Beat est du passé, c'est sûr, mais la musique électronique se réinvente sans cesse. 

En video : [![video](/images/youtube.png)](https://www.youtube.com/watch?v=Xu3FTEmN-eg)