---
layout: post
title: Pause Photo et Poésie - L'envol
category: poesie
tags:  poésie, poême
---

Ces moments nocturnes où ils passent,

visages souriants, larmes, rires,

j'aimerai les revoir, les toucher sans casse.

Et le miroir se brise, les minutes s'étirent.

La feuille froissée s'envole à jamais,

les mots perdus au vent qui me charmait.


![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/poemeenvol.jpg)


