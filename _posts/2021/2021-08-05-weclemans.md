---
layout: post
title: Souvenir de Gamer - WEC Le Mans (1986)
category: geek
tags:  geek, jeu video, retrogaming, 1980s, course
---

**Ce fut l'un de mes jeux préférés dans mon époque Amstrad CPC pour la course. Il faut dire que malgré une réalisation classique et convenue, le fait d'aller sur le mythique circuit du Mans au volant d'une Porsche 956 groupe C, parle au passionné de course automobile... que j'étais.**

Je dois dire que je n'ai pas terminé ce jeu, malgré des heures à apprendre la succession de voitures que l'on rencontre sur le circuit. Si j'ai parlé des 24h du Mans, le jeu n'est évidemment pas du tout en temps réel. Il faut juste en faire quelques tours, mais cela n'a rien d'évident avec le couperet du chrono qui diminue peu à peu le temps imparti à chaque tour. Le Checkpoint, ça se mérite. Et pour cela, on doit éviter toutes ces GT, ces véhicules en perdition dans des virages pourtant connus.

Ce n'est pas du tout une simulation mais un pur jeu d'arcade, comme l'est par exemple Outrun dans la même époque. Deux vitesses, pas plus et accélérateur ou frein sont votre salut. On repère vite les points clés à se souvenir, par exemple quand on a deux véhicules en même temps devant soi et que l'on sait très bien qu'il y en a un qui va se décaler. Sang froid et réflexe doivent s'ajouter à la mémoire. Car c'est aussi un jeu d'apprentissage. Il faut s'acharner, puisque la piste reste simple à apprendre. A l'époque, les Hunaudières n'étaient pas coupées...

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/weclemans.jpg)

Pour les puristes, on peut tout de suite abréger...Le jeu reste pourtant un élément qui a fait la légende du circuit au Japon, bien avant que Mazda ne gagne l'épreuve, et Toyota depuis. Car c'est bien un jeu japonais, notamment pour cet aspect rabachage et apprentissage. Il a fallu bien des années pour retrouver un jeu d'un bon niveau pour rendre hommage à l'épreuve mancelle, et encore... Je lui trouve du charme encore aujourd'hui, même sur mon Amstrad où il fallait se contenter de moins de couleurs.


Sur cpc ça donnait ça : [![video](/images/youtube.png)](https://www.youtube.com/watch?v=bBgL_-CBpwQ)

Au moins, il restait rapide... Je me suis laissé tenter par l'arcade aussi. Forcément, c'est plus beau et ça parlera à tous les joueurs des années 80 pour la manière de rendre la vitesse par clignotement de couleur. Une technique bien révolue maintenant avec le bon vieux sprite qui décompose les mouvements du véhicule. Pour les crashs, elle s'en remettent bien, ces voitures, tout de même. On me dira qu'en dehors de la licence et de l'environnement, le jeu reste d'un classicisme éprouvé. C'est vrai, mais les souvenirs d'un gamer tiennent parfois à peu de chose...Sachant qu'il n'y a eu que des adaptations sur ordinateurs 8 bits.

*WEC Le Mans par Konami sur Arcade, C64, MSX, ZX Spectrum, Amstrad CPC*