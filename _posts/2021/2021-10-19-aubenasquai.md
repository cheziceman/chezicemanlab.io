---
layout: post
title: Littérature - Le quai de Ouistreham de Florence Aubenas (2009)
category: litterature
tags: journalisme, littérature, reportage, social, sociologie, chomage
---

**Et si je lisais un livre journalistique ? Cela fait plusieurs fois que je tourne autour de ce genre. Pas un roman, un essai. Et tant qu'à faire, sur un sujet que je connais de plus ou moins loin : La France, le social**

C'est encore madame qui m'a confié le bébé, je ne sais plus trop comment. Mais bon, Florence Aubenas, je connais. Ancienne grande reporter / journaliste de guerre, otage en Irak pendant 5 mois, puis un retour en France et un virage très marqué par le social, quelques excellents papiers dans Le Monde en mémoire, comme par exemple sur les ronds points lors de la crise des gilets jaunes. Mais là, c'est après la crise de 2008 que j'ai vécu d'une manière très particulière...J'y reviendrai un jour. Elle a décidé d'être "en immersion" comme ils disent maintenant en se mettant carrément à la place d'une demandeuse d'emploi pendant cette crise, dans un endroit qu'elle ne connaît pas (Caen), à presque 50 ans, sans expérience, ce qui corse considérablement les choses. Et là voilà donc tout simplement à postuler en intérim et à aller à Pôle emploi.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/aubenasquai.jpg)

L'accueil est frais, c'est le moins que l'on puisse dire. Tout juste si les agences d'intérim ne lui claquent pas la porte au nez, en cette période où on dégraissait sévère dans toutes les filières après les actes criminels des banquiers dans les subprimes. "Le fond de la casserole" plutôt que le "haut du panier" lui répond on sans ménagement. Chez Pôle emploi, c'est un net virage vers le chiffre, vers des gens du commercial pour faire de l'abattage. Mais on lui propose de s'orienter sur les métiers de l'entretien...Non, pas celui de l'embauche mais celui du ménage, de la propreté. Elle vit dans son petit meublé, sans voiture. Heureusement qu'il y a des transports à Caen car on la fait aller d'agence en agence pour toutes les démarches avec des impératifs horaires imposés. Par chance, pas d'enfants à charge.

Finalement, elle décroche un job à Ouistreham, pas seulement la station balnéaire mais aussi le port d'où partent les ferries vers l'Angleterre, non loin de Caen. 16km quand même. Je le connais très bien ce port... Et quelques heures du soir, plus d'autres missions en journée, tôt le matin. Les heures payées sont sous-dimensionnées par rapport au travail à réaliser, car les entreprises se font une guerre des prix. Ok, tout ça, je ne le découvrais pas vraiment. Florence Aubenas a donné de sa personne durant toutes cette période mais au moins au bout a-t-elle une sortie. Le récit est vivant, avec ses rencontres, ses témoignages, ses moments où l'on aurait envie de donner une petite leçon de respect et savoir-vivre à ces chefaillons qui se croient tout permis vis à vis de ces "invisibles". Un livre qui a le mérite de montrer une réalité qui expliqua d'ailleurs le mouvement gilet jaune. Juste 10 ans avant...

Le livre est adapté cette année au cinéma sous le titre "Ouistreham". Le livre me suffit bien mais pour ceux qui préfèrent le visuel...

