---
layout: post
title: BD - Sacrificial Vote de Edogawa et Kasai (2015)
category: bd
tags: manga, seinen, survival horror, adolescence, réseaux sociaux, haine
---

**Voilà un manga pour adulte (Seinen) qui traite d'un sujet à la mode dans les années 2010 : Les réseaux sociaux et l'adolescence. Mais dans ces 7 tomes, ça va vite partir en live !**

Nous suivons la jeune Minato qui est arrivé il y a peu dans un nouveau lycée. Comme tous les ados de sa classe, elle passe son temps sur son smartphone à chater avec ses amis. Mais un jour une mystérieuse application s'installe toute seule sur les smartphones de tous les élèves de cette classe : Sacrificial Vote. Ils doivent tous voter pour la personne qu'ils veulent "éliminer socialement". Tout le monde se pose la question de la signification de ce terme. Mais après la première "victime", la fille la plus populaire du groupe, ils vont très vite comprendre que rien ne sera jamais comme avant.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/sacrificialvote0.jpg)

Bienvenue dans le monde adolescent ? Par extension, c'est un peu [ce que j'écrivais](https://www.cheziceman.fr/2021/theoriensemble/) sur les relations humaines dans les réseaux sociaux, forums... On aime assouvir sa vengeance vis à vis des leaders, des privilégiés, prendre leur place et reproduire les même schémas. On va retrouver tout cela à la fois dans les premières victimes et dans la recomposition du groupe. Evidemment, j'ai pensé aussi à un classique japonais du genre : Battle Royale. Au début, il n'y a pas ce côté survival horror. Il vient surtout dans le 3ème et 4ème tome. C'est d'abord comprendre la raison de cette application (Tome 1). Puis chercher le coupable (Tome 2) et à ce moment tout est en place pour un joyeux massacre que la police ne tente pas trop de comprendre.

Tout cela se fait dans un dessin remarquablement précis et réaliste, comme tout bon Seinen. Les deux premiers tomes sont palpitants, efficaces. On essaye d'avoir une cohérence, de répondre par cette intrusion technologique par la technologie, car évidemment, il y a un "hacker" dans le groupe, celui qui sait coder. Ouf, pas de capuche...Loin de s'arrêter à la simple dénonciation du smartphone et de sa capacité d'espionnage, Kasai nous emmène dans son histoire...Qui aurait peut-être dû s'arrêter au tome 4. C'est souvent le problème des séries de manga, il faut faire durer le spectacle. La prolongation me paraît assez artificielle ensuite et d'ailleurs j'ai l'impression que beaucoup de lecteurs s'arrêtent là.

Je n'ai pas boudé mon plaisir malgré tout et dans cette thématique, ça fait partie des bonnes séries. Juste pourrait-on reprocher le côté caricatural de certaines planches quand le dessinateur matérialise la folie par des visages déformés et des yeux luminescents. 


![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/sacrificialvote1.jpg)

