---
layout: post
title: Série du passé - Les petits génies (1983)
category: serie
tags: série, 1980s, geek
---

**Encore une série des années 80 qui marqua mon imaginaire geek. Apparemment inspirée par le succès du film Wargame, et autres succès pour adolescents, cette série fut pourtant très éphémère.**

Elle ne dura que 18 épisodes, malgré un producteur qui sortait du succès de la série policière "Simon & Simon". Pourtant la recette était intéressante : Un groupe d'adolescents autour d'un petit génie de l'informatique (Richie joué par Matthew Laborteaux, vu dans "la Petite maison dans la prairie"), une banlieue typique des USA, des enquêtes variées allant de la tentative de corruption à l'espionnage et la défense nationale en passant par l'escroquerie. On y retrouve aussi quelques bons seconds rôles vus dans diverses séries à succès. Et le groupe réunit comme il se doit la "Girl next door" (Alice), le sportif (Hamilton) et le black cool (Jeremy). 

L'ordinateur de Richie, baptisé "Ralf" fait un peu rêver en 1983 mais pas pour très longtemps. On retrouve du Commodore, de l'Apple et autres matos en vogue à l'époque sans qu'il n'y ait de placement produit éhonté tout de même. Les références à ET, [Wargames](https://www.cheziceman.fr/2020/wargames/) et autres films pour ados sont présentes dans des clins d'oeil appuyés. Ca me faisait penser aussi à un "Club des cinq" en plus moderne, les énigmes étant tout de même plus courtes de par le format série limité à 47 minutes. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/petitsgenies.jpg)

Le temps rend tout cela suranné, voire Kitsch et pourtant c'est ce voyage nostalgique qui fait le charme de la série. On trouve assez facilement les épisodes de "Whiz Kids" en version originale aujourd'hui et je n'avais pas gardé les souvenirs exacts. En effet, ce sont les parties consacrées à nos héros et leur utilisation de l'informatique qui m'intéressaient le plus. Les enquêtes étaient souvent assez banales et c'est peut-être ce qui explique son manque de succès. Quoi que "Simon et Simon"... Même les épisodes croisés avec cette série ne la sauveront pas. 

Aujourd'hui, ça reste une curiosité pour nostalgiques, pour fans qui ont vécu cette période. Et en même temps, nous étions bien dans la figure typique du Geek tel qu'il est longtemps apparu dans les séries et films avec ce Richie, qui n'a pas eu la belle vie...Hum. 

[Le Générique ![video](/images/youtube.png)](https://www.youtube.com/watch?v=I650VhszTU4){:target="_blank"}
