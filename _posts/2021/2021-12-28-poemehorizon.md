---
layout: post
title: Pause Photo et Poésie - Horizon
category: poesie
tags:  poème , photographie
---

Regard sur l'horizon,

masqué par cette brume,

une vague de nuit à ma façon,

et de nuages en forme d'écume.



J'envoie mes rêves au firmament,

en écoutant l'eau et la terre se parler.

Que puis-je y comprendre maintenant,

que les langues se défient de râler ?



Mes yeux se couchent dans le loin,

lovés dans un rêve trop petit.

Je grelotte et et regarde mes mains,

usées et déformées par les écrits

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/poemehorizon.jpg)

