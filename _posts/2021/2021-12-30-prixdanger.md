---
layout: post
title: Cinéma - Le Prix du Danger d'Yves Boisset (1983)
category: cinema 
tags:  cinéma, cinémathèque idéale, 1980s, dystopie, télévision, médias
---

**Ce n'est pas si souvent que le cinéma français s'attaque à la science-fiction, et encore moins à une dystopie (Fahrenheit 451 de Truffaut ? ). Yves Boisset l'a fait en 1983 dans un film un peu oublié aujourd'hui mais qui mérite sa place dans une cinémathèque.**

A l'origine de ce film, il y a une nouvelle de 1958 de Robert Scheckley, "The Prize of Peril". Le sujet n'a pas été modifié, juste les lieux, et l'histoire du héros (tiens, pas d'allusion à la corrida...). Cette nouvelle influença un certain Stephen King pour The Running Man, qui donna un film avec Schwarzy. Mais alors, l'histoire, justement ? 

*"Dans un futur proche, un jeu télévisé intitulé Le Prix du danger fait fureur. Les règles sont simples : un homme doit parvenir à rejoindre un endroit secret, en échappant à cinq traqueurs chargés de le tuer. Si le candidat gagne, il se voit attribuer la somme de 1 million de dollars, ce qui n'est encore jamais arrivé... Le tout se déroule en pleine ville, filmé et retransmis en direct sur la chaîne de télévision CTV. François Jacquemard, un jeune chômeur, veut sortir de son quotidien morose et malgré les réticences de sa compagne, Marianne, décide de participer au jeu."*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/prixdudanger.jpg)

Il faut rajouter que la raison d'exister de ce jeu est que d'une part qu'une loi sur l'euthanasie a été passée et qu'il y a 5 millions de chômeurs. On y parle en dollar, ou dizaine de milliers de dollars. Accessoirement, le film fut tourné du côté de La Défense mais aussi de Belgrade (on y voit même des tourons de câbles marqués en cyrillique). Au casting, nous avons le héros à la mode à l'époque, Gérard Lanvin, au jeu un peu brut et hésitant. Face à lui, il y a Michel Piccoli extraordinaire en présentateur bateleur, Bruno Cremer en directeur de chaîne cynique, Marie-France Pisier toujours aussi magnétique, et quelques bons seconds rôles de l'époque comme Catherine Lachens, Jean Rougerie. Andréa Ferreol a un peu de mal à exister en tant qu'opposante, et Gabrielle Lazure a à peine le temps d'apparaître.

A la lecture du sujet, on pense à une sorte de Survivor (Koh Lanta pour les français) poussé à l’extrême. Si je parlais de Corrida, c'était justement pour montrer qu'il y a des gens pour se délecter de la mort et du sang comme spectacle, dont certains gardes des sceaux. Alors forcément, c'est ici le retour des jeux du cirque, sous couvert de pouvoir choisir sa mort lorsque l'on est désespéré par la vie. C'est aussi une sorte de télé-réalité avant l'heure, comme le montrera Peter Weir dans "The Truman Show". Il y a derrière tout cela une machine à fric, à audience comme dans "Quizz show". Et Boisset y va fort dans le ridicule des publicités de l'émission avec des chorégraphies outrancières. Étrangement, cette même année, c'est l'arrivée d'Hervé Bourges à la tête de TF1 pour préparer la privatisation de la chaîne avec une offensive dans le divertissement.

Il faut bien avouer que le film est très daté par ses effets spéciaux, notamment les scènes aériennes. Cela peut gâcher un peu le spectacle d'un spectateur qui aurait du mal à contextualiser. La force de l'histoire l'emporte un peu sur la mise en scène. Boisset a quand même changé des détails, comme le fait que les "tueurs" soient des gens comme le héros mais qui veulent juste assouvir leurs pulsions meurtrières. Plutôt bien trouvé pour accentuer le fait que l'humain a quelque chose de mauvais. La productrice (Marie-France Pisier) a quelque chose d'ambigu dans son rôle et cela aurait mérité d'être développé. Boisset semble avoir monté le film au plus court pour en faire un "blockbuster" d'action. Ce qui fait que l'on ne s'y ennuie pas mais que cela paraît trop court dans son dénouement. Forcément, "The Running man" bénéficiera de plus de moyens en 1987, mais y perdra aussi sur le fond. Il n'en reste pas moins daté aujourd'hui aussi.

L'évolution de la télévision n'a pas été encore jusque là mais à voir les excès de certains youtubeurs pour "exister" et le recours au buzz et au clash permanent, le sujet reste d'actualité. La Dystopie de cet univers d'anticipation se rapproche potentiellement à grand pas. 

Bande annonce : enfin plutôt la [scène d'intro ![video](/images/youtube.png)](https://www.youtube.com/watch?v=_YmZK_RCHGY)

