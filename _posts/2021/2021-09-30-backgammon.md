---
layout: post
title: Souvenir de Gamer - Le Backgammon
category: geek
tags: jeu de société, jeu, geek, 
---

**Je ne sais plus trop vers quel âge j'ai commencé à jouer à ce jeu de société. Juste que j'ai commencé par les problèmes qui paraissaient dans ["Jeux et Stratégie"](https://www.cheziceman.fr/2016/jeuxetstrategie/). Et aussi parce que j'avais une mallette de jeux de société où il y avait ce curieux plateau séparé en deux avec deux fois 12 triangles alternés de deux couleurs.**

Pourquoi j'ai accroché ? Pour ce mélange de calcul de probabilité de tirage de deux dés et de stratégie. Les problèmes me paraissaient plutôt simples (je devais être en fin de collège, début de lycée je pense) contrairement à d'autres où mon sens de la stratégie n'était pas aussi aiguisé. Cela fait donc 30 ans que je joue à ça épisodiquement et que j'ai trouvé des adversaires qui me conviennent sur bien des supports. Car l'humain, c'est bien mais dans ce cas où la probabilité est primordiale, l'ordinateur a vite fait de mâter un humain.

![image](https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/Backgammon_lg.jpg/640px-Backgammon_lg.jpg)

Le principe du jeu ? Sur le "tablier" (le plateau comme ci-dessus) le joueur gagne lorsqu'il retire tous ses pions du tablier. Il déplace ses pions à l'aide de deux dés, chaque joueur allant en sens inverse de l'autre par rapport à une position de départ. Le camps de chaque joueur est à droite du tablier sur la photo ci-dessous. Un dé spécial (videau) permet aussi de faire "une enchère" et de multiplier le score. On marque un point lorsque tous ses pions sont sortis et que l'adversaire en a déjà sorti un, deux points lorsque l'adversaire n'a pas commencé à en sortir, trois points lorsque l'adversaire a encore des pions dans notre camps. 

![image](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Bg-movement.svg/350px-Bg-movement.svg.png)

Comme tout jeu de hasard, la facilité de programmation est de permettre à l'ordinateur d'avoir les meilleurs tirages de dés plutôt qu'il ait seulement la meilleure décision en fonction d'un tirage réellement aléatoire. J'en ai vu des programmes dans ce genre, comme par exemple celui d'AI Factory sur Android. Il y en a de plus frustres visuellement qui sont pourtant bien meilleurs sur la partie stratégie. Je ne suis guère chanceux sur les tirages de dés en général et de toute façon le sel du jeu est de construire sa stratégie quelque soit la chance. On peut jouer neutre, défensif ou offensif. Neutre c'est qu'on se contentera de déplacer ses pions sans "frapper" l'adversaire lorsqu'il a un pion isolé sur une case et en choisissant simplement la probabilité la meilleure. Défensif c'est ne pas laisser de pion seul, coûte que coûte. Offensif c'est laisser l'adversaire nous frapper pour nous retrouver dans son camps et l'empêcher de sortir ses pions. 

Et J'en ai connu des parties mal engagées où j'ai changé de stratégie pour passer à l'offensive et retourner la situation. C'est ce que j'aime dans ce jeu, le fait que ce n'est jamais perdu et que l'on peut (parfois) inverser le jeu et remporter la partie avec le multiplicateur donné par l'adversaire. Donc des scores de 6 points parfois. Je les compte quand même sur les doigts de deux mains. On fait plus souvent des victoires à 2 points ou seulement à un point avec une finale où ça se joue à quelques pions pour la sortie, parce que l'adversaire peut sortir le double au bon moment (le double permet de jouer 4 pions au lieu de 2)

Aujourd'hui, je cherche toujours le jeu qui puisse m'accompagner partout pour une petite partie quand j'en ai envie. Celui de "[42 jeux indémodables](https://www.cheziceman.fr/2018/42jeuxindemodables/)" n'est pas trop mal par exemple, alors que les meilleurs sur mobile sont plutôt moches et instables, issus de version Windows la plupart du temps pour la programmation de l'IA. Disons que si j'ai déjà réussi à faire "Backgammon" (victoire alors que l'adversaire a des pions dans mon camp), je n'ai pas vu de tricherie manifeste sur le tirage aux dés un peu trop conciliant. 

