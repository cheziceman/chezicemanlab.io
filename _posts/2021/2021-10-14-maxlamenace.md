---
layout: post
title: Série du passé - Max la Menace (1965-1969)
category: serie 
tags: série, humour, espionnage, 1960s
---

**C'est une série qui a marquée mon enfance à la télévision, encore. Une série d'humour décalé et parodique comme je les affectionne. Et j'en ignorais pourtant beaucoup**

A commencé par le fait qu'elle a été créée par **Mel Brooks** (ou j'avais oublié) qu'on retrouvera longtemps au cinéma dans des comédies souvent parodiques comme les Folles histoires ..., les Producteurs, Le grand Frisson (High Anxiety) entre autres. Oui, j'aurai quand même pu me douter de quelque chose puisque c'est le même genre d'humour avec de l'absurde, du gag bien tarte à la crême parfois et le tout dans une ambiance rétro qui tient uniquement au fait que je l'ai vue 15 à 20 ans après sa création. L'histoire ? "Max la menace (Maxwell Smart dans la version originale), l'agent 86, travaille pour CONTROL, une organisation de contre-espionnage qui contrecarre tous les maléfiques projets de KAOS, organisation visant à répandre le ... chaos." 

![image](https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/DonAdams.jpg/393px-DonAdams.jpg)

La personnalité de la série tient aussi beaucoup à son acteur principal (Don Adams) et à son doubleur français (Guy Piérauld). Max est gaffeur, vantard, séducteur raté et donc bon client pour une comédie. Avec lui, il y a l'agent 99 (Barbara Feldon) qui sous des airs parfois naïfs est en réalité la vraie pro du duo. Autour de ce duo, il y a tous les gadgets propre à l'espionnage dont la fameuse chaussure-téléphone de Max. J'ai essayé avec les miennes, ça ne fonctionne pas.  Le charme commence pourtant par le générique avec les portes et la cabine téléphonique. Et puis on retrouve les vieilles recettes des clowns avec l'opposition entre Max et son chef. Les méchants sont bien caricaturaux avec des hommages aux films de genre des années 40-50, en plus des clins d’œil à un certain James et les autres espions célèbres.

[Générique ![video](/images/youtube.png)](https://www.youtube.com/watch?v=sWEvp217Tzw){:target="_blank"}

Étrangement, on trouve aujourd'hui peu d'extraits français et c'est dommage. Surtout qu'on a eu une pitoyable adaptation cinéma qui n'a absolument pas le charme et l'intensité du duo originel. Inutile d'espérer revoir aussi cette série dans les rediffusions. Comme HBO a les droits, c'est un peu mort pour l'Europe. Ne me reste alors que des souvenirs pour cette série et ce n'est peut-être pas plus mal. La série me paraissait ancienne, classique, elle le reste aujourd'hui. Si des fois vous la croisez dans un catalogue...Parce qu'évidemment le remake a tué le truc. Grr ! 

[Extrait ![video](/images/youtube.png)](https://www.youtube.com/watch?v=16_6XrPlV-w){:target="_blank"}



