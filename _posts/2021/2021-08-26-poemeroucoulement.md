---
layout: post
title: Pause Photo et Poésie - Roucoulement
category: poesie
tags: poésie
---



Deux pigeons amoureux

sur le haut de mon toit

où l'arbre défie les cieux,

chantent l'amour chez moi.

Deux ramiers migrateurs,

chaque année à chercher

dans les grandes hauteurs,

à protéger leur nichée.

Et la sérénade du matin

se poursuit jusqu'au soir

comme un son de câlin

nourri de leur espoir.


![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/poemeroucoulement.jpg)