---
layout: post
title: Littérature - Expiration de Ted Chiang (2020)
category: litterature
tags:  2020s, littérature, nouvelles, science-fiction, anticipation, uchronie
---

**J'avais repéré ce recueil de nouvelles lorsqu'il avait eu le prix Hugo et certains thèmes m'intéressaient. Sans suspens, je n'ai pas été déçu du(des) voyage(s) proposés par cet auteur.**

Même si, comme dans tout recueil, il y a du bon et du moins bon. Alors pour une fois, je vais scinder la chronique en plusieurs parties.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/expiration.jpg)

### Le Marchand et la Porte de l'alchimiste, 2020 ( The Merchant and the Alchemist's Gate, 2007)

C'est une histoire de voyage dans le temps qui a la particularité de nous emmener à Bagdad et au Caire. Cela rajoute une note dépaysante avec un peu plus de magie, façon mille et une nuits. A l'intérieur de cette histoire, il y a des sortes de petits contes qui posent l'éternelle question de ce que l'on peut faire ou pas en remontant le temps, avec les paradoxes que cela comporte. Distrayant et réussi.

### Expiration, 2020 ( Exhalation, 2008)

On reste ici dans la notion de temps mais dans une sorte de civilisation robotique que l'on peut penser extra-terrestre. Le corps de ces êtres est mécanique, pneumatique mais présente bien des inconnues dans leur fonctionnement pour ces mêmes êtres. Notamment en ce qui concerne la mémoire et le cerveau. Ce n'est que lorsque le temps paraît se décaler que l'un d'eux comprends ce qui se passe dans cet univers fini. Et le récit de basculer dans une fable écologique et scientifique nous rappelant aussi ce qu'est notre monde et ses dépendances à son univers. Très réussi. 

### Ce qu'on attend de nous, 2020 ( What's Expected of Us, 2005)

Une télécommande dont le bouton s'allume 1 seconde avant que l'on appuie dessus. Voilà le curieux postulat de cette courte nouvelle qui aurait mérité d'être plus développée. On y aborde notre capacité à sortir d'un chemin tracé, notre libre arbitre. Dommage. 

### Le Cycle de vie des objets logiciels, 2020 ( The Lifecycle of Software Objects, 2010)

Une longue nouvelle autour d'une forme de vie créée de manière logicielle mais qui avec l'apprentissage devient plus autonome au point qu'elle peut prendre réellement vie dans des robots. On sent le passé d'informaticien de l'auteur mais toutes les questions qui se posent autour de cette question sont passionnantes. Je trouve pourtant que cette nouvelle tire trop en longueur avec une tentative de fil rouge dans la relation Ana/Derek, ceux qui ont élevés ces "digimos". Mais on retrouve les thèmes chers à l'auteur, c'est à dire le temps, l'éducation, l'informatique (machine learning, IA...). La nouvelle est presque trop riche en thème justement avec par exemple les problèmes de durée de vie de ces intelligences et de leurs supports, la prolongation d'une vie artificielle...Inégal.

### La Nurse automatique brevetée de Dacey, 2020 ( Dacey's Patent Automatic Nanny, 2011)

Une nouvelle courte sur la question de l'apprentissage et comment élever les enfants. Derrière cette nouvelle, on peut voir la question de l'enseignement à distance qui est proposé de manière mécanique et détruit toute relation humaine, et en même temps l'humain dans son inconstance et son chaos perpétuel. Réussi malgré sa taille un peu courte.

### La Vérité du fait, la vérité de l'émotion, 2020 ( The Truth of Fact, the Truth of Feeling, 2013)

Imaginons un monde où savoir écrire, lire n'est plus nécessaire car il y a des assistants qui le font, traduisent, etc. Imaginons un monde où un logiciel vient remplacer notre mémoire en piochant dans nos souvenirs vidéos stockés pour nous montrer ce que nous désirons au fil même de notre conversation. Voilà le cauchemar de cette nouvelle. Et nous n'en sommes pourtant pas loin à force de stocker dans le cloud/nuage. Et parallèlement à cela, l'auteur nous conte une histoire d'apprentissage de l'écriture avec un missionnaire dans une tribu qui en découvre les aspects positifs et négatifs. Entre vérité écrite et tradition orale, la vérité est-elle toujours la solution? Passionnant.

### Le Grand Silence, 2020 ( The Great Silence, 2015)

Le postulat de départ est le [paradoxe de Fermi](https://fr.wikipedia.org/wiki/Paradoxe_de_Fermi) qui tient à la possibilité de vies extra-terrestre. Malgré nos explorations, est-ce que les extra-terrestres ne devraient pas se cacher pour se protéger ? Mais à force de chercher l'intelligence ailleurs que sur terre, n'oublie-t-on pas de communiquer avec ceux qui sont à coté de nous ? Comme ce perroquet narrateur... Un peu court.

### Omphalos, 2020 ( Omphalos, 2019)

Une archéologue spécialiste en datation des arbres. Des coquillages préhistoriques créés par dieu...Créationnisme, vous croyez? Plus un monde où la religion utiliserait la science pour se justifier. J'ai eu du mal à me passionner sur cette nouvelle par rapport à ce qui précède parce que cela met un peu de temps à décoller. Et pourtant, la question qui la motive est passionnante. Imparfait.

### L'angoisse est le vertige de la liberté, 2020 ( Anxiety Is the Dizziness of Freedom, 2019)

Imaginez des "prismes" permettant de se mettre en liaison avec des réalités parallèles et notre "parallêtre", notre autre moi séparé par une unité quantique. Voilà le sujet de cette nouvelle très réussie et qui reprend encore les thèmes de la mémoire, du paradoxe temporel, du stockage de données... On en redemande.


Voilà donc un recueil qui s'inscrit parfaitement dans les tendances du moment. Dans sa globalité, il y a tout ce que l'on attend d'un recueil de nouvelles, à savoir un thème général mais suffisamment de variété à l'intérieur. C'est plutôt passionnant par ses sujets modernes, et bien traduit, ce qui n'est pas évident avec les aspects techniques abordés. Il y a un fond scientifique documenté, une tendance à l'anticipation plutôt que de la pure science-fiction, voire à l'uchronie. Bref, pour les amateurs du genre, foncez sans respirer.

