---
layout: post
title: BD - Dômù de Katushiro Otomo (1980)
category: bd
tags: manga, seinen, science-fiction, japon, 1980s
---

**Si j'avais lu Akira du même auteur lors de sa sortie en France dans les années 90 (plus précisément 88/89), j'avais laissé un peu le reste de son œuvre de côté. Erreur réparée avec cet ouvrage qui regroupe les 3 volumes de son ouvrage précédent.**

En réalité, Otomo a aussi fait d'autres manga avant Domû mais c'est vraiment là qu'il commence à marquer les esprits. Et on retrouve quelques points communs avec *Akira*. Pourtant c'est bien dans un japon du début des années 80 que nous sommes. Dans un groupe d'immeubles de Tokyo, une série de suicides et de morts interroge la Police. Aucun point commun à priori mais pourquoi cette crise soudaine ? Un commissaire prestigieux enquête mais ... trouve aussi la mort dans des circonstances mystérieuses. Une petite fille assiste à un horrible suicide alors qu'un nouveau commissaire est dépêché sur place.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/domuotomo.jpg)

On peut croire à une enquête plutôt classique au départ. Et puis peu à peu, l'auteur insiste sur les enfants qui vivent et jouent dans cette cité. Parmi eux, un adulte un peu "attardé" au physique de colosse. Un vieillard mutique qui reste sur un banc. Autant de suspects dont on ne voit pas comment ils auraient pu faire. On part sur des pistes plus mystiques et on entrevoit des sortes de "fantômes". Je ne vais pas tout dévoiler quand même. Disons que le sous-titre de l'intégrale en dit déjà une part.

Quand je parlais de points communs avec Akira, ce n'est pas dans le monde qui y est décrit mais dans la place qu'ont des enfants dans cet intrigue. C'est aussi une histoire totalement urbaine dans une ville déshumanisée. Le dessin y est tout aussi précis, d'une finesse incroyable même quand le chaos est sur chaque page. La découpe est parfaitement maîtrisée tout comme le scénario qui se déroule jusqu'au crescendo final. Une "apothéose" qui dure presque la moitié de l'histoire tout de même, même si on ne comprend pas forcément tout de suite le fin mot de l'histoire.

Otomo y introduit donc de l'occultisme et des croyances toutes japonaises qu'il mêle avec un thème qu'on peut qualifier de récurent maintenant. Apparemment, il s'est inspiré d'une histoire vraie (une série de suicides dans un groupe d'immeuble) mais y a apporté son "explication". Le résultat est un manga adulte un peu plus gore qu'Akira, d'une efficacité redoutable qui en fait un classique intemporel. Et je comprends qu'après avoir donné autant dans les années 80, il ait levé le pied dans les années 90 avant de passer à l'animation dans les années 2000.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/domuotomo2.jpg)


