---
layout: post
title: Pause Photo et Poésie - Emmurée
category: poesie
tags: poésie, poême, mort, vieillesse
---

Je vois ces sourires et ces regards.

Je lis parfois des mots familiers,

mais toujours ces silences hagards.

Aucune chanson sinon du passé.

Une chambre pour confidente.

Des mains invisibles qui entrent.

Et je roule sur cette lente pente,

qui me fera naître du ventre,

encerclé de pierres blanches.

Alors j'entendrais clouer mes planches.


![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/poemeemmuree.jpg)


