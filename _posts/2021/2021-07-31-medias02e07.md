---
layout: post
title: La Revue de Médias S02E07, Juillet 2021
category: geopolitique 
tags: géopolitique, culture, 
---

**Septième épisode de l'année, parce que la rubrique ne prend pas de vacances ... Comme toujours, vous pouvez ouvrir les liens dans d'autres onglets par utilisation de la touche CTRL ou bien appui long sur smartphone. Plus l'icone coups de coeur ![cc](/images/coeur.png)**

### Géopolitique

* **Afrique** : 
	* La litanie des nouvelles attaques terroristes fait que l'on n'en parle même plus dans nos médias. Et pourtant...Le *Burkina Faso* [cette fois](https://www.rfi.fr/fr/afrique/20210622-burkina-faso-au-moins-11-policiers-tu%C3%A9s-dans-une-attaque-dans-le-centre-nord). Mais au *Nigéria*, c'est un [avion de chasse](https://www.rfi.fr/fr/afrique/20210720-nigeria-un-avion-de-chasse-abattu-par-des-bandits-fait-craindre-une-escalade) qui a été abattu.
	* En *République centrafricaine*, la [situation humanitaire](https://www.rfi.fr/fr/afrique/20210701-centrafrique-les-humanitaires-s-inqui%C3%A8tent-d-un-acc%C3%A8s-aux-populations-de-plus-en-plus-difficile) ne s'améliore guère. 
	* Au *Mali* la France est partie, [revenue](https://www.rfi.fr/fr/afrique/20210704-reprise-de-la-coop%C3%A9ration-militaire-avec-le-mali-emmanuel-macron-a-fini-par-revenir-%C3%A0-la-raison), sans que l'on comprenne vraiment pourquoi sinon l'amateurisme au sommet de l'état qui n'écoute plus depuis longtemps sa diplomatie face au militaire. 
	* Et qu'elle semble loin *l'Afrique du sud* de Mandela [en ce moment](https://www.rfi.fr/fr/afrique/20210715-en-afrique-du-sud-le-bilan-des-violences-continue-de-s-alourdir). 
	* Le continent africain, grand oublié des vaccins (accaparés par l'occident), va pouvoir tardivement avoir des moyens [en produisant](https://www.rfi.fr/fr/afrique/20210710-covid-19-au-s%C3%A9n%C3%A9gal-accord-sign%C3%A9-pour-la-cr%C3%A9ation-d-une-usine-de-fabrication-de-vaccins) ses propres vaccins ou tests.
	* On sous-estime encore l'influence Chinoise sur le continent à travers la politique de la dette. Ici c'est la *RDC* qui bénéficie des [largesses (EN)](https://asiatimes.com/2021/06/china-agrees-to-restructure-dr-congos-borrowings/).
	* Et puis cela dégénère dans la corne de l'Afrique, j'en reparle le mois prochain.
* **Asie** : 
	* C'est *l'Afghanistan* qui occupe la Une avec le départ des États-unis et le retour [des Talibans](https://www.rfi.fr/fr/asie-pacifique/20210621-afghanistan-influence-croissante-des-talibans-le-gouvernement-en-pleine-d%C3%A9bandade). Les [conséquences régionales](https://www.courrierinternational.com/article/menace-lombre-des-talibans-plane-sur-les-pays-dasie-centrale) sont inquiétantes. Au point que [Russie et Chine (EN)](https://asiatimes.com/2021/07/russia-china-advance-asian-roadmap-for-afghanistan/) s'en mêlent. 
	* Mais au *Pakistan* voisin aussi c'est dangereux de fréquenter certaines routes. Des chinois viennent de le [payer de leur vie](https://www.rfi.fr/fr/asie-pacifique/20210714-pakistan-explosion-meurtri%C3%A8re-d-un-bus-transportant-des-ouvriers-chinois) et ce ne sera pas sans conséquences diplomatiques et économiques. 
	* En *Chine*, on parle des Ouïghours mais toutes les religions sont condamnées à être sous la coupe du parti, comme le [futur Dalaï Lama](https://asiatimes.com/2021/06/unholy-battle-to-decide-tibets-next-dalai-lama/). Un pays qui après les 100 ans du parti, se félicite de la fin du [paludisme](https://www.rfi.fr/fr/asie-pacifique/20210630-chine-le-paludisme-%C3%A9radiqu%C3%A9-dans-le-pays-apr%C3%A8s-70-ans-de-lutte). 
	* Par contre c'est de *Taïwan* qu'on parle beaucoup, entre volonté de la Chine de récupérer ce territoire perdu et liens géostratégiques, comme avec [le Japon (EN)](https://asiatimes.com/2021/07/tokyos-talk-on-taiwan-gets-tougher-and-louder/) qui y voit une occasion de peser à nouveau [régionalement (EN)](https://asiatimes.com/2021/07/how-far-would-japan-really-go-to-defend-taiwan/). 
	* Si la dette est utilisée par la Chine en Afrique et ailleurs, [les USA l'utilisent (EN)](https://asiatimes.com/2021/07/americas-debt-diplomacy-in-cambodia/) en Asie, comme ici au *Cambodge*, prolongeant de fait le front de cette nouvelle guerre froide.
* **Amérique du nord** : 
	* Si *Haïti* est en [plein chaos](https://www.rfi.fr/fr/en-bref/20210707-ha%C3%AFti-le-pr%C3%A9sident-ha%C3%AFtien-jovenel-mo%C3%AFse-a-%C3%A9t%C3%A9-assassin%C3%A9-dans-la-nuit-de-mardi-%C3%A0-mercredi), le rôle des USA est encore à clarifier. La [sortie de crise](https://www.rfi.fr/fr/am%C3%A9riques/20210714-%C3%A0-la-une-une-sortie-de-la-crise-politique-est-elle-envisageable-en-ha%C3%AFti) est-elle encore possible ? 
	* On est en droit de [s'interroger](https://www.rfi.fr/fr/am%C3%A9riques/20210713-cuba-le-pr%C3%A9sident-diaz-canel-accuse-washington-d-avoir-provoqu%C3%A9-les-manifestations-sur-l-%C3%AEle?) sur ce qu'il se passe à *Cuba*, avec tous les précédents etats-uniens (cf Iran). Surtout du fait d'un embargo toujours illégal qui pèse sur la situation de la population. La répression de ces manifestations a été [plutôt "douce"](https://www.rfi.fr/fr/am%C3%A9riques/20210715-cuba-les-autorit%C3%A9s-tentent-d-amadouer-une-population-remont%C3%A9e) comparativement à d'autres états et périodes du pays. (Parenthèse, c'est sous Obama/Biden que l'on a fait le plus d'assassinats ciblés dans des pays tiers par les USA...)
* **Amérique du sud et centrale** : 
	* Suite et on l'espère Fin des éléctions au *Pérou* avec [l'habituel racisme](https://www.rfi.fr/fr/am%C3%A9riques/20210622-le-p%C3%A9rou-frapp%C3%A9-par-une-vague-de-haine-raciste-envers-les-populations-pauvres-et-rurales) envers les populations indiennes qui ressort chez libéraux et extrême-droite (voir la Bolivie récemment) puisque c'est un [gauchiste métisse](https://www.rfi.fr/fr/am%C3%A9riques/20210721-p%C3%A9rou-pedro-castillo-un-pr%C3%A9sident-novice-et-m%C3%A9tis-face-%C3%A0-de-nombreux-d%C3%A9fis) qui remporte le scrutin.
	* Au *Chili* après la rue, c'est maintenant aux élus de mettre en place une [nouvelle constitution](https://www.lemonde.fr/international/article/2021/07/04/le-chili-lance-le-processus-de-redaction-de-sa-nouvelle-constitution_6086952_3210.html) pour faire oublier toute trace de la dictature. Le Pérou pense faire de même mais le consensus est moindre.
* **Europe** : 
	* Cela est passé un peu inaperçu mais la [Suède](https://www.rfi.fr/fr/europe/20210621-su%C3%A8de-le-premier-ministre-renvers%C3%A9-au-parlement-une-premi%C3%A8re) a eu des remous politiques. De même que l'arrivée potentielle d'un chanteur à la tête de [l'état Bulgare](https://www.rfi.fr/fr/europe/20210712-bulgarie-le-chanteur-anti-syst%C3%A8me-trifonov-cherche-%C3%A0-composer-un-gouvernement-minoritaire). Aucun rapport à priori, sinon l'instabilité politique Européenne.
	* Il y avait comme un vieil air de [guerre de Crimée](https://fr.wikipedia.org/wiki/Guerre_de_Crim%C3%A9e) après cet [incident maritime (EN)](https://responsiblestatecraft.org/2021/06/23/shots-fired-as-brits-enter-black-sea-and-russian-ire/) entre Royaume (dés)uni et *Russie*. Une Russie qui porte plainte à la [cour européenne](https://www.rfi.fr/fr/europe/20210723-la-russie-saisit-la-cour-europ%C3%A9enne-des-droits-de-l-homme-contre-l-ukraine) des droits de l'homme contre l'Ukraine, ce qui fait plutôt rire...Mais en même temps, voit son vaccin injustement boudé si on en croit [Nature (EN)](https://www.nature.com/articles/d41586-021-01813-2?utm_source=pocket_mylist), ce qui empêche d'avoir un pass sanitaire européen.
	* En *Turquie* voisine, on réécrit comme chez nous avant le ["roman national" (EN)](https://asiatimes.com/2021/06/the-place-of-museums-in-turkeys-national-narratives/).
	* Au confins de l'Europe et de l'Asie, le conflit du *Haut-Karabakh* devrait nous faire réfléchir sur le [réalisme en géopolitique](https://responsiblestatecraft.org/2021/06/28/how-the-nagorno-karabagh-conflict-is-a-lesson-to-the-west-in-geopolitical-reality/). 
	* Et l'Europe continue à ne pas traiter correctement les [réfugiés](https://www.rfi.fr/fr/france/20210703-france-plus-d-une-centaine-de-migrants-secourus-en-une-journ%C3%A9e-dans-la-manche) en *Méditerranée*, dans cette période où l'on voit [cette mer](https://www.rfi.fr/fr/europe/20210708-en-m%C3%A9diterran%C3%A9e-l-ocean-viking-est-coinc%C3%A9-avec-572-personnes-secourues-%C3%A0-son-bord) comme un lieu de vacances. On retrouve aussi cet [égoïsme](https://www.rfi.fr/fr/afrique/20210711-migrations-plus-de-2-000-personnes-mortes-en-mer-en-tentant-de-rejoindre-l-espagne) dans l'instauration du [pass sanitaire européen](https://www.courrierinternational.com/article/covid-19-le-certificat-sanitaire-europeen-vu-dafrique-un-systeme-injuste), vu comme injuste vis à vis du continent africain. 
* **France 2022** : 
	* Entre la taxation annoncée des GAFAM et les actes de Macron, il y a [un océan](https://www.challenges.fr/entreprise/les-dessous-de-l-incroyable-soutien-du-gouvernement-a-amazon_769927) et ce sont encore les petits qui vont trinquer. C'est quand même pas comme si on aidait [les plus riches](https://www.marianne.net/economie/economie-francaise/impots-ce-discret-coup-de-pouce-du-gouvernement-a-lheritage-des-plus-riches), [non](https://www.rfi.fr/fr/%C3%A9conomie/20210707-le-nombre-de-milliardaires-fran%C3%A7ais-est-pass%C3%A9-de-95-%C3%A0-109-malgr%C3%A9-la-pand%C3%A9mie) ? 
	* Quand le manipulateur montre le supposé islamo-gauchiste, l'imbécile le regarde, mais le curieux voit [l'armée et ses séditieux anti-sémites](https://www.politis.fr/articles/2021/06/antisemites-de-bonne-compagnie-43331/). En même temps, avec un président plus concerné par les [crop-tops](https://www.gentside.com/societe/emmanuel-macron-sa-remarque-sur-les-crop-top-qui-passe-mal_art99271.html) que par la situation politique, on peut s'interroger.
	* La diplomatie des armes de la France avec le VRP Le Drian montre qu'on utilise toujours les mêmes vieilles recettes. Les [Rafales](https://www.rfi.fr/fr/france/20210703-rafale-papers-la-justice-fran%C3%A7aise-ouvre-une-enqu%C3%AAte-pour-soup%C3%A7ons-de-corruption) sont parfois comme des Boomerang. 
	* La France du football "black blanc beur" mais visiblement pas Asiatique...Ce [racisme](https://www.courrierinternational.com/article/controverse-linternational-francais-ousmane-dembele-coupable-de-racisme-anti-asiatique) que l'on oublie. Les sponsors de Griezman, eux, ont agi ! La [Pandémie](https://www.rfi.fr/fr/france/20210708-france-le-racisme-anti-asiatique-mis-en-lumi%C3%A8re-par-la-pand%C3%A9mie) n'a rien arrangé. 
	* On parle beaucoup de [liberté en ce moment](https://www.politis.fr/articles/2021/07/le-choc-de-deux-libertes-43401/)...pour le vaccin et cette politique poltronne de l'entre-deux. J'y reviendrai.
* **Moyen orient** : 
	* Le nouveau dirigeant en *Iran* n'a pas d'autre choix que de retrouver ses alliés traditionnels en [Chine et Russie (EN)](https://asiatimes.com/2021/06/raisi-era-will-move-iran-closer-to-russia-and-china/). Au passage, il n'est jamais inutile de rappeler le rôle des USA en Iran dans le [coup d'état de 1953 (EN)](https://responsiblestatecraft.org/2021/07/02/revisionists-want-to-downplay-u-s-role-in-1953-iran-coup-dont-listen/) et l'instauration de la dictature. 
	* Le *Yemen* est toujours le théâtre [d'affrontements](https://www.rfi.fr/fr/moyen-orient/20210627-y%C3%A9men-les-combats-font-rage-autour-de-la-province-de-marib-r%C3%A9gion-cl%C3%A9-du-nord-ouest) entre les belligérants soutenus par les deux puissances régionales, Iran et Arabie Saoudite. Le départ des USA d'Afghanistan et l'inaction des USA dans ce conflits interrogent sur [l'influence des USA dans cette zone (EN)](https://responsiblestatecraft.org/2021/07/17/us-middle-east-influence-in-afghanistans-shadow/).
	* Au *Liban*, le chaos continue. [L'action française](https://www.courrierinternational.com/article/echec-macron-est-en-partie-responsable-de-laggravation-de-la-crise-au-liban), aussi naïve et caricaturale qu'en Afrique, a-t-elle fait pire que le mal ? La situation est catastrophique pour [les enfants](https://www.rfi.fr/fr/moyen-orient/20210701-liban-un-tiers-des-enfants-dort-le-ventre-vide-selon-l-unicef) et les familles doivent stocker, anticiper tout.
* **Droit des femmes** : 
	* Une fois encore, les protections en place sautent peu à peu, comme [en Turquie](https://www.rfi.fr/fr/europe/20210701-turquie-manifestation-contre-le-retrait-officiel-d-un-trait%C3%A9-prot%C3%A9geant-les-femmes), pays pourtant en avance sur la France et d'autres sur le droit de vote féminin du temps de Mustafa Kemal. Le départ des USA d'*Afghanistan* n'est qu'un autre signal pour le retour en arrière du [droit des femmes](https://www.rfi.fr/fr/asie-pacifique/20210719-afghanistan-les-femmes-%C3%A0-nouveau-sous-contrainte-dans-les-zones-reprises-par-les-talibans) vers des coutumes plus tribales que religieuses. 
	* Alors qu'au début de *l'informatique*, il y avait beaucoup de femmes, aujourd'hui en France c'est le parent pauvre, autant en emploi qu'en [formation à l'outil](https://www.rfi.fr/fr/technologies/20210710-informatique-derri%C3%A8re-les-%C3%A9crans-des-femmes-encore-peu-form%C3%A9es). 


### Culture

* **BD lus non chroniquées** : 
	* Le Tome 2 de l'intégrale *Buck Danny*, avec toujours un peu de racisme anti-asiatique (ça plaiera à Griezman et Dembele) mais le coté aventure est mieux construit. 
	* Le Tome 3 de *Crusaders*, qui ne sera donc pas une trilogie mais qui reste de durer longtemps. Plutôt hardu à suivre pour les théories pseudo-quantiques et les différents peuples qui animent ce space-opera qui part un peu trop loin. Alias avait chroniqué les 2 premiers tomes, et confirme dans [sa chronique](https://alias.erdorin.org/crusaders-tome-3/) de ce tome.
	* Je ne chronique par *Radium Girls* de la dessinatrice Cy, parce que c'est encore une histoire de gens malades qui meurent et qu'on trouve que j'en fais toujours. Ici, les femmes qui peignaient les numéros sur les cadrans de montres avec une peinture au radium dans les années 20. Tout en crayon de couleur avec une belle histoire d'amitié. A lire. ![cc](/images/coeur.png)
	* La série de 2 tomes d' *Ellis Island*, rappelant le sort des émigrés italiens au début du 20ème siècle. Sympathique à lire à travers l'aventure de l'un d'eux avec ce qu'il faut de Maffia, d'amour, d'intrigue.
	
* **Autres livres lus** : 
	* Pour les anglophones, *The Utopia of rules* du regretté David Graeber est un recueil de différents essais que l'ami [Alias avait chroniqué](https://alias.erdorin.org/the-utopia-of-rules-de-david-graeber/). Donc... allez le lire !
	
* **Documentaires vus** : 
	* Le documentaire d'Arte sur *Susan Freund*, photographe souvent trop réduite au portrait. Quel personnage, et quelle vie. 
	* Pas vraiment un documentaire mais les interviews ["Video Club" sur Konbini](https://www.youtube.com/watch?v=sr_A6DBkMFk&list=PL6yqY0TQJgwcSGYD6a2P4YgpHIp-HCzZn) sont un délice de cinéphile.

* **Presse** : 
	* Le numéro de *Geek Magazine* spécial Superman avec quelques interviews intéressantes, comme celle des créateurs de Robot Chicken, série animée culte (tiens, j'en ai pas parlé!), et une vision un brin différente sur le type au collant bleu et rouge. 
	* Comme chaque année depuis 35 ans, l'annuel Automobile de l'Automobile magazine, même si ce n'est plus ce que c'était avec beaucoup de coquilles. Je suis toujours plus intéressé par ce qui se passe ailleurs chez les petits que par le reste de cette industrie définitivement vendue au culte du SUV pour quelques années et toujours très conservatrice dans les formes.
	* Le numéro 50 de *Polka* dormait dans la pile depuis longtemps. J'ai adoré le noir et blanc très contrasté de Daido Moriyama, même si on préfère aujourd'hui des choses plus douces. 
	![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/daidomoriyama.jpg)
	

* **Films vus non chroniqués** : 
	* Le nouvel opus de *Pierre Lapin* (Peter Rabbit en V.O.), mignon et joliment fait mais un peu moins réussi pour le scénario que le premier. Familial.
	* Le dernier Marvel, *Black Widdow*, un film d'action banal (car on sort du pur film de super-héros) et longuet avec quelques séances FX réussies même si improbable. Évitable. 
	
* **Musique** : 
	* Avec le streaming, [les rapaces sont de sortie](https://www.courrierinternational.com/article/industrie-musicale-ces-fonds-qui-rachetent-de-vieilles-chansons-prix-dor), en rachetant tout ce qui traîne en terme de musique.
	
* **Séries et téléfilms** : 
	* Vraiment pas eu le temps d'en finir une. *Miracle Workers* a une nouvelle saison mais je sens que ça va tirer sur la corde.
	
* **Jeux vidéos** : 
	* Le jeu en ligne *iGP Manager*, jeu de gestion d'écurie de F1 plutôt bien fait : Dans les points à la première course, doublé avec 30s d'avance à la deuxième en mode débutant. Et quelques revers ensuite parce que j'ai mal jaugé les investissements, avant d’enchaîner d'autres victoires. En mode Pro et Elite, les réglages ont plus d'amplitude, donc il va falloir bosser ça. En attendant, j'ai remporté un championnat débutant/f3, même si le second m'a tiré une sacré bourre dans les derniers grand-prix avant d'abandonner en pro. Victoire facile en pro/F2 grace à la bonne gestion des intempéries. Me reste à défier le boss de la ligue (première course, je lui ai mis une valise)...et d'aller dans une ligue plus relevée. Car je suis 3 à 5 secondes au tour derrière les meilleures ligues, tout de même.
	![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/medias2e07.jpg)
	* Le concurrent *GPro.net* est bien plus difficile d'accès avec des amplitudes de réglages plus pointues mais une interface trop confuse à mon goût. Je veux m'amuser quand même un peu et pas avoir l'impression d'être au boulot... Et là j'avais l'impression de faire du trading. 
	* J'ai rejoué à *Me and My Katamari* sur la PSP, jeu totalement délirant dans la série des autres...Et forcément je vais craqué pour la trop courte version Switch, *Katamari Reroll*. Oui, je veux aussi avoir la plus grosse boule :p ...va falloir que je refasse un article sur la saga car l'ancien a disparu dans les déménagements.
	* le nouveau *Beach Buggy Racing 2* de Vector Unit reste dans la même veine que le premier : Mieux que le Mario Kart Android mais avec une orientation beaucoup plus lucrative que le premier. Plus beau aussi avec l'augmentation de la résolution, des animations et de nouveaux bonus etc...Mais moins intéressant à jouer, car trop de bonus et moins axé sur pilotage et stratégie, à l'image aussi de Mario Kart.

* **peinture / photo** : 
![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/medias2e07a.jpg)

* **Et on danse** : 
Oui mais pas trop près, donc ça sera le mois prochain.

  
### Environnement

* Pour faire la transition entre culture et environnement, rien de tel que de rappeler que [nos instruments](https://www.courrierinternational.com/article/ecologie-la-guitare-un-instrument-destructeur-pour-la-planete) sont faits avec des bois dits précieux mais issus de la déforestation.
* Oh les bons légumes et les bonnes céréales...aux *pesticides*. Le moment de se demander qui sont les bons et [les mauvais en la matière](https://www.rfi.fr/fr/europe/20210620-pesticides-les-bons-et-les-mauvaises-%C3%A9l%C3%A8ves-au-sein-de-l-union-europ%C3%A9enne). 
* Le *dégel du permafrost* risque d'avoir des conséquences inattendues. Comme l'apparition d'[anciens virus (EN)](https://arstechnica.com/?p=1775475), comme si nous n'en avions pas assez. Car c'est bien l'humanité qui sera [la victime du réchauffement](https://www.rfi.fr/fr/science/20210623-environnement-l-humanit%C3%A9-ne-pourra-pas-se-remettre-du-d%C3%A9r%C3%A8glement-climatique-selon-le-giec) qu'elle a créé, en plus des extinctions d'autres espèces. On le voit par exemple à [Madagascar](https://www.rfi.fr/fr/afrique/20210626-le-changement-climatique-pousse-madagascar-au-bord-du-d%C3%A9sastre-humanitaire), ou dans l'aggravation de phénomènes connus comme au [Canada](https://www.courrierinternational.com/article/temperatures-le-rechauffement-climatique-responsable-du-dome-de-chaleur).
* **L'énergie**, c'est bien de s'en occuper, c'est mieux si on y met de la cohérence. Parce qu'en [Guyane](https://news.google.com/articles/CAIiEEv-OVsc1PZqjxpdr9LGaG4qGQgEKhAIACoHCAowm_2NCzDYzaADMJrHhgc?ceid=FR%3Afr&gl=FR&hl=fr), c'est plutôt l'inverse. Dans le [New-Jersey (EN)](https://arstechnica.com/?p=1777578) c'est le vent que l'on privilégie en Off-shore. Mais au Japon, il y a encore le [charbon (EN)](https://asiatimes.com/2021/07/japan-still-seeking-to-exploit-coal-power-loopholes/). Tandis qu'à Singapour on crée des [fermes solaires flottantes](https://asiatimes.com/2021/07/singapore-unveils-huge-floating-solar-farms/). On a même des [prototypes automobiles](https://www.moniteurautomobile.be/actu-auto/nouveaux-modeles/lightyear-one-700-km-au-soleil.html) sur cette énergie. 
* L'**automobile** et la mobilité ne cessent de passionner. Quelques [réponses ici](https://www.moniteurautomobile.be/actu-auto/coin-de-la-redaction/redacteurs-sans-filtre-la-mobilite-en-2030.html) qui montrent que la révolution sera lente. Surtout que l'on manque de [bornes de recharges](https://lehollandaisvolant.net/?d=2021%2F06%2F29%2F18%2F59%2F06-pourquoi-les-communes-devraient-installer-des-bornes-pour-voitures-electriques), trop souvent en pannes ou avec des systèmes d'abonnement privatifs. Mais alors que les constructeurs annoncent être au tout électrique à cette date de 2035, les voilà qui râlent quand même contre [l'Europe](https://www.moniteurautomobile.be/actu-auto/environnement/2035-shift-electrique-europeen-rails.html). 
* L'**Hydrogène**  ... Rien ce mois-ci mais si on parlait du train à la place et de la [lévitation (EN)](https://www.courrierinternational.com/article/grande-vitesse-avec-son-train-levitation-la-chine-se-reve-en-superpuissance-des-transports) qui va être la technologie à la mode en Asie. A défaut de voitures volantes...
* **l'eau** est source de vie, une denrée rare source de conflits et donc de migrations. La [Syrie](https://www.politis.fr/articles/2021/07/dans-le-nord-est-syrien-la-guerre-de-la-soif-43386/) en est victime depuis bien avant la guerre. 
* Après les masques jetables, voilà le [Plexiglas](https://www.courrierinternational.com/article/transparence-passe-la-pandemie-que-fera-t-de-tout-ce-plexiglas) comme conséquences du COVID-19. 
* **Condition animale** : Bonne nouvelle, la [chasse à la glu](https://www.lemonde.fr/planete/article/2021/06/28/la-chasse-a-la-glu-declaree-illegale-par-le-conseil-d-etat_6086052_3244.html) est interdite. Reste à faire appliquer la loi localement. Autre bonne nouvelle, l'interdiction du [saumon d'élevage](https://www.courrierinternational.com/article/environnement-largentine-premier-pays-au-monde-interdire-lelevage-de-saumons) en Argentine avec toutes les dérives que cela induit. 

### Science et informatique

* **j'ai testé** : 
	* Après [5 ans](https://www.cheziceman.fr/2016/tutanota-protonmail/), j'ai retesté Tutanota par rapport à Protonmail pour les *Emails sécurisés*. Si l'accessibilité de Tutanota est meilleure pour un débutant en mode webmail, l'application Android est bien trop basique et c'est rédhibitoire pour moi. Dommage car le calendrier intégré est un plus. Et les deux passent à travers les proxy de M. Xi... Par contre sur Android, GPG est une catastrophe avec le peu d'applications disponibles (OpenKeychain...ou rien + K9mail ou Fairmail). Tuto à venir.
* **j'ai relu** : 
	* Le *Tilt N°27* de décembre 1985 avec un certain Commodore 64 en superstar du moment. Et au détour d'un article, on apprenait qu'Atari et Commodore étaient pourtant au bord de la rupture alors que le 520 ST sortait (2 fois le prix d'un C64 ou d'un Amstrad 6128, lui même le double d'un MSX) et que l'Amiga entrait en production. Sony sortait un très beau compatible MSX2 (le HB500) déjà dépassé comme le standard. On rêvait de faire du graphisme et des dessins animés sur ces machines. Un lecteur de disquettes externe, sur ces machines, valait à lui seul le prix de la machine. Pas étonnant que le lectorat soit jeune mais d'une classe moyenne aisée en majorité. Mes parents ont attendu que les prix baissent.
* **Le Web** : 
	* La guerre contre les *GAFAM* est engagée aux USA pour éviter tout monopole. Mais c'est une lutte de tous [les instants (EN)](http://feedproxy.google.com/~r/timeblogs/nerd_world/~3/Z1qwtim-JA4/). Et se niche parfois dans des [applications](https://www.courrierinternational.com/article/antitrust-trente-six-etats-americains-portent-plainte-contre-google-pour-son-play-store). 
	* Si les cryptomonnaies sont dans l'ornière, les monnaies numériques intéressent les grands états. L'Europe [s'y prépare](https://www.rfi.fr/fr/%C3%A9conomie/20210714-la-banque-centrale-europ%C3%A9enne-lance-le-chantier-de-l-euro-num%C3%A9rique). La [Chine](https://www.rfi.fr/fr/%C3%A9conomie/20210717-la-chine-pourrait-%C3%AAtre-l-un-des-premiers-pays-au-monde-%C3%A0-avoir-sa-propre-monnaie-virtuelle) sera-t-elle la première ? 
	* Un petit guide de la [Free Software Foundation](https://emailselfdefense.fsf.org/en/) sur l'email et la sécurité, ça vous dit? C'est en Anglais alors je vous prépare un truc...
* **Intelligence artificielle**: 
	* RAS ou bien on perd la notre ?
* **Hardware** : 
	* L'[Affaire Pegasus](https://www.rfi.fr/fr/am%C3%A9riques/20210719-affaire-pegasus-au-mexique-un-journaliste-assassin%C3%A9-parmi-les-personnes-espionn%C3%A9es) touche les téléphones portables de journalistes, notamment au Mexique, au Maroc, en Israel...De quoi faire réfléchir sur ce qu'il est possible de faire si des moyens techniques tombent dans de mauvaises mains. 
* **Espace**
	* La [voie lactée](https://www.courrierinternational.com/article/astronomie-freinee-par-la-matiere-noire-la-voie-lactee-tourne-24-moins-vite) qui tourne moins vite, ça ne vous a pas changé la face du monde ?
	
### Humour

Et pour terminer avec Humour, Giedré nous renvoie à la place du poil dans la société

*   [Giedré - A Poil ![video](/images/youtube.png)](https://youtu.be/RibXS85ZTVY){:target="_blank"}


