---
layout: post
title: Réflexion ou Chanson - Laisse pas traîner ton vieux (et ton fils)
category: reflexion
tags:  politique, France2022, élections, racisme, extrème droite
---

**Autour de moi j'entends et je vois de plus en plus de discours haineux, racistes. Je vois de plus en plus de gens adhérer aux thèses des Valeurs actuelles, CNews, C8, et autres hérauts de l’extrême droite. Même dans ma propre famille. On banalise à force de mettre un joli ruban "Buzz" ou "Clash" pour nous attirer. Et comme ceux qui votent le plus sont les personnes agées...Laisse pas traîner ton vieux aurait pu dire NTM ou un autre aujourd'hui.**

Et voilà 2020

T'as du mal déjà le matin

A bouger ton boul, à sortir le clébard

T'a ptet un taf, mais t'en a d'jà trop marre

Tu te sens iench, d'un système qui te bouffe la rate

Tu vois ton ieuv pioncer qui en devient trop fat

Il t'parle de sa jeunesse, de sa préhistoire

Le 20ème, les glorieuses qu'il a rangé dans l'tiroir

Il vit toujours sa gloire d'opérette

Même il drague en survet dans sa supérette

Il a oublié l'frangin tos de derrière

Et l'daron revenu cassos de la der

Le v'la qui hisse les couleurs

d'la marine mais sans beur.

Il croit qu'il dviendra roi du quartier

A s'pavaner dans le cabrio, son carosse de bobo du passé.

Et toi tu lorgnes dessus comme devant ton steak soja

pour aller pécho à Ibiza

Super plan d'avenir, mec

Et la rocade futur, direction Balbec ? 

Tu fais c'que tu veux, mais si tu les laisses faire tranquille

C'est pas pour 5 ans que tu rempiles

T'fais pas bourrer l'urne par leurs rêves, tu vas manger plus que leurs morts

**

*Refrain*

Laisse pas traîner ton vieux

Valeurs passées au pieu

Tu mérites bien mieux

Laisse pas traîner ton vieux

Renvoie l'Riquet chez eux

**

T'as pas choisi ta famille, ni même ton pays, ta cité

Tu connais pas ta chance de vivre dans c'paté

Les autres ils vivent l'illusion réécrite d'un passé

Révise, à ton tour les livres délaissés

Parce que c'est ton histoire demain qu'on rayera des cartes

Salue tes potes avant qu'ils partent

Ta liberté d'glander tu l'auras au fronton

La fraternité tu l'as déjà laissé au front

Le reste tu l'avais dans l'album panini

Mais pas d'échange de carte dans la cour, c'est fini

T'as choisi celle que tu laisses avec les ptite pièces rouges

Sur la commode à l'entrée, Faudrait ptet que tu la bouges.

Le daron s'est bien servi, il a même pris les billet

Il les fait griller avec les chipos, pendant qu'la mer monte

Il transpire le vieux, mais préfère manger son steak épicé

L'écolo peut chanter tout l'été, la cigale a plus d'hiver

Deux degrés de plus

Monte le chauffage, ça fera pousser sa beuh, plus besoin d'bonus

M'en fout j'ai la clim, trop bien oualalaradime

Asperge bien ton vieux

Il mérite pas mieux

**

*Refrain*

**

Tu t'disais que les infos ça instruit les gens

Qu'les talk shows, ça t'rend plus intelligent

Faut juste regarder avant l'pédigré

Là, tu t'es fait enfiler

Ta boite, y'a marqué idées périmées

Bien pourrie même

Chez toi et lui ça passe crème

Ton cerveau tu t'le défonce, et leur bleu fonce

Parce que les mecs en costards ont trouvé leurs réponses

T'as peur et eux aussi, mais vous suivez l'mauvais chemin

C'lui des cauchemars où l'boss est zinzin

Il te dira demain "t'es plus l'bienvenu, merci pour nous, va"

Bye, bye, vers l'au-delà

Maintenant, les jeux sont pas faits, t'as encore le jackpot

Bouge-toi, il t'ont pas jeté à la flotte

Réveille le daron, parle aussi à la mère

Laisse pas traîner ta vieille

Adieu la carte vermeil

Tu mérites bien mieux

Renvoie l'Riquet chez eux


**

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/laissepastrainer.jpg)

*streetart ([source](https://billetsdemissacacia.com/2016/03/28/totem-et-addenda-des-artistes-se-mobilisent-contre-le-racisme/))*


#### Et l'original ça donnait ça :

> A l'aube de l'an 2000

> Pour les jeunes c'est plus le meme deal

> Pour celui qui traîne, comme pour celui qui file

> Tout droit, de tout façon y a plus de boulot

> La boucle est bouclee, le systeme a la tete sous l'eau

> Et les jeunes sont saoules, salis sous le silence 

> Seule issue la rue meme quand elle est en sang

> C'est pas un souci pour ceux qui s'y sont prepares, si ca se peut

> Certains d'entre eux meme s'en sortiront mieux

> Mais pour les autres, c'est clair, ca s'ra pas facile

> Faut pas s'voiler la face, il suffit pas d'vendre des "kil"

> Faut tenir le terrain pour le lendemain

> S'assurer que les siens aillent bien

> Eviter les coups de surin

> Afin de garder son bien intact

> Son equipe compacte, soudee, ecoute de scanner pour garder le contact

> Ou decider de bouger, eviter les zones rouges, et

> Surtout jamais prendre de conges

> C'est ca que tu veux pour ton fils ?

> C'est comme ca que tu veux qu'il grandisse ?

> J'ai pas de conseil a donner, mais si tu veux pas qu'il glisse

> Regarde-le, quand il parle, ecoute-le !

> Le laisse pas chercher ailleurs, l'amour qu'y devrait y avoir dans tes yeux

**

*Refrain* 

> Laisse pas trainer ton fils

> Si tu ne veux pas qu'il glisse

> Qu'il te ramene du vice

> Laisse pas trainer ton fils

> Si tu veux pas qu'il glisse

**

> Putain, c'est en me disant :"J'ai jamais demande a t'avoir !"

> C'est avec ces formules, trop saoulees, enfin faut croire

> Que mon pere a contribue a me lier avec la rue

> J'ai eu l'illusion de trouver mieux, j'ai vu

> Ce qu'un gamin de quatorze ans, avec le decalage de l'age

> Peut entrevoir, c'etait comme un mirage

> Plus d'interdit, juste avoir les dents assez longues

> Pour croquer la vie, profiter de tout ce qui tombe

> La rue a su me prendre car elle me faisait confiance

> Chose qui avec mon pere etait comme de la nuisance

> Aucun d'entre nous n'a voulu recoller les morceaux

> Toute tentative nous montrait qu'on avait vraiment trop d'ego

> Mon pere n'etait pas chanteur, il aimait les sales rengaines

> Surtout celles qui vous tapent comme un grand coup de surin en pleine poitrine

> Croyant la jouer fine. Il ne voulait pas, ne cherchait meme pas

> A ranger ce putain d'orgueil qui tranchait les liens familiaux

> Chaque jour un peu plus

> J'avais pas l'impression d’être plus cote qu'une caisse a l'argus

> Donc j'ai du renoncer, trouver mes propres complices

> Mes partenaires de glisse

> Desole si je m'immisce

**

*Refrain*

**

> Que voulais-tu que ton fils apprenne dans la rue ?

> Quelles vertus croyais-tu qu'on y enseigne ?

> T'as pas vu comment ca pue dehors

> Mais comment ca sent la mort ?

> Quand tu respires ca, mec, t'es comme mort-ne

> Tu finis borne

> A force de tourner en rond

> Ton cerveau te fait défaut, puis fait des fonds

> Et c'est vraiment pas bon quand t'en perd le controle

> Quand pour les yeux des autres, tu joues de mieux en mieux ton orle

> Ton orle de "cai-ra", juste pour ne pas

> Qu'on te dise : "Voila tu fais plus partie de la "mille-fa" d'en bas"

> C'est dingue mais c'est comme ca

> Sache qu'ici-bas, plus qu'ailleurs, la survie est un combat

> A base de coups bas, de coups de "tom-ba"

> D'esquives et de "Paw !" de putains de "stom-bas"

> Laisse pas trainer ton fils

> Si tu veux pas qu'il glisse

> Qu'il te ramene du vice

> Non laisse pas trainer ton fils


[A chanter là dessus ![video](/images/youtube.png)](https://www.youtube.com/watch?v=biYdUZXfz9I){:target="_blank"}




