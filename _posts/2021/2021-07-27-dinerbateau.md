---
layout: post
title: Littérature - Un diner en bâteau de Akira Yoshimura (2020)
category: litterature
tags: littérature, japon, 2020s, 1950s, 1960s, 1970s, classique
---

**A la découverte de la littérature japonaise, je suis tombé sur ce recueil de nouvelles d'un auteur considéré comme classique. Paru en 2020, il compile des œuvres des années 50 à 70.**

Difficile de trouver un fil conducteur autre que la vie, alors je vous propose de découvrir cela par nouvelles. 

#### Le Poisson

Le Narrateur nous parle de la période de la guerre. Il est lycéen pendant la 2ème guerre mondiale mais de santé fragile, il ne participe pas au conflit alors que ses camarades sont mobilisés à partir de 1942. Une croyance de l'époque disait qu'avoir un poisson chez soi protégerait de la guerre. Il achète un petit aquarium avec un poisson et se souvient de cette période, du malaise qui l'habite toujours quelques années après en revoyant des camarades de classe revenus de cet enfer. Si le thème du poisson disparaît peu à peu, on sent la part d'autobiographique dans le récit. 

#### Fumée de charbon

Le Narrateur prend le train pour aller chercher de quoi survivre dans le japon dévasté de l'après guerre. Les tickets de rationnement ne suffisent pas et il va échanger des vêtements de travail contre des sacs de riz, malgré la loi l'interdisant. Une peinture très réaliste de la misère de l'époque, la famine, la résilience aussi. 

#### Le Premier Mont Fuji

Une réunion de famille pour le nouvel an, dans le temple familial près du Mont Fuji. L'occasion de parler du passé, des relations conflictuels entre frères ou belle-famille. Une petite chronique familiale assez anodine finalement, même si on y retrouve les thèmes chers à l'auteur.

#### Début de Printemps

Le narrateur apprend que le père d'un cousin lointain est atteint d'un cancer. Mais ni le malade, ni sa femme ne sont au courant, croyant à un rhume. Mais pourquoi sa tante veut-elle lui parler ? Une autre chronique familiale autour du patriarcat mais qui reste en surface.

#### Bientôt l'Automne

Le narrateur parle de son sevrage de l'alcool pour cause d'infection hépatite, et revient sur cette maladie d'enfance qui l'a empêché de faire la guerre, d'une tuberculose passée. Cela influe sur son comportement jusqu'à l'automne de sa vie. Il parle du vide dans sa vie dû à l'abstinence, ce qui aurait pu aussi être l'absence de travail. Mais aussi des mutations qui imprègne la japon moderne en filigrane. C'est bien du temps qui passe que l'on parle ici avec pudeur.

#### L'échantillon

Un peu comme un prolongement de ce qui précède, le narrateur est à l'hôpital. Mais cette fois il décrouvre que l'on a gardé des échantillons prélevés lors de son opération pour soigner sa tuberculose. C'est en rencontrant de nouveaux chirurgiens qu'il prend conscience du temps qui passe, de la technologie qui progresse, de la souffrance passée. Un récit éprouvant.

#### Cigale du Japon

Le narrateur se rend à la cérémonie de décès de sa cousine et se remémore sa vie passée, leurs relations, les liens familiaux. Il imagine aussi ce qu'il en sera du reste de sa vie en approchant de la soixantaine et angoisse sur la souffrance physique à venir tout en trouvant le réconfort à travers ses souvenirs. Touchant.

#### Pluie de Printemps

Cette fois le narrateur nous embarque dans une autre cérémonie de décès qui concerne une confrère écrivaine qu'il avait croisé et quelque peu humilié. Il se remémore alors cette tranche de vie et les raisons qui le poussent à venir à cette cérémonie, alors que la pluie se déchaîne. Une réflexion discrète sur ce qu'il reste de nous et sur notre vanité. 

#### Le Mur blanc

Le narrateur est à l'hôpital pour des problèmes d'oreille. Il regarde une mante religieuse qui est venu dans sa chambre et se remémore le début de ses problèmes dans son enfance, pendant la guerre et remet son cas en perspective des autres patients.

#### Un Dîner en Bateau

Le narrateur est invité à voir un feu d'artifice sur un bateau restaurant naviguant sur la Sumida à Tokyo. L'occasion pour lui de se remémorer l'enfance et de voir l'évolution de la ville, de sa population. Des épisodes terribles parfois, où l'on croise la figure du père, en filigrane de beaucoup des nouvelles précédentes.

On retrouve tout au long de ces nouvelles très inspirées de la vie réelles de l'auteur, la guerre manquée, la maladie, la famille, l'âge. Si certaines des nouvelles sont assez banales en apparence, elles forment les briques d'un tout, ce qu'on appelle une œuvre. Nous sommes spectateurs de la vie de l'auteur dans cette sorte d'auto-fiction. Ce genre me met mal à l'aise d'habitude mais ici il y a un naturel et une sincérité saisissante. Cela renvoie aussi le lecteur à sa propre vie, aux aléas du quotidien et à relativiser dans une forme de pensée très japonaise. Les nouvelles sont tout de même générationnelles, comme un regard d'une génération née autour de la guerre et qui a reconstruit le pays. 


![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/dinerbateau.jpg)
