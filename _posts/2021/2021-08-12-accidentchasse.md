---
layout: post
title: BD - L'Accident de chasse de Carlson et Blair (2020)
category: bd
tags: biographie, thriller, famille, 2020s
---

**Parfois la réalité dépasse la fiction, dit-on. Et ce véritable roman graphique de près de 500 pages laisse planer le doute durant son déroulement.**

Chicago, 1959, le jeune Charlie Rizzo emménage avec son père aveugle, après que sa mère soit décédée. Matt, son père, lui raconte l'histoire qui a fait de lui un aveugle, un accident de chasse avec des copains. Et puis dans ce qui reste du Little Italy de Chicago, Charlie tourne mal. Le jour où il est arrêté par la Police, son père lui raconte la vérité sur cet accident. Nous voilà parti dans une histoire où Matt va faire la rencontre d'un certain Nathan Leopold...

Si vous faites une recherche sur Nathan Leopold, vous allez tomber sur "le crime du siècle" ou l'affaire [Leopold et Loeb](https://fr.wikipedia.org/wiki/Leopold_et_Loeb), deux jeunes gens de bonne famille de 18 et 19 ans qui tuent un voisin de 14 ans, "pour s'amuser". En réalité, le mobile est plus complexe et sera développé dans des films et écrits, comme par exemple "La Corde" d'Hitchcock. Le récit de ce roman graphique ne tourne pas autour de cette affaire mais autour de l'art, de l'imaginaire et notamment dans une prison ou pour un aveugle comme Matt. Matt est sorti de prison, a rencontré la mère de Charlie et a une vie bien rangée, justement, mais une passion pour les grands écrivains, pour Dante notamment et sa Divine comédie.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/accidentchasse1.jpg)

Je n'en dirai pas plus sur cette sorte de biographie mêlée à un thriller et à une relation père-fils. Le récit est riche en thème et joliment développé, surtout qu'il s'agit d'une première œuvre de l'auteur. Au dessin, Blair nous offre un dessin en noir et blanc hachuré du plus bel effet. C'est contrasté, sombre, à l'image de cet enfer de Dante (on verra plus tard pour les deux autres volumes). C'est à la fois réaliste dans le dessin et onirique dans les scènes imaginaires, avec un brin de naïveté.

Il faut aller à la toute fin du récit pour bien comprendre l'enchaînement des choses, pour réaliser ce qu'est aussi le véritable sujet de cet ouvrage. La figure de Dante et de sa Divine comédie est omniprésente, évidemment mais la poésie également. Il y a une histoire dans l'histoire qui entrecoupe les chapitres. Et finalement, Carlson, néophyte dans ce type d'écriture, a eu bien raison de se faire confiance dans cet imaginaire qu'il a mis au service de ... Charlie.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/accidentchasse2.jpg)