---
layout: post
title: BD - Superman, Red Son de Mark Millar (2020)
category: bd
tags: bd, comics, 2020s, uchronie, super-héros
---

**Je traite assez rarement de comics parce que le genre est surpeuplé, manque de vrai renouvellement. Mais là, il fallait oser l'uchronie et pas qu'un peu en plus. Imaginez un Superman communiste !**

Voilà en effet le postulat au départ de cet album de 176 pages. Kal-el (le vrai nom de superman), au lieu de s'écraser aux USA (comme par hasard...) va tomber en Ukraine. Il va donc être elevé dans une ferme, découvrir ses pouvoirs et se retrouver à défendre la veuve et l'orphelin dans le monde communiste du petit père des peuples, Staline. Entre ça et les USA McCarthistes, mouais. Lois Lane n'est donc pas amoureuse de lui, il n'est pas journaliste. Mais Lex Luthor est un savant génial qui... épouse Lois Lane mais devient maladivement jaloux de ce héros. Au point de tout faire pour le détruire.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/redsuperman.jpg)

On ajoute à cela la mort de Staline, le fils caché de Staline qui dirige le KGB, Batman et Green Lantern, Wonder Woman, une lutte de pouvoir, la guerre froide qui prend une tournure inversée et on tombe dans un album plutôt sombre, assez fun pour qui apprécie les références. Comme je préfère DC à Marvel, ça tombe plutôt bien. Evidemment, niveau crédibilité, on est parfois au niveau zéro mais le scénariste s'en amuse justement.

Le dessin est un mélange de classicisme et de modernité avec des inspirations sur la propagande soviétique ou américaine de l'époque. Toute notre histoire se passe entre les années 50 et 70 environ, ce qui fait déjà beaucoup. Je vous passe les quelques questions philosophiques autour d'un monde parfait dirigé par des super-héros. J'ai trouvé cela intéressant et je n'ai pas l'air d'être le seul, même si les fans basiques du genre risquent de déchanter. Le dénouement n'est pas celui que l'on attendrait, hé, hé. Evidemment, une adaptation animée existe.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/redsuperman2.jpg)
