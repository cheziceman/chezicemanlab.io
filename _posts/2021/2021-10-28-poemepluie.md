---
layout: post
title: Pause Photo et Poésie - Pluie
category: poesie
tags:  photographie, poème, poésie
---

Elle ruisselle sur les feuilles,

gouttes qui perlent et s'unissent.

Elle paraît souvent pleine d’orgueil

bravant jusqu'au soleil du solstice.

Je la regarde bien à l'abri, s'amuser.

Je viens parfois jouer avec elle,

sans penser à ce que la vie m'a usée,

me lavant jusqu'au péché originel.

Les branches ploient sous elle.

Le poids des ans s'en repart vers l'éternel.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/poemepluie.jpg)


