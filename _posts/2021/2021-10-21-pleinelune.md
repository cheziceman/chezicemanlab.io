---
layout: post
title: BD - Pleine lune de Chabouté (2000)
category: bd 
tags: thriller, bd, humour noir, 2000s
---

**Encore un album qui va me fâcher avec la team "premier degré". Si vous aimez les films noirs, l'humour noir, les polars, l'acide sulfurique au petit déjeuner, ça peut vous intéresser. Sinon fuyez tel le héros de cet ouvrage !**

C'est l'histoire d'Edouard Tolweck, employé à la CAF. Oh il est dévoué ce guichetier modèle. Tellement qu'il part toujours à l'heure, traite les dossiers avec zèle, beaucoup de zèle. Il ne doit rien manquer, sinon la petite remarque raciste/misogyne/homophobe au pauvre qui se présente devant lui. Il ne doit pas manquer une seconde de réaction pour se présenter à lui aussi. Et tout ça pour la promotion ou l'augmentation de son chef. Mais ce jour là, tout bascule alors que ce chef vénéré lui demande de donner en main propre une mystérieuse lettre.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/pleinelune0.jpg)

C'est l'histoire d'une descente aux enfers, un enfer dont on se dit qu'il le mérite bien en lisant les premières pages. Tolweck est odieux et on se dit qu'avec tout ce qui lui arrive, il changera peut-être. A travers lui, c'est le procès d'une administration froide, mécanique, qui broit ceux qu'elle est sensé aider. Tout cela est dessiné dans un noir et blanc très contrasté, aux angles acérés comme le propos. On se dit que c'est assez à chaque page et Christophe Chabouté en rajoute pourtant une couche. On rit d'abord de ce retour de bâton bien mérité et puis comme le "héros", on cherche l'issue, la fuite qui le ramènera dans la normalité... ou pas.

C'est haletant, bien mis en scène, bien dessiné mais évidemment, il faut avoir envie de ça. Il y a un petit coté "[C'est arrivé près de chez vous](https://fr.wikipedia.org/wiki/C'est_arriv%C3%A9_pr%C3%A8s_de_chez_vous)" parfois. C'est un road-movie dessiné qui nous emmène dans cette France ignorée, celle que Tolweck déteste, piétine. Incontestable réussite du genre !

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/pleinelune1.jpg)
