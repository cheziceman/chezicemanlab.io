---
layout: post
title: BD - Le Chanteur perdu de Didier Tronchet (2020)
category: bd 
tags: bd, enquête, biographie, 2020s
---

**J'avais entendu parlé de cet ouvrage l'année dernière en lisant un article sur ce vrai chanteur perdu, Jean-Claude Rémy. Et puis j'étais allé visiter le site de l'auteur où il a mis en ligne les chansons. Puis enfin je me suis décidé à lire cet album dans un moment plus propice.**

Tronchet ne se met pas en scène dans l'enquête qu'il a réellement menée. Il crée une sorte d'alter-ego avec ce bibliothécaire souffrant d'un burn-out et qui se remémore des chansons qui le suivent depuis sa jeunesse. Nous voilà d'abord à Morlaix avant de découvrir qui est ce Rémy Bé (donc Jean-Claude Rémy), métisse vietnamien n'ayant pas de souvenir de sa véritable mère, et qui va se lancer dans la chanson pour exorciser ce passé. Et quelque part, c'est aussi Tronchet qui revient sur son propre passé et le pourquoi de cette relation avec ce chanteur et ses chansons.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/chanteurperdu0.jpg)

Etant plutôt fan de l'auteur, j'aime son dessin et sa manière de raconter. Ce n'est plus vraiment comme les albums de Jean-Claude Tergal, évidemment, le temps étant passé. Mais on retrouve quand même quelques points communs, notamment dans un ou deux lieux. Tronchet a choisi une palette plus sépia pour les souvenirs de notre héros tandis que le reste est en couleur, notamment sur des nuances grises et bleutées lorsqu'il se trouve dans des villes bien tristes et déprimantes. 

Nous avons une véritable enquête pour retrouver ce chanteur qui a tout fait pour se cacher et finalement pour éviter le succès. On y croise alors la famille, mais aussi un certain [Pierre Perret](https://www.cheziceman.fr/2017/pierreperrettribu/), dont je ne savais pas qu'il avait aussi signé des artistes comme producteur. Forcément, quand on a la musique à écouter à côté, la lecture pousse à se plonger dans les chansons en même temps. Une musique datée dans les arrangements, comme le précise Tronchet en fin d'album, mais dont les paroles ne peuvent que toucher les amoureux des textes. 

Vous vous doutez bien que s'il y a des [chansons en ligne](http://www.jeanclaudetergal.fr/index.php/le-chanteur-perdu), dont des nouvelles, c'est qu'il y a un chanteur retrouvé, vivant sa vie comme il le souhaite. Cela en rappelle d'autres qui ont fait ce choix de ne pas suivre un plan de carrière, sinon le leur. Un bien joli album, comme sait toujours faire Tronchet.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/chanteurperdu1.jpg)


