---
layout: post
title: Série - Mixte de Marie Roussin (2021)
category: serie
tags: série, 2020s, féminisme, sexisme, mixité
---

**Diffusée sur la plateforme de streaming du méchant chauve, cette série a l'air d'une romance pour adolescent si on n'y prend pas garde. Et pourtant, elle est plutôt riche pour ses seulement 8 épisodes**

Le Sujet : *1963. Le lycée Voltaire, jusqu’alors réservé aux garçons, accueille pour la première fois des filles. Elles sont peu nombreuses, mais avec leur arrivée, c’est toute la vie du lycée qui est bouleversée, pour les élèves comme pour les professeurs.*

Vous l'aurez compris, on va parler de l'évolution des droits des femmes depuis les années 60. C'est si proche...Et pourtant on y voit à la fois le chemin parcouru depuis et la remise en cause actuelle de certains "acquis". Ici, c'est le garçon roi à qui on passait tout et la fille que l'on ne voit que dans des rôles subalternes. C'est aussi les castes ou classes sociales avec, dans cette petit ville, des fils de notables, des enfants d'ouvriers ou de professions décriées, et ne parlons pas des mères célibataires. Est-ce que cela a vraiment évolué autant qu'on le voudrait ? La série nous renvoie clairement cette question.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/mixteserie.jpg)

Et puis il y a l'aspect romance entre les personnages, qu'ils soient adolescents (avec toujours des acteurs trop vieux pour les rôles) ou adultes. Il y a, au passage, le sujet de l'homosexualité et des rumeurs. On y parle aussi système éducatif et la perpétuation du sexisme dans ce système, des castes professorales entre matières nobles et moins nobles, entre agrégés et non agrégés. Si le casting n'est pas toujours crédible en terme d'age, il est plutôt bon avec Pierre Deladonchamps (le surveillant général) pour emmener cela avec Nina Meurisse (la prof d'anglais). Mention spéciale pour Anouk Villemin (la copine) On ne tombe pas dans le cliché des ados gravures de mode comme l'on voit dans les séries US. Ca peut faire penser aussi à "La Boum" version 1960, parfois, mais alors sans Sophie Marceau et Claude Brasseur. 

Je ne sais pas si le succès aidera à faire une saison 2 ou pas. Les épisodes sont courts (35-40 minutes) ce qui nous donne donc un équivalent de 3 films. Dommage que la série ne soit que sur ce format de streaming, finalement. 


[bande annonce ![video](/images/youtube.png)](https://www.youtube.com/watch?v=HgE_3fJI2es){:target="_blank"}
