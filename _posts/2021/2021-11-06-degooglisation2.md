---
layout: post
title: Blog – État de ma déGooglisation et de ma DéGAFAMisation 2 ans de plus
category: geek
tags: gafam, geek, informatique, 
---

**J'ai écrit dernièrement sur le sujet en septembre 2019. Il s'en est passé des choses depuis et pourtant j'avais l'impression d'une relative stabilité. Je repars donc de ce dernier article.** 

Je rappelle donc qu'il s'agit de se passer des Google, Amazon, Facebook, Apple et Microsoft. Je le dis tout de suite, pour le boulot, ce n'est pas de ma faute, c'est imposé. Personnellement, je n’ai aucune utilisation des deux derniers en tant que fournisseurs de systèmes d'exploitation ou de hardware si ce n’est un vieux PC qui traîne sous Win7 jusqu’à sa mort. On verra à ce moment là. Qui sait ce que l'avenir nous réserve, à part Windows 11.

### Google  / Alphabet

Je continue à utiliser Gmail comme boîte poubelle et la firme sait donc sans doute ce que j’achète parfois et reçoit du spam à foison. Cela a tendance à se calmer avec le gros ménage que j'ai fait. Par contre aucun mail personnel ou aucun mail officiel (administrations, banques, etc… ) ne transite par ici puisque j’ai protonmail, la mère Zaclys, etc… Je réfléchis à basculer tout ça sur une autre boite mail à l'antispam efficace. Mais je n’utilise quasi plus qu’à 10% la recherche google et uniquement via DuckDuckGo (que je continue à préférer à Qwant, désolé pour le made in france qui n’en est pas vraiment vu le recours à d’autres résultats). Je regarde assez peu Youtube et uniquement via l’application NewPipe. Il faudrait, par contre, que je compte encore Waze, filiale de Alphabet/google et qui rend de sacrés services en région parisienne avec son info-trafic (voir l'essai de Magicearth dans la [revue de médias d'Aout](https://www.cheziceman.fr/2021/medias02e08/)). Pas de Google Maps, ou même de Google Translate, quand des concurrents (deepl, osm, here,…) font aussi bien (sans être tous libres pour autant). Évidemment, il n’y a pas non plus de Google Chrome pour naviguer, même sur le mobile, à moins de considérer qu'il y a du chrome dans Opera…

Oui, …. il y a le smartphone, en Android et là, pas de secret, il y a toujours une part qui va chez Google malgré toutes les attentions, comme l'utilisation de [Blokada](https://www.cheziceman.fr/2019/blockada/). Il y a des jeux notamment qui ne peuvent se passer de Googleplay ou de compte Google (sinon Facebook argh). J'ai arrêté de jouer sur ce type de jeu. Aujourd’hui la seule vraie alternative est de ne pas avoir de Smartphone mais un « dumbphone »… difficile de se forcer à un tel retour en arrière...il faudrait que je regarde si vraiment un OS comme /E/ (basé lui même sur LineageOS) est complètement sain et stable, avec le bon hardware. J'ai lu des essais contradictoires, selon où l'on place le curseur d'acceptation de bugs. 

### Amazon

Si j’achète les livres via un concurrent français qui a possibilité de livraison en magasin près de chez moi, il reste malheureusement des denrées très spécifiques qui sont plus pratiques à acheter sur Amazon... Je regarde toujours si je peux trouver aussi bien, c'est à dire le même produit, pas beaucoup plus cher, avec une garantie d'expédition correcte, chez des enseignes françaises ou européennes spécialisées. Il faut reconnaître qu'ils soignent leur SAV et leur livraison et ça, au prix d'un gâchis sur invendus et retours. A prendre en compte, comme l'aspect social, hélas recopié par les concurrents français. J'ai du quand même faire appel à leurs services pour des trucs spécifiques, des urgences. Un peu ma faute lorsque je n'ai pas anticipé. Les magasins physiques, c'est devenu des centres commerciaux stéréotypés, alors je pense plus à faire appel à de petites structures online qui ont su montrer leur sérieux depuis des années. On en trouve quelques unes sur des produits techniques, par exemple, car Amazon c'est un peu le Carrefour/Leclerc/Fnac/Auchan/Boulanger. Je suis nostalgique des quincailleries, épiceries, magasins de jouets d'avant, où l'on n'était pas obligé de prendre des lots de 25 pour avoir une pièce, ou un rouleau de 10 ou 30m d'un truc dont on veut seulement 1m. Ca se retrouve aussi sur le net mais...le frais de port. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/degooglisation2.jpg)

### Facebook / Meta

J’ai conservé longtemps un compte et c’était vraiment pour faire acte de présence avec quelques contacts qui ne veulent pas en sortir. Je l'ai fermé en Avril et tant pis pour les souvenirs de jeunesse. Qui m'aime me suive ? 

### Microsoft

Aux oubliettes, il y a deux ans, j'ai repris un peu d'Office365 sur Android parce que j'ai eu des besoins qui ne sont pas satisfaits par onlyoffice ou libreoffice lorsque je veux pouvoir mettre à jour un document en ligne où que je sois. Il faut que j'y travaille pour simplifier sur base de Nextcloud, je pense. C'est un de mes objectifs de l'année à venir. Et puis je m'étais donné un an ici pour savoir si je conservais le mail Outlook pour vos commentaires. On verra bientôt ce qu'il en est en fin d'année.

### Et après

Il est encore utopique de se passer à 100% des services de ces boites. Après tout, elles ont aussi de bons services mais il faut surtout savoir se prémunir du pillage d'information, du dumping social. A ce sujet, je suis inquiet quand je vois le déploiement de Office365 dans beaucoup de sociétés et jusqu'à quel degré d'information on est capable de chercher. L'espionnage industriel est largement possible. Je sais que des experts sécurité désespèrent de voir les décisions prises par les services achats de l'informatique. Idem avec tout ce qui transite par Android, Apple sur les téléphone pro, quand autrefois on avait peur de Blackberry. Le duo Firefox/Opera reste de mise chez moi, tout en me disant que le panda roux risque un jour de bouffer du Chromium comme tout le monde. 

J'ai passé l'outil [Exodus Privacy](https://exodus-privacy.eu.org/fr/) sur mon Androphone et ça donne toujours les mêmes problèmes : 
 
* Autotagger (l'arme absolue pour bien retagger les fichiers audios sans passer par le PC)
* Babelio (parce que c'est devenu plus ergonomique et complet que le site sur mobile et PC)
* Bandcamp (pour écouter et découvrir de la musique qui rémunère les créateurs)
* Bitwarden (le gestionnaire de mot de passe)
* Calculatrice xiaomi (je n'ai pas osé le virer puisque j'ai l'émulation d'une célèbre calculatrice libre)
* Gestionnaire de fichier xiaomi (je n'ai pas osé le virer)

Mais Blokada bride tous ces pisteurs, après quelques réglages.

Fort heureusement, pas autant de problèmes sur le PC en Ubuntu. Et Microsoft ne se soucie plus trop d'un PC moribond en Win7 que j'ai du quand même bétonner comme je pouvais pour la sécurité. C'est un gros point sensible à l'avenir, comme convaincre madame de ranger un peu mieux et sauvegarder là où il faut pour que ça soit recopié... Je ne peux pas être derrière, ça serait de l'ingérence :p. Le prochain sera en ..buntu ou Mint.

[Bande son : Lynyrd Skynyrd ![video](/images/youtube.png)](https://www.youtube.com/watch?v=QxIWDmmqZzY){:target="_blank"}

#### Commentaires

**Odysseus par mail**

> A propos de l'article du jour, je n'utilise personnellement plus Blokada qui me semblait énergivore. Je l'ai remplacé par [PersonalDNSFilter](https://f-droid.org/en/packages/dnsfilter.android/). Avec une personnalisation plus poussée par [Sebsauvage](https://sebsauvage.net/wiki/doku.php?id=dnsfilter).La question de Google pour ce cas-ci ne se pose donc plus.

**Fred par mail**

> merci pour cet intéressant billet. Si jamais ça intéresse, j'ai une page wiki sur laquelle je suis ma propre dégooglisation/dégafamisation: [https://radeff.red/dokuwiki/saygoodbye2google](https://radeff.red/dokuwiki/saygoodbye2google)
