---
layout: post
title: Souvenir de Gamer - Les jeux de golf Nintendo (1992-2021)
category: geek 
tags: retrogaming, jeu vidéo, golf, 1990s, 2000s, nintendo
---

**Alors qu'un nouvel opus de Mario Golf se profilait sur la Switch, je me suis repenché sur la saga Mario Golf et sur les autres jeux du même type de l'éditeur japonais**

Je n'ai pas joué au tout premier Mario Golf sur sa console d'origine, c'est à dire sur le Gameboy color et sur la Nintendo 64. Mais il faut dire qu'officiellement, le Golf chez Nintendo commence en 1992 sur la NES avec **NES Open Tournament Golf.** Il y a bien Mario et Luigi dedans mais le jeu n'est pas estampillé avec le plombier moustachu. Ce n'est que 7 ans plus tard que cette erreur marketing sera rattrapée. Je n'ai pas compris pourquoi la SNES n'avait pas eu droit à son opus. Surtout qu'Electronic Arts avait entamé sa série PGA dessus...

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/mariogolf0.jpg)

*la version NES*

C'est donc la petite portable couleur et la retardataire du 64 bits qui ont droit aux deux premiers vrais opus de la série. Pour compliquer tout ça, il y a aussi un Mobile Golf avec Mario mais édité par Camelot sur la même portable. J'ai commencé à jouer à cette série en 2004 avec le **Mario Golf Advance Tour** de la GBA. En même temps sort une version sur le Gamecube, plus spectaculaire sans doute mais j'avais fait un choix à l'époque. Là encore, c'est Camelot qui s'occupe de cette version Salon, pendant que Nintendo assure sur la mobile. C'était un peu particulier puisque ça ressemblait un peu à un JRPG avec une univers 2D, des personnages à aller voir, des objets, des points d'expérience.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/mariogolf1.jpg)

*La version 64*

Reste que la mécanique et le système de tir de ce Mario Golf Advance étaient très bon. J'étais déjà adepte de jeux de golf sur PC avec Links à l'époque et je ne suis pas moi même golfeur, préférant préserver mes vertèbres et mes hanches pour d'autres activités. J'apprécie juste ce type de jeu pour trouver le meilleur enchaînement de coups, se concentrer sur le drive ou le putt. Et pour cela, le système de tir est primordial. Chez Mario Golf, malgré l'aspect parfois enfantin, il est bien aussi évolué que pour les titres plus "pro". On peut gérer la vitesse de frappe, l'impact, l'effet. Il y a la prise en compte du vent, du relief et tout ça avec des pseudo-vues 3D. S'y rajoute des coups spéciaux pour la petite couche Mario pour que ça soit plus fun.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/mariogolf3.jpg)

*La version Gamecube*

J'ai eu l'occasion de jouer la version Gamecube qui se positionnait face aux *Everybody's Golf* de Sony. C'était plutôt fun et accessible aussi. Lorsque la DS est sortie, je m'attendais à un nouvel opus de la série, surtout que le stylet ouvrait des perspectives intéressantes. Étrangement, il n'y en a pas eu. Enfin si, mais sans Mario, baptisé **Nintendo Touch Golf - Birdie Challenge**. Là aussi le système de jeu était bien fait avec la possibilité de faire son drive au stylet et de tout gérer. Pas de mode RPG mais des suites de tournois, plus de 5 parcours et une 3D plutôt correcte par rapport aux performances de la machine. Le jeu étant sorti en début de carrière de la console, il se trouvait facilement en occasion. Il fallut attendre la 3DS pour retrouver Mario, soit 10 ans !

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/mariogolf4.jpg)

*La version 3DS*

Je n'ai pas eu en main cette version 3DS qui semble un compromis entre celui de la DS et celui de la GBA. Je n'attends pas forcément grand chose de nouveau de l'opus Switch mais je me dis, à utiliser ces deux versions en émulation sur mobile, que Nintendo pourrait aussi sortir quelque chose sur Android, quitte à simplifier un peu pour avoir moins de parcours. On arrive très bien par un simple appui à tout paramétrer dans ce type de jeux. A rejouer aujourd'hui à ces vieilles versions, j'ai perdu totalement de vue la quête du photoréalisme qu'il y avait sur PC. Ici c'est bien la jouabilité qui prime. Il n'est pas toujours évident de juger du relief du terrain sur une résolution assez basse, par contre.

[Ma petite vidéo !](https://videos.pair2jeux.tube/videos/watch/3325474a-af01-4af8-bb8e-6af047cbcd1c){:target="_blank"}

J'ai retrouvé les parcours et trous qui m'horripilaient alors. J'ai retrouvé la joie de faire un putt de plus de 10 mètres, la haine de voir la balle passer sur le bord sans tomber ou arriver trop vite. C'est aussi le choix de faire parfois un parcours plus sécuritaire que de risquer le tout pour le tout au dessus d'un plan d'eau. Ou alors de faire freiner la balle dans le rough juste avant le green pour qu'elle arrive au plus près du poteau. Finalement, je retrouve ce petit plaisir de ce type de jeux mais je n'ai strictement aucun plaisir à voir ça à la télévision, et donc à y jouer. Étrange ? Le challenge est différent car c'est aussi un petit travail sur soi même, une histoire de stratégie aussi en gérant les risques avec le vent et la pluie qui peuvent intervenir. La difficulté de ces jeux est uniquement dans la capacité de l'adversaire à atteindre une perfection peu humaine. 


