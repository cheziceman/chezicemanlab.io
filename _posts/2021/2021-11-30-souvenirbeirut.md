---
layout: post
title: BD - Je me souviens, Beyrouth de Zeina Abichared (2008)
category: bd
tags: bd, enfance, liban, guerre, 2000s, 
---

**Beyrouth et le Liban sont revenus à nouveau dans l'actualité. Ce pays dont la diaspora est importante après les 15 ans de guerre, était déjà profondément meurtri. Zeina Abichared nous parle de ses souvenirs d'enfance.**

Elle est née en 1981, soit 6 ans après le début des hostilités. Son enfance a dont été marquée par la violence, les privations mais aussi une petite part d'insouciance propre à l'enfance. C'est ce qu'elle nous décrit dans ces 100 pages en noir et blanc et dessin vectoriel. Ce choix de dessin renforce l'aspect enfantin propre à ces souvenirs tout en gardant une part de dramatique par cette bichromie. C'est aussi le style de prédilection de l'autrice avec des variations. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/souvenirbeirut0.jpg)

Si le début s'attache au contexte à la fois géopolitique et familial, c'est vite balayé pour parler de ces souvenirs qui font l'enfance, de quelques "Madeleine de Proust", de quelques scène hallucinantes. L'autrice et dessinatrice habitait à quelques encablures du "No Man's Land" qui séparait alors la ville en deux, entre l'Est et L'Ouest (depuis c'est plus morcelé encore, à part le sud, peut-être). On sourit, on rit beaucoup avec les enfants, même si la gravité n'est jamais loin (comme la musique du père qui essaie de couvrir les fracas de la guerre). L'école continue aussi malgré la dangerosité pour rejoindre l'établissement. Les enfants collectionnent des débris de cette guerre...

Comme tous les enfants, il y a le souvenir de la voiture, même si ici cette R12 a beaucoup souffert. Il y a les départs, les voyages, les embouteillages. Tout cela pourrait paraître normal mais il y a l'épée de Damoclès de la guerre que l'on essaie de masquer le plus possible aux enfants mais qu'ils ressentent et qui les marquent. Ce sont des "vacances" forcées à Chypre, toute proche. Et ainsi de suite... Jusqu'à la fin de cette guerre. Mais ça c'est aussi une autre histoire.

Et pour une fois, une bande son citée dans le livre.
[Sabah - Allo Bayrout ![video](/images/youtube.png)](https://www.youtube.com/watch?v=QiBQr71luyU){:target="_blank"}

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/souvenirbeirut1.jpg)

