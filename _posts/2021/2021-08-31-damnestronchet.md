---
layout: post
title: BD - Les Damnés de la terre associés de Tronchet (1987-2000)
category: bd 
tags: bd, pauvreté, société, 1980s
---

**Amis de l'humour noir et du second degré, bien le bonjour. Vous connaissez peut-être Jean-Claude Tergal ou Raymond Calbuth ? Tronchet n'est pas que ça, mais aussi une autre double trilogie regroupée sous cet acronyme hyper-vendeur : Les Damnés de la terre associés.**

Dans ce doux monde du commerce, du capitalisme et de la mondialisation joyeuse, qu'il est bon d'avoir un regard acéré, que dis-je acide sur tous ces pauvres qui nous côtoient, nous autres qui avons le loisir de lire des BD dans notre si précieux temps libre. Enfin côtoyer, n'abusons pas tout de même...

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/damnestronchet0.jpg)

Tronchet aime à regarder ces invisibles, ces situations cocasses qui mettent la lumière sur les pauvres malheureux. Que cela soit dans "Stars d'un jour", 'Stars toujours", "Jours de stars", il y a de quoi faire dans cette galerie de portraits qu'aucun musée ne voudrait exposer et que la lucarne magique se refuse à montrer sous cet angle. Regardez donc cette joie dans le dénuement, c'est communicatif. Qui sommes-nous pour nous plaindre ou nous moquer...Sacré Tronchet. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/damnestronchet1.jpg)

"Au bonheur des drames" porte bien son nom finalement car il n'y a de drames que dans la tête. Perdre son emploi et sa maison, avoir une maladie rare, n'avoir qu'une boite de pâté pour chien à offrir à ses enfants à noël, c'est encore avoir un peu de vie tout de même. Le sourire et la bonne humeur, ça compte bon sang et ça ne se vend pas en plus. Mouais, un peu dans ces albums à l'unité ou en intégrale mais Tronchet doit bien manger aussi. Son dessin est tout en rondeur, avec une mise en couleur chatoyante comme une maladie vénérienne. De quoi rencontrer "Les rois du rire" et se dire que l'on peut être "Pauvres mais fiers".

Quand je pense qu'il y en a qui vont encore se plaindre que l'on se moque des pauvres, gna gna gna...Que nenni, ce sont bien les bourgeois et les riches que l'on moque en réalité avec tous les travers de notre société où il fait si bon vivre ensemble. Pas de quoi remettre ça en cause, non. Il faut que ça continue parce que sinon il va raconter quoi, le Tronchet ? Hé ouais...

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/damnestronchet2.jpg)


