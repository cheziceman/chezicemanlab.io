---
layout: post
title: Musique - Opium Moon - Opium Moon (2019)
category: musique
tags: musique, new age, ambient, world, 2010s
---

**Je crois que je dois cette jolie découverte à un "mastonaute" mais je ne sais qui remercier. Adeptes de la décontraction musicale, venez vous détendre 5 minutes avec cette chronique**

Je dis 5 minutes, mais aucun titre de l'album ne fait moins de 6 minutes 23. Et aucun besoin de prendre une quelconque drogue pour profiter de cette musique New Age. Car il s'agit bien d'un savant mélange plutôt planant concocté par 4 musicien(ne)s de Los Angeles. Pour la petite histoire, on y retrouve Hamid Saeidi au [Santoor](https://en.wikipedia.org/wiki/Santoor) (un instrument à corde iranien plutôt complexe), Lili Haydn au violon (qui officie aussi dans l'orchestre philarmonique de la même ville), MB Gordy aux percussions et Itai Disraeli à la basse. Voilà pour les présentations. L'album a reçu un Grammy en 2019 dans sa catégorie (oui, il y en a une) et pour une fois, c'est mérité. Laissons tout cela de côté pour embarquer vers l'orient où nous emmène cet album.

![cover](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/opiummoon.jpg)

Car la New Age, c'est vague mais l'utilisation conjointe du Santoor et du Violon avec des percussions orientale nous crée tout de suite une ambiance. Je m'imagine tantôt sur un fleuve paisible ou dans l'escalade d'une montagne sur des hauts plateaux déserts, près de troupeaux. Car les sonorités créent un imaginaire qui nous est propre. On y retrouve aussi dans une belle unité un véritable petit monde musical où chaque note paraît ciselée et parfaitement à sa place avec les autres instruments. Les percussions peuvent se faire ainsi discrètes pour laisser le violon faire sa tirade, puis revenir gronder un peu plus loin dans une atmosphère que l'on imagine dans un temple isolé. On médite volontiers avec une telle musique en tête.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/opiummoon2.jpg)

L'album est un tout et on ne peut ressortir un "single" d'une telle oeuvre. Chaque titre est à sa place et je ne me suis même pas essayé à sortir de l'ordre prescrit. Vous viendrait-il à l'idée dans un voyage, d'aller d'abord à l'arrivée et de faire les villes étapes dans le désordre? J'y ai ressenti une progression ici et cela se finit par un titre qui est une rencontre entre l’extrême-orient et l'occident. C'est doux, apaisant et c'est typiquement ce dont on a besoin dans les moments difficiles pour sortir un peu de sa réalité. Je m'interroge pourtant sur la capacité de chacun à ressentir des émotions similaires sur cet album en fonction de sa propre histoire, de ses propres expériences. Le mieux, c'est donc d'essayer et d'embarquer. 

En video : [![video](/images/youtube.png)](https://www.youtube.com/watch?v=HBEzWI3MEUc){:target="_blank"}

#### Commentaires :

**Alias par Mastodon**

> Très sympa, Opium Moon; après, je ne sais pas si "new-age" est la classification la plus adaptée (j'aurais sans doute parlé de "folk-prog orientalisant"), mais comme tu le dis, c'est une catégorie assez fourre-tout. Ça m'a un peu fait penser à Minimum Vital.