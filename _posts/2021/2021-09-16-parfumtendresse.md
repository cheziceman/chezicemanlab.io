---
layout: post
title: Littérature - Le Parfum de la Tendresse d'Alice Quinn (2021)
category: litterature
tags: roman, feelgood, tranche de vie, 2020s
---

**Et voilà le premier roman que je lis qui parle de cette période COVID. La prolifique autrice Alice Quinn nous parle d'un prof de lettres, Jo, et du cataclysme qui va s'abattre dans sa vie. Mais quelque chose me disait dans cette couverture que ça finirait bien**

Pour tout vous dire, c'est madame qui m'a conseillé ce livre. Elle sait ce qui me fera pleurer comme une madeleine et sourire béatement. Et j'avais vu que ce livre passablement l'avait chamboulé. Je vais essayer quand même de ne pas trop en dire sur cette histoire. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/parfumtendresse.jpg)

Joseph, dit Jo, est un prof dans un lycée, veuf. Il ne voit plus sa fille depuis la mort de sa femme, mort dont elle le rend coupable. Elle vit loin de lui, est en ménage avec un ressortissant indien avec lequel elle a eu un fils. Un jour, Jo fait la rencontre de Sissi, une petite chatte abandonnée qu'il recueille. Le même jour, sa fille adopte un chien, Pouf, pour son fils Rohan. Mais quelques semaines plus tard, après avoir dépose Rohan à la crèche, le couple décède dans un accident de voiture en allant emmener Pouf pour être stérilisé chez le vétérinaire. Jo "hérite" donc de Rohan dans sa vie. Pouf est perdu et cherche sa famille. Et une certaine Gladys, ancienne militaire, emménage avec fracas dans l'immeuble de Jo.

Ce n'est pas un roman sur le deuil, pas une romance, pas un thriller, juste une roman décrivant une tranche de vie, avec tout ce qu'elle peut avoir d'improbable. La réalité dépasse parfois la fiction, c'est bien connu. Et Alice Quinn y a trouvé de l'inspiration. Je disais qu'il y a des moments de larmes et c'est la force de ce petit roman, notamment quand elle donne la parole à Sissi ou à Pouf. On peut lui reprocher de trop personnifier l'animal mais je lui trouve de la justesse dans ces moments. Des moments de tristesse donc, mais qui ne versent pas dans l'excès de pathos que l'on pourrait supposer. Il y a bien des pistes un peu trop grosses dans ces croisements de personnages, ces destins mais j'y ai adhéré facilement.

Ce n'est certes pas le roman de l'année mais c'est un livre avec lequel on se sent bien, où l'on apprécie d'accompagner ces personnages et de les voir ... grandir. C'était mon premier livre de cette autrice qui aime changer les styles, mais vraisemblablement pas le dernier.

