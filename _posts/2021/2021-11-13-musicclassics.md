---
layout: post
title: Musique - Les classiques que j'emporte partout
category: musique, blog
tags: musique, blog, 
---

**Je ne suis pas adepte du streaming, pour de multiples raisons que j'ai déjà exposées. Aussi, j'emporte un certain nombre d'albums au format numérique partout avec moi, du smartphone au PC de bureau, quand je n'ai pas accès à mon NAS et pour économiser les données**

Si j'ai essayé de limiter un peu ce nombre d'albums, je me suis aperçu que c'était mission impossible pour moi parce que je suis trop éclectique. Voilà qui fait autour de 15  Go quand même, mais qu'est ce que cela aujourd'hui. Alors ils font partie de la longue liste des albums que j'emmènerai sur une île déserte. Même si c'est par période, je ne peux m'en passer. Ils font partie de ma vie, de mon passé, de mon présent, de mes souvenirs, de mes joies et mes peines. Beaucoup ont d'ailleurs été chroniqués ici. Je ne vais pas expliquer pourquoi ils font partie de cette liste car c'est personnel. Mais juste un petit commentaire pour vous donner envie. La règle de un album par artiste n'est pas respectée pour mes petits chouchous évidemment. Et qui sait ce qu'elle deviendra dans 5 ans...

### Classique / Musiques de films
* Anton Dvorak - [Symphonie du nouveau monde](https://www.cheziceman.fr/2010/dvorak-symphonienouveaumonde/) (1893) : La quintessence de ce compositeur qui continue d'inspirer.
* Agnes Obel - [Aventine](https://www.cheziceman.fr/2013/agnesobel-aventine/) (2013) : Parce qu'une voix et un instrument valent parfois mieux que de grands orchestres surtout quand cela s'allie à la modernité.
* Cecile Corbel - [Le petit monde Arrietty](https://www.cheziceman.fr/2011/cecilecorbel-arrietty/) (2010) : Pour la douceur qui émane de la harpe et de la voix de l'artiste,  qui rencontre l'univers de Miyazaki.
* [Joe Hisaishi](https://www.cheziceman.fr/2020/hisaishidream/) - Hana-Bi (1997) : Pour le souvenir indélébile du film et des notes qui touchent à l'épure.
* Joan Sebastian Bach - Variations Goldberg (1740) : Le piano tout simplement avec des mélodies intemporelles.
* Kenji Kawai - [Avalon](https://www.cheziceman.fr/2020/avalon-kawai/) (2001) : Un film énigmatique et une musique grandiose.
* Ludwig Van Beethoven - Symphonie N°7 (1811): La Symphonie qui ne cesse de me toucher au plus profond de mon coeur.
* Yann Tiersen - [Le Fabuleux destin d'Amélie Poulain](https://www.cheziceman.fr/2016/yanntiersenamelie/) (2001) : Parce que cette musique crée une ambiance et des souvenirs.
* Zoe Keating - [Into the Trees](https://www.cheziceman.fr/2015/zoekeating/) (2010) : Parce que son violoncelle est magique

### Jazz
* George et Ira Gerschwin - [Porgy and Bess](https://www.cheziceman.fr/2020/porgyandbess/) (1935) : Un classique du jazz et du blues devenu aussi un classique tout court avec son casting exclusivement noir.
* Jane Monheit - [In the sun](https://www.cheziceman.fr/2018/janemonhiet-sun/) (2002) : Parce que c'est un album qui réchauffe et apaise.
* Louis Armstrong et Duke Ellington - Recording together for the first time (1966) : Parce que ce sont deux grands qui ont bercés ma jeunesse et que le piano de l'un, la trompette et la voix de l'autre s'accordent parfaitement.

### Blues
* BB King - Singin' the blues (1956) : Parce que seule une compilation peut montrer l'étendue du talent de ce bluesman et guitariste hors-pair.
* John Lee Hooker - The very best of (1962) : Encore une compilation parce que sa carrière fut aussi longue que sa reconnaissance tardive.
* Muddy Waters - Hard again (1977) : Le triptyque des trois grands du blues est ainsi complet avec cet album du come-back.

### Rock
* Anathema - [Hindsight](https://www.cheziceman.fr/2008/anathema-hindsight/) (2008): Quand un groupe metal change et épure ses morceaux
* Chris Rea - [Auberge](https://www.cheziceman.fr/2008/chrisrea-auberge/) (1991) : Son album parfait qui annonce déjà un virage bluesy.
* Chris Rea - The Road to hell (1989) : Parce qu'avant l'Auberge, c'était l'enfer qui est pavé de notes de guitare.
* Creedence clearwater revival - [Willy and the poor boys](https://www.cheziceman.fr/2019/creadanceclearwater/) (1969) : Un classique du rock bluesy.
* FFF - Vivants (1997) : La Fusion à la française explose littéralement sur scène. Mythique. 
* FFF - [FFF](https://www.cheziceman.fr/2008/fff-fff/) (1996): Un album studio qui clôt (presque) la carrière du groupe comme s'ils avaient atteint leur summum. 
* Kate Bush - [The whole story](https://www.cheziceman.fr/2020/katebush/) (1986) : Pour sa voix et son univers qui me transporte.
* Midnight oil - [Blue sky mining](https://www.cheziceman.fr/2008/midnightoil-blueskymining/) (1990): Le rock australien se découvre une conscience et une autre voix.
* Pat Benatar - [Greatest hits](https://www.cheziceman.fr/2017/patbenatar/) (2005): Parce que cette chanteuse est incroyable avec des hits que l'on n'oublie pas.
* Pink Floyd - [The Wall](https://www.cheziceman.fr/2010/pinkfloyd-thewall/) (1979) : Le summum du concept-album doublé d'un film.
* R.E.M. - [Automatic for the People](https://www.cheziceman.fr/2008/rem-automaticforthepeople/) (1992) : Pour la profondeur de ses chansons, quand Michael Stipe était au top. 
* R.E.M. - Monster (1994) : l'album maudit mais qui reste pour moi une synthèse entre le grunge de l'époque et le rock intello du groupe. 
* Roxy Music - [Roxy Music](https://www.cheziceman.fr/2016/roxymusic/) (1972): Quand ça se frictionnait entre les deux Brian. 
* Status quo - [Live](https://www.cheziceman.fr/2010/statusquolive/) (1977) : Parce que leurs hits y étaient et leur énergie irradiait la scène. 
* The Answer - [Everyday Demons](https://www.cheziceman.fr/2010/theanswer/) : Quand le blues rock rencontre l'Irlande, c'est détonnant.
* The Beatles - Sergent Pepper's lonely hearts club band (1967): Mythique, inventif et moderne.
* The Cure - [Standing on a beach](https://www.cheziceman.fr/2020/thecure/) (1985) : une période musicale sombre et mystérieuse.
* The Police - [Synchronicity](https://www.cheziceman.fr/2010/police-synchronicity/) (1983) : L'équilibre était trouvé entre les multiples influences
* Tom Petty & the heartbreakers - [Into the great wide open](https://www.cheziceman.fr/2008/tompetty-greatwideopen/) (1991): L'album parfait
* The Who - The greatest hits and more (1960-1980): Un best of pour un groupe qui marqua son époque.
* U2 - Singles (1980): Le groupe a une telle collection de hits que seule une compilation pouvait être la solution.
* VAST - [Visual audio sensory theater](https://www.cheziceman.fr/2009/vast-vast/) (1998) : le premier album de ce génie multi-instrumentiste de Jon Crosby
* VAST - Music for people (2000) : l'album est indissociable du précédent car écrit dans le prolongement.
* VAST - [April](https://www.cheziceman.fr/2020/vastapril/) (2007) : Une période plus acoustique de l'artiste qui met en valeur son talent de mélodiste et ses textes si touchants. 

### Metal / Hard Rock
* [AC/DC](https://www.cheziceman.fr/2009/acdc-answers-paris/) - Live! (1992) : Parce que l'énergie du groupe est décuplée sur scène
* [Audrey Horne](https://www.cheziceman.fr/2013/audreyhorne-youngblood/) - Audrey Horne (2010) : On pourrait presque penser que c'est leur premier, tellement ils ont l'air d'avoir tout donner dans leur Norvège natale. 
* [Cheap Trick](https://www.cheziceman.fr/2011/cheaptrick-dublin/) - At Budokan (1978) : L'autre live du Budokan / Dream police
* [Coheed and Cambria](https://www.cheziceman.fr/2008/coheedcambria-noworld/) - Good Apollo, I'm burning star IV... (2005) : Le troisième fut le bon pour ce groupe au leader inclassable.
* Devin Townsend - [Ocean Machine](https://www.cheziceman.fr/2020/devintownsend/) (1997) : Un voyage chez un génie fantasque.
* Emperor - [In The Nightside eclispe](https://www.cheziceman.fr/2015/emperor-eclipse/) (1993) : s'il ne doit y avoir qu'un album de black, c'est celui là.
* Evanescence - [Fallen](https://www.cheziceman.fr/2009/evanescence-fallen/) (2003) : Le metal au féminin devint populaire
* Foreigner - [4](https://www.cheziceman.fr/2009/foreigner4/) (1981): Un album quasi parfait aussi avec un chanteur incroyable.
* [Guns'n Roses](https://www.cheziceman.fr/2009/gunsnroses-chinesedemocracy/) - Appetite for destruction (1987) : Parce qu'ils n'ont jamais fait mieux en terme de glam Rock moderne.
* Joe Satriani - [The Extremist](https://www.cheziceman.fr/2020/satriani-extremist/) (1992) : Pour avoir un instrumental de guitare à la fois virtuose et mélodieux.
* Journey - Escape (1981) : Un album de hits AOR qui ne se démode même pas.
* [Judas Priest](https://www.cheziceman.fr/2011/judaspries-duffmckagan/) - British steel (1980) : Le Metal God est né.
* Kiss - [Alive!](https://www.cheziceman.fr/2008/kissalive/) (1975) : Un groupe fait pour le live et qui aligna ici ses premiers hits.
* Metallica - [Black Album](https://www.cheziceman.fr/2009/metallica-blackalbum/) (1991) : Peut-être plus édulcoré que leur début mais imparable.
* Nirvana - [Nevermind](https://www.cheziceman.fr/2008/nirvana-nevermind/) (1991) : Et le Grunge apparut à la face du monde pour 10 ans... et l'éternité.
* [Process](https://www.cheziceman.fr/2008/process-possederappartenir/) - Lourde attirance (2004) : Pour un gros regret sur un groupe français qui n'eut pas sa chance.
* Queen - [The day at the races](https://www.cheziceman.fr/2008/queen-dayattheraces/) (1976) : Mais je prendrais bien la majeure partie de la discographie
* [Queen](https://www.cheziceman.fr/2008/queen-paris/) - Live Killers (1979) : Parce qu'il fallait aussi Freddie en live.
* Queen - Jazz (1978) : Parce que c'est tout sauf du jazz mais que ça donne de la joie aussi.
* [Scorpions](https://www.cheziceman.fr/2011/scorpions-paris2011/) - Tokyo Tapes (1978) : Pour conclure la première carrière du groupe par un live mythique.
* [Trust](https://www.cheziceman.fr/2009/trust-skiptheuse/) - Répression (1980) : Qui a dit que le Hard-rock ne pouvait être français? 
* Van Halen - 1984 : Le premier album fut une révolution pour bien des guitaristes.

### Chanson française
* Francis Cabrel - [Samedi soir sur la terre](https://www.cheziceman.fr/2015/franciscabrel-samedisoir/) (1994): Quand la chanson française se teinte de folk.
* Kent - [En scène](https://www.cheziceman.fr/2008/kent-enscene/) (1995) : Parce que c'est sur scène qu'il livre tout de ses textes et musiques. 
* La Grande Sophie - [Nos histoires](https://www.cheziceman.fr/2016/grandesophie/) (2015) : Une voix, une présence et des titres qui me touchent.
* [Zazie](https://www.cheziceman.fr/2015/zazie-zen/) - Zest of (1995-2008): Pour sa pop française intelligente.
* [Zebda](https://www.cheziceman.fr/2019/zebda/) - Essence ordinaire (1998) : Parce que la chanson française n'est jamais aussi belle que lorsqu'elle est métissée.

### Pop
* Abba - [Gold](https://www.cheziceman.fr/2019/abbagold/) (1975-1981) : Pour un groupe en or qui révolutionna le disco et la pop. 
* [Depeche Mode](https://www.cheziceman.fr/2008/depechemode-blackcelebration/) - Violator (1990): Pour toute la collection de hits et l'ambiance qui s'en dégage.
* Kim Wilde - [The Singles collection](https://www.cheziceman.fr/2017/kimwilde/) (1981-1993) : Parce que c'est une artiste de singles.
* Mika - Life in cartoon motion (2007) : Quand un "petit" génie mais toute sa vie dans un album.
* Supertramp - [Even in the quietest moments](https://www.cheziceman.fr/2014/supertramp-quietest/) (1977) : La magie opère toujours comme à la première écoute.

### Soul et Funk
* Earth Wind and Fire - Indispensable (1969-1984) : Parce qu'il fallait aussi une compilation ou tout prendre de ce groupe disco-Funk. 
* Kool & the gang - Celebration (1964-1984) : Le disco résonna encore grace à eux.
* [Michael Jackson](https://www.cheziceman.fr/2009/michaeljackson-offthewall/) - HIStory, Past, present and future (1979-1995): Album hybride entre compilation et studio et qui sonne comme un premier testament de l'artiste.
* [Stevie Wonder](https://www.cheziceman.fr/2015/steviewonder-talkingbook/) - The Definitive collection (1961-1988) : Parce que malgré tous mes efforts, il manque toujours un hit sur les autres albums de ce pur génie.


### Musiques du monde
* [Angelique Kidjo](https://www.cheziceman.fr/2016/angeliquekidjo/) - Logozo (1992) : L'album des premiers hits et une voix qui sait déjà tout faire.
* [Asian Dub Foundation](https://www.cheziceman.fr/2008/asiandubfoundation-factsandfiction/) - Rafi's revenge (1998) : presque l'album fondateur d'un style avec la rage du moment.
* Lisa Gerrard (et Peter Bourke)- [Duality](https://www.cheziceman.fr/2019/duality/) (1998) : Un voyage dans le monde de Lisa.
* Ofra Haza - Yemenite songs (1984) : Celui qui la fit découvrir au monde.
* Rachid Taha - [Tekitoi](https://www.cheziceman.fr/2018/rachidtaha/) (2004): C'est Rock, c'est Rai, c'est Arabe, c'est Rachid.

### Rap / Hip Hop
* Wu Tang Clan - [Enter the Wu Tang](https://www.cheziceman.fr/2008/wutangclan-enterthewutang/) (1993) : Ces mecs ont inventé un style et inspiré tellement de monde

### Electro / Techno
* FatBoy Slim - [You've come a long way, baby](https://www.cheziceman.fr/2020/fatboyslim/) (1998) : Le Big Beat dans sa face la plus Pop
* Goldfrapp - [Supernature](https://www.cheziceman.fr/2014/goldfrapp-supernature/) (2005) : Une voix, un son, et en plus on bouge dessus. 
* [Ian van Dahl](https://www.cheziceman.fr/2008/ianvandahl/) - Ace (2002) : Pour ce mélange de Trance, de Dance et de Pop.
* [Jean-Michel Jarre](https://www.cheziceman.fr/2013/jmj-odysseythrougho2/) - Oxygène (1976) : Le fondateur d'un genre.
* Kosheen - [Resist](https://www.cheziceman.fr/2010/kosheen-resist/) (2001) : Parce que c'est la découverte de la voix de Sian Evans
* Kosheen - Kokopelli (2003) : Et le groupe confirma dans un second album pour prolonger mon plaisir d'écouter leur Trip-Hop.
* Mirwais - [Production](https://www.cheziceman.fr/2019/mirwais/) (2000) : Pour moi (et Madonna), ce fut la french touch
* [Morcheeba](https://www.cheziceman.fr/2010/morcheeba-paris/) - Big Calm (1998) : Parce que c'est le meilleur pour le bonheur qu'il procure, avec ...
* Morcheeba - Charango (2002) : Parce que c'est le premier album de Trip-hop que j'ai acheté.
* The Prodigy - [Music for Jilted Generation](https://www.cheziceman.fr/2008/prodigy-musicforjilted/) (1994) : L'album qui fit exploser le Big Beat aux yeux du monde.
* The Prodigy - [The Fat of the land](https://www.cheziceman.fr/2018/prodigy-fat/) (1997) : Parce que je ne peux le dissocier de l'autre.

## Chronologie

Si je classe tout ça par décénies, ça nous donne : 
* Avant: ■■■■
* 1950 : ■
* 1960 : ■■■■■■■
* 1970 : ■■■■■■■■■■■■■■■■■
* 1980 : ■■■■■■■■■■■■■■■■■■■■■
* 1990 : ■■■■■■■■■■■■■■■■■■■■■■■■■■■■
* 2000 : ■■■■■■■■■■■■■■■■■■■■
* 2010 : ■■■■
* 2020 : 

Et pourtant vous trouverez des manques dans tout cela, des groupes comme les Rolling Stones, Led Zeppelin, Deep Purple, du Mozart, du Wagner, les Commodores, Bo Diddley, Robert Johnson, du punk, du swing ou que sais-je encore ? Oui, il y a aussi des singles, des titres isolées que j'aime avoir mais ça sera, peut-être une autre histoire. Mais c'est quand même bien centré sur ma quarantaine, non ? Certains albums non chroniqués apparaîtront peut-être plus tard ici.

