---
layout: post
title: Pause Photo et Poésie - Sourire
category: poesie
tags: poésie, poème
---

Ce rayon de soleil dans le ciel

transperçant les tours quadruplées.

Voilà mon prince des saisons de miel

qui s'abandonne sur mon reflet.

J'y plisse l'heure passée à frémir

pour écouter la voix encore recluse.

La dent dure au quatre zéphyrs

j'emmène mon âtre diffuse

dans un rire intérieur et voyant

qui m'attendait là pour l'an. 


![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/poemesourire.jpg)

