---
layout: post
title: BD - Vois comme ton ombre s'allonge de Gipi (2014)
category: bd
tags:  guerre, folie, art, bd, italie, 2010s
---

**Attention, pour public averti. Voilà un roman graphique complexe à aborder avec deux thèmes habilement imbriqués, tout comme les styles graphiques.**

Une fois encore, c'est la couverture qui a joué son rôle pour m'attirer. Le nom de cet auteur italien en gros, Gian Alfonso Pacinotti alias Gipi, une scène de guerre, un arbre...Et il aborde justement souvent ce thème de la guerre. Mais cette fois il y a le personnage de Landi, ... Une station service perdue dans la campagne, cet homme que l'on retrouve perdu, seul. Un hôpital psychiatrique. Et en même temps ces scènes de guerre d'un poilu qui lui ressemble, qui écrit des lettres à sa femme alors qu'il part pour une mission suicidaire.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/voiscommeombre.jpg)

Il faut au moins lire la moitié de l'album pour comprendre le lien entre ces histoires. Le trait est crayonné pour la partie contemporaine ajoutant au trouble du personnage central dans la lecture que l'on peut en faire. Idem pour le lettrage, manuscrit. Et face à cela, il y a ces scènes du passé à l'aquarelle. Quand je dis du passé, c'est d'abord celui de la guerre, qui semblent comme des rêves ou des hallucinations pour notre personnage. Sans révéler le lien entre eux, les aquarelles vont peu à peu envahir aussi le présent, comme pour un retour à la réalité.

Outre la complexité de la première lecture (c'est typiquement un livre à relire), c'est la beauté de ces pages qui frappe le lecteur. Je vous en ai mis un exemple ci-dessous mais c'est évidemment plus beau en réel, pleine page, avec le touché pour en voir les nuances, les détails. C'est une œuvre pour adulte qui parle aussi de l'environnement psychiatrique, des relations humaines et familiales, de l'imaginaire que l'on se construit. Difficile d'en parler bien sans en dévoiler trop. C'est un livre qui ne peut se lire distraitement, qu'il faut prendre complètement avec soi, même si on ne le lit pas d'une traite. 

Mais je comprendrai aussi ceux que cela rebute, autant par les thèmes que par la forme. Moi, ça m'a donné envie de lire ou relire du Gipi, aussi.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/voiscommeombre2.jpg)
