---
layout: post
title: Cinéma - Une Femme du Monde de Cécile Ducrocq (2021)
category: cinema
tags: 2020s, cinéma, film, prostitution, adolescence
---

**Voilà un film qui sort dans une période peu propice à ce sujet difficile de la prostitution. Un premier long métrage pour une scénariste de série, en plus.**

Marie (Laure Calamy) vit à Strasbourg. Son fils Adrien (Nissim Renard), 17 ans, veut poursuivre des études de chef cuisinier et vit dans un foyer étudiant. Mais après une bêtise d'adolescent, il est viré de son école. La seule issue est une école privée mais qui demande 9000 Euros pour l'année. Sauf que Marie a pour métier la prostitution, dont elle est aussi militante pour la dépénalisation. Comment faire quand aucune banque ne veut prêter de l'argent dans sa situation ? 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/femmedumonde.jpg)

C'est évidemment le parcours d'une femme, d'une mère courage qui est en plus en proie au passage de l'adolescence à l'age adulte de son fils. Elle doit à la fois combattre son côté immature et rebelle mais aussi la vision de la société envers son métier. Laure Calamy incarne avec brio ce rôle, mais on peut citer aussi l'acteur qui joue son fils ou encore Romain Brau qui joue le "collègue" trans-sexuel étudiant en droit. La réalisatrice filme cela sans voyeurisme mais en montrant tout de même la dureté de ce métier dans ses divers aspects. On y voit aussi bien l'indépendante qu'est Marie que les "esclaves" du sexe dans des camionnettes tenues par des macs, ou les maisons closes de l'Allemagne voisine.

C'est en filmant le quotidien de cette femme que la réalisatrice rappelle justement qu'au delà de l'image sordide ou glamour que l'on peut donner à ce métier, il y a une femme comme les autres, une femme qui doit élever seule ses enfants avec en plus le regard que peut avoir cet enfant et qu'il doit subir de ses amis. La photographie Noé Bach est aussi à saluer, ne sombrant pas dans l'esthétisation que l'on retrouve trop souvent sur ce sujet. Le sujet est délicat et l'on y montre des clients assez différents, de celui qui est complexé à celui qui est drogué du sexe en passant par les violents. On ne sombre pas non plus dans le documentaire et l'on voit que même entre militantes, il peut y avoir des divergences sur ce sujet.

Une belle réussite qui sort dans une période où l'on pense plutôt aux fêtes et aux enfants. Espérons qu'il aura sa chance.

[La Bande annonce ![video](/images/youtube.png)](https://www.youtube.com/watch?v=6u3_3uag27c){:target="_blank"}
