---
layout: post
title: Véganisme au quotidien - Douceurs d'hiver
category: vegan 
tags: recettes, vegan, vegetarien, végétarisme
---

**A la demande générale d'une personne, je vais mettre quelques petites recettes que j'utilise. Je n'ai rien inventé, mais au moins, contrairement à d'autres blogs qui recopient et traduisent des recettes américaines, j'ai testé, modifié et approuvé ce qui suit. Evidemment c'est vegan mais tout le monde peut en manger.**

***Rien de tel qu'une bonne boisson chaude et gourmande pour se mettre au chaud chez soi. Et bien plutôt que de se faire arnaquer par une chaine de café bien connue, on peut faire tout ça soit même avec un :***

**Café Gingerbread Latte**

Sirop (pour plusieurs jours d'utilisation...ça se conserve 5 jours)
*  35 cl d’eau
*  300 g de sucre en poudre
*  2 cuillères à café de gingembre
*  1/2 cuillère à café de cannelle
*  1/4 cuillère à café de vanille en poudre
*  une pincée de Noix de muscade

Faire cuire le sirop jusqu'à frémissement 15 minutes
Pour chaque personne, prévoir :
* 125 ml de lait
* une double dosette de café ou un double café.
Mettre le lait à réchauffer et ajouter 6 cuillerée à soupe pour 250ml de lait (2 personnes)
Rajouter le café et répartir dans les mugs. Rajouter de la chantilly et saupoudrer de canelle.

***C'est devenu très à la mode et on en met partout. Sauf qu'en version vegane, c'est plus rare. On peut en mettre sur des crêpes, des gaufres et sur les recettes qui suivent en dessous. C'est le :***

**Caramel beurre salé**

Ingrédients pour 1 grand pot à confiture :

*  200 g de lait végétal au choix
*  200 g de sucre blond
*  100 g de sirop d’érable ou d’agave.
*  80 g de margarine végétale
*  1 voire 2 c. à soupe bombée de fécule de maïs ou d’arrow-root
*  1 c. à soupe d’eau
*  1 c. à café de vanille liquide ou moulue, ou un sachet de sucre vanillé.
*  1 c. à café de gros sel

Verser le lait végétal, le sucre et le sirop dans une casserole. Chauffer à feu moyen jusqu’à ébullition, en mélangeant régulièrement au fouet.
Mélanger la fécule et l’eau dans un petit bol et verser dans la casserole.
Fouetter sans arrêter jusqu’à épaississement du caramel (environ 3 minutes).
Hors du feu, ajouter la margarine, la vanille et le sel. Mélanger jusqu’à l’obtention d’un caramel lisse et onctueux.
Verser dans un pot préalablement passé à l’eau bouillante et laisser refroidir.
Conserver au réfrigérateur deux semaines.

***Ces petites crêpes, très courantes aux USA et au Canada, se prennent au petit déjeuner avec du sucré de toute sorte. Pas besoin de préparation toute faite quand il est si simple d'en faire. Ce sont les :***

![pancake](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Pancakes_on_table.jpg/640px-Pancakes_on_table.jpg)

*Photo wikimedia par [Shisma](https://commons.wikimedia.org/wiki/User:Shisma) et ça donne vraiment ça*

**Pancakes vegan**

* 175 g de farine
* 20 g de fécule de maïs (Maïzena)
* 1 cuillère à soupe de sucre en poudre
* 1 cuillère à soupe de levure chimique
* 1 bonne pincée de sel
* 250 ml de lait végétal (soja pour moi)
* 2 cuillères à soupe d'huile de tournesol

Mélanger les ingrédients secs.
Creuser un puits et rajouter lait et huile puis mélanger au fouet.
La pâte doit être un peu épaisse. La faire une heure avant pour que ça prenne bien.
Huiler la poêle et la faire bien chauffer. Verser une petite louche ou une grosse cuillère de pâte et retourner quand il y a des bulles.

***La Bûche peut prendre bien des formes, qu'elle soit glacée ou patissière. Et pour sortir de la forme roulée habituelle, on peut faire la :***

**Bûche au pain d'épice**

* 12 à 13 tranches de pain d'épice
* 200g de chocolat noir
* 180 g de crème de soja ou coco
* 3 cS de noix de coco râpé
* 1 CS de rhum ou liqueur
* 350g de bananes
* du café froid
* 1 tablette de chocolat pour la ganache extérieure

Couvrir un moule à cake de film transparent.
Préparer le ganache à feu doux en faisant revenir au bain marie la crème et le chocolat puis ajouter le l'alcool et éventuellement de la noix de coco
tremper les tranches de pain d'épice dans le café
mettre trois tranche au fond, mettre de la ganache puis des bananes tranchées fines. laisser refroidir
et ainsi de suite jusqu'en haut du moule
couvrir du film transparent
le lendemain, démouler et mettre la ganache extérieure; rajouter de la poudre coco ou autre décoration.

![pain buche](https://media.paperblog.fr/i/381/3810212/buche-noel-pain-depices-sans-cuisson-vegan-sa-L-2.jpeg)

*Par un des [sites](https://www.paperblog.fr/3810212/buche-de-noel-au-pain-d-epices-sans-cuisson-vegan-sans-oeuf-ni-plv/) qui a recopié la recette de vegemag*

**Bon Appétit, joyeuses fêtes**




