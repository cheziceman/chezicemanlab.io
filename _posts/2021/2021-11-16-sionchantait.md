---
layout: post
title: Cinéma - Si on chantait de Fabrice Maruca (2021)
category: cinema
tags: film, cinéma, 2020s, comédie, musical
---

**Il y a parfois des films sans prétention, sans budget qui donnent le plaisir simple de se sentir bien quand on sort de la salle. Ce film en fait partie.**

Je dois dire qu'à la vision de la bande annonce, je n'attendais pas beaucoup. Une impression de "Full Monty" à la française, avec du social sur la région Nord, un peu comme "[Mine de rien](https://www.cheziceman.fr/2020/minederien/)". Car l'histoire est simple : Après la fermeture d'une usine, des anciens employés qui avaient formé une chorale, se regroupe pour créer une société...de livraison de Chansons. Vous savez, les billets chantés, pour un anniversaire, un message personnalisé, etc. Tout ça se passe à Quiévrechain, la ville d'origine du réalisateur, qui utilise même la maison natale.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/sionchantait.jpg)

Sauf que le réalisateur justement est sincère dans ce qu'il propose et le fait bien passer à l'écran. Il a su aussi prendre un bon casting, emmené par Jeremy Lopez (de la comédie française) que je ne connaissais pas. On y ajoute Alice Pol, la bonne copine idéale du cinéma français, Clovis Cornillac, le comédien le plus sous-employé du cinéma français, plus Artus qui se révèle bien plus ici qu'ailleurs, Chantal Neuwirth, toujours excellente et Annie Grégorio, second rôle éternel. On ajoute à cela la fine fleure de la variété française, de l'émotion au kitsch en passant par l'improbable. Le film a le bon goût de ne pas faire trop bien chanter les acteurs mais de donner envie de chanter.

Alors bien sûr, il y a du prévisible (réconciliation père-fils, amour-amitié, ...) mais ça passe bien dans la sympathie contagieuse de ce film. On ressort simplement avec le sourire, content de l'heure et demi passée. Pas besoin de vulgarité, d'artifice de mise en scène, d'effets spéciaux. C'est tout simple en apparence et ça marche, même chez un blasé comme je peux être parfois pour certains genres. Ok, j'aurais rêvé d'un "Coron" dans le stade de Valenciennes avec les supporters Lensois qui y étaient mais ça sera pour une autre fois. Le bonheur, c'est parfois tout simple en fait, avec des moments d'émotion, du rire, du ridicule, de l'amitié, de l'entraide. Au point d'en oublier tous les défauts, mais aussi ses petites tristesses.

[La Bande Annonce ![video](/images/youtube.png)](https://www.youtube.com/watch?v=mJEXE8bC7Mo){:target="_blank"}
