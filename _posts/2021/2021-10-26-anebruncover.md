---
layout: post
title: Musique - Ane brun - Leave me Breathless (2017)
category: musique
tags:  musique, 2010s, pop, folk
---

**Je ne suis pas particulièrement fan de tous les albums de reprises que l'on nous sert ces dernières années. Entre massacres et manque d'originalité, je fuis très souvent. Mais il y a parfois des moments de grâce**

J'étais donc passé à côté de celui-ci, même si c'est Ane Brun, artiste dont j'ai déjà parlé [ici](https://www.cheziceman.fr/2016/anebrun/). Mais comme j'apprécie beaucoup l'artiste, je vais régulièrement sur sa chaîne Youtube et je tombe sur une version live de "I Want To Know what love is", le titre de Foreigner. Moi qui tient Lou Gramm pour un des plus grands chanteurs de rock, il m'en faut pour me convaincre. Et j'ai été saisi par cette version qui n'a rien à voir avec l'original mais qui semble totalement nouvelle, avec la fragilité d'Ane Brun. Bon, vous patienterez bien jusqu'à la fin de l'article pour la voir?

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/anebruncover.jpg)

Donc me voilà à chercher d'autres reprises et à tomber sur cet album qui est disponible en plus sur Bandcamp. Allez hop, je craque après quelques écoutes. Là on sent que la sélection n'est pas l'œuvre d'un attaché marketing mais que c'est l'artiste qui a pris parmi sa playlist et ses affinités. Donc après cet intro monumentale, on retrouve une sacrée liste de titres tout en guitare-voix plus quelques discrets claviers et violoncelles. Je ne vous refais pas le couplet sur la comparaison avec Kate Bush, ou ce qu'a dit Peter Gabriel de cette chanteuse...

Alors "Always on my mind" vous fera penser à bien des personnes, peut-être plus que l'original de Brenda Lee, et que toutes les autres reprises (Willie Nelson, Pet Shop Boys, Johnny Cash...). Et j'en avais presque oublié l'"Unchained Melody", reprise par les Righteous Brothers ou bien Roy Orbison. Ane Brun a réussi l'exploit de m'en faire oublier les autres versions par la simplicité de sa proposition. C'est doux, plus lent peut-être, caressant. Le "Hero" de Maria Carey paraissait aussi difficile à oublier. Mais pas besoin de démonstration vocale comme la diva américaine pour ressentir une émotion. Le timbre si particulier de la magicienne norvégienne me suffit. Il y a des silences qui comptent aussi pour installer cette atmosphère. L'arrangement à la steel guitar y est aussi pour quelque chose. 

J'avais oublié le "Show me heaven", titre de Maria McKee popularisé par un film passable de Tom Cruise. Tina Arena en avait fait quelque chose mais là encore, c'est transfiguré. Un peu linéaire peut-être. Je ne connaissais pas "Into My Arms" de Nick Cave mais ça m'a fait écouter les deux versions. Et là encore il se passe quelque chose que je n'arrive pas à décrire tant ça me touche profondément et de manière très personnel. Pas besoin de me dire "Stay" car je reste...Les Shakespeare Sisters sont enterrées...quoi que la chanson est très différente avec ses légers chœurs en fond. La production est assurée par Ane Brun, son guitariste Johan Lindström et par Anton Sundell. 

Mon autre grand moment est "How to disappear Completely" de Radiohead. L'ambiance est installée par le piano en quelque note et par les aigus d'Ane Brun qui sont sur un fil tendu à l'extrême. On est pourtant bien là dans ces quelques minutes à se demander si ce qui est autour de nous est vrai. C'est un peu tout l'album qui est un moment suspendu. Évidemment, la reprise de Sade, "By your side" était presque attendue et moins surprenant car Sade c'est aussi des moments suspendus par une voix magique. Le côté folk d'Ane Brun s'exprime dans une reprise de Bob Dylan, "Girl from the North Country", hé hé... Et elle arrive à y mettre sa touche toute personnelle sur ces quelques notes de guitare et de steel guitar. Il y a aussi "Make you feel my love". Elle ne pouvait me faire plus plaisir en reprenant aussi Tom Petty et son "No Reason to cry". C'est la partie plus folk de l'album. Elle se poursuit par le "Right in time" de Lucinda Williams. C'est peut-être les titres où elle se détache moins des originaux, sans démériter pour autant. Et forcément avec un tel bagage, il fallait un titre de Joni Mitchell. L'arrangement est très différent de l'original, plus planant et éthéré, presque Gospel par l'ambiance.

Bref, vous l'aurez compris, j'adore et c'est donc un des rares albums de reprise de ma discothèque. Il faut une personnalité vocale, de bons arrangements et une excellente playlist pour réussir cet exploit. Cela m'a laissé "Breathless" à bien des moments et il continue à tourner en boucle depuis 2 mois au moins...

[Ane Brun en Live ![video](/images/youtube.png)](https://www.youtube.com/watch?v=dBL17pOdzs4){:target="_blank"}
