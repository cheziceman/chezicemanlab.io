---
layout: post
title: Dans le Rétro-gaming - Février 2025 
category: geek
tags: geek, jeu vidéo, retrogaming
---

**Deuxième mois déjà pour cette rubrique consacrée à tous les jeux anciens ou qui s'en réclament. Car on trouve à nouveau des jeux développés récemment sur d'anciennes plateformes. Entre les calculs nécessaires pour la 3D et le développement et le codage en 64 bits, on voit que revenir au source n'est pas si mal.**

### Lucky Luke sur GBC (1999)

Un des nombreux jeux sur cette licence commis par Infogrames. Pas un si mauvais [jeu de plateforme](https://www.uvlist.net/game-179411-Lucky+Luke) mais le level design pèche un peu face aux mastodontes du genre à l'époque. On retrouve pourtant notre héros et son cheval dans un décor de western avec des ennemis qui reviennent sans cesse, poussant à hater le pas et économiser les balles ou les batons de dynamite. La difficulté n'est pas négligeable mais c'est surtout à cause de la précision du jeu que ça ne fonctionne pas totalement, malgré des idées empruntées à un certain prince de perse. FRANCHOUILLARD

![luke sur gbc](https://s.uvlist.net/l/y2010/09/75189.jpg) 

### Coffee Crisis sur Megadrive (2017)

Oui, 2017 sur Megadrive avec une belle cartouche toute neuve. C'est un univers très indé pour ce [Beat'em up](https://www.uvlist.net/game-280350-Coffee+Crisis) délirant qui amène des tenanciers de café (au choix entre Madame et Monsieur) défendent la terre d'une invasion d'aliens et d'humains endoctrinés. On donne des coups de torchon à tout va pour buter ce petit monde, qui peut aller jusqu'à une paisible mamie en déambulateur. Un bel hommage aux classiques du genre gaché un peu par le manque de précision des coups, le peu de combos disponibles qui rend le jeu parfois difficile par rapport à ses ainés. Les graphismes sont originaux et sympa, comme la musique metal originale. ICONOCLASTE.

![crisedecafé sur md](https://s.uvlist.net/n/y2025/01/283590.jpg)

### Amagon sur NES (1988)

Ce [jeu de plateforme](https://www.uvlist.net/game-7535-Amagon) essentiellement cantonné au Japon, est inspiré par un certain plombier mais ça s'arrête là. Ce marine au design un peu niais n'a pas de capacités hors-normes. Il récolte des bonus en tirant sur des ennemis plutôt fantaisistes dans cette île du pacifique sud. Il doit passer des précipices en évitant de méchants oiseaux ... qui tirent des balles. L'intérêt du jeu est plutôt à voir dans sa difficulté et le design imprévisible des ennemis, plutôt drôle. La petite NES s'en sort plutôt bien sinon pour l'époque. Mais il n'y a pas la richesse du level design de son ainé moustachu apparu 3 ans plus tôt. CURIOSITE.

![amagon nes](https://s.uvlist.net/n/y2016/05/143527.jpg) 

### Buggy Run sur Master System (1993)

Un [jeu de course de buggy en isométrique](https://www.uvlist.net/game-9229-Buggy+Run), c'était encore courant dans ces années là. On y retrouve 4 voitures à l'écran dans un univers coloré et des pistes plus ou moins tortueuses avec des sauts. Le problème du jeu est que sa physique est inexistante malgré des options d'améliorations pour améliorer uniquement la vitesse et l'accélération. Pas de progressivité ou peu dans l'accélération, pas de glissades donc dès qu'on relache, la voiture reste plantée là. Dommage car graphiquement ça tenait la route. DECEPTION.

![buggyrun sms](https://s.uvlist.net/n/y2010/04/68118.jpg)

### Evasion sur Amstrad CPC (1987)

Un [jeu d'aventure textuel](https://www.uvlist.net/game-201164-Evasion) avec quelques images et en français, il y en avait quelques uns sur le CPC. Je n'ai jamais été très fan du genre, mais le coté Escape game de celui-ci est assez actuel, finalement. Une fois compris les rudiments du langage compréhensible par la machine, on récupère les objets et on comprend l'enchainement des tâches à réaliser et objets à trouver. L'auteur du jeu sera aussi celui de L'Ile. Il y avait deux disquettes pour celui là puisque deux aventures se suivant. ESCAPADE.

![evasion amstradcpc](https://s.uvlist.net/n/y2013/11/119484.jpg)

### Redline: F1 Racer sur SNES (1993)

Un [jeu de course à l'ancienne](https://www.uvlist.net/game-7029-Aguri+Suzuki+F+1+Super+Driving), qui arborait au Japon la figure du pilote Aguri Suzuki, première gloire du pays en F1. Et le jeu vaut mieux que le pilote, même si ça reste plutôt arcade. On trouve même des petites scènes en 3D comme intermède, annonçant la révolution à venir. Mais c'est bien du sprite et du clignotement qui gère la vitesse de manière convaincante. Les habituels modes championnat ou entraînement permettent de découvrir des circuits dont quelques fantaisies d'ailleurs pour rendre le jeu mondial. Il avait la licence FOCA pour les noms. Un bon choix pour de la F1 des années 90. Et le niveau reste relevé par manque de progressivité de la difficulté. PATRIOTE.

![agurisuzuki snes](https://s.uvlist.net/n/y2025/01/284603.jpg) 

### Pickies sur CAPCOM CPS (1994)

Si PuyoPuyo est la déclinaison SEGA d'un puzzle-game développé par Compile, Pickies est son [équivalent](https://www.uvlist.net/game-2521-Pnickies) sur la plateforme arcade CPS1 de Capcom. Le principe est totalement identique avec 2 boules de couleurs qui tombent et des appairages à faire...Au détail près qu'il faut avoir deux étoiles dans les boules de couleur pour déclencher la disparition. Ce petit détail rajoute de la stratégie dans les batailles en mode 2 joueurs. La vitesse devient rapidement injouable, démontrant que le jeu est surtout intéressant en mode Vs...Sauf qu'il y manque un mode aventure au profit du simple scoring. Il n'aura pas eu autant de développement chez Capcom, Compile ayant poursuivi avec le nom PuyoPuyo sur les consoles portables et de salon. ADDICTIF.

![pnickies sur capcomcps](https://s.uvlist.net/n/y2006/9/10316.jpg)

### Columns sur Arcade (1990)

La version [originale du Puzzle Game](https://www.uvlist.net/game-2937-Columns) qui fit les beaux jours de Sega sur toutes ses plateformes. Le principe reste un peu trop compliqué face à Tetris et Puyo Puyo, comme BlockOut...Il y a trop de combinaisons avec 3 gemmes à permuter, sachant qu'ensuite il faut les aligner soit en horizontal, en vertical, mais aussi en diagonale. Le challenge devient vite relevé et nécessite d'avoir de nouveaux réflexes. Là encore, le mode Vs. aurait pu permettre d'étendre la durée de vie du jeu mais il arrivera plus tard. Si la version Gamegear n'a pas sauvé la console face au Gameboy et son Tetris, c'est aussi à cause de cette difficulté et cette complexité (en plus des piles). HISTORIQUE.

![columns arcade](https://s.uvlist.net/n/y2005/6/389.jpg)

### Sega Tetris sur Arcade (1999)

Qui se souvient que le [célèbre Puzzle-game](https://www.uvlist.net/game-109086-Sega+Tetris) a eu aussi une déclinaison chez Sega ? Sur l'architecture Naomi en plus ce qui rend le truc un peu plus 3D. Mais bon, ça ne rend pas le jeu meilleur, au contraire. Cela donne une visibilité moins précise que l'original et le mode deux joueurs n'est pas plus innovant ou difficile. Donc ça reste du niveau d'autres clones et la licence du nom est une longue histoire qui a fait l'objet de documentaires. CLONE.

![segatetris naomi](https://s.uvlist.net/n/y2025/02/285631.jpg)

### Parasol Stars: The Story of Bubble Bobble III sur PC Engine (1991)

Grand fan de Bubble Bobble, amateur de Rainbow Island, je ne pouvais pas manquer le [troisième opus de la série](https://www.uvlist.net/game-10789-Parasol+Stars+The+Story+of+Bubble+Bobble+III) : Parasol Stars. Les niveaux ressemblent au premier et le design au second. Mais c'est un peu plus brouillon à force de trop vouloir en mettre à l'écran. C'est justement la simplicité du premier qui me plaisait, et l'innovation du second sur les arc-en-ciel à maîtriser. Là, le parasol n'est pas si bien utilisé que cela et on retrouve les bulles d'eau et autres timing de la mort. L'univers reste coloré à souhait et ça peut se jouer à deux. La PC Engine est bien adaptée pour ce type de jeu en plus. TRILOGIE.

![Parasolstars pcengine](https://s.uvlist.net/n/y2006/11/13810.jpg)

### V-Rally championship edition sur GameBoy color (1999)

Sur le petit GameBoy color, peu de [jeux de course](https://www.uvlist.net/game-163522-V+Rally+Championship+Edition) mais il fallait caser la nouvelle license V-Rally qui avait été connue sur la PlayStation. Pas de 3D évidemment mais de la 2D avec clignotement du décor pour l'effet de vitesse. C'est plutôt bien fait mais ça n'a rien à voir avec le Rallye, plutôt de la course de circuit. J'ai presque cru faire un WEC Le Mans, c'est dire (les voitures se retournent). Déjà du retrogaming pour l'époque donc mais ça a son petit charme. RETRO.

![vrally GBC](https://s.uvlist.net/l/y2007/07/39736.jpg)

### Goal! Goal! Goal! sur NeoGeo (1995)

Avant ISS Pro et Fifa, il y avait du [foot Arcade](https://www.uvlist.net/game-1805-GOAL+GOAL+GOAL) sur les consoles, à commencer par la Rolls, la NeoGeo. Et moi qui suit nul avec les jeux actuels (et au foot réel), j'ai pris du plaisir à celui là qui se limite à Passe-Tir-Interception-Tacle comme actions possibles et à trois boutons. De gros sprites sur un terrain bien vert avec des équipes de pays pas forcément dans les as du foot, c'est du foot japonais des années 90. J'adore mais c'est quand même pas si évident de trouver comment défendre malgré des stratégies pré-définies. FUN.

![goalgoalgoal neogeo](https://s.uvlist.net/n/y2006/04/17485.jpg)

### Nightmare in the dark sur NeoGeo (2000)

En fin de carrière, la console de SNK envoyait du paté...Et ce [jeu de plateforme](https://www.uvlist.net/game-105933-Nightmare+in+the+Dark) inspiré par Bubble Bobble et Ghouls'n ghost est visuellement beau. La mécanique est simple : Cramer les monstres avant qu'ils ne s'emparent de votre âme. Sauf qu'un coup d'allumette ne suffit pas, il faut y aller joyeusement avec ses sorts de feu contre des squelettes, des fantômes, des momies, etc. Chaque monde est en 5 niveaux dont le dernier est un énoooooorme boss. Autant vous dire que je suis content d'en avoir fait déjà trois. Une mécanique éprouvée pour un jeu arrivé trop tard pour avoir été adapté. Dommage car c'est jouable à deux en plus. HORRIFIQUE.

![nightmaredark neogeo](https://s.uvlist.net/n/y2006/7/12894.jpg)

### Days of Thunder sur C64 (1990)

Un mauvais film pouvait-il engendrer autre chose qu'un mauvais [jeu de course](https://www.uvlist.net/game-35914-Days+of+Thunder) ? Ça peut arriver mais là on partait de loin. Mindscape a fait le choix d'un jeu de course classique en 2D imitant vaguement la 3D pour les banking des pistes de Nascar en clignotant. Aucune physique, un décor pauvre, des couleurs tristes. Il n'y a pas grand chose à sauver sinon qu'il y a un peu de stratégie avec l'usure des pneus et de la carrosserie, les ravitaillements au stand. Quasi aucun élément du film dedans mais qui s'en plaindrait. ON TOURNE EN ROND.

![daysofthunder c64](https://s.uvlist.net/n/y2018/11/188416.jpg)

### Rampage Puzzle Attack sur GBA (2001)

Curieux choix d'adapter la licence Rampage sur la GBA dans un [puzzle game](https://www.uvlist.net/game-158108-Rampage+Puzzle+Attack). Pourtant le concept est pas inintéressant avec une ligne de couleurs à permuter pour faire disparaître des cubes en dessous. Dans les blocs avec un cristal on peut faire disparaître la totalité et créer des enchaînements. Un principe simple qui fonctionne dans le mode libre mais aussi dans un mode puzzle ou aventure. Graphisme un peu simpliste où l'on retrouve nos monstres du premier Rampage sans vraiment en voir l'intérêt. SYMPA.

![rampagepuzzle gba](https://s.uvlist.net/n/y2011/10/88800.jpg)

### Mr Driller: Drill Spirits sur DS (2004)

La DS a eu 2 versions de Mr Driller, le [classique de Namco](https://www.uvlist.net/game-153827-Mr+Driller+Drill+Spirits). Celui là est plutôt réussi avec toujours le principe de creuser plus profond en détruisant des zones de couleur, en évitant les gros rochers et sans se faire ensevelir par ce qui est au dessus. Le petit mineur qui ne s'appelle pas Jean emprunte à Osamu Tezuka son look et les couleurs sont pastels. Il faut aller vite, très vite, agir presque par réflexe en mode Pad et bouton ou en stylet (moins précis). Pas de révolution donc mais un portage plutôt FIDELE. 

![MrDriller NDS](https://s.uvlist.net/n/y2018/11/188719.jpg)

### Agony sur Amiga (1992)

Depuis le temps que je voulais y rejouer...J'étais tombé amoureux de ce [Shoot'em-up](https://www.uvlist.net/game-18210-Agony) à l'époque avec cette magnifique chouette effraie qui doit éviter et tuer des ennemis, hum, étonnants. Psygnosis a commencé à faire parler de lui avec ce jeu. Et je pensais qu'avec le temps ça ne ferait pas le même effet. Mais non, je trouve toujours ce jeu magnifique à tout point de vue avec son scrolling différentiel et son magnifique décor, ses sprites travaillés, sa musique grandiloquente. Bref, tout! UN MUST DU GENRE.

![agony amiga](https://s.uvlist.net/n/y2006/8/10663.jpg)

### DIRT 2 sur Nintendo DS (2009)

Dans la série des Colin McRae, les Dirts sont une déclinaison arcade et rallye avec toutes sortes d'engins. Je n'y avais jamais goûté sur la petite portable Nintendo et c'est plutôt bien fait. De bonnes sensations même si la physique reste basique. Des pistes sélectives mais une IA qui reste sur des rails. Un des rares [jeux de course](https://www.uvlist.net/game-184388-DiRT+2) disponible en dehors des Mario Kart like. PAS MAL. 

![dirt2 nds](https://s.uvlist.net/n/y2018/09/183866.jpg)

### Race Driver GRID sur Nintendo DS (2008)

Et on pouvait s'attendre à un aussi bon résultat avec la [version piste des licences](https://www.uvlist.net/game-172675-Race+Driver+GRID) de Codemasters. Pas de chance, c'est plutôt raté même si le moteur graphique est sympa sur cette petite console. GRID est plus orienté simulation d'habitude mais là on a affaire à des caisses à savon estampillées Aston Martin ou Ferrari dans des pistes inintéressantes et tournant vite au stock-car vu le manque de précision. D'autant plus dommage que le mode carrière présente de l'intérêt avec différents types de véhicules et circuits. RATE.

![racedrivergrid nds](https://s.uvlist.net/n/y2025/02/286159.jpg)

### Missile Command sur Atari 5200 (1983)

La simple [adaptation de ce classique](https://www.uvlist.net/game-20261-Missile+Command) de l'arcade 3 ans après sa sortie. J'avais déja traité de l'original et j'adore ce concept tout simple de défense par des missiles qui coupent la trajectoire des envahisseurs. Il faut gérer de plus en plus de bombardements et d'objets volants en déplaçant le petit viseur à l'écran à la manette. Mais même à la souris ce serait dur. Avec la manette de l'Atari 5200, nous sommes loin du gros stick directionnel. UN CLASSIQUE.

![missilecommand atari5200](https://s.uvlist.net/n/y2022/06/234196.jpg)

### OO7 Living daylights sur Atari 800 (1987)

Pour mon premier test sur Atari 8bits, j'ai pris la première rom qui se présentait, parce que les James Bond n'ont jamais été gâtés niveau jeu vidéo. C'est un [jeu de tir](https://www.uvlist.net/game-155770-The+Living+Daylights) plutôt original puisqu'il faut à la fois scroller avec le personnage mais aussi viser avec un pointeur pour descendre les ennemis. D'abord à l'entraînement puis en mission. Une sorte de Cabal avec un scrolling mais le décor ne se détruit pas. C'est plutôt sympa à jouer et assez difficile parce que justement il y a une mécanique à comprendre. Les ennemis sont assez couillons pour ne pas viser sur James Bond. 

![007livingdaylights atari800](https://s.uvlist.net/n/y2014/05/125796.jpg)

### Beamrider sur A5200 (1984)

Un [jeu de tir futuriste](https://www.uvlist.net/game-20215-Beamrider) donnant l'impression de 3D par des lignes sur lesquels doivent se trouver les ennemis pour pouvoir être détruits. Le principe est simple mais se complique rapidement avec des tirs ennemis, des navettes, des missiles, etc. A chaque niveau, on doit détruire tous les ennemis et il y a 99 niveaux. Un jeu de score et de progression donc qui est rapidement prenant, malgré la monotonie du décor. RARE.

![beamrider a5200](https://s.uvlist.net/n/y2021/02/206297.jpg)
