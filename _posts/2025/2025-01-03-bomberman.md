---
layout: post
title: Souvenir de Gamer - Bomberman (1983 - ??) 
category: geek
tags: geek, jeu vidéo, 1990s, 1980s, Japon 
---

**Si tu n'as jamais joué à Bomberman avant 50 ans, tu as raté ta vie ! Voilà ce que l'on pourrait dire car il n'y a pas une machine qui n'ait eu une version ou un clone de ce jeu multijoueur avant l'ère. Cela fit la fortune d'Hudson Soft...jusqu'à sa mort.**

Lorsqu'il sort sur la Famicom en 1985 et sur les ordinateurs de l'époque (MSX et autres particularités japonaises) en 1983, il n'est pas très sexy : Un labyrinthe un peu simpliste avec des blocs fixes et un personnage qui pose des bombes pour à la fois faire son chemin et tuer des monstres. Ça rappelle [Boulder Dash](https://www.cheziceman.fr/2015/boulderdash/) et d'autres jeux de l'époque. Mais le truc c'est que le gameplay est simple et immédiat. On peut acquérir le pouvoir de mettre plusieurs bombes à la fois, avoir un "souffle" de l'explosion plus destructeur, etc...J'y ai joué assez tôt en 1991 chez un ami qui avait un Amiga 500. Ma maladresse légendaire a fait de moi un piètre adversaire...Oui adversaire car l'une des forces du jeu c'est de permettre le multijoueur. A 4, c'est un gros délire avec évidemment des possibilités d'aliances et des techniques de défense ou d'attaque.

![Bomberman83](https://upload.wikimedia.org/wikipedia/en/f/f1/Bomberman_%28NES%29_gameplay.png)

Depuis peu, je m'y suis remis sur la légendaire version PC-Engine, la machine d'Hudson Soft et de Nec. Les réflexes sont peu à peu venus et ma maladresse aussi. Combien de fois me suis-je fait exploser moi-même. OK, par les temps qui court, un jeu où l'on pose des bombes, ça passe pas toujours bien chez la Team 1er degré. Et malgré l'aspect répétitif du challenge (les labyrinthes changent de couleur et de taille), on se prend au jeu. Le graphisme basique fonctionne toujours parce que ça part dans tous les sens. On apprend le comportement de chaque type de monstre et à la fin, on va sauver la princesse en détresse, ou un truc du genre. Le graphisme est kawai (mignon) à souhait et le personnage est presqu'aussi iconique qu'un Mario ou un Chocobo. 

Je n'ai pas rejoué en multijoueur car je suis trop nul pour ça. Les versions en ligne de toutes sortes existent. J'ai du jouer au moins à une version sur chaque console que j'ai eu en main, sauf en arcade. Mais je préfère cette première version NEC pour son coté authentique. Il y en a eu aussi en 1993, 1994, l'age d'or du jeu en quelque sorte. Les critiques de l'époque ne s'y sont pas trompé avec des notes à 90%. Ca devait être des parties endiablées dans les rédactions. Ah tiens, je vois que Tilt n'avait mis que 12/20. Pff, déjà le [déclin du canard](https://www.cheziceman.fr/2017/tiltmagazine/)...Et puis je n'ai pas pris la peine de jouer sur la version Switch non plus, vu qu'il y en a une sur la virtual console. Même après 40 ans, le jeu a encore des notes à 6 ou 7/10, c'est rare. Bref, c'est hautement re-jouable et on a toujours le choix des armes, euh de la console. En plus c'est un jeu de partage qui se joue aussi en famille.

[Un longplay sur la version NEC PC Engine![video](/images/youtube.png)](https://www.youtube.com/watch?v=-Ys6f5TI8nM){:target="_blank"}

***Sorti par NMK et Jaleco en 1983 et après sur Amiga, Android, Arcade, Atari Lynx, Atari ST, Commodore 64, Custom NEC PC, DOS, Dreamcast, Famicom Disk System, FM-7, Game Boy, Game Boy Color, Game Boy Advance, GameCube, i-mode, iOS, J2ME, Jaguar, Mega Drive, MSX, N-Gage, Neo-Geo AES, NES, PC-6001, Nintendo 3DS, Nintendo 64, Nintendo DS, DSiWare, Nintendo Switch, PC-88, PC-98, PC-Engine, PlayStation, PlayStation 2, PlayStation 3, PlayStation 4, PlayStation Portable, Saturn, Sharp MZ, Sharp X1, Super Nintendo, Téléphone mobile, Virtual Boy, Wii, WiiWare, Wii U, Windows, X68000, Xbox 360, Xbox One, ZX Spectrum***
