---
layout: post
title: Souvenir d'Auditeur - Les 2 minutes du Peuple (1990-2016)
category: geek
tags: geek, humour, podcast, québec
---

**Avant même que les podcasts d'humour deviennent à la mode, il y avait cette pastille humoristique venue tout droit du Québec...Et ça durait, heu, 2 minutes.**

A l'origine de tout ça, il y a Francois Pérusse, chanteur, acteur québecois qui s'amuse à triturer sa voix pour faire les différents personnages dans de courts sketchs survitaminés avec de bons jeux de mots. Forcément, les amis québecois sont à cheval sur la langue française. Enfin à cheval, plutôt sur un skidoo maintenant.

Vite devenus cultes dans son pays, ces capsules traversent l'Atlantique et sont diffusées sur Rire et Chanson ou Europe 2. C'est délicieusement absurde avec des calembours bien épais et assumés et c'est pour ça que ça fonctionne. Pérusse ose ! Il a d'ailleurs localisé son humour en insistant sur des travers très français parfois. Il parodie donc des émissions françaises de l'époque (ça nous rajeunit pas ...).

Et aujourd'hui c'est assez facile de retrouver ces moments sur le net puisqu'en dehors des albums de chaque année, il y a eu des enregistrements amateurs. Cela faisait des années que je n'en avais pas réécouté et ça n'a pas perdu de sa pertinence. Juste qu'il y en a eu peut-être trop et trop longtemps. D'ailleurs Pérusse a-t-il fini par se lasser lui même. Pendant la période Covid, il y en a eu de nouvelles mais c'est resté là. Imitées mais jamais égalées, c'est un beau morceau de l'histoire radiophonique et un podcast idéal à emporter partout pour une bonne petite dose d'humour. Ca se prend en capsule et sans modérations.

[Un exemple ![video](/images/youtube.png)](https://www.youtube.com/watch?v=tTGwBX0B0ow&list=PLnGaZWkydyfA8yC1i0RJN0iE7idaI2dv2){:target="_blank"}

Et si j'avais à choir une capsule en particulier, j'en serai bien incapable. Sur le lot, je vais bien trouver des choses qui ont du vieillir. Qu'importe...C'est avec de la nostalgie que je les réécoute un peu par moment. Il paraît que ça fait près de neuf heures à écouter bout à bout. Alors bon courage, ha ha...pour le 2Minutes-Challenge.
