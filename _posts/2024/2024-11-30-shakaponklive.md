---
layout: post
title: Musique - Shaka Ponk - Paris 2024
category: musique
tags: musique, live, concert, fusion, metal, electro, bercy, paris
---

**Le vieil adage qui dit que la France n'est pas rock est démenti par un ou deux groupes toutes les décennies. Shaka Ponk était là pour ça pour clore sa carrière de 22 ans !**

Nous étions à l'avant dernière date de cette tournée baptisée Final Fucked Up Tour. Un Bercy en configuration assez classique avec juste une petite scène au milieu de la fosse dont on devine l'utilisation. Le public est quand même autour de l'age du groupe maintenant, c'est à dire entre 30 et 60 ans avec aussi des enfants plutôt jeune dont les parents ont certainement voulu parfaire leur éducation musicale (Frah en rira aussi plus tard)...Mais pas seulement puisque des ados et de jeunes adultes traînaient aussi quelques parents réticents. Nous n'avions même pas la confirmation d'une première partie. Mais pile à l'heure prévue, ce sont les anglaises de **Nova Twins** qui déboulent. Quelle énergie pour ce Rock fusion d'une grande efficacité. Dommage que le son mette un peu de temps à se régler, comme souvent dans ce type d’évènement. Un batteur isolé à gauche et les deux "jumelles" défoncent tout sur le reste de la scène à coup de riffs cinglants. Il serait temps qu'un bon single les mettent en tête d'affiche, si j'en crois la collection de première partie prestigieuses et les commentaires tout aussi élogieux. En tout cas, très bon choix pour nous chauffer un peu après les frimas de la soirée.

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/shakaponklive1.jpg)

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/shakaponklive2.jpg)

Après environ 20 minutes d'entracte, qui voit-on débouler de l'autre coté des tribunes au fond de la salle ? Frah, Sam et CC avec quelques gardes du corps qui aident à fendre la foule. Ils mettent du temps à faire un tour sous les loges des happy few pour rejoindre cette fameuse avant-scène munie juste de quelques cubes pour s'y asseoir. CC empoigne une guitare acoustique pendant que Frah nous présente un peu le contexte, à savoir la dernière tournée, l'avant-dernier concert, l'émotion de voir son public, le million et demi de gens déjà...Mais aussi le contexte politique, écologique car Shaka Ponk est un groupe engagé, ce qui motive aussi cette fin de carrière dans la fleur de l'age. Et c'est la version acoustique de "Picky" qui entame le concert. 

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/shakaponklive3.jpg)

#### La Setlist

>**Acoustique**  
> I'm Picky (Unplugged)  
> Gung Ho  
> Run Run Run  

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/shakaponklive4.jpg)

>**Concert principal**  
> The House of the Rising Sun (The Animals)  
> Je m'avance  
> Wanna Get Free  
> Twisted Mind  
> J'aime pas les gens  
> Tout le monde danse  
> I'm Picky  
> Circle Pit  
> Smells Like Teen Spirit (Nirvana)  
> Sex Ball  
> Dad'Algorhythm  
> 13000 heures  

>**Rappel**  
> Rusty Fonky

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/shakaponklive5.jpg)

Le public répond rapidement présent avec ce début de set acoustique et se lève, même dans les gradins les plus lointains. Je n'avais vu ça que rarement à Bercy, notamment avec Maiden. Sam quitte l'avant scène alors que CC empoigne une guitare électrique pour une version endiablée de l'"House of the rising sun" des **Animals**, plus connue en France avec la très sage "Les portes du pénitencier" de qui vous savez. Frah et CC la rejoignent et on découvre vraiment la scène qui était restée dans l'ombre. La mascotte du groupe, Goz le gorille, ne tarde pas à faire son apparition dans l'immense écran qui va rythmer le concert. Il est dans une version robotique. A noter l'excellent travail sur les lumières qui magnifient la présence du groupe et oscillent entre le rouge, le bleu, le violet. Mais si la puissance du groupe s'exprime avec l'énergie habituelle de Frah et Sam, ce n'est pas tout. Il y a la surprise de découvrir des chœurs sur les deux cotés de l'écran : 20 choristes toutes de blanc vêtues avec de longues robes descendant jusqu'au sol dans un magnifique drapé du plus bel effet. Surtout qu'elles ne font pas que chanter mais ont aussi des chorégraphies à exécuter. On apprendra après que c'est le groupe **Sankofa Unit**. Frah enchaine les Stage Diving et Crowd Surfing avec délectation tandis que Sam assure au chant avec l'étendue que l'on connaît. Derrière, il y a aussi du lourd avec la basse de Mandris (qui manie aussi la contrebasse en acoustique), les claviers de Steve, la batterie de Ion. Du gros son qui fait mouche dans cette arène déjà surchauffée.

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/shakaponklive6.jpg)

Et puis Frah est bavard entre les chansons, parfois même un peu trop (pour préparer le Circle Pit et meubler pour qu'on prépare le prochain set...). C'est aussi pour ça qu'on l'aime. Il y a un invité spécial, l'astrophysicien écologiste **Aurélien Barreau** qui déclame un très beau texte sur des arpèges slidés par CC. Et puis il y a le militantisme clairement à gauche du groupe qui n'hésite pas à arborer des drapeaux palestiniens et surtout à parler longuement de Paul Watson. Un QRCode s'affichera sur l'écran pendant 2 chansons pour rediriger vers la pétition. Il y avait beaucoup de t-shirt et sweat-shirts Sea Shepherd dans la salle, d'ailleurs. Certains apprécieront peut-être moins l'apologie de la weed... Il faut bien amener ce circle pit tellement emblématique des concerts Metal, notamment au Hellfest. Pas d'incident ou de blessés (il a bien prévenu) ce soir là. Il fera aussi monter sur scène deux fans du premier rang puis d'autres fans sur l'avant scène pour du Stage Diving. Le rite d'initiation ultime. Ça, le groupe sait manier son public depuis 20 ans. Et si parfois quelques transitions tombent un peu à plat, ça retombe toujours sur ses pattes avec l'énergie déployée. Ce son electro-fusion unique est leur marque de fabrique avec la rudesse et la grandiloquence du metal, une efficacité mélodique pop, une énergie parfois hip-hop et un fond de son électronique, le tout enveloppé dans ces images de synthèse. On en prend plein les yeux avec des effets de profondeur, des singes qui descendent du plafond, des amplis qui explosent, etc... Ça évite d'utiliser la pyrotechnie et ce n'est pas moins efficace. Du gros show qu'il faut compenser écologiquement parlant et ça motive aussi le discours environnemental du groupe qui n'hésite pas à appeler à la désobéissance, au boycott des multinationales, etc... Les anti-wokes et fachos ne devaient pas être nombreux dans la salle de toute façon. Ils seront partis quand se déploieront des drapeaux LGBTQ. Bémol : La diffusion d'une vidéo qui mettait un peu tout dans le même sac, à savoir les vaccins covid, la guerre, le massacre de l'environnement, Gaza, ...ce qui s'ajoute à des messages pas toujours très clairs sur Instagram.

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/shakaponklive7.jpg)

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/shakaponklive8.jpg)

L'habituelle cover de Nirvana passe comme une lettre à la poste alors que l'on a allègrement dépassé la première heure de concert. C'est que nos vieux os commencent à se faire ressentir à rester debout, à sauter, crier, applaudir. On cherche les pauses mais il n'y en a finalement que peu, en dehors des quelques discours. Et puis il serait dommage de rater un effet visuel en restant assis. Frah promettait d'élever le ton, c'est bien le cas en cette fin de concert. Et même jusqu'au rappel, fait après une courte pause. Les choristes sont maintenant sur la scène et dansent autant qu'iels chantent. Le discours final est d'autant plus émouvant avec l'impression de quitter de vieux amis. Ils ont à peu près nos ages et ont marqué le rock français comme d'autres avant eux (je pense à FFF, Mass Hysteria, Gojira, Lofofora, par exemple). Leur médiatisation n'a été que tardive et leur aura a dépassé largement les frontières. Il y avait apparemment des fans japonais dans la salle. Nous étions tellement chaud en sortant que les 6 degrés ne nous ont même pas atteint. Et dire qu'il y aura des chanceux demain pour leur dire vraiment au revoir pour la dernière fois. Car comme tous les groupes qui font une tournée d'adieu, on espère qu'il y aura quand même quelques piges, un jour... Un groupe qui restera dans nos cœurs musicalement, et qui fera une projection cinéma le 3 avril prochain puisque tout cela était capté par des caméras.

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/shakaponklive9.jpg)

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/shakaponklive10.jpg)

[Le fameux Circle Pit](https://videos.pair2jeux.tube/w/2BK6j3ok6BneQQK2RwvffJ){:target="_blank"}
