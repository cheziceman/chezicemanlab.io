---
layout: post
title: Blog - Paul Watson, français ? 
category: reflexion, vegan
tags: protectionanimale,veganisme,réflexion,géopolitique*

---

**La mobilisation internationale continue après déjà 4 mois d'incarcération. Au point que le capitaine Watson pourrait devenir français.**

De reports en reports, la justice Danoise joue avec nos nerfs et surtout la santé de Paul Watson. Depuis le [premier article](https://www.cheziceman.fr/2024/paulwatson/) du blog sur le sujet, l'extradition vers le Japon est toujours l'épée de Damoclès au-dessus de sa tête. Mais les défenseurs de Paul Watson, et notamment l'antenne française de Sea Shepherd Origins essaient de trouver toutes les alternatives juridiques. Il y avait déjà la demande d'asile politique : Peu probable tant que le capitaine reste sur le territoire Danois. Mais comme il vivait en France avant son arrestation, il peut aussi demander la naturalisation française… Il y a eu des sportifs naturalisés pour moins que cela. Cela permettrait alors qu'il ait un autre statut en tant que citoyen français et de faciliter les négociations avec le Danemark.

D'où une pression à garder dans les médias et vis à vis des poltiques. La première pétition de soutien avait eu un grand succès. Lui succède celle concernant sa naturalisation : 
[Pétition en ligne](https://www.mesopinions.com/petition/nature-environnement/citoyen-francais-souhaitons-paul-watson-devienne/235460)

Le chanteur Florent Pagny a même participé à un titre pour participer à l'effort collectif, avec :  Alix, Sacha Bogdanoff, Ingrid Courrèges, Chloé, Chloésie, Arielle Dombasle, Elise D'avell, Patrizia Gattaceca, Cécile Hames, Lucile Jehel, Caroline Legrand, Delia Lucia, Francine Massiani, Claudia Meyer, Negrita, Nicoletta, Océane, Patsy, Chaï Roos, Véronique Sanson, Anne Sila, Stone, Fabienne Thibeault, Corinne Touzet, Zazie , Hugues Aufray, Laurent Baffie, Cali, Carré Blanc, Jacques Culioli, Tommy Chiche, Mario Dalba, Nico Davel, Christian Delagrange, Manu Pi Djob et ses gospels (environ 60 choristes ), François Feldman, Guizmo du groupe Tryo, David Hallyday, I surghjenti, Francis Lalanne, Manu Lanvin, David Libeskind, Raphaël Mezrahi, Jean Menconi, David Minster, Yannick Noah, Florent Pagny, Hugues Rambier, Roby the lord, Pierre Theunis, Serge Ubrette

[La vidéo](https://www.youtube.com/watch?v=heCOEHRAJCk)

A suivre… en espérant un dénouement positif, quand la mer continue d'être agressée par l'activité humaine.



