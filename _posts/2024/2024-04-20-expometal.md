---
layout: post
title: Musique - Exposition Metal à la Philarmonie de Paris (2024)
category: musique
tags: musique, métal, exposition, paris
---

**Comme il était prévu de faire des billets imprévus, c'est sur une exposition pas prévue que je chronique. Mais vous pouvez prévoir d'y aller jusqu'en Septembre...et je dirai même avant Juillet, histoire d'éviter l'affluence. Le thème : Metal, Diabolis in Musica.**

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/metalexpo6.jpg)

*Bienvenue en enfer !*

Le sous-titre a une importance car si l'exposition dans le cadre prestigieux de la Philharmonie de Paris (la conque métallique le long du périphérique) est là pour introduire la musique Metal au sens large, elle est aussi centrée sur le lien avec la religion, l'occultisme, le diable....dans la musique. Il ne faut pas être effrayé pourtant car il n'y a pas d'images pouvant choquer les jeunes âmes ou  même Tipper Gore, Christine Boutin etc. La preuve, il y avait des personnes de 60 ans comme des jeunes de 20 ans, et même de jeunes enfants avec leurs parents venus les initier au culte de .… la Les Paul ou de la BC Rich Warlock....cf ci-dessous. Personne n'a crié, vociféré ou n'a tenté de sacrifier quelqu'un ! Même pas l'ombre d'une canette de biere à l'entrée.

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/metalexpo5.jpg)

*Les armes des cavaliers de l'apocalypse ?*

Après le contrôle des sacs et des poches à l'entrée (encore à cause d'une religion paraît-il...) nous voilà dans une salle toute de noire vêtue, jusqu'aux rideaux qui tentent d'isoler comme ils peuvent des décibels environnants. Et d'entrée, nous voici à célébrer le triptyque historique : Led Zeppelin, Deep Purple, Black Sabbath. On se prosterne donc devant les saints Robert Plant, Jimmy Page, Jason Bonham, John Paul Jones, après s'être débarrassé du «Helter Skelter» des Beatles. Un peu d'historique donc, des vidéos de concerts de l'époque pour se plonger dans l'ambiance et des objets de collections internationales, réunis ici. Deep Purple a droit à une basse de Roger Glover, par exemple. Mais une grosse partie est consacrée à Black Sabbath et sa majesté Ozzy ainsi que Tony Iommi. On est loin des facéties télévisuelles qui l'ont remis dans les mémoires du grand public. Ici, on parle de «Paranoid» et de sa genèse ainsi que des inspirations des musiciens et auteurs. Après ces deux premières petites salles, nous voilà dans une plus grande enceinte, sorte de cathédrale dévolue aux différents genres. D'ailleurs, des vitraux se transforment parfois pour diffuser une vidéo d'un live avec le son tonitruant qui va avec. Les boucles sont suffisamment courtes pour pouvoir goûter à l'ensemble des styles. On passera alors d'un Scorpions dans un coin à un AC/DC dans un autre, ou … la mort qui vous parle, ha ha !

Après le début du métal, on parle Heavy-Metal, Hard-Rock, Thrash-Metal, Black-Metal, Nu-Metal, etc.… il y aura même un tableau qui tente de tout lister en toute fin de visite. C'est compliqué...Mais au moins l'exposition est simple à comprendre, si ce n'étaient les affichages explicatifs parfois placés trop bas et écrits trop petit, surtout dans cette ambiance sombre, pardon «Dark». D'autant qu'on n'a pas vraiment pensé aux malvoyants avec des zones tactiles. Guitares, tenues, set-lists, disques d'or, batteries, brouillons de texte, affiches de concert, tickets...tout y passe. Mais on voit aussi les inspirations des pochettes parmi les peintures classiques et religieuses. Détournement ou détourage, les artistes ont été habiles. Cela donne l'envie de les revoir de près...et d'entendre ce qu'il y a sur le 33 tours à l'intérieur. Forcément, on tombe à un moment sur un groupe ou un disque dont on est fan et tout revient en tête. Ou alors c'est comme un jeu de blind test lorsque l'on entend quelques notes d'un titre. Facile, ce sont des classiques maintenant. La visite n'est pas forcément facilitée par le labyrinthe de pièces. Une consacrée à la scène française, une aux guitar heroes, une aux fans et aux tenues, etc... Et même une sur le metal dans le monde, notamment en Afrique où l'on oublie trop les groupes qui tentent de se faire une place, dont certains sont féminins ! Là aussi, la testostérone est trop souvent mise en avant dans ce style pour que l'on rappelle aussi quelques figures féminines. 

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/metalexpo1.jpg)

*Le Diable est souvent dans les détails*

On est loin des clichés, justement pour essayer de montrer la richesse et la complexité de cette musique. Quelques manques évidemment, puisque pour ce sujet, on s'attendait à voir des objets liés à Venom mais on a du Celtic Frost ou du Bathory...ou même du Behemoth. Ou dans un registre plus calme, pourquoi pas Blue öyster Cult dont les thèmes étaient occultes ou ésotériques?  Comme le souligne madame, pourquoi Aerosmith à travers un objet, dans cette cathédrale, sinon peut-être pour les groupies se prosternant devant Saint Steven ? Pourquoi pas d'Iron Maiden ou si peu ? Parce que pas assez occulte, peut-être... Pour les plus contemporains et mainstream, pas de Ghost non plus, groupe qui n'a rien inventé non plus, il est vrai mais qui utilise allègrement l'iconographie du diable et de la religion. Un peu pop sans doute? Bref, l'exposition évite les nombreuses guerres de chapelles et vu l'espace dédié, des choix devaient être faits, certainement aussi avec la disponibilité des objets chez les sponsors. On doit cela à Jean-Pierre Sabouret, Christian Lamet, respectivement rédacteur en chef de Best, Hard Rock Magazine (qui a réussi à caser ses groupes préférés), et rédacteur en chef d'Hard Force (une surprise vous attend dans un coin...), ainsi qu'à deux commissaires d'exposition dont l'anthropologue et sociologue Corentin Charbonnier, spécialiste du Metal et auteur d'une thèse sur le ...Hellfest. Le sponsoring a donc bien aidé et un peu orienté.

 Le catalogue de l'exposition permet d'aller plus loin pour ceux qui découvrent. J'ai pris plaisir à voir ou revoir ces objets, ces jolies guitares de Steve Vai, Joe Satriani, Slash, Tony Iommi , pour ne citer que ceux là, avec toute la patine et le vieillissement des plastiques. On peut même se plonger dans un festival comme si on y était, pour ceux qui n'osent pas aller dans «La Fosse» / le «Snake pit» ou les premiers rangs. On n'a quand même pas tenter de «pogoter» avec les gardiens autour. Cela donne envie de tout réécouter...et j'imagine que pour quelqu'un qui connaît moins, de découvrir pas à pas en suivant ou pas la chronologie. Je ne vais pas mettre de vidéo (des photos, c'est déjà pas mal) pour autant car ce serait trop en montrer....Ce n'est déjà pas si long et certains tiqueront sur le prix. Le lieu est prestigieux et nous avons pu entrevoir d'autres merveilleux instruments historiques ou lointains qui sont visibles dans d'autre expositions, payantes ou gratuites. 

 ![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/metalexpo4.jpg)

 *A qui est cette veste à patch ?*

Comptez 1 bonne heure pour voir tout en prenant un peu de temps parfois pour les vidéos de concert. L'accès est aussi simple par le métro qu'en voiture (parking sous la philarmonie juste à la sortie du périphérique). Et c'est jusqu'au 29 septembre 2024 avec [pleins de tarifs](https://www.exposition-paris.info/expo-paris-metal-diabolus-in-musica-philharmonie-musee-de-la-musique/#infos) réduits mais pas pour tout le monde.




