---
layout: post
title: Cinéma - Perfect Days de Wim Wenders (2024)
category: cinema
tags: cinéma, film, 2020s, japon
---

**Qui parierait sur un film autour d'un employé des toilettes de Tokyo ? Pas grand monde, à moins que l'on précise que c'est Wim Wenders derrière la caméra. Et me voilà à rempiler pour une nouvelle chronique cinéma.**

Si je devais résumer le film, je dirais simplement : 

*Un matin à Tokyo*  
*Une journée d'Hirayama*  
*Sourire chez moi*  

Oui, le haïku a été souvent cité pour dépeindre le film mais ce n'en est pas un. Si c'est une belle leçon de cinéma de la part de Wim Wenders, pendant ces 2h, ce n'est aucunement pesant ou rébarbatif. A ceux qui font des circonvolutions temporelles et des effets de styles, le cinéaste allemand presque international répond par la simplicité. Un sujet qui peut paraître banal, un lieu filmé des milliards de fois, et pourtant cela fonctionne car tout est parfaitement maîtrisé à l'image de ce que réalise chaque jour notre héros, Hirayama (Kōji Yakusho).

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/perfectdays.jpg)

Il faut déjà parler de l’interprète principal, qui a une immense carrière derrière lui sans pourtant de prix, à part l'interprétation à Cannes (et un peu plus depuis). Le rôle d'Hirayama est celui d'un taiseux et cette économie de mot est compensée par la profondeur du regard de cet acteur, son charisme et son magnétisme, son sourire. Face à lui, il y a des rencontres, brèves et pourtant puissantes par l'effet qu'elles produisent sur le spectateur. Ils sont tous magnifiés par la mise en scène de Wenders qui soigne chacun de ses plans comme son héros soigne chacun de ses arbres ou des ses autres compagnons de vie. Il s'agissait au départ d'un travail de commande et on pouvait craindre une vision touristique de Tokyo mais il sort des sentiers battus, nous emmènent dans un Tokyo plus secret et tout ça au volant de la petite Daihatsu Move ou avec sa bicyclette, la compagne de notre héros. Wenders réussit tout de même à faire de cette ville parfois froide et mécanique à l'écran, un écrin de douceurs que nous fait découvrir Hirayama par ... les toilettes.

L'autre élément important est la musique, avec des classiques des années 70 et 80, dont un de Lou Reed, presque la guest star du film. Une musique qui sied évidemment à ce héros qui semble flotter hors du temps dans son petit appartement qui n'accueille que ses nuits et ses bonzaïs. La lumière est elle aussi parfaitement gérée par Franz Lustig, pourtant assez récent dans le métier. Il en ressort une poésie qui, malgré les répétitions, captive le spectateur. Cela n'est pas comme «Le jour sans fin» de Ramis mais plutôt une sorte de contemplation délicate qui peut rappeler parfois Ozu, avec tout de même plus de rythme et de mouvement. Avec une économie de moyen, Wenders parvient à faire rire et sourire, aidé par son magnifique acteur.

Je préfère, à l'image du héros, restez taiseux et ne pas partir dans des interprétations de tous les éléments d'un film qui laisse aussi quelques mystères quant à la place de certains éléments. Comme le titre l'indique, recherche-t-on justement un jour parfait dans cette succession de moments bien réglés ou est-ce dans les petits chaos que se cachent nos bonheurs ? Je ne peux m'empêcher de regarder l'affiche ci-dessus qui nous aspire dans cette quiétude. Un horizon qui paraît infini, un intérieur dépouillé avec juste musique et livres dans le silence de la nuit. Et c'est dans le silence d'une salle obscure que le spectateur peut trouver sa séance parfaite.

[Une Bande-annonce![video](/images/youtube.png)](https://www.youtube.com/watch?v=kLYFBhcwYj4){:target="_blank"}
