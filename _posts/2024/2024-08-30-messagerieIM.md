---
layout: post
title: Tuto - Choisir une messagerie instantanée multimédia
category: geek, tuto
tags: geek, tutoriel, test, messagerie instantanée
---

**Je recherchais une messagerie instantanée efficace, libre, légère, sécurisée...Et à l'heure du choix, j'ai eu des surprises.**

J'ai commencé par regarder les fonctionnalités. Mon besoin est assez classique et je me fiche un peu de pouvoir avoir des tonnes de gif animés ou d'emojis/smileys : 
- Messages textes
- Appels audios et vidéos
- Envoi de fichiers
- Chiffrement de bout en bout
- Si possible open source ou mieux encore en logiciel libre
- Une ergonomie correcte

J'ai aussi regardé l'usage sur Android en terme de stockage, de mémoire, de consommation de batterie après des éliminations qui ont laissé deux applications. Je ne m'attendais vraiment pas à ce résultat mais je garde à l'esprit que c'est en ce moment et qu'une version future peut tout changer.

J’ai finalement (après des remarques sur la première version de cet article) gardé le vieux protocole XMPP/Jabber, dont les performances en audio et vidéo dépendent des clients (ici aTalk) et des serveurs, mais éliminé Telegram. Mais on m’a demandé quand même d’y inclure un des clients les plus intéressants : Quicksy. Pour le réseau Matrix qui succède à XMPP dans l’esprit, je suis retourné voir Element. J’ai sélectionné aussi des réseaux propriétaires comme Viber, Lime, Whatsapp, Skype. J’ai aussi sélectionné Signal et Wire, applications open source recommandées par qui vous savez. Au moment de l’installation j’ai trouvé que Viber (racheté par Rakuten) était bien trop gourmande en taille d’appli et j’imagine ensuite en taille de stockage. Lime manquait de fonctionnalités. Donc exit Viber et Lime. Aucune des applications citées ne peut normalement être utilisée en Chine. Il reste la solution de passer par des VPN et dans les faits, il y a des fois où Whatsapp et Skype fonctionnent. Je n’ai évidemment pas testé QQ ou Wechat dans leur version récente. L’une est exclusivement en chinois et non disponible sur le play store, tandis que l’autre fonctionne assez mal ici (sans parler de l’espionnage du gouvernement chinois). Au final, j’ai donc 4 applications «libres» contre deux applications «propriétaires»

### A l'installation

Aucun problème d'installation à partir du playstore (voir de F-Droid pour Signal, Wire et Element). Il reste ensuite à créer ou retrouver son compte avec des procédures de vérifications différentes. Pour Element, il y a une clé de chiffrement qui rappelle PGP et le mail/nom d'utilisateur. Pour Skype, ça passe par le compte Microsoft/Windows....beurk. Pour Wire, on crée un compte lié au mail. Pour Whastapp, Signal, c'est le numéro de téléphone. Surprise ensuite sur la taille à l'installation : 

1- Quicksy 27Mo téléchargés, 29Mo installés  
2- Whatsapp 48Mo téléchargés, 109Mo installés  
3- Signal 41Mo téléchargés, 142Mo installés  
4- Wire 46Mo téléchargés, 146Mo installés  
5- aTalk 64 Mo téléchargés, 110 installés  
6- Skype 71Mo téléchargés, 160Mo installés  
7- Element 46Mo téléchargés, 204Mo installés  

Premier éliminé, Element, dont l'ergonomie est aussi discutable en desktop PC.

### Utilisation mémoire

Le protocole est simple : Lancer l'application, la laisser 1 minute en tache de fond puis un Kill qui affiche la mémoire libérée. On prend la moyenne de 3 essais.

1- Quicksy …rien mais il n’y avait rien à afficher  
2- Whatsapp 27Mo  
2- Wire 27Mo  
4- Signal 45Mo  
5- aTalk 64Mo  
6- Skype 107Mo  
(Element était à 69Mo)  

Deuxième éliminé, Skype, dont la version «Lite» destinée au marché indien ne fonctionne plus sur les dernières versions d’Android. On se demande dans quoi passe toute cette mémoire… our Quicksy, vu qu’on n’y trouve aucun contact et qu’on ne peut s’auto-tester, c’est compliqué de faire un bilan. Quelques soucis d’affichage aussi sur le smartphone pour les menus de création de contact.

### Temps de démarrage

C'est important que l'application démarre «instantanément»...et là, surprise, cela a été clairement oublié au cahier des charges d'une des applications.

1- Quicksy 1s  
2- Whatsapp 2s  
2- Signal 2s  
4- aTalk 4s  
5- Wire 20s  
(Skype était à 6s et Element à 4s)  

Troisième éliminé, Wire qui a un gros problème dans le temps de réponse ce qui est très dommage. Il y a également un gros manque d'utilisateurs qui fait qu'on aura du mal à convaincre des connaissances à venir dessus.

### Utilisation prolongée et usage de la batterie

J'avais en mémoire des problèmes d'utilisation de la batterie avec Whatsapp à une époque et je m'attendais donc au pire. J'ai laissé les applications fonctionner en tache de fond et fait les memes actions (conversation, envoi d'un fichier...) sur le même temps. J'ai regardé ensuite ce qu'indiquait le système en consommation de batterie.

1- Quicksy moins de 0,5%…mais pas d’activité à gérer  
2- Whatsapp 1,3% pour 8 minutes  
3- Signal 2% pour 8 minutes  
4- aTalk 7% pour 8 minutes 

Mais pour ces utilisations prolongées, la mémoire utilisée a été différente

1- Quicksy 4Mo libérés  
2- aTalk 70Mo libérés  
3- Signal 86Mo libérés  
4- Whatsapp 125Mo libérés   

Donc match nul ou presque. 

### Au sujet de XMPP

Si Quicksy a tout pour lui sur le papier, il reste austère, avec des soucis ergonomiques et surtout impossible de trouver le moindre contact en dehors de moi même sur un autre serveur XMPP. aTalk s’en sort un peu mieux pour l’ergonomie mais l’interface rappelle un Android 1.6. J’ai testé aussi conversations en deux versions et ça fonctionne…presque. Pour performances et fonctions, ça va dépendre du serveur et du client en face pour la réussite d’échanges, le temps de réponse… (de 1s à 3 minutes dans mes tests). Le problème vient plus souvent de ce qu’il y a en terme de client sur PC que sur mobile, aujourd’hui car entre un pidgin qui oublie le chiffrement en standard ou un gajim qui oublie l’audio/video, c’est compliqué de trouver un compromis. Bref, convaincre des utilisateurs non «vieux geek» d’aller sur XMPP sera plus compliqué malgré un produit qui est plutôt simple d’accès pour ce fork de Conversations ou cette évolution lente d’une vieille application. A réserver à des spécialistes ou avec un bon accompagnement dans une sphère limitée. Je garde quand même un oeil sur l’évolution des clients de ce protocole car c’est potentiellement le plus adapté pour des configurations légères.

### Conclusion

Whatsapp, c’est Meta donc Facebook, Insta, etc….Donc le mal. C’est propriétaire mais il faut avouer qu’ils ont bien travaillé pour que l’application fonctionne mieux. Le test a été fait sur une version Android 11Go et peut donc varier selon le téléphone. Signal, c’est libre, plus respectueux des données, etc…. Le problème qui fera pencher la balance, c’est qu’il y a plus d’utilisateurs chez Whatsapp que chez Signal, et je ne parle pas de Quicksy ou aTalk, vu précédemment. Donc aujourd’hui, je conseille Signal pour la globalité mais je peux mieux concevoir qu’on utilise Whatsapp. Signal a pour lui le meilleur des deux mondes mais il sera difficile de faire changer tous vos contacts, de toute manière. Il manque assez peu de chose à Wire pour venir troubler ce duo. Skype est maintenant has been et Microsoft mise sur l’horrible Teams. Je n’ai toujours pas compris la politique de Google sur ce marché avec un Messages trop limité, un Hangout abandonné et un Chat qui reste en parallèle d’un Meet qui a remplacé Google Duo il y a 3 ans. Quand ils seront décidés, ils pourront toujours venir me voir, hé hé. Pour l’ergonomie de Signal ou Whatsapp, c’est un faux problème car je trouve des problèmes dans les deux. Les habitudes sont faites pour changer parfois. En a-t-on vraiment besoin ? A chacun de voir, mais n’aimant pas montrer ma trombine à tout crin, c’est parfois pratique pour ne pas payer le téléphone avec des contacts lointains.