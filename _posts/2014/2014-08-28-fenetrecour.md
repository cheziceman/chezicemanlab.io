---
layout: post
title: Cinéma - Fenêtre sur cour d'Alfred Hitchcock (1954)
category: cinema
tags: cinéma, film, 1950s, thriller, comédie dramatique
---

**Même après près d'une dizaine de visionnage, je ne me suis jamais lassé de ce film de l'oncle Alfred. Et pourtant, il s'en dégage tellement de sentiments malsains.**

Nous sommes à Greenwich Village, New York dans l'appartement de L.B. «Jeff» Jefferies, photographe de son état, mais qui se retrouve immobilisé dans un fauteuil après un accident. Ses seules visites sont celles de son infirmière, et sa petite amie, la très mondaine Lisa Fremont. Et sa seule distraction devient vite l'observation de son voisinage par la fenêtre. Une danseuse, un jeune couple, une femme seule, un musicien sans le sou, une vieille artiste, un vieux couple...Et puis un couple qui l'intrigue : Un représentant de commerce et une femme alitée par sa maladie, dont les disputes sembles fréquentes. Tout bascule une nuit où cet homme sort plusieurs fois, puis la femme disparaît. 

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/fenetrecour.jpg)

Hitchcok est un maître pour manipuler le spectateur. Il nous met à la place de Jeff mais nous donne quelques indices en plus...fausse piste ou pas ? Nous sommes aussi bien les voyeurs comme Jeff, dont c'est un peu le métier d'être observateur, et les juges. Doit-on le croire, alors qu'il parvient peu à peu à convaincre son entourage ? Avec tous les éléments donnés, on peut échafauder une histoire, tout comme on se surprend à en écrire d'autres pour tous les voisins présents. La tension s'installe et monte crescendo quand Lisa l'aide et va voir chez ce voisin, Thornwald. Evidemment, je ne vais pas dire s'il est coupable ou pas, s'il y a un twist... Mais même en connaissant le pot au roses, je prends toujours autant de plaisir à revoir cette comédie de meurtre. En effet, ce qui est aussi savoureux que l'intrigue, c'est le ton de la comédie entre Grace Kelly (Miss Fremont) et James Stewart (Jeff), malgré les 21 ans de différence entre les acteurs. Cela ne dérange évidemment pas Hitch qui trouve d'un coté son égérie, sa femme de cinéma idéale, et de l'autre un maître de la comédie. On trouverait presque du Capra dans cet Hitchcock.

Et puis je parlais d'éléments malsains. Outre la méthode supposée du meurtre, il y a l'élément central du voyeurisme. Nous sommes, comme spectateurs, des voyeurs de situations et de personnages en regardant des films, bien plus encore qu'un lecteur. Bien sûr, c'est une fiction le plus souvent mais avec on peut s'interroger sur la fascination que nous procure l'image. Et puis qui n'a pas regardé ce qu'il se passait chez les autres, sans tomber dans l'excès de ce héros ? Nous aimons, nous humains, les potins, observer nos semblables. Bien des années après, il y aura ce même débat lors des premières télé-réalités, ou bien encore dans des films comme Truman Show. Hitchcock nous embarque dans cette sorte de perversité et nous rend aussi curieux que ces héros. Si Jeff avait l'excuse de la déformation professionnelle, en convaincant d'autres personnages de sa théorie, il nous inclut. 

Techniquement, le décor peut apparaître parfois artificiel et quelques effets spéciaux (l'hélicoptère...) sont datés. Et pourtant, cela fonctionne car cela ressemble véritablement à ce petit village dans Manhattan. Pour une fois, ce n'est pas Bernard Herrmann qui se charge de la partition mais Franz Waxman. On n'y perd pas au change finalement, même s'il n'abuse pas des effets sonores comme dans d'autres films. Il utilise plutôt la caméra subjective avec des mouvements assez inédits pour l'époque, ou des caches rappelant le téléobjectif ou les jumelles. Avec une unité du lieu et presque de temps, Hitchcock explore toujours ce type de mise en scène minimaliste en apparence (The Rope, Lifeboat...). Pas de site touristique à cette époque, mais ses traditionnelles apparitions, si on est attentif.  Un chef d'oeuvre à bien des niveaux et qui reste intemporel, aujourd'hui.

*Ce film fait partie du challenge IMDB Top250*

[Une Bande-annonce![video](/images/youtube.png)](https://www.youtube.com/watch?v=m01YktiEZCw){:target="_blank"}