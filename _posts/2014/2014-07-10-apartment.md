---
layout: post
title: Cinéma - La Garçonnière de Billy Wilder (1960)
category: cinema
tags: cinéma, film, comédie dramatique, 1960s,
---

**Décidément, je révise mon petit Billy Wilder illustré. Cette fois c'est une comédie dramatique avec Jack Lemmon...dont je préfère le titre US : The Apartment.**

Oui, une garçonnière est un appartement pour un homme célibataire mais cela est souvent vu péjorativement comme un «baisodrome», un lieu où un homme amène des conquêtes. Ce n'est pas tout à fait le cas ici : CC Baxter, dit Buddy (Jack Lemmon) vit seul dans un appartement au milieu de New-York. Il est employé dans une assurance mais pour se faire bien voir par ses chefs, il lui arrive de leur prêter son appartement pour leurs escapades adultères. Les voisins se plaignent peu à peu des bruits qui proviennent de l'appartement. Sauf qu'un, jour, Buddy tombe amoureux d'une liftière (une employée qui commande l'ascenceur quand ceux-ci n'étaient pas automatiques) de la compagnie, Fran (Shirley McLane) qui se trouve aussi courtisée par le directeur du personnel, client de l'appartement de Buddy…

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/apartment.jpg)

On est presque dans du Vaudeville avec un tel scénario et Jack Lemmon est parfait dans ce rôle de gentil garçon servile et effacé. D'ailleurs Billy Wilder avait envisagé ce scénario avec I.A.L. Diamond comme une pièce de théâtre. La légende dit que c'est une idée de Tony Curtis basé sur du vécu...sauf que lui empruntait les appartements des autres. Une pure comédie de mœurs qui va pourtant un peu plus loin. Le spectateur se prend forcément d'affection pour ce gentil garçon mais ne désire qu'une seule chose : Qu'il dise non, qu'il prenne sa vie et son destin en main. C'est aussi un désir que l'humain a souvent dans sa propre vie donc cela facilite l'identification. Après, c'est le talent des acteurs et de la mise en scène qui fait le reste. Et dans ce registre de comédie, Wilder est un maître. Maître aussi dans la critique acide de la société US...puisqu'il est allemand, ne l'oublions pas.

Si le cinéma français a mis du temps à distinguer les bonnes comédies, il n'en est rien du cinéma US qui récompensa ce film...et peut-être Wilder à retardement. On reste pourtant sur une recette classique avec un triangle amoureux, un lieu où l'on se croise, des quiproquos, des dialogues savoureux. Et c'est un peu ce qui peut me décevoir par rapport à d'autres pointures du genre. Malgré l'interprétation remarquable, je préfère les comédies de Cukor (Indiscrétions par exemple) ou Hawks (Les hommes préfèrent les blondes, l'impossible Mr Bébé), ou d'autres de Wilder...la précédente par exemple : Certains l'aiment chaud, déjà avec Jack Lemmon. Shirley McLane sera aussi plus tard d'Irma la douce avec le même partenaire. Mais Wilder avait sa préférence pour ce film. Cela reste une des meilleures comédies du genre par sa mise en scène réservant des surprises au spectateur mais je trouve le développement trop long avec une perte de rythme en milieu de film. A voir en V.O. évidemment car comme la traduction du titre, les dialogues pâtissent de certaines tournures. 

*Ce film fait partie du challenge IMDB Top250*

[Une Bande-annonce![video](/images/youtube.png)](https://www.youtube.com/watch?v=OcslkrBMLGc){:target="_blank"}