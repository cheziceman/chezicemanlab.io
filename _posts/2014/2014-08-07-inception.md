---
layout: post
title: Cinéma - Inception de Christopher Nolan (2010)
category: cinema
tags: cinéma, film, science-fiction, 2010s, thriller
---

**Mis souvent au sommet des films à voir, il me paraît surcoté comme toute l’œuvre de Christopher Nolan. Mais ce thriller de Science-fiction reste aussi bien dans son époque.**

L'histoire a tout pour me plaire : Dom Cobb (Leonardo Di Caprio) est un voleur expérimenté d'un certain genre : sa spécialité consiste à s’approprier les secrets les plus précieux d’un individu, enfouis au plus profond de son subconscient. Très recherché pour ses talents dans l’univers trouble de l’espionnage industriel, Cobb est aussi devenu un fugitif traqué dans le monde entier qui a perdu tout ce qui lui est cher. Mais une ultime mission pourrait lui permettre de retrouver sa vie d’avant :  Au lieu de subtiliser un rêve, Cobb et son équipe doivent faire l’inverse : implanter une idée dans l’esprit d’un individu, l'inception. Voilà qui rappelle du Philip K.Dick (je vous laisse trouver les films inspirés par...), et quelques films de SF comme le très bon Dark City (Proyas en 1998). Mais Nolan y développe son petit dada sur le temps et les paradoxes, le rêve et la réalité. Évidemment, on a aussi la machination qui semble plus forte que notre héros.

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/inception.jpg "Image")

Techniquement, il n'y a rien à dire, c'est du grand art. Nolan maîtrise les effets spéciaux pour donner au spectateur ce qu'il veut : De l'action et du grand spectacle. Et pourtant, au bout d'un moment, je m'ennuie devant ces scènes d'actions qui tirent en longueur et sont plus là pour masquer les lacunes du scénario. Il est aisé de faire se croiser les réalités pour perdre le spectateur mais il y a de grosses incohérences quand on prend le temps de se poser devant ce film. Ce n'est évidemment pas le cas quand on le voit la première fois, quand on est happé par l'action ou la splendeur des effets spéciaux (surtout la première partie). Les défauts seront plus criants pour Tenet, quelques années plus tard.Mais je ne veux pas gâcher le film pour ceux qui voudraient le découvrir. Si je n'ai pas retrouvé l'ambiance d'un vrai film de SF mais plutôt celle d'un banal film d'action plus ambitieux que la moyenne, c'est sans doute parce que la volonté initiale du film ne se retrouve pas ensuite, bloquée par les doutes de notre héros. Et je ne m'y retrouve pas non plus dans ce récit là non plus.

Dans la pléthore de blockbusters de Hollywood, Nolan sort du lot, incontestablement par son style et ses thèmes. C'est sans doute cela qui lui donne un côte dans ce top 250. Mais ce n'est qu'avec le temps si cher au réalisateur que l'on verra si c'est une réalité, ou si ce n'était qu'un rêve.

*Ce film fait partie du challenge IMDB Top250*

[Une Bande-annonce![video](/images/youtube.png)](https://www.youtube.com/watch?v=YoHD9XEInc0){:target="_blank"}