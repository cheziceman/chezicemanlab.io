---
layout: post
title: Cinéma - Mademoiselle de Park Chan-Wook (2016)
category: cinema
tags: cinéma, film, corée, japon, 2010s, drame, thriller, cinémathèque idéale, érotisme
---

**Il fut l'un des films de l'année 2016 et je ne sais pas pourquoi je ne l'ai pas vu à ce moment. Une odeur de souffre, peut-être ?**

Plus probablement un problème de distribution dans mon coin car en parlant de l'histoire tumultueuse entre le Japon et la Corée, en parlant de condition féminine, tout ça enveloppé dans un thriller, le film avait tout pour me plaire. Le titre français est une traduction littérale, quand les anglais préfèrent «Handmaiden» (servante). Nous sommes dans l'occupation japonaise de la Corée. «Une jeune femme, Sook-hee (Kim Tae-Ri), est engagée comme servante d’une riche japonaise ,Hideko (Kim Min-Hee), vivant recluse dans un immense manoir sous la coupe d’un oncle (Jo Jin-Woong) tyrannique. Mais Sook-hee a un secret. Avec l’aide d’un escroc, «Fujiwara» (Ha Jeong-Woo), se faisant passer pour un comte japonais, ils ont d’autres plans pour Hideko…» 

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/mademoiselle.jpg)

L'Affiche laisse penser à une certaine sophistication visuelle. Il est vrai que nous sommes dans la haute société japonaise de l'époque qui se plaît à garder des traditions. C'est un film effectivement magnifique quasiment à chaque plan, entre la lumière, les couleurs, les aspects graphiques, un des musts du genre, gâce à Chung Chung-Hoon le directeur phot attitré de Park Chan-Wook. Mais ça ne fait pas un film pour autant, il faut une histoire. Le film étant divisé en plusieurs actes, il prend le risque d'être inégal. Le premier est intense avec un twist pour le moins inattendu. Il y a une sorte de triangle amoureux entre Sook-hee, Hideko et le comte, une tension presque érotique (le film reste relativement chaste). Mais les actes sont liés et ne sont qu'un moyen de s'affirmer comme un drame classique, dans le bon sens du terme. Le thème vient d'un roman britannique, d'ailleurs, pourtant récent, mais qui déjà lorgnait sur la littérature du fin 19ème siècle. Malgré la lenteur du film, on est peu à peu embarqué dans cette machination qui se dévoile chez chaque personnage.

Le film est sombre et vénéneux, malgré sa beauté plastique. En effet, cette beauté n'a d'égale que l'horreur qu'il cache chez l'oncle d'Hideko, cet amateur de littérature… érotique. Il n'aime visiblement pas que le marquis de Sade. Voilà qui rapproche le film d'un autre classique, l'empire des sens. Sauf que le geste libérateur du film d'Oshima est ici bien différent. Mais à trop en dire je gâcherai le film et son intrigue. La relation Corée-Japon n'est pas assez exploitée à mon goût, même si il y a une tirade sur cela dans la seconde moitié du film. On est dans un thriller un brin érotique et particulièrement bien ficelé (sans mauvais jeu de mots). On traite ici de la relation dominant-dominé(e) dans divers registre. Ce n'est donc pas un hasard de transposer cette histoire dans la période où le Japon domina la Corée, la domination d'une culture occidentale sur la culture asiatique durant cette période... Le film peut donc avoir plusieurs lectures et dévoile tout sa complexité au fur et à mesure dans ce rythme propice à la réflexion ou la contemplation. Ce n'est sans doute pas le plus facile d'accès dans la filmographie du réalisateur, mais il serait aussi dommage de le réduire à son érotisme chic.

Je n'ai pas forcément eu d'émotions comme dans d'autres films, mais il marque incontestablement son spectateur. Ceci explique sans doute sa place dans les meilleurs films de l'histoire.

*Ce film fait partie du challenge IMDB Top250*

[Une Bande-annonce![video](/images/youtube.png)](https://www.youtube.com/watch?v=rUQY-iBKauc){:target="_blank"}