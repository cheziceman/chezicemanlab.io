---
layout: post
title: Sherlock Jr - Buster Keaton(1924)
category: cinema
tags: cinéma, film, muet, 1920s
---

**Il est toujours difficile d'aborder les chefs d'oeuvre du muet, surtout dans le registre comique. Les modes et le jeu ont évolué que cela paraît complètement démodé. Et pourtant, une fois passé ce premier choc, il y a des prouesses qui marquent durablement. Ainsi en est-il de beaucoup de films de Buster Keaton**

 Bien que ne durant que 45 minutes, il était considéré à l'époque comme un long métrage. Tout commence par un employé d'un cinéma qui balaye la salle et lit un livre sur comment devenir détective. Amoureux d'une jeune femme d'une famille plus aisée, il essaie d'économiser pour lui offrir une boite de friandises. Mais il fait face à un rival plus costaud et surtout moins honnête que lui. Accusé à tord d'un vol, il s'endort alors qu'il projette un film…

 Il faut remettre dans le contexte de l'époque quand on regarde ce type de films. Les effets spéciaux étaient rares et pas toujours bien maîtrisés. Le cinéma était encore un art en devenir même si des majors émergeaient. La séquence du rêve de notre héros se matérialise par une simple surimpression qui est pourtant totalement crédible et qu'on retrouve ensuite dans de nombreux films. Le personnage qui semble entrer ou sortir d'un écran de cinéma, c'est aussi quelque chose que l'on retrouvera dans d'autres films et qui ajoute encore à cette magie. Buster Keaton, c'est «l'homme qui ne rit jamais». Son style c'est ce visage impassible tout le long du film même quand on se moque de lui, qu'on l'agresse ou quand il est amoureux. Il diffère ainsi de son homologue Chaplin qui avait créé son personnage de Charlot.  

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/sherlockjr.jpg)

Ce qui me fascine dans ce film c'est le rythme dans ce type de comédie qu'on appelle le slapstick. Et pourtant, dans ces poursuites et cascades, il y a une poésie, comme lorsque Keaton suit le méchant de l'histoire en l'imitant, en essayant de se rendre invisible. Jacques Tati empruntera beaucoup à Keaton....et s'il n'y avait que lui. C'est typiquement le type de film qu'il faut voir pour comprendre le mécanisme de la comédie, son évolution dans le cinéma. Tout paraît si fluide, monté au cordeau malgré les moyens limités de l'époque. Lui aussi côtoiera le grand Fatty Arbuckle (ruiné par un scandale sexuel...déjà), autre génie du muet. 

Alors bien sûr l'histoire est très caricaturale et simpliste au global. Il y a des plans un peu long ou d'un comique d'un autre age (la peau de banane, la feuille qui se colle partout...). Et pourtant, on se surprend à rire, surtout lorsque l'on est enfant. Je me souviens de cette époque où l'on diffusait des courts métrages muets en interlude, ou … à l'école primaire. Les succès populaires de la comédie doivent beaucoup à ces maîtres des années 20, auxquels on peut rajouter le français Max Linder. Il y a maintenant une impression d'être en dehors du temps à revoir de tels films, et de se souvenir surtout que ce sont des oeuvres d'artisans géniaux, bien loin des usines à effets spéciaux numériques que l'on voit aujourd'hui sortir des très très longs métrages qui apportent parfois beaucoup moins d'émotions. Difficile alors de noter ces films à la même enseigne que les autres.

*Ce film fait partie du challenge IMDB Top250*

[Le film complet sur Archive.org](https://archive.org/details/sherlockjr1924_201909){:target="_blank"}