---
layout: post
title: Cinéma - Tu ne tueras point de Mel Gibson (2016)
category: cinema
tags: cinéma, film, 2010s, guerre, mélodrame
---

**J'avais zappé tout ce qu'avait fait Mel Gibson depuis ses «problèmes» d'alcool et de justice. Alors je n'ai pas fait attention à ce film dont la thématique avait tout pour me plaire.**

Mel Gibson parle du seul homme engagé dans la seconde guerre mondiale et qui ne tira aucun coup de feu puisque sans arme mais qui fut pourtant médaillé pour sa bravoure. Il était objecteur de conscience et pourtant engagé dans de grandes batailles du pacifique, comme Okinawa (le nom du film en anglais vient d'un lieu de cette île). Desmond Doss, puisque c'est son nom, était soignant/médecin militaire mais était aussi très pieux, comme l'est devenu Mel Gibson … Et là, je crains forcément un peu trop de bondieuseries de la part de notre Australien mystique. 

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/hacksaw.jpg)

Tout commence pourtant dans une sauvagerie sans nom, au point de faire passer l'intro d' «Il faut sauver le soldat Ryan» pour un film pour enfant. Flammes, explosions, corps déchiquetés, il y a de quoi entrer dans l'état d'esprit de notre héros pour refuser la guerre...mais surtout de son père, traumatisé par la 1ère guerre mondiale où il vit ses amis disparaître atrocement. Le frère de Desmond s'engage pourtant dans le rang malgré l'opposition des parents. Desmond fait la rencontre d'une infirmière et désire soigner plutôt que tuer. Voilà qui fait entre le film dans la romance et le mélodrame des plus classiques. Le choix du peu charismatique Andrew Garfield pour incarner Desmond Doss est dicté surtout par sa ressemblance physique. Il est un peu monsieur tout le monde, dégageant de la sympathie par sa naïveté apparente. L'affiche ne laisse pourtant pas de doute : C'est un film de guerre que l'on va voir et on s'attend à un film anti-militariste.

J'ai été étonné de le retrouver dans le classement des 250 meilleurs films, même si cela glorifie l'action des USA dans la 2nde guerre mondiale. En fait, c'est mon problème avec ce film : Tout en parlant de l'objection de conscience, il en arrive à une glorification du fait guerrier, au sacrifice à une cause, l'Occident face à l'orient s'affichant comme les élus de dieux contre le diable. Le sujet de l'objection de conscience me parle particulièrement puisque mon grand-père, qui n'a quasiment pas connu son père tué en 1915, n'a jamais voulu porter une arme non plus lorsqu'il fut mobilisé....tout en résistant à sa manière à l'occupant. Mais il n'avait clairement pas le même état d'esprit que Desmond Doss. Autant le père de Doss se demandait pourquoi il avait fait cette première guerre mondiale, autant cela ne semble pas interpeller notre héros. Le sens profond de ce film me dérange particulièrement dans ce moment où l'on revoit s'exacerber des nationalismes à travers le monde.

Pourtant, techniquement, il y a peu de films qui vont aussi loin dans le réalisme pour décrire la boucherie, la sauvagerie des combats les plus vains, à savoir ici une colline d'une île qui changea de main sans arrêt. En cela, le film est à conseiller mais redescend dans mon estime (et ma notation) pour son propos. Il est en cela fidèle aux précédents films de Gibson, Apocalypto et La Passion du Christ et cette fascination étrange pour la violence la plus crue et la religion la plus rigoriste parfois, fussent-elles mêlées.

*Ce film fait partie du challenge IMDB Top250*

[Une Bande-annonce![video](/images/youtube.png)](https://www.youtube.com/watch?v=h1Jv5WdOrz8){:target="_blank"}