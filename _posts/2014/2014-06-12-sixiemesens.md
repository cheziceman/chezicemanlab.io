---
layout: post
title: Cinéma - Sixième Sens de M. Night Shyamalan (1999)
category: cinema
tags: cinéma, film, cinémathèque idéale, thrille, fantastique, 1990
---

**Voilà un film qui fit rentrer immédiatement son réalisateur dans l'histoire du cinéma...au point de l'enfermer aussi dans un genre**

J'ai mis du temps à le voir après sa sortie mais on n'arrétaît pas de m'en parler autour de moi. Grand amateur d'Hitchcock, j'étais assez intrigué par les comparaisons avec le maître du suspens. A cette époque, le réalisateur indo-états-unien n'est pas connu après seulement deux films, dont un avec une part autobiographique. L'acteur en tête d'affiche, Bruce Willis est plutôt connu pour ses films d'actions depuis quelques années. Alors rien ne me poussait à me ruer dessus. Et puis j'y suis finalement allé pour découvrir cette histoire : «Alors qu'il rentre chez lui et s'apprête à fêter avec son épouse une vie couronnée de succès, le Dr Malcolm Crowe (Bruce Willis), qui est psychologue pour enfants, a la surprise de voir l'un de ses anciens patients, Vincent Grey (Donnie Wahlberg), pénétrer chez lui. Cet homme est dénudé, sous l'emprise d'un stress important, et avec une arme à la main. L'individu accuse Malcolm de l'avoir abandonné à une vie de terreur puis lui tire dessus et se suicide. Quelque temps plus tard, le Dr Crowe fait la rencontre d'un jeune garçon de neuf ans, Cole Sear, qui lui rappelle Vincent Grey au même âge. Comme lui, Cole (Haley Joel Osment) est régulièrement en proie à des accès de terreur inexplicables, ce qui en fait une proie facile pour les autres garçons de son âge.»

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/sixiemesens.jpg)

Et comme tout le monde, je me suis laissé embarquer dans cette histoire, je me suis laissé faire par le réalisateur et balader jusqu'à ce twist...M. Night Shyamalan est devenu définitivement l'homme du Twist final, au point d'en attendre à chacun de ses films depuis, justement parce qu'il a usé de cet artifice sur ses productions suivantes. Et puis, clin d'oeil à Hitchcock, le réalisateur apparaît aussi dans ses films. Mais même si les quelques suivants furent de bons films, aucun n'avait la puissance de cette histoire inspirée d'un épisode de série TV. On s'attache particulièrement à notre héros et à cet enfant (magnifiquement interprété aussi) et dans cette relation presque père-fils, on en oublie les détails, les possibilités, on reste rationnels. C'est là où c'est très fort, même si cela gâche la potentialité de nouveau visionnage. Enfin c'est ce que je croyais mais à chaque fois, je me prends au jeu de chercher ce qui aurait du me mettre la puce à l'oreille. Dans ce jeu, Night-Shyamalan nous donne à la fois le côté enfant qui joue et le côté adulte qui cherche à comprendre. 

Je comprends aussi ceux qui disent que l'astuce est facile. C'est la virtuosité du réalisateur à le faire qui fait croire à la facilité. Mais je leur répondrai : Faites le ! C'est justement parce qu'il a essayé de refaire cela dans plusieurs films sans atteindre le même niveau que l'on se dit que ce n'est pas facile. Il avait trouvé alors la bonne alchimie, les bons interprètes, la bonne histoire, qui se doit de sembler banale, comme aurait dit aussi le grand Alfred. Il avait trouvé le bon style graphique, la bonne ambiance sonore. C'est un tout et ce film au budget relativement modeste reste un exemple. Je n'ose pas le revoir trop souvent quand même, justement pour retrouver cette sensation initiale du premier visionnage, comme si j'enviais ceux qui le découvrent maintenant.

*Ce film fait partie du challenge IMDB Top250*

[Une Bande-annonce![video](/images/youtube.png)](https://www.youtube.com/watch?v=G3ZULSQ_Fp0){:target="_blank"}