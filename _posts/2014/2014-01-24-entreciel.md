---
layout: post
title: Cinéma - Entre le ciel et l'enfer d'Akira Kurosawa (1963)
category: cinema
tags: cinéma, film, japon, classique, cinémathèque idéale, 1960s, thriller
---

**J'avais vu quelques films du grand Akira Kurosawa, mais curieusement, celui-ci n'était pas passé devant mes yeux. Voilà chose faîte.**

Encore un film de genre puisqu'ici c'est un film de Kidnapping. On est presque dans un huis-clos ou du théatre filmé car la première partie du film se passe dans le domicile de la victime ...ou presque. En effet, M. Gondo (Toshirô Mifune), un self-made-man qui a réussi dans la chaussure (!), veut prendre les commandes de son entreprise, contre l'avis d'associés qui veulent les prendre eux pour faire une production à bas prix. Son refus de les suivre a-t-il pour conséquence le rapt de son fils dans les heures qui suivent ? Peut-être? Mais le kidnappeur se trompe et enlève le fils du chauffeur de M. Gondo. Malgré l'erreur, il demande toujours une rançon ...qui ruinerait M. Gondo. Mais suivant l'avis de sa femme à contrecoeur, il donne l'argent et l'enfant est libéré...sans lever le mystère de cet enlèvement ou récupérer l'argent. Le voilà héros tout en étant assailli par ses créanciers.

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/entreciel.jpg)

De film de Kidnapping, on change alors pour un Thriller mené par l'inspecteur chef Tokura (Tatsuya Nakadai) avec l'inspecteur chef «Bos'n» Taguchi (Kenjirô Ishiyama). Le film sort alors de ce huis-clos sans perdre de son intensité, malgré une durée de plus de deux heures. Un film dominé par la personnalité de son acteur principal, Toshirô Mifune, un patron «dur mais juste», autoritaire mais qui se range pourtant à l'avis de sa femme Reiko (Kyôko Kagawa), assez symbolique de la femme japonaise. L'intensité du jeu de Mifune est à l'image de l'attente d'un père. Sauf qu'ici, la pression est à la fois par la culpabilité d'avoir mis le fils de son chauffeur dans cette situation et le fait d'être au bord de la ruine sans pouvoir sauver son entreprise de la perte par son plan financier. On se met à douter de la sincérité de son adjoint et même de l'efficacité de la Police. Là encore, il y a une dimension sociale car Gondo est le symbole de la réussite et du redressement du Japon tandis que l'on voit encore la pauvreté et la dureté de la vie alentours. Si le titre anglais, High and Low est énigmatique, on voit plus le rapport avec le titre français ou japonais,Tengoku to Jigoku. 

Kurosawa nous emmène doublement sur des fausses pistes. Fausse piste sur le mobile de l'enlèvement, à différentes reprises, bien qu'il reste classique en apparence pour ce genre. Mais aussi fausse piste dans ces trois actes du film qui change complètement de genre pour arriver à un film des plus noirs. Le premier visionnage m'a laissé circonspect à cause de cela. J'étais perdu à chercher le véritable but du réalisateur. L'aspect technique n'est pas forcément mis en avant, en dehors du troisième acte où la photographie gagne en profondeur après le terne du premier acte. Je comprends finalement mieux le fait que le film apparaisse dans les meilleurs films du maître japonais et il se lasse aisément revoir pour en distinguer la subtilité de mise en scène.

*Ce film fait partie du challenge IMDB Top250*

[Une bande annonce ![video](/images/youtube.png)](https://www.youtube.com/watch?v=Rxnneq2W87k){:target="_blank"}