---
layout: post
title: Cinéma - La Liste de Schindler de Steven Spielberg (1993)
category: cinema
tags: cinéma, film, holocauste, 1990s, cinémathèque idéale
---

**C'est peut-être le film qui devait tenir le plus à coeur à son réalisateur. Un film clairement à part dans la carrière de ce grand entertainer.**

Mais Spielberg est ainsi à pouvoir aborder tous les genres, à faire du pur divertissement, du film pour ados mais aussi des sujets comme l'esclavage ou le politique US(Amistad, Lincoln). Là, le sujet le touchait plus directement encore : L'holocauste juif n'a peut-être pas touché ses parents mais on lui a suffisamment parlé du sujet, des pogroms qu'il a à coeur de mettre en image cette histoire d'un industriel allemand, Oskar Schindler, racontée dans un roman de Thomas Keneally. Le fait qu'on y parle aussi de pardon n'est sans doute pas étranger à cela et c'est justement ce qui nourrit certaines polémiques.

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/schindler.jpg)

En effet, Oskar Schindler n'est d'abord qu'un profiteur de guerre qui pense à son profit et adhère au parti nazi pour arranger ses affaires.Il emploie des juifs sous-payés après avoir récupéré par corruption une usine. Mais quand les camps d'extermination se montent, que le ghetto de Varsovie est créé, le voilà qui réalise l'horreur de la situation. Et le voilà qui tente de manipuler le SS Amon Goeth qui organise ce massacre. Mais, en effet, il ne néglige pas totalement son profit en le faisant et en cela, le film ne montre pas un personnage clairement héroïque. S'il montre une certaine part d'horreur, il ne s'attarde pas sur la réalité de cette extermination, de cette solution finale. Mais pourtant, sans montrer, il nous émeut par tout le reste, car nous, spectateurs, nous savons qu'au bout de ces convois et ces trains, il y a la mort, il y a l'horreur.

Alors évidemment, Spielberg fait du cinéma, pas un documentaire, pas un livre et prend donc quelques libertés dans la chronologie, dans la mise en scène, dans… la réalité de tous les faits. Cela ne me dérange pas par rapport à son but dans ce film. Il n'en fait pas un spectcacle et laisse la part d'ombre nécessaire aux différents personnages, même celui de Stern (Ben Kingsley). On ne peut pas avoir de totale sympathie envers Schindler (Liam Neeson) alors que l'on a de la haine envers Goeth (Ralph Fiennes). Le choix du noir et blanc en 1990 peut surprendre aussi mais il l'utilise pour bien marquer la différence passé-présent mais aussi réalité-cinéma. Et que dire de cette musique d'un John Williams très inspiré et très loin de ses habituels accents wagneriens (heureusement...). Le son d'un violon suffit parfois, même au elà des mots et des images.

Le film donne justement envie de s'intéresser au sujet et de ne pas s'en tenir à ce simple épisode Schindler. Le sujet de la culpabilité des allemands dans leur ensemble est un autre sujet à vif et qu'il convient de ne pas oublier. Un grand film.

*Ce film fait partie du challenge IMDB Top250*




[Une bande-annonce![video](/images/youtube.png)](https://www.youtube.com/watch?v=gG22XNhtnoY){:target="_blank"}