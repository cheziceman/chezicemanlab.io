---
layout: post
title: Cinéma - Ratatouille de Brad Bird et Jan Pinkava (2007)
category: cinema
tags: cinéma, film, cuisine, 2000s, animation
---

**Il fait partie des tous meilleurs films d'animation du studio Pixar et en plus il se passe à Paris. Autant de raisons de voir et revoir ce film !**

C'est un film dont je ne compte pas les visionnages. Il a ce talent de faire se sentir bien. C'est à la fois un conte, une comédie, une romance et … un hommage à notre gastronomie, même si elle est fantasmée par nos amis états-uniens. Quelle drôle d'idée pourtant de la faire conter par un animal aussi détesté (surtout à Paris en ce moment), que le rat ! Car notre héros est un rat qui se découvre un talent pour faire la cuisine grâce à son odorat hors pair. Il apprend à travers le livre de cuisine de la maison où il se trouve et dont l'auteur est un certain chef Gusteau. Mais après avoir été découverts, les rats fuient notre héros, Rémy, se retrouve à Paris. Le hasard fait qu'il tombe sur un jeune commis, Linguini, dans ...l'ancien restaurant de Gusteau, tenu par un nouveau chef, Skinner. Alfredo Linguini est un maladroit peu sûr de lui mais le hasard fait que Remy aide secrètement à rattraper une bêtise d'Alfredo et révèle son talent. Une jeune apprentie cheffe, Colette, s'intéresse à Linguini...qui se révèle aussi le potentiel héritier de l'empire Gusteau.

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2024/ratatouille.jpg)

Comme toujours chez Pixar, il y a plusieurs niveaux de lecture grace à plusieurs histoires. Les plus jeunes trouveront notre héros si mignon. Les amteurs.trices de romance aimeront la potentialité de la relation Colette/ Alfredo. Il faut aussi déjouer les plans machiavéliques de l'infâme Skinner pour vendre la marque Gousteau et hériter. Et puis il y a l'aspect initiatique à travers le parcours d'un jeune qui ne croît pas en lui, quand ce petit rat lui donne l'exemple. Tout cela est merveilleusement rendu par un dessin moderne et détaillé, des lumières très travaillées pour ce Paris qui fleure bon les années 60 par moment. Evidemment, on voit des grandes figures culinaires à travers Gousteau, mais c'est aussi une chronique sur ces grands noms qui se vendent à des marques pour de l'argent mais peuvent en oublier l'esprit même de la cuisine. Une vision évidemment idylique qui colle à l'aspect conte. 

Le burlesque de certaines situation dédramatise certaines situation du film, quand des twists et rebondissements le rendent assez enlevé sans abus de scènes d'action survitaminées. C'est bien l'égal des plus grandes comédies hollywoodiennes avec des acteurs en chairs et en os...à moelle. On pourra y voir peut-être un petit retour de baton sur les critiques de tous poils à travers Anton Ego (le nom déjà...) et c'est de bonne guerre, tant certains se croient pouvoir décider de la vie et la mort d'un.e cuisinier ou d'un établissement, comme cela peut aussi être le cas d'un artiste. Si je revois ce film volontiers de multiples fois, c'est qu'il me donne un sentiment de bien-être, malgré les tensions qu'il peut y avoir dans ce scénario. J'aime tous les détails cachés, les allusions, les comiques de situation et les personnages secondaires. C'est du grand divertissement fait d'une manière intelligente et il mérite très largement sa place dans ce petit panthéon du cinéma.

*Ce film fait partie du challenge IMDB Top250*

[Une Bande-annonce![video](/images/youtube.png)](https://www.youtube.com/watch?v=Q_dWB5ukGd0){:target="_blank"}