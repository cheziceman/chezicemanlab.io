---
layout: post
title: Cinéma - La complainte du sentier de Santyajit Ray (1955)
category: cinema
tags: cinéma, film, classique, inde, 1950s, cinémathèque idéale
---

**Bien qu'étant un film indien, ce n'est pas un film de Bollywood mais un classique du cinéma réaliste. Je me devais donc de le voir, tant il est cité par des cinéphiles**

Cinéma réaliste indien des  années 50, casting en partie amateur, manque de moyen....voilà qui pourrait rebuter au premier abord ou au contraire attiser la curiosité. C'est l'adaptation d'un classique de la littérature indienne : «Dans le Bengale rural des années 1920, le brahmane (une caste indienne) Harihar Roy vit dans la maison de ses ancêtres qui nécessite des réparations, mais il est trop pauvre pour les payer. Il vit avec sa femme Sarbajaya, sa fille Durga qui vole régulièrement des fruits dans le verger qu'ils ont été amenés à vendre aux voisins, son jeune fils Apu, et Indir, une vieille parente. Incapable de gagner assez d'argent pour subvenir aux besoins de la famille, Harihar part en quête d'un nouveau travail, laissant Sarbajaya seule pour gérer la famille.»

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/complainte.jpg)

Ce film est d'abord beau par sa photographie avec des contrastes très marqués dans les soirées et un soleil de plomb qui traverse même le noir et blanc de Subrata Mitra. C'est une vision quasi documentaire de l'inde rurale des années 20, que nous propose le réalisateur, un témoignae poignant. On n'y voit toutefois pas le colon anglais mais les castes, les jalousies, les mariages arrangés, etc.. J'ai retrouvé une ambiance à la Ozu, cette lenteur de développement très asiatique et bien loin, pour le coup, du cinéma bollywoodien. Mais je n'ai pas été emmené comme je le fus par un film comme l'île nue qui faisait aussi dans la contemplation sociale et le réalisme. Si c'est le premier épisode de la trilogie d'Apu, le personnage du petit garçon n'est pas centrale, la soeur Durga prenant plutôt ce rôle dans la première partie. Il n'y a pas de présentation de certains personnages clés, sans doute aussi dans le livre d'origine, mais c'est ce qui manque pour passer l'étape du cinéma. Bien sûr, c'est un premier film et je mets ça sur le compte de la maladresse mais j'ai alors du mal à ranger ce film dans les «indispensables». L'exotisme et la beauté plastique ne pardonnent pas tout, surtout face à l'épreuve du temps. 

Au final, un film intéressant pour l'histoire du cinéma indien, attachant par ses héros mais qui ne parvient pas à sortir du lot dans ce type de cinéma.

*Ce film fait partie du challenge IMDB Top250*


[Une Bande-annonce![video](/images/youtube.png)](https://www.youtube.com/watch?v=mgv68E_o6VM){:target="_blank"}
