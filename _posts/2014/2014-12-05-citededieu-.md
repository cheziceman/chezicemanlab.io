---
layout: post
title: Cinéma - La Cité de Dieu de Fernando Meirelles et Kátia Lund (2002)
category: cinema
tags: cinéma, film, Brésil, 2000s
---

**Le Cinéma Brésilien est plutôt rare. Mais il n'est jamais aussi bon que quand il parle de son pays.**

Avant d'être un film, Cidade de Deus, est un roman de Paulo Lins qui se situe dans une favela de Rio dans les années 60 jusque dans les années 80. Le film s'en tiendra à une décénie. Le roman suit le parcours d'un gamin qui va faire son chemin jusqu'à devenir un des chefs de la favela...comprendre d'un des gangs de la favela. Forcément, l'adaptation cinéma ne pouvait faire un héros d'un chef de gang. C'est donc un enfant qui rêve de devenir photographe qui va être le héros. Mais le contexte reste le même : Pauvreté, violence, gangs, drogue… Et si ce synopsis peut paraître non réjouissant ou trop documentaire, le film est pourtant d'une puissance qui laisse des traces dans la mémoire du téléspectateur. Il garde même toute son actualité dans un Brésil toujours aussi inégalitaire après les années Bolsonaro.

![Image](https://upload.wikimedia.org/wikipedia/en/0/0e/Cidade_de_Deus_A_Luta_Nao_Para.jpg)

Le casting est non professionnel. Fernando Meirelles ne trouvait pas de personnages à la mesure du roman ou du scénario repris par Bráulio Mantovani. Il a fallu voir près de 2000 personnes avant de trouver tous les acteurs du film. Ils étaient du cru et forcément plus aptes à retranscrire les us et coutumes d'une favela, surtout que la paire de réalisateur était étrangère à ce monde plutôt clos. Et comme il y a deux époques donc deux castings… On y voit la montée en puissance des gangs, les poursuites de la Police, la violence et les abus aussi, la corruption. Et forcément, on s'attache à un de ses gamins ou jeunes gens en se disant qu'ils font parfois le mauvais choix mais en avaient ils d'autres dans ce contexte. Les dérapages s'enchaînent avec des morts. La drogue fait des ravages, les vengeances aussi. Un film de genre évidemment mais aussi un film avec amitié et amour. Il faut bien 2H10 pour toute cette saga et pourtant...

C'est un film coup de poing qui est plein de rythme, soutenu d'ailleurs par une excellente bande son. Même pendant les moments de «calme», la tension reste palpable car on se dit que c'est trop beau pour être vrai. Outre l'excellent casting (dont certains feront carrière… et d'autres mourront prématurément), la mise en image vaut le détour et participe largement à l'expérience. Les couleurs sont chaudes, tirant sur le jaune et le orange. La caméra est proche des personnages, très mobile, toujours dans le feu de l'action. Oui c'est violent et sans concession mais il fallait cela pour prendre conscience de cette situation. D'ailleurs, un documentaire de 2013 regarde la situation 10 ans après. Puis une mini-série sortie cette année donne une suite au film. Pourtant, rien n'arrive à la hauteur de ce film militant sans oser le dire. Les prix de l'époque ont été légion et bien mérités. Le fait que Rocket/Fusée soit un photographe dans le film était une bonne idée pour montrer à quel point l'image peut être le meilleur des témoins. Mais encore faut-il savoir la regarder.

*Ce film fait partie du challenge IMDB Top250*

[Une Bande-annonce![video](/images/youtube.png)](https://www.youtube.com/watch?v=RrJ2OTMiFDg){:target="_blank"}