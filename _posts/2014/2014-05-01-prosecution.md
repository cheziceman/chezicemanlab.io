---
layout: post
title: Cinéma - Témoin à charge de Billy Wilder (1957)
category: cinema
tags: cinéma, film, classique, 1950s
---

**Encore une pépite de Billy Wilder et encore un film de procès, un genre dont le cinéma fut friand après guerre.**

Mais cette fois, c'est en Angleterre que nous sommes et ça donne un peu plus d'exotisme avec tout le rituel du procès à l'anglaise. En plus, on a droit à une actrice allemande (Marlene Dietrich) pour donner la réplique à l'Américain Tyrone Power et au «so british» Charles Laughton. L'histoire reste classique avec une affaire de meutre à résoudre : 
«A peine remis d'un infarctus qui a failli le terrasser, Sir Wilfrid Robarts (Laughton), ténor du barreau, accepte de prendre la défense de Leonard Stephen Vole (Power), accusé de meurtre. L'affaire, déjà difficilement plaidable, se complique encore lorsque Christine Vole (Dietrich), l'épouse du prévenu, devient l'un des témoins capitaux de l'accusation...»

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/prosecution.jpg "Image")

Alors que l'on s'attend à tout le sérieux d'un film de procès, le film commence sur un ton de comédie avec le Laughton bougon (ce qui n'était pas forcément un rôle de composition à en croire certains réalisateurs...) qui fait face à une infirmière enjouée et taquine. Billy Wilder est aussi un maître du genre (son premier film «The Major and the Minor», «, «Sept ans de Réflexion» et «Certains l'aiment chaud» avec Marilyn...). A la photo, un autre vieux routier, Russel Harlan, un habitué des films de Howard Hawks. Mais surtout, le scénario est issu d'une pièce de Madame Agatha Christie. En voilà une qui s'y connaît pour ménager suspens et rebondissements dans une histoire, avec un brin d'humour britannique. Wilder a su parfaitement s'adapter au contexte en alternant les scènes de procès et d'autres plus proches de la comédie. Mais si Power et Dietrich sont le couple en tête d'affiche, le véritable héros, c'est le grand Charles Laughton en avocat manipulateur et grognon. On sent l'expérience théâtrale du bonhomme et pourtant Wilder ne fait pas du théâtre filmé.

Nous sommes finalement loin de «12 hommes en colères» et autres films de procès classiques. Il y a bien de l'enquête et le spectateur se retrouve sur des fausses pistes où on lui met la puce à l'oreille. Mais bien malin celui qui trouvera le ou la coupable, comme aime à le rappeler l'avocat. J'avais vu ce film il y a très longtemps et je l'avais un peu oublié. Mais un nouveau visionnage a vite rappelé les souvenirs. Comme le dit l'affiche, c'est un divertissement agréable qui n'a pas vieilli et reste un modèle de comédie dramatique policière. Lui aussi mérite largement sa place dans les meilleurs films.

*Ce film fait partie du challenge IMDB Top250*

[Une Bande-annonce![video](/images/youtube.png)](https://www.youtube.com/watch?v=GMlJfiA2u7Y){:target="_blank"}