---
layout: post
title: Cinéma - Vol au-dessus d'un nid de coucou de Milos Forman (1975)
category: cinema
tags: cinéma, film, cinémathèque idéale, 1970s, folie, drame
---

**J'ai vu la première fois de film il y a très longtemps mais quel choc à l'époque ! Je l'ai revu plus récemment et si le choc s'est amoindri, l'intensité restait la même.**

Ce qui m'avait marqué à l'époque, c'était l'injustice de la situation, le jeu de Nicholson dont je fus fan à l'époque (c'était avant Batman...), et l'univers de la psychiatrie. Car l'histoire reste celle d'un homme, Randall Patrick McMurphy, qui en simulant pour échapper à la prison, se retrouve enfermé dans un hôpital psychiatrique, accusé de viol sur mineure. Le rôle du «héros» n'a évidemment rien de sympathique car rien n'est dit sur sa culpabilité.Il apparaît d'abord comme un égoïste grande-gueule qui ferait tout pour échapper à ses responsabilités.En réalité, le film n'est pas sur le cas de McMurphy mais sur l'univers de la psychiatrie qui ne soigne guère mais enferme, dans tous les sens du terme. McMurphy n'est que le révélateur de cette privation de liberté, bien pire qu'une prison car ici on met des camisoles, on enferme dans des camisoles chimiques par les médicaments et on restreint encore plus les mouvements pour des personnes qui n'ont pas commis de si grands crimes, sinon d'être différentes.

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/volcoucou.jpg)

Alors bien sûr, rétrospectivement, Jack Nicholson en fait des tonnes et le personnage s'y prête. Il saura montrer parfois plus de sobriété quand il ne se laisse pas aller au cabotinage. Mais c'est bien le Nicholson de Shining qui est déjà sur l'écran, avec une énergie qui semble parfois incontrôlable. Face à cela, il y a Louise Fletcher, la terrible infirmière en chef Mildred Ratched, froide et dure. Mais l'autre personnage qui m'avait marqué, c'était le «Chef» (Will Sampson), un indien mutique et impressionnant qui se lie d'amitié avec McMurphy. C'est clairement le personnage incompris, «différent» pour la société et qu'on a mis là pour le faire disparaître sans qu'il est probablement commis grand chose. L’injustice apparaît peu à peu dans cette adaptation d'un livre de Ken Kesey, déjà adapté au théâtre. C'est encore un huis-clos mais qui sait habilement sortir du piège du théâtre filmé. Si le film fonctionne bien, c'est aussi par son casting avec aussi Danny DeVitto, Christopher Loyd ou Brad Dourif.

Milos Forman a été bien justement récompensé pour cette réalisation qui n'a pas pris une ride. Même si les méthodes ont évolué, la privation de liberté parfois arbitraire subsiste et se double d'un manque de moyens que l'on tente de compenser par l'unique médicament. Forman ne tombe pas dans le piège de faire de McMurphy un héros, le scénario y aidant aussi. Le film était déjà dans ma cinémathèque idéale avant d'être dans le Top 250 d'iMDB et il le restera de nombreuses années. J'ai pourtant l'impression qu'on hésite à le rediffuser, le sujet rebutant certains, qui préfèrent sans doute cacher les problèmes comme on cache certains patients dans ces établissements.

*Ce film fait partie du challenge IMDB Top250*

[Une bande-annonce![video](/images/youtube.png)](https://www.youtube.com/watch?v=68r9lK7OFT4){:target="_blank"}