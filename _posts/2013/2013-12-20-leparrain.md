---
layout: post
title: Cinéma - La Trilogie Le Parrain de Francis Ford Coppola (1972-1990)
category: cinema
tags: cinéma, film, classique, 1970s, cinémathèque idéale, saga
---

**Pour beaucoup de cinéphiles et de mâles, c'est la trilogie par excellence, même si elle mit plus de 10 ans à se conclure. J'ai mis du temps à me l'approprier et je n'ai pas cet avis. Pourtant, cela reste de grands films pour moi.**

Quand j'ai parlé de mâles, c'est parce que je trouve cette trilogie très genrée. Après tout, il n'y a pas beaucoup de rôles féminins forts. On peut bien citer celui de Kay Adams (Diane Keaton) mais justement, c'est quand elle veut prendre sa liberté de femme que son couple explose. Les filles ne peuvent même pas choisir leurs époux sans que les mâles ne mettent leur véto (cf Mary Corleone, jouée par Sofia Coppola, propre fille du réalisateur). Sinon, elles sont des femmes au foyer, parfois même battues. Alors forcément, cela renforce l'image macho du mâle d'origine italienne. Le Parrain reste la saga familiale de la famille Corleone, une des familles qui dirige la pègre New-yorkaise. A chaque fois, ce sont des films de près de 3h qui décrivent une époque de cette famille. Si le premier est centré sur Vito Corleone (Marlon Brando), le second passe la main à Michael Corleone (Al Pacino). Mais on y revient aussi sur les origines de cette saga. Il faudra pourtant attendre plus de 15 ans pour que Coppola mette fin à cette saga par un troisième opus. Fin d'une famille ou d'une époque ? Il fallait finalement ce laps de temps pour que les acteurs soient crédibles en ayant réellement 15/20 ans de plus.

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/leparrain.jpg)

Le film de gangster n'a pas été inventé par Le Parrain ou par Coppola, pourtant. Mais en poussant cela au delà de ce qui a été fait et en créant une saga totale, ce n'est pas un film mais l'ensemble des films qui rentrent au panthéon du genre. Alors bien sûr, j'ai vu plusieurs fois le premier, seulement deux fois le deuxième et une seule fois le troisième et on retrouve finalement souvent ce classement dans les préférences. D'ailleurs, le 3ème n'est même pas dans le Top 250 qui motive cet article. Pas qu'il soit mauvais mais on a l'impression que l'essentiel a été dit et qu'il n'est là que pour un retour en grâce du réalisateur et conclure vraiment la fin des Corleone. 

Le film agit un peu comme un négatif du rêve américain toujours lisse et parfait et plein de bons sentiments. Les années 70 comprennent beaucoup de films qui dénoncent cette vision, justement mais en lui donnant du lustre et du glamour, Coppola réussit parfaitement son coup. On peut penser, de prime abord, qu'il légitime la mafia mais il montre surtout son implication à tous les étages de la vie du pays, que ce soit évidemment dans la politique et les affaires mais aussi le cinéma et les arts en général tout autant que dans le syndicalisme. Mais à force de consolider un genre, je trouve que le film fait une abondance de steréotypes ou de clichés. Il nous consolide finalement dans notre vision fantasmée, comme le fait aussi le Scarface de son collègue DePalma. Ce n'est peut-être pas un hasard si Coppola ne sort pas le troisième opus dans des années 80 bien mièvres, alors que le tournant vers les années 90 est moins joyeux. 

Au regard de ces années 2020, j'ai encore plus de mal à laisser ces films dans le top10. Il faut les regarder avec cette part d'histoire et de contexte, pas jusqu'à aller au documentaire mais aussi pour la maîtrise d'un grand du cinéma qui n'a pourtant pas tant de films à son actif. Et puis il y a le thème inoubliable de Nino Rota ainsi que la photographie très riche de Gordon Willis qui fut aussi des trois films. Bref, ils méritent bien d'être cité dans les films à avoir vus

*Ce film fait partie du challenge IMDB Top250*

[Une bande annonce de l'époque![video](/images/youtube.png)](https://www.youtube.com/watch?v=1x0GpEZnwa8){:target="_blank"}