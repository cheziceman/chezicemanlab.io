---
layout: post
title: Cinéma - Assurance sur la Mort de Billy Wilder (1944)
category: cinema
tags: cinéma, film, classique, cinémathèque idéale, 1940s
---

**Le titre français est assez plat  alors que je trouve le titre original, Double Indemnity, plus mystérieux. Car si tout part d'une assurance contre les accidents, il y a un peu plus de mystères dans ce film noir classique.**

Pour beaucoup de cinéphiles et cinéastes, c'est l'exemple même du film noir, un thriller implacable donc la noirceur n'apparaît pourtant pas tout de suite. On sait qu'il y a un meurtre dès le début, Walter Neff (Fred MacMurray, acteur trop oublié), assureur de son état, avouant cela à un «dictaphone» de l'époque. Tout part de sa rencontre avec Phyllis Dietrichson (Barbara Stanwyck avec sa célèbre frange), épouse d'un mari supposé violent et rustre. Neff en tombe amoureux et elle lui glisse l'idée qu'elle a déjà eu envie de tuer son mari, ne parvenant à s'échapper de ce mariage malheureux. Ensemble, ils échafaudent un plan pour non seulement tuer le mari mais escroquer l'assurance pour toucher une... «double indemnité». Neff avoue cela à son chef et ami, Barton Keyes (Edward G. Robinson), qui joue ici le rôle d'enquêteur ou plutôt d'analyste.

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/assurancemort.jpg)

Mais pour le spectateur, pas d'enquête mais plutôt une quête pour comprendre comment Neff, un vendeur plutôt exemplaire et sympathique, a pu basculer ainsi dans cette terrible machination. On fait la rencontre de la fille de Dietrichson, Lola (Jean Heather), dont le rôle semble trouble, entre ingénue et lolita. Cette noirceur qui surgit peu à peu est magnifiée par la photographie de John F. Seitz. Juste peut-on reproché l'abus de filtres et d'éclairage lors des gros plan, un truc de l'époque et très utilisé par le maître de Wilder, Ernst Lubitsch. Wilder travaille sur les éclairages, les ombres et lumières, notamment sur le costume clair de son «héros». La musique de  Miklós Rózsa reste efficace, même si elle est dans le style de l'époque. Si Barbara Stanwyck méritait un oscar (elle fut nommée), je trouve la prestation de Robinson toujours aussi magnétique que d'habitude. Et puis il y a Fred MacMurray qui durant la guerre, accéda à la lumière en remplacement des stars de l'époque. Il fut la star de cette période avant de sombrer dans l'oubli de ce coté de l'Atlantique ou de passer à la télévision aux USA. 

Le problème de ce film est qu'il faudrait le voir avant tous les autres films noirs et thriller sortis depuis. Beaucoup l'ont copié ou s'en sont largement inspiré. L'histoire elle même paraît moins originale et peut même faire datée en mettant beaucoup sur le dos des femmes de ce récit (machiavéliques ou ingénues). C'est ce que l'on peut croire au premier degré en tout cas mais c'est une manière de retourner l'image lissée des studios où Barbara Stanwyck et d'autres n'étaient pas toujours à l'écran avec autant de liberté. L'histoire fonctionne évidemment toujours et je ne me suis pas ennuyé. Les dialogues sont riches, avec quelques phrases cultes mais le débit est rapide, en V.O.. Aussi la valeur historique de ce film peut elle rehausser sa note chez certains, tout en le laissant à la place qu'il se doit : L'un des meilleurs films noirs du cinéma.

*Ce film fait partie du challenge IMDB Top250*

[Une bande-annonce d'époque![video](/images/youtube.png)](https://www.youtube.com/watch?v=yKrrAa2o9Eg){:target="_blank"}


