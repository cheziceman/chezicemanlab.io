---
layout: post
title: Cinéma - Taxi Driver de Martin Scorcese (1976)
category: cinema
tags: cinéma, film, classique, cinémathèque idéale, 1970s
---

**J'ai mis beaucoup de temps à revoir ce film qui marque à chaque visionnage. Peut-être pour son coté malsain, malgré sa beauté stylistique ?**

C'est le film qui lanca définitivement Martin Scorcese pour une carrière sans fin. C'est le film qui fit entrée Jodie Foster dans les radars. C'est le film qui installe définitivement Robert DeNiro dans le paysage des acteurs qui comptent. Mais c'est aussi un film qui montre un Harvey Keitel magnétique, et une Cybil Shepherd lumineuse. Mais l'autre acteur/actrice du film, c'est bien New York, pour moi. Un New-york nocturne, ou plutôt un Manhattan crépusculaire que Scorcese rend fascinant. Rien n'a vieilli dans sa mise en image, ce qui est rare.

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/taxidriver.jpg)
Mais il y a ce coté malsain avec l'histoire de son héros, Travis Bickle (DeNiro), un type dont on ne sait rien et qui veut devenir chauffeur de taxi un jour, pour rompre son ennui et son insomnie. On ne peut pas se prendre d'affection pour se type qui parcourt les cinémas X, reste seul dans une petite chambre miteuse. Ancien Marines ? Peut-être...Mais peut-on le croire sur parole. Il s'amourache soudain de la belle Betsy (Cybil Shepherd), assistante du sénateur Palantine, candidat à la présidentielle, mais montre son coté beau parleur...et surtout glauque. On le sent raciste, réac et petit à petit prêt à basculer. Pourtant, le film révèle aussi un coté justicier qui interroge le spectateur et titille ses plus mauvais instincts. 

C'est justement ce qui en fait un film fort, pas encore dans la «bien-pensance» des années 80, mais avec un aspect visuel qui ne le démode pas comme bon nombre des films des années 70. On y voit déjà cette opposition des classes entre la bonne société new-yorkaise qui regarde de haut ce taximan supposé ancien-combattant. Dans la scène du taxi entre Palantine et Travis, on se demande ce que pense justement le sénateur de son interlocuteur qui lui déclare être supporter de sa candidature. Il s’aperçoit de son ignorance du programme chez cet électeur et pourtant n'essaye pas de vendre ses idées (que l'on devine opposées?… ou simplement opportunistes). Il y a déjà tout ce qui fit passer les USA de Carter à Reagan. Un pays que l'on retrouve encore aujourd'hui, même si New York est bien plus paisible dans cette île de Manhattan. Enfin, même si la partition jazzy de Bernard Hermann (souvent associé à Hitchcock) n'est pas la plus inspirée, elle participe bien à l'ambiance générale.

Je ne m'étendrai pas sur les parallèles avec des films de John Ford que certains cinéastes y voient. Il y a le plaisir simple du spectateur à suivre cette histoire, à ressentir des émotions contradictoires. Et 50 ans plus tard, Taxi Driver réussit cela.

*Ce film fait partie du challenge IMDB Top250*

[Une Bande-annonce![video](/images/youtube.png)](https://www.youtube.com/watch?v=UUxD4-dEzn0){:target="_blank"}

