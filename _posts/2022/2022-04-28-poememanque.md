---
layout: post
title: Pause Photo et Poésie - Le Manque
category: poesie 
tags:  poésie, poême
---

Des jours que tu me fuis.

Grains de sable dans le vent,

goutte d'eau sous la pluie.

Je ne saisis plus tes instants.

 .

Il s'en faut d'une seule minute,

parfois même une seconde,

pour que d'une phrase abrupte,

j'écrive une pensée vagabonde.

 .
 
Sur un tronc, une feuille, un linceul,

gravé pour moi ou quelques autres

le murmure qui me laisse seul,

au milieu de tant d'apôtres.


![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/poememanque.jpg)
