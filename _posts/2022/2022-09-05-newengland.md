---
layout: post
title: Musique - New England (1979-1982)
category: musique
tags:  musique, hard FM, 1980s, 
---

**Un petit bonus musical lié au dernier [live report](https://www.cheziceman.fr/2022/girlschoolvaureal/), histoire de faire découvrir une petite histoire de la musique, par Shandi (Mme Iceman)**

Je profite de la récente double affiche Girlschool/Alcatrazz à Vauréal pour mettre en lumière New England, le premier groupe de Jimmy Waldo (claviers) & Gary Shea (basse), qui a sorti trois albums en 1979, 1980 & 1981.

Courant '70s, originaire de Boston, le groupe se forme autour de Gary Shea à la basse, Jimmy Waldo aux claviers, John Fannon, chant guitare, et Hirsh Gardner à la batterie.
Ils sont découverts par **Bill Aucoin**, manager à l'époque de **KISS**. Le style est FM, "power-melodic-orchestrated-song-oriented rock", selon le groupe !

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/newengland0.jpg)

Signé chez Infinity Rec. leur premier album "New England" sort en 1979 co-produit par Paul Stanley et Mike Stone. Leur premier single "Don't ever wanna lose ya" est #40 du Billboard. Leur deuxième single "Hello hello hello" cartonne bien aussi. Le groupe tournera aux US à sa sortie avec KISS.

Changement de label ensuite pour Elektra, deuxième album "Explorer suite" en 1980 qui a moins de succès. Et dernier album studio officiel "Walking wild" en 1981 qui ne marchera pas, malgré Todd Rundgren à la production (Badfinger, Grand Funk Railroad, NY Dolls, Meat Loaf...bonjour le cv !).

John Fannon quittera New England, les trois autres continueront avec Vinnie Vincent (futur guitariste de KISS) au sein de Warrior. Quand Vincent rejoindra KISS, Gary Shea et Jimmy Waldo eux rejoindront Alcatrazz...

En 2002 New England se réunira pour quelques dates à Boston et en Californie. Ce qui provoquera la réédition en CD des 2 premiers albums, d'un live et de démos.

...Comme fan de KISS & Paul, évidemment à l'époque j'ai cherché le premier album produit par Paul, introuvable à Paris même cher en import (pas d'internet). Ce sera finalement lors d'un séjour aux US que je trouverai mon précieux, soldé en vinyle !
J'aurais pu l'amener à Vauréal au concert Girlschool/Alcatrazz pour être signé, collector !!! Il est resté bien au ... frais

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/newengland1.jpg)

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/newengland2.jpg)

Alors pour se rattraper, il reste quelques vidéos : 

[Clip du premier album, Top 40 de l'époque ![video](/images/youtube.png)](https://youtu.be/UVlkIlsBBtE){:target="_blank"}

[Reunion de 2014 ![video](/images/youtube.png)](https://youtu.be/MqNNWqmktX0){:target="_blank"}

[En 2016 au Japon ![video](/images/youtube.png)](https://youtu.be/-EShnF2zhJE){:target="_blank"}
