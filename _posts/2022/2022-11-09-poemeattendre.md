---
layout: post
title: Pause Photo et Poésie - Attendre
category: poesie
tags: Poésie
---

Se poser sur ces grains,

attendre simplement la fin.

Fin d'un jour, fin d'une vie.

Fin d'une ère, fin d'une léthargie.

.

Regarder la vague qui se meurt,

s'étend à nos pieds comme seul bonheur.

Et attendre son retour en vain

quand sa sœur nous offre son amour.

.

Regarder l'oiseau voler contre le vent

comme immobile face aux éléments.

Et attendre son renoncement

quand nous n'essayons plus comme avant.

.

Attendre le départ du nuage

qui nous envoyait un message.

Mais il n'aura pas de réponse, 

juste l'écho des annonces.

.



![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/poemeattendre.jpg)
