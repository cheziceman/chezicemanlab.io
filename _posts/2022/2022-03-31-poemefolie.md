---
layout: post
title: Pause Photo et Poésie - Folie
category: poesie 
tags: poème, photo, poésie, folie
---

Fracas soudain d'une vie.

Des pleurs, des cris,

et des paroles insensées,

des mots délaissés.

.

Fatigue, chagrin, cauchemar,

angoisse, paranoïa, avatars,

hallucinations d'un matin

ou peur d'un soir chagrin.

.

La glissade est perpétuelle

chaque jour plus loin.

Aucune bouée, aucune main,

pour se réfugier dans une chapelle.

.

La foi se perd aux tréfonds,

d'une vie sans lumière,

où l'on se masque l'horizon,

jusqu'à ne voir que des hier.


![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/poemefolie.jpg)


