---
layout: post
title: BD - Maudit sois-tu de Pelaez et Puerta (2019-2022)
category: bd
tags: fantastique, science-fiction, horreur, BD, 2020s
---

**Voilà une trilogie singulière qui réunit des figures du fantastique horrifique dans une fiction contemporaine. Et tout cela sous une plume et des pinceaux franco-espagnols.**

Quand je dis figures du fantastique, on va aller du fantastique du fin du 19ème siècle jusqu'à des films très noirs. *2019, un homme est retrouvé mort dans les égouts de Londres. L’enquête se dirige rapidement vers la petite amie du défunt, car leur liaison a été arrangée par leur employeur commun, Nicholas Zaroff. Ce mystérieux oligarque russe n’a en fait qu’un seul but : se venger de ceux qui, 170 ans auparavant, ont causé la perte de son aïeul.* 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/mauditsoistu1.jpg)

Vous aurez donc reconnu déjà *Les Chasses du comte Zaroff*, adaptation en [1932](https://fr.wikipedia.org/wiki/Les_Chasses_du_comte_Zaroff) d'une sombre nouvelle de 1924. Mais le film a aussi eu des remakes. Le premier tome de la trilogie porte son nom. Le second porte le nom du Dr Moreau, soit le nom d'un personnage de H.G. Wells qui crée des créatures sur son [île](https://fr.wikipedia.org/wiki/L%27%C3%8Ele_du_docteur_Moreau). Nous ne sommes pas loin du successeur du Dr Frankenstein en effet. Et justement, le troisième tome s'appelle Shelley. Mary Shelley est l'autrice du [Frankenstein](https://fr.wikipedia.org/wiki/Frankenstein_ou_le_Prom%C3%A9th%C3%A9e_moderne) originel. La boucle est bouclée. Et sur la piste de tout cela, on trouve un inspecteur qui ne fait pas dans la finesse, une journaliste un peu junkie, un légiste alcoolique, etc...mais qui ne savent encore rien de la terrible machination du milliardaire russe Zaroff.

J'aurais pu passer devant cette trilogie s'il n'y avait déjà des couvertures qui claquent, puis ce nom de Zaroff qui fascine les amateurs d'horreur classique et de fantastique. Mais ça ne s'arrête pas à la couverture. Il y a bien sûr le scénario habile de Philippe Pelaez mais il y a la patte graphique de Carlos Puerta. Bien loin de la ligne claire franco-belge, nous sommes dans une véritable peinture infographique de toute beauté. Seul bémol, l'aspect statique et parfois trop photographique des personnages. Parfois on a l'impression d'un roman-photo. J'avais peur que cela ne s’essouffle après les premières pages mais non, c'est un festival de détails et de nuances.

Le scénario recèle bien d'autres liens avec la littérature fantastique classique anglo-saxonne et j'aime ce genre de détournement et de réécriture moderne. Peut-être que le premier tome se finit trop vite, sans cette pression pesante que ressentait le spectateur du film. On se demande si le terrible Zaroff va réussir son entreprise mais aussi jusqu'où ira le non moins maléfique Dr Moreau, et où mènent les liens que l'on découvre entre nos personnages. Les albums sont à la fois indépendants et liés pour comprendre toute l'histoire "familiale" puisque cela prend sa source au 19ème pour se terminer au 21ème siècle. Bref, une belle surprise pour tous les amateurs du genre.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/mauditsoistu2.jpg)


