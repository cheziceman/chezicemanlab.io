---
layout: post
title: Série du passé – Blackadder, La Vipère noire (1983-1989)
category: serie 
tags: tv,série, humour,angleterre, 1980s
---

**Si comme beaucoup j'ai découvert Rowan Atkinson dans Mr Bean, j'ai aussi vu d'autres facettes de son talent dans cette série, diffusée à l'époque sur Arte, en V.O. en plus. Elle avait juste mis 13 ans à traverser la Manche.**

Car en fait, cette série précède Mr Bean. On la doit à Rowan Atkinson mais aussi à un certain Richard Curtis, que l'on connaît mieux pour les scénarios des grandes comédies anglaises des années 90 (4 mariages.., coup de foudre à Notting Hill, Bridget Jones), et la réalisation de *Love Actually*, *Good Morning England*. *Cette série humoristique suit les mésaventures de deux personnages, Edmund Blackadder (Rowan Atkinson) et son domestique Baldrick (Tony Robinson), à travers les faits marquants de l'histoire de l'Angleterre, de 1485 à 1917. Les deux héros sont, à chaque saison, les descendants de leurs familles respectives. Il s'agit, en quelque sorte, d'une saga familiale s'étendant par pointillés sur plus de quatre siècles.* On y rencontre aussi de nombreux seconds rôles comme Stephen Fry, Miranda Richardson, Hugh Laurie, Colin Firth et même... Kate Moss.

[Un épisode entier ![video](/images/youtube.png)](https://www.youtube.com/watch?v=BUkThjDt470){:target="_blank"}

*La BBC a mis des épisodes en ligne sur Youtube*

Mais qu'est-ce qui fonctionne dans cette série ? L'humour anglais évidemment avec son lot d'absurde. Si vous avez aimé les films des Monty Python, il y a des chances que vous aimiez. Mais Blackadder est un être méchant, traître, qui veut accéder au pouvoir, ou au moins être reconnu par sa famille (un lien avec Macron???). Du moins dans les premières saisons qui se passent de 1400 à 1600. Petit à petit on se rapproche du 20ème siècle. La 4ème et dernière saison marque un virage avec la 1ère guerre mondiale. Cette fois, Blackadder est un officier qui veut échapper aux combats, tandis que ses comparses veulent participer à des assauts héroïques. Il n'y a que 6 épisodes par saison, malheureusement. Je n'ai pas souvenir d'avoir vu les épisodes "inédits" hors saison, par contre. Il y a un petit coté De Funes dans son personnage, d'ailleurs, quelques mimiques en moins, ce qu'il ajoutera ensuite un peu plus à sa palette d'acteur.

Peut-être que l'histoire typiquement anglaise des premières saisons n'aide pas à la compréhension totale des dialogues mais au moins cela pousse à s'intéresser à ce sujet. Après tout, d'autres séries ont repris des trames historiques avec beaucoup de succès. L'opposition entre Blackadder et son domestique fonctionne parfaitement comme beaucoup de comédie. On a aussi le ressort du Quiproquo. Et puis les "Blackadder" évoluent, étant parfois moins fourbes et plus intelligent selon les saisons. La troisième saison le faisait même être un domestique... enfin un Majordome quand même, l'idiot étant son "maître" (Hugh Laurie). Les scénarios puisent aussi dans des classiques de la littérature anglaise, pour chaque époque. On y croise même Shakespeare (Colin Firth tout de même).

Malgré l'age de cette série, elle a un coté aussi intemporel que celles de ses ainés, les Monty Python. Après, si on est étanche à l'humour anglais, ça ne va pas aider, évidemment. A voir aussi en V.O. comme à l'époque de sa diffusion. Les rires de public peuvent déranger, même si certaines sessions étaient publiques. Et que reste-t-il de tout cela à la fin sinon la folie des hommes.

[La fin ?  ![video](/images/youtube.png)](https://www.youtube.com/watch?v=vH3-Gt7mgyM){:target="_blank"}