---
layout: post
title: BD - Une Vie, Winston Smith (1903-1984), la Biographie retrouvée de Perissin et Martinez (2015)
category: bd
tags: bd, histoire, 2010s
---

**Selon la légende, c'est en trouvant un vieux livre d'un certain Winston Smith chez un bouquiniste que Christian Perissin eut l'idée de cette adaptation. Un mystérieux "aventurier" anglais né au début 20ème et nous voilà embarqué dans l'histoire de ce siècle.**

Est-ce que tout cela est vrai ? Difficile à dire comme pour toute autobiographie où quelqu'un se construit une légende. Christian Périssin adapte, donc change des éléments. Il y a cette jeune niçoise que l'on appelle pour lui dire qu'un vieil anglais a laissé une enveloppe pour elle avant de disparaître. Elle y découvre des éléments troublants, comme le fait qu'il connaissait sa mère, qu'il avait une photo d'elle petite avec sa mère sur la promenade des anglais, etc... Et puis il y a le manuscrit d'une biographie. Le jeune Dover Winston Smith est né d'une mère allemande exilée en Angleterre et de père inconnu. Il passe son enfance dans une école assez huppée où sa mère travaille mais sans pouvoir dire que c'est sa mère. La 1ère guerre mondiale se déclenche et être allemand n'est pas bien vu. Mais le directeur de l'école le protège. Winston tombe amoureux de Julia, la très jeune femme du directeur. Et de fil en aiguille, il parvient à faire son trou, aller à Eton, dans les prestigieuses universités anglaises...

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/winstonsmith1.jpg)

L'histoire du jeune Winston est passionnante comme une bonne saga. Il faut dire que si ce jeune solitaire, persécuté par ses camarades privilégiés, poète à ses heures, paraît de prime abord sympathique, on découvre très vite un coté sombre : Tricheur, Manipulateur, Traître. Tous les coups sont permis pour lui pour se sortir de sa condition, même si on peut lui trouver des circonstances atténuantes. Et l'enfance et adolescence occupe bien la bonne moitié des 5 volumes (réunies en intégrale). Un peu comme la distorsion du temps qui nous fait trouver l'enfance si longue par rapport à notre vie d'adulte, puis à nouveau cette période de vieillesse qui semble passer au ralenti. 

Winston Smith fait des rencontres, notamment avec deux hommes : [Aldous Huxley](https://fr.wikipedia.org/wiki/Aldous_Huxley#%C3%89crivain) qu'il a comme prof à Eton. [Eric Blair](https://fr.wikipedia.org/wiki/George_Orwell), dit George Orwell qui est sont camarade dans cette même école. Il y a également Steven [Runciman](https://fr.wikipedia.org/wiki/Steven_Runciman) que l'on connaît moins. Il vous aura peut-être échappé que Winston Smith est aussi le personnage de 1984 d'Orwell... Faut-il oublier tout cela ou prendre tout pour argent comptant ? Est-on manipulé jusque dans la construction de l'histoire de ce manipulateur. Tout cela rajoute du mystère dans cette longue histoire qui se lit pourtant facilement. La mise en image de Guillaume Martinez est classique, précise, détaillée. Je lui reproche juste certains "cadrages" inutilement sexy sur la jeune héroïne souvent dénudée.

L'histoire fonctionne vraiment bien, entre roman d'aventure, roman initiatique et témoignage historique. Car à travers Winston Smith on découvre l'Angleterre puis l'histoire tumultueuse de l'Europe de l'entre-deux guerres. Et puis il y a cette romance en fil rouge. Bref, il y en a pour tous les goûts et finalement, malgré le dégoût que certaines traîtrises peuvent nous inspirer, on s'attache à cette histoire.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/winstonsmith2.jpg)
