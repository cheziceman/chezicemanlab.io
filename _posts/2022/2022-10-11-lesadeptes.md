---
layout: post
title: Littérature - Les Adeptes de Ingar Johnsrud (2016)
category: litterature
tags: roman, littérature, norvège, 2010s, thriller, scientifique, politique
---

**Décidément, le Polar nordique est à la mode. L'ancien journaliste norvègien Ingar Johnsrud entame ce qui promet d'être une trilogie avec ce roman mêlant politique, science, religion, eugénisme et histoire. Rien que ça !**

Tout commence pourtant simplement : Une disparition. Sauf que c'est la fille d'une figure politique d'un parti de centre droit. Sauf que cette fille était dans une secte "La Lumière de Dieu" qui s'illustre par son conservatisme (homophobie, anti-avortement, ...), ses conflits avec des mouvements musulmans. Et à peine la recherche est-elle lancée que la secte est victime d'une tuerie de masse. Pourtant, pas de trace d'Annette, la femme disparue, ni de son enfant. Pire encore, on découvre une pièce mystérieuse, entre bunker et laboratoire et une bible ayant appartenu à un savant proche des nazis et théoricien de l'hygiène raciale. Au cœur de tout ça, un inspecteur, Fredrik Beier, divorcé, traumatisé par la disparition accidentelle de son fils, rebelle avec sa hiérarchie, et un peu obsédé sexuel quand même. Bref, du classique, pour ce qui est de l'enquêteur.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/lesadeptes.jpg)

Mais Johnsrud nous construit ça de manière habile, à travers des flashbacks et des scènes qui s'arrêtent sur un cliffhanger. Efficace ! Le lecteur a du mal à reconstruire les pièces du puzzle dans ce qui ressemble à une machination scientifico-politique. Et le voilà qui introduit un tueur impitoyable mandaté par un homme tout aussi mystérieux. Du bon thriller avec de l'action, du rebondissement. Beier est épaulé par Andres et Kafa mais je trouve qu'il laisse assez vite son inspecteur un peu exotique pour la jeune inspectrice aussi brillante que débutante. Tout ça pour peut-être introduire un triangle amoureux plus tard? Comme je l'ai dit c'est potentiellement une trilogie, même s'il n'y a pas eu de nouvel opus depuis 6 ans. On sent qu'il y a tout pour une suite, même s'il y a bien une fin pour celui là. La machination est trop grosse, les mystères toujours là.

Il y a du rythme, de l'enjeu. Parfois un peu de facilité, des remarques malaisantes (comme on dit aujourd'hui) de la part de cet inspecteur ou d'autres hommes de l'histoire. Mais ça se lit facilement, pour ces 560 pages qui nous emmènent autour d'Oslo mais aussi un peu en Suède et dans différentes régions du royaume. Un goût d'inachevé comme dans tout premier épisode mais pour qui n'est pas repoussé par quelques descriptions sanguinolantes, c'est une bonne distraction policière.


