---
layout: post
title: Blog - Changement
category: reflexion, 
tags: blog,
---

**Le changement, c'est maintenant. Peut-être l'effet de cette intense campagne mais d'autres choses aussi. Tout le blog va changer.**

Pas dans la forme mais dans le rythme. Souvent j'avais le sentiment que tout n'était pas au niveau, déjà. A me relire, je ne trouvais pas ce qui n'allait pas mais c'était un sentiment profond. Certaines chroniques étaient même superflues. Et puis surtout ça prend du temps, trop de temps par rapport au reste. Je devais rééquilibrer les choses, surtout en ce moment intense. Alors des 3 billets par semaine, je passe à 2 à partir de ... la semaine prochaine. Toujours le billet du samedi, même si là aussi je me demande si je ne changerai pas les choses. Donc un seul autre billet le mercredi maintenant. Ce billet sera culturel ou technique avec toujours de l'imprévu.

Surtout, je trouve que j'ai trop dérivé sur des choses personnelles ces derniers temps. Ce fut un tort et ça ne me ressemble pas. Alors je vais rester sur des domaines plus restreints sans doute, selon les envies du moment. Ainsi j'ai épuré un peu la présentation dans le à propos. J'ai supprimé quelques articles inutiles aujourd'hui. Je ne pense pas qu'il y avait trop de liens vers eux. Et pourtant, j'espère continuer à intéresser, à rendre curieux, à essayer des choses. Pour les poèmes, les derniers étaient mauvais, ce n'est pas le bon moment. Peut-être le poids de l'age, je n'en sais rien mais il n'y en aura plus sauf s'ils s'imposent d'eux-mêmes.

La question qui me taraude depuis longtemps (le début?) est de savoir quand cela s'arrêtera, quand je n'aurai plus l'envie ou le besoin de faire un article. Comme j'ai déjà pu le dire, tout n'est pas aussi linéaire que cela le paraît. Ainsi je n'ai quasi pas écrit d'articles depuis deux semaines et c'est déjà un problème. Il y a des moments prenants. Et le besoin d'écrire, de mettre des mots comme exutoire n'est jamais le même selon les moments. Il y a le goût du partage évidemment. Il faut juste savoir partager au juste nécessaire. Parfois le trop est l'ennemi du bien.

Mon principal problème, aujourd'hui, reste cette revue de médias mensuelle. Ça prend du temps et j'avais trouvé cette solution plutôt qu'un partage par les réseaux sociaux ou même par Shaarli (il y en a eu un il y a longtemps). J'ai ré-envisagé Shaarli, comme j'avais aussi pensé à d'autres solutions propriétaires de partage. Bof, je ne trouve pas ça bien, pas cohérent pour la présentation, trop long et sans la synthèse nécessaire, ponctuellement, le recul aussi. Je crois surtout qu'il y a trop de choses dans cette revue de médias, que je dois plus tailler dedans pour ne faire ressortir que l'essentiel, à savoir ce qui n'est pas assez traité dans l'Actualité avec un grand A.

![image](https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/Wassily_Kandinsky_-_Impression_III_%28Concert%29_-_Google_Art_Project.jpg/616px-Wassily_Kandinsky_-_Impression_III_%28Concert%29_-_Google_Art_Project.jpg)

*Impression III par Vassily Kandinsky - 1911*

Prenons par exemple ce dernier mois : On a eu les législatives, la NUPES... je suis bien placé pour le savoir, et puis Ukraine, Ukraine, Ukraine, Burkini, Taha Bouhafs, Burkini, Premier Ministre, MBappé/Macron, Burkini...A se demander s'il y a inflation, pauvreté, hôpitaux en souffrance, etc... Rien sur la situation dramatique en Somalie, Soudan, Éthiopie. Quasi rien sur la situation climatique en Inde et Pakistan. Plus rien sur Ouïghours, Rohingya. Rien sur le Liban, la Palestine, la Syrie. A peine a-t-on parlé du Brésil mais rien sur l'Amérique du sud. Et on a plus parlé du rachat de Twitter par Elon Musk que des mouvements sociaux aux USA, que cela soit les vagues de démissions, l'émergence d'un nouveau mouvement syndical, etc..Voilà pourtant qui forgerait une toute autre vision du monde. Alors peut-être qu'en esquissant quelques thèmes principaux, ça serait mieux. Réflexion en cours. Rendez-vous la semaine prochaine pour le résultat. 

Je n'ai par ailleurs plus traité de jeux vidéos, de culture geek depuis un moment. Déjà parce que je ne suis plus dans le coup niveau machine et pas envie de replonger dans une PS5 ou Xbox. La Switch est un monde à part, malgré tout, comme l'a toujours été Nintendo. Et le jeu sur mobile est devenu d'un ennui comme les spécialistes du domaine le prophétisaient il y a 5 ou 6 ans dans les souvenirs. De là l'émergence du rétro-gaming et sa récupération par le capitalisme avec spéculation, remakes, reboot...J'ai une liste longue comme le bras de jeux à rejouer ou redécouvrir. Juste que le temps manque pour le faire correctement, surtout que d'autres le font tellement mieux en vidéo.

Après tout, après 15-20 ans quasiment, un blog vieillit comme son auteur....et ses lecteurs ? Les rythmes de vie, les envies et besoins ne sont plus les mêmes. Mais plutôt que de tout casser comme autrefois, je garde tout comme c'est et j'enlève plus discrètement le superflu, tout en continuant gentiment mon bonhomme de chemin. Il est possible que les articles "politiques" repassent sur mon ancien blog... politique, que j'ai rouvert pour la campagne ([icezine.wordpress.com](https://icezine.wordpress.com)). Pour le rythme, je suis allé de 7 articles par semaine à 1 selon les périodes. Aujourd'hui c'est devenu plus stable. Je vois que je ne lis plus assez régulièrement les collègues qui en font plus de 2 par semaine donc ça me paraît un rythme cohérent. 

Et puis je garde quand même l'oeuvre d'art et la musique dans ce billet du samedi comme une marque de fabrique. Même si je suis sûr qu'il y a eu des doublons. Plus qu'une marque de fabrique, c'est aussi montrer d'autres choses que j'aime (ou pas) mais qui ouvrent les esprits. En fait, comme durant toutes ces années, il y a eu des moments où le fil s'est perdu un peu, où l'on se cherche à nouveau, et aussi où l'on s’aperçoit que l'on change, que tout change autour de soi aussi. Il faut se garder de rater le bon wagon...

Finalement, il est court, ce billet du samedi.

[Et le changement commence...maintenant ![video](/images/youtube.png)](https://www.youtube.com/watch?v=7Xwh1J6VQnI){:target="_blank"}
