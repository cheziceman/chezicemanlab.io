---
layout: post
title: Cinéma - Illusions Perdues de Xavier Gianolli (2021)
category: cinema
tags: cinéma, littérature, ambition, journalisme, 2020s, classique, Balzac
---

**En adaptant une partie de la Comédie Humaine de Balzac, Xavier Gianolli s'attaquait à une forte partie. Mission réussie si l'on en croit les césars. Mais ce n'est pas sans contre-partie.**

Le film a reçu 7 césars et il ne les a pas plus volés que d'autres. La meilleure adaptation, déjà, puisqu'il s'agit de l'adaptation du roman du même nom d'Honoré de Balzac. Mais il va un peu plus loin en se concentrant sur toute la vie du héros du film, Lucien Chardon dit De Rubempré (Benjamin Voisin). Jeune orphelin d'une aristocrate et d'un roturier, Lucien de Rubempré veut devenir poète. Épris de Madame de Bargeton (Cécile de France), le jeune homme d'Angoulême n'a d'autre choix que de partir à Paris pour assouvir son ambition : Devenir poète mais aussi récupérer sa particule. Mais les complots aristocrates le poussent dans le petit monde de l'édition et du journalisme qui se mêle alors à la vie artistique parisienne comme à la politique. Il en oublie la poésie pour devenir une plume acérée, sous la férule de Lousteau (Vicent Lacoste), et devient cynique sans perdre de son ambition. Il y fait aussi la connaissance de Raoul Nathan (Xavier Dolan), meilleur ennemi et alter-ego artistique. L'amour d'une actrice, Coralie (Salomé Dewaels), le pousse à des excès dans les compromissions de ce milieu. Les illusions du poète semblent alors bien ... perdues.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/illusionsperdues.jpg)

Le film est effectivement très bon, malgré sa longueur (2h30) mais s'éloigne un peu du sujet Rubempré pour traiter du sujet "Journalisme". Gianolli avait traité déjà du sujet dans L'Apparition où le personnage était un journaliste. Mais cela s'arrêtait vite. Ici il tente de faire le grand écart entre l'époque de Balzac et la notre, entre les compromissions et les puissances de l'argent qui s'emparent de la presse aujourd'hui et celles d'hier qui n'étaient pas si différentes dans l'instrumentalisation politique. Cela fonctionne et le réalisateur avec son co-scénariste Jacques Fieschi ont bien travaillé. La photographie est agréable sans abuser de l'obscurité comme c'est trop souvent le cas. Les décors et costumes sont à l'avenant et j'ai presque l'impression d'être une de ces plumes en écrivant ces mots...Sauf que je n'ai rien touché pour.

Si le film fonctionne, c'est aussi par son casting où je pourrais rajouter beaucoup d'excellents second rôles, le regretté Jean-François Stévenin en tête. Le propos reste moderne dans ses dialogues avec ses "punchlines" comme on dit aujourd'hui. Cela reste dans l'esprit de la comédie humaine avec son aspect lutte des classes. Rubempré est proche d'un autre personnage de Balzac, Rastignac, d'Angoulême lui aussi et tout aussi ambitieux. Mais l'un a un talent que n'a pas l'autre... Ils se croisent d'ailleurs dans l’œuvre de Balzac mais pas ici. Forcément, une adaptation prend des libertés narratives et chronologiques. Pour ceux qui aiment la complexité de la Comédie Humaine, il y aura de la déception sans doute mais le film peut aider à mieux percevoir le/les romans.

[Bande Annonce ![video](/images/youtube.png)](https://www.youtube.com/watch?v=umiWu_sjRhg){:target="_blank"}
