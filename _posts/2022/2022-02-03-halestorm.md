---
layout: post
title: Musique - Halestorm - The strange Case of ... (2012)
category: musique 
tags:  musique, metal, pop-rock, 2010s, 
---

**Voilà un groupe que j'ai longtemps hésité à chroniquer. Musicalement, ce n'est pas forcément mon trip, avec ce mélange entre metal et pop qui reste très radiophonique. Mais il faut reconnaître qu'il y a du talent**

A l'origine de tout cela, il y a les Hale. Le frangin à la batterie, un temps le père à la basse pour les accompagner car ils ont commencé quand ils étaient ados. Et puis il y a la frangine, Lzzy au chant et un peu à la guitare aussi. Mine de rien, quand ils sortent cet album en 2012, ils ont déjà 15 ans de carrière musicale alors que ce n'est que le deuxième album. Depuis le début, ils ont un son qui emprunte au neo-metal, au grunge, aux groupes alternatifs US mais ... avec une chanteuse. Et c'est ça qui fait toute la différence car Lzzy n'a pas beaucoup de concurrence. J'ai parlé d'[Evanescence](https://www.cheziceman.fr/2021/evabitter/) qui a permis pas mal de chose en sont temps mais ce n'est pas du tout le même style de voix, même si les répertoires sont parfois proches. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/halestorm.jpg)

Si je parle d'Halestorm c'est beaucoup pour le talent de la front-woman qui a pu le montrer sur de vraies reprises de standards du metal, comme par exemple du Judas Priest. Je ne m'y retrouve pas vraiment dans les compositions des albums qui visent plus les kids américains par les paroles et le style. Pourtant dans celui-là, ça envoie du lourd d'entrée avec "Love Bites". Voix hurlée, gros son, tout y est. Le "Mz Hyde" qui suit est très pop, une sorte de Marylin Manson parfois, mais la voix parvient à faire passer le truc. Les Oh oh de "I Miss the misery" sont dans le plus pur style power pop mais là encore il y a le supplément d'âme apporté par la chanteuse. Idem pour "A freak like me", là aussi très accrocheur avec une bonne intro rentre dedans. Ça aurait pu être du P!nk. Efficace.

Mais que se passe-t-il alors à enchaîner des titres mid-tempo et des balades d'une banalité sans nom. Je m'ennuie à mourir, même si c'est excellemment chanté. Ca reprend gentiment sur "Rock Show", il était temps, j'allais partir. Bon, il faut quand même "Daughters of darkness" pour se reprendre vraiment dans une veine plus metal dans le son. On est quand même dans des riffs power-pop bien bateau mais bon, ça marche. Ne comptez pas sur de la double pédale et des solos trop imposants. Avec un autre arrangement, on peut imaginer ça dans un registre de chanteuse pour ados. "You call me a bitch like it's a bad thing" est déjà bien plus intéressant car moins immédiat. Bien sûr on reste dans le fond de commerce du groupe mais le morceau est plus complexe avec un solo (plutôt court) et de la place accordé au guitariste du groupe. On a même un peu de sonorités bluesy et country sur "American Boys", pas forcément très intéressant mais elle est à l'aise.

Lzzy peut sans doute tout chanter avec talent mais terminer l'album sur "Here"s to us" est assez sidérant. Il ne se passe pas grand chose dans cette balade à émotion, malgré toute l'énergie. Et voilà tout le drame d'Halestorm, un groupe qui a du talent mais qui ne parvient pas à passer, pour moi, à l'age adulte, sans doute par facilité du label qui les gère aussi. Si je chronique le groupe, c'est donc pour toutes les bonnes dispositions que je vois et en attendant le fameux album de ... la maturité. Alors en attendant, je mets ce qui paraît le plus proche de ce que j'attends. 

La Video : [Love Bites ![video](/images/youtube.png)](https://www.youtube.com/watch?v=FmkHqUwa4zg){:target="_blank"}