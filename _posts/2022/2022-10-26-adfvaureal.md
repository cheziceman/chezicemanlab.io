---
layout: post
title: Musique - Asian Dub Foundation + Dizzy Brains - Vauréal 2022
category: musique
tags: concert, live, 2020s, Asian Dub Foundation
---

**Cela fait 25 ans que j'écoute ce groupe et pourtant je ne les avais jamais vu en concert. Je savais aussi en y allant que ce n'était plus le collectif des débuts.**

Sur ma dernière [chronique](https://www.cheziceman.fr/2021/adf2020/) d'il y a deux ans, j'avais déjà vu le changement, dans la continuité. Mais qu'est-ce que ça donnait en live. A priori, le line-up promis ressemblait un peu au ADF original. Et puis y aurait-il du monde ? Autant de questions qui me taraudaient en allant dans la "plus grande des petites salles"...Arrivé avant l'ouverture des portes, il n'y avait qu'une vingtaine de personnes qui doubleront au début de la première partie : **Dizzy Brains**, un groupe estampillé Rock venant de ...Madagascar. J'étais curieux de les voir justement parce que leur musique promettait des influences punk, de l'engagement politique et une bonne réputation scénique.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/dizzyvaureal1.jpg)

Difficile quand même de commencer un set devant un public qui reste loin de la scène. On sent le chanteur, Eddy Andrianarisoa, embêté par la situation devant cette salle encore bien vide. Il va falloir faire le job et convaincre un public clairement venu pour la vedette du soir. Un quatuor donc avec guitare-basse-batterie et un certain charisme pour ne rien gâcher. Je trouve pourtant le début du set propre mais encore timide. Et puis peu à peu, ça devient plus enragé. Un petit côté Danko Jones pour l'énergie rock, la rage, la colère aussi par rapport au message : "la corruption qui détruit Madagascar et cantonne la population à la misère". Alors c'est en Malgache souvent mais on ressent ça. Dommage en effet qu'il n'y ait pas eu de malgaches dans le public. Le chanteur de Antananarivo est habité par ses titres mais laisse aussi la place à son guitariste, très bon sur son Ibanez sur le côté droit. Les applaudissements et cris sont encore timides même si en fin de set, la salle s'est remplie. Le groupe méritait mieux et pourrait donner sa pleine mesure en festival (ils s'étaient faits connaître aux transmusicales de Rennes en 2015), par exemple, mais aussi en revoyant cette setlist pour ce genre de configuration. A écouter sinon sur [Bandcamp](https://thedizzybrains.bandcamp.com/).

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/dizzyvaureal2.jpg)

Le staff français d'**Asian Dub Foundation** fait le job pour installer le matos. Les pédales et amplis pour la guitare coté droit, la basse au fond à gauche. Et puis le batteur (enfin celui que j'identifierais ensuite...) vient lui même rajouter ses éléments à la base laissée par les Dizzy. Ajustements, réglages, tests, il s'en faut d'une demi-heure avant que les lumières se ré-éteignent. La salle s'est enfin remplie, l'ambiance monte. Le public a plutôt une trentaine bien tassée ou la quarantaine. Assez normal puisque le top du groupe fut dans la fin des années 90, début 2000. Le merchandising s'est consacré à "La Haine", cette illustration musicale produite par le groupe sur le film de Kasso, ou bien sur les derniers albums, Access Denied (2020), Tank (2005) ou Enemy of the enemy (2003). Ca risque d'être dur d'entendre des titres des premiers albums (RAFI, Facts and fictions). 

A leur entrée, surprise à la basse (j'avais un doute), pas de Dr Das mais un nouveau, "Jammaz Jamz" qui a l'air sympa aussi. Sinon, on retrouve un des fondateurs, Chandrasonic, à la guitare. Pour la batterie aussi, il y a du changement puisque pas de Rocky Singh non plus. Il s'agit sans doute de Brian Fairbairn, comme dans le dernier album. Pour l'instant, pas de chant pour débuter mais sur la gauche, il y a un certain Nathan Lee avec sa ...flûte traversière. Chandrasonic installe l'ambiance entre son sampler et ses effets de guitare qui sont sa marque de fabrique...et même celle de ce groupe inclassable. Le public réagit bien dans ce début (issu d'Access Denied) encore un peu planant. Mais on voit que ça va groover à la basse, que ça va cogner derrière les fûts. Et puis cette flûte nous ramène aussi dans des contrées musicales inconnues.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/adfvaureal5.jpg)

*Chandrasonic concentré*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/adfvaureal1.jpg)
![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/adfvaureal6.jpg)

*Les petits nouveaux sont monstrueux*

Après cela, les chanteurs font leur entrée : On a un duo bien rodé maintenant avec d'un coté Aktarv8r pour les parties hip-hop et Ghetto Priest pour les parties dub-reggae. Là aussi du lourd, puisque le premier n'a rien perdu de son débit, assurant même sur quelques mots en français et le second est une pointure de la scène reggae londonienne. Si Aktarv8r est sobre et jovial en apparence, Ghetto Priest en impose entre sa musculature, son allure de yogi-rasta du ghetto qui vient nous prêcher la bonne parole. C'est le dernier album qui est à l'honneur avec un son puissant, un chant qui fait mouche. Le "Can't Pay, Won't Pay" tombe à point nommé en ce moment. Moi qui attendait quelques anciens titres... Mais je ne suis pas déçu pour autant, le live donnant une autre tonalité à ces chansons. Pas de doute, c'est vraiment un groupe de live et je suis étonné qu'il n'y ait pas eu beaucoup de captations dans ce domaine (le live Keep banging on the walls d'il y a presque 20 ans). On passe quand même à *La Haine* ou bien *Fly Over* toujours aussi efficace.

Mais vient aussi le moment du solo de ... Nathan Lee : 

[Nathan Lee](https://videos.pair2jeux.tube/w/qLApgm42ozUkP3ikHQ98DB){:target="_blank"}

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/adfvaureal4.jpg)

Une découverte que ce type là : Faire de la flûte et du beatbox en même temps, c'est une performance. Je ne suis pas plus fan que ça des beatboxers mais là, ça le fait et le solo passe très bien. J'ai réussi à en capter un extrait cette fois. Il y a toujours des moments de pur dub, comme "Stand Up" qui font revenir notre prêtre rasta parti fumer en coulisse. Maintenant, ça danse, ça saute dans le public. Et puis ça chante surtout avec Aktarv8r qui fait le job pour faire participer tout le monde à la fête. Chandrasonic qui paraissait assez fermé au tout début, s'éclate sur scène avec sa bande. L'âge n'a pas de prise non plus et le voilà qui jongle avec ses pédales entre les morceaux pour nous sortir des sons incroyables. Je suis content de retrouver le titre *Naxalite* qu date de 1997. Là aussi, ça n'a pas vieilli. Mes voisins de premier rang, plus jeune, semble découvrir. Ça saute, ça bouge de plus en plus, la sueur ruisselant malgré une température plutôt clémente à la base. Mine de rien, on arrive déjà à 1h de set pour un premier au revoir...Et un rappel qui vient assez vite. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/adfvaureal3.jpg)

*Ghetto Priest dans ses oeuvres*

D'abord instrumental, ce rappel fait à nouveau rentrer notre duo en action pour un *Fortress Europe* enfiévré ! Oui, ça tape toujours à la porte de la forteresse Europe....et Royaume Uni.
Les musiciens sourient, sont complices et ça fait plaisir à voir, comme tous les sourires qui illuminent maintenant les visages dans le public. Les concerts paraissent toujours trop courts quand ils sont bons. Et là, c'est du très bon. Alors il faut bien un second rappel avec ...un titre de 1995 de Facts and Fictions ! Me voilà totalement comblé. Pas une minute d'ennui, pas une seconde de répit dans ce concert qui prouve encore que cet ovni musical est un incontournable. En plus, on a des tarifs accessibles dans ces petites et moyennes salles ! Alors pourquoi s'en priver. Promis, ils reviendront ! Et allez savoir quand ce collectif arrêtera de nous produire des albums en plus puisque l'héritage des anciens est en de bonnes mains.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/adfvaureal2.jpg)

**Setlist :**

* Realignement
* Mindlock
* The Signal and the Noise
* Can't Pay Won't Pay
* Stealing the Future
* Zig Zag Nation
* Access Denied
* Frontline
* La Haine
* Flyover
* Nathan Solo (Flutebox Solo)
* Stand Up
* Charge
* Swarm
* Naxalite
* Oil

Encore:
* New Alignment
* Blade Ragga
* Fortress Europe

Encore 2:
* Rebel Warrior 

[Fortress Europe (Extrait)](https://videos.pair2jeux.tube/w/ryJghY6ACCUtkAYhkT1wJx){:target="_blank"}

Asian Dub Foundation est aussi sur [Bandcamp](https://asiandubfoundation.bandcamp.com/) avec presque toute la disco.
