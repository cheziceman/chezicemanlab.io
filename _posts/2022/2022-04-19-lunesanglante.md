---
layout: post
title: Littérature - Lune Sanglante de James Ellroy (1984)
category: litterature 
tags: livre, roman, thriller, policier, los angeles, 1980s
---

**Alors que j'ai lu plus de la moitié de l'oeuvre d'Ellroy, je n'avais jamais pris le temps d'en chroniquer un seul. Donc je m'y suis remis.**

Cela faisait bien 20 ans que j'avais quitté cet auteur. J'avais commencé par le ["quatuor de los angeles"](https://fr.wikipedia.org/wiki/Quatuor_de_Los_Angeles), paru de 1988 à 1991. Mais avant cela, il y a la trilogie Lloyd Hopkins, à commencer par ce premier tome "Lune Sanglante". Comme son nom l'indique, le personnage est le Sergent Lloyd Hopkins, inspecteur au LAPD. Ellroy est très attaché à Los Angeles...Cette fois, nous nous retrouvons dans une époque contemporaine à l'écriture puisque c'est dans les années 70 que le roman commence mais il se passe principalement dans la période "Disco", à la fin du mouvement Hippie. Hopkins est un personnage "Ellroyen", donc pas forcément très sympathique, grand, costaud, macho, coucheur, instinctif. Cet instinct en fait un génie pour résoudre les énigmes, profileur avant l'heure, mais aussi asocial avec ses collègues, avec des symptômes que l'on pourrait qualifier d'autistiques. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/lunesanglante.jpg)

Dans son œuvre, Ellroy est fasciné par les féminicides, sans doute à cause de sa mère assassinée...Cette fois, nous sommes sur les traces d'un tueur de femmes qui maquille souvent le meurtre en suicide. Jusqu'à perdre le contrôle et laisser libre cours à la sauvagerie. Ellroy ne s’embarrasse jamais de protéger son lecteur des détails les plus crus. Le vocabulaire des personnages n'est pas non plus dans la dentelle, même avec le filtre de la traduction. Ce tueur a la particularité d'avoir été violé par des hommes dans sa jeunesse et Hopkins le devine assez vite. Pourtant, impossible de trouver un lien entre ses victimes. Ce qui maintient en haleine, c'est le fait d'alterner la vision du tueur et celle de l'inspecteur. Ce n'était pas si courant à cette époque mais beaucoup ont repris ce découpage depuis.

Pour les amateurs du LA des années 70, 80, il y a ce qu'il faut de description, notamment du strip et des bars mythiques comme le "Whisky a go go" ou le Gazzari's. Pas encore de Hair metal pourtant et en plus Hopkins déteste la musique, pour des raisons que l'on apprend en fin d'ouvrage. Comme il se doit, la vie personnelle de l'inspecteur est chaotique, presque à l'image de son auteur de l'époque. Il doit y avoir une bonne part d'Ellroy, au premier abord, ... Même si je trouve que tout n'est pas encore en place (il y a aussi une bonne partie où il faut installer le personnage"), c'est du Ellroy pur jus, cru à souhait, qui peut rebuter ou captiver. Alors que je trouve qu'il se perd peu à peu après son quatuor, il y a de l'énergie à revendre ici, une force dans le récit qui peut aussi assommer. Ellroy, c'est violent, forcément, et souvent réac sur les bords. A prendre ou à laisser.
