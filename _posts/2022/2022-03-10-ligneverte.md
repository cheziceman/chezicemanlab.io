---
layout: post
title: Cinéma - La ligne verte de Frank Darabont (1999)
category: cinema 
tags: Cinémathèque idéale, 1990s, cinéma, film, drame, injustice, fantastique
---

**Encore un classique que je revois toujours avec plaisir...ou plutôt avec émotion, tant ce film peut tirer des larmes, notamment quand je pense à l'un des acteurs.**

*En 1996, Paul Edgecomb, un ancien gardien-chef d'un pénitencier dans les années 1930, entreprend d'écrire ses mémoires. Il revient sur l'affaire de John Coffey — ce grand Noir au regard absent, condamné à mort pour le viol et le meurtre de deux fillettes — qui défraya la chronique de 1935. Le pénitencier où travaille Paul est surnommé la « Ligne verte » : il s'agit du Cold Mountain, en Louisiane, là ou tous les condamnés à la chaise électrique passent leurs derniers jours.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/ligneverte.jpg)

Voilà donc l'histoire de ce film et on pourrait croire à un enième film de prison, un enième film sur la peine de mort. Mais quand on apprend qu'il s'agit d'une nouvelle de Stephen King, on sent que le thriller et le fantastique ne doivent pas être loin. Et pourtant, tout semble normal, enfin sombre, mais normal. Un pénitencier isolé où les gardiens se laissent aller sur les détenus, normal ? Mouais, dans l'environnement carcéral US, on a l'habitude de cette violence. Mais justement il y a quelque chose de troublant chez ce John Coffey (Michael Clarke Duncan). Son physique est impressionnant, mais la douceur qui émane de lui aussi. Et soudain, il y a une souris. Elle devient une mascotte, apporte de l'humanité alors que Percy (Doug Hutchison) a essayé de la tuer. Elle devient la compagne d'un des condamnés mais aussi le révélateur de l'aspect fantastique du film.

Si le film fonctionne c'est par ce huis-clos pesant mais aussi par son casting. Tom Hanks évidemment dans le rôle de Paul. Michael Clarke Duncan aussi, mais on a besoin de personnages troubles, de méchants comme David Morse ou James Cromwell. Tout cela est magnifiquement mis en image par David Tatersall qui fit ses classes avec George Lucas dans la seconde trilogie qui est la première :p. La musique sait se faire présente sans trop en faire grâce à la partition de Thomas Newman. Ce qui m'a tout de même marqué, c'est la présence de Duncan à l'écran. John Coffey a gardé l'innocence d'un enfant de 5 ans dans le film et le livre. Et tout se passe par ce décalage entre le physique musculeux de John et son esprit. Il fallait rendre cela par le geste, par le physique et pas seulement la voix et le regard. L'injustice autour du sort de Coffey ne dédouane pas les autres condamnés, dont certains sont violents, ne regrettent rien de leur geste. Mais cela met quand même la peine de mort sur la sellette.

En plus de tirer des torrents de larmes par sa conclusion, ce film arrive à faire sourire parfois, réagir sinon à la violence. Bref, il émeut. Mais ce qui achève de m'émouvoir, c'est que Michael Clarke Duncan a eu un destin tragique. Lui qui commença comme videur ou garde du corps pour vivre tout en rêvant d'être acteur, a été découvert sur le tard. Il n'aura eu que 13 ans pour profiter de la gloire qu'amena ce film, avant qu'une crise cardiaque ne l'emporte à 54 ans. Il était engagé dans la protection animale aussi et avait semble-t-il une part d'innocence comme son personnage, certains diraient naïveté. 

Malgré les larmes et l'injustice, j'aime revoir ce film qui amène aussi du rêve et du fantastique dans l'endroit le plus sordide. Il fait un peu croire à la bonté plutôt qu'à la noirceur. Et sans dévoiler la fin, elle n'est pas si négative que cela.

[Bande Annonce ![video](/images/youtube.png)](https://www.youtube.com/watch?v=mccs8Ql8m8o){:target="_blank"}



