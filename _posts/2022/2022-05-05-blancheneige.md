---
layout: post
title: Cinéma - Blanche Neige et les sept nains de David Hand (1937)
category: cinema 
tags: cinémathèque idéale, 1930s, cinéma, film, dessin animé, conte
---

**Premier long métrage des productions Walt Disney, ce film a encore toute sa place dans une cinémathèque idéale. Parce qu'il y a tant à en dire.**

Ce film a été le mètre étalon de l'animation pendant 50 ans, installant la firme qui le produit comme le studio phare du dessin animé. Mais qui se souvient du nom de son réalisateur, mis au rang de simple opérateur dans la grande firme d'oncle Walt. C'est pourtant un des pionniers de l'animation dans le studio, celui qui mit au point la technique quasi industrielle pour passer des petits animés type "Silly Symphonies" à ce premier vrai long métrage animé de l'histoire. Plus d'une heure et vingt minute, c'était déjà long pour un film des années 30.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/blancheneige.jpg)

Et pourtant à le revoir, on trouve que l'histoire va vite, très vite. Il n'y a pas ou peu de scène pour présenter les personnages, le contexte de ce royaume, la relation entre la princesse Blanche-neige et son infâme et cruelle belle mère. Pour en savoir plus, il faut aller voir les frères Grimm dont Walt Disney s'est un peu écarté. On est directement dans l'action même si les scènes les plus cruels sont suggérées. Le film s'adresse à des familles mais il y a toujours eu de la cruauté et de la violence chez les frères Grimm. Disney ne peut tout gommer dans ce qui est une oeuvre initiatique.

Alors bien sûr, le film a vieilli sur bien des points. D'abord il met en place un patriarcat très marqué, le rôle de la princesse étant de faire le ménage, le linge et la cuisine d'une bande de nains, ou d'attendre un prince charmant. On peut aussi y voir un petit discours sur l'hygiène, ne serait-ce que la très drôle scène du nettoyage des mains. Celui qui ne parle pas est un "simplet" alors qu'au contraire il semble plutôt malin à vouloir resquiller le baiser de la princesse. On a une vision assez utilitaire des animaux de la forêt qui aident ces petits humains qui sont pourtant bien bruyants et destructeurs. Mais voilà, nous sommes en 1937 aussi.

Et c'est parce que nous sommes en 1937 que ce film est remarquable. Il y a ces couleurs chatoyantes, ces décors incroyables de beauté. Il y a une qualité d'animation que l'on mettra beaucoup de temps à égaler de part le monde, tant elle est fluide. Il y a des dessins de personnages qui serviront d'inspiration à bien d'autres héros. Il y a ce parfait accord entre musique et animation, ce qui avec les moyens de 1937 était loin d'une sinécure. La musique, les chansons qui entrecoupent le film, tout est déjà là pour des années de films Disney et d'autres studios. Le fait d'utiliser des couleurs plus unis pour les parties animées quand le reste du décor est plus fouillé est aussi là. C'était aussi un budget d'un block buster d'aujourd'hui, un pari incroyable.

C'est un film à la valeur historique indéniable et qui malgré ses scènes éprouvantes, parvient toujours à fasciner petits et grands. On voit par le choix des animaux qu'il est curieusement situé en Amérique du nord, mais bon, qui s'en soucie. Beaucoup des chansons sont devenues intemporelles. Il faudra 5 ans avant de retrouver Hand et sa maestria dans un autre film mythique : Bambi. Mais c'est bien ce film qui permit tout le reste et créa l'empire Disney. Et je prends toujours du plaisir à le revoir, alors qu'enfant, j'avais découvert l'histoire dans un livre/disque de la firme.

[Bande annonce ![video](/images/youtube.png)](https://www.youtube.com/watch?v=t1mEHEgDehg){:target="_blank"}
