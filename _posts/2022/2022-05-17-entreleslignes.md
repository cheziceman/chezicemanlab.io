---
layout: post
title: BD - Entre les lignes de Beaulieu et Mermoux (2021)
category: bd
tags: bd, histoire, biographie, guerre, romance, 2020s, adaptation
---

**Il y avait la promesse d'une belle histoire, celle d'une couverture énigmatique dans cette BD. Trop de promesses, presque et il fallait que j'aille au bout, pourtant.**

Cette BD est l'adaptation du roman "Toutes les histoires d'amour du monde" de Baptiste Beaulieu. Je n'ai d'abord pas compris pourquoi avoir changé le titre. Et puis en entamant l'ouvrage, j'ai été perturbé : On y voit l'auteur du roman qui raconte son histoire et celle qu'on me promettait, cette romance pendant la 2ème guerre mondiale et même avant. Une romance entre le grand-père de l'auteur et une femme mystérieuse, qui n'apparaît que tardivement. En attendant, il y a la relation difficile entre Baptiste Beaulieu et son propre père, pour des raisons qui sont tues; il y a des non-dits; il y a la mort du grand-père, une photo, des carnets de lettres, une boite.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/entreleslignes1.jpg)

Entre les lignes, c'est pour ce qui n'est pas dit dans le récit des lettres. C'est pour ce qu'essaye de combler Baptiste pour son père malade par rapport à la vie de son grand-père. C'est une enquête et une relecture, un retour sur ce passé anonyme et pourtant bouleversant. Le grand-père s'appelle Moïse mais n'a rien de juif. Il est né près de la frontière belge et subit l'invasion allemande de la 1ère guerre. Son père y meurt, comma tant d'autres et il nourrit une certaine haine des Allemands. Pourtant, il rencontre un jour une jeune fille allemande, Hennie, qui devient son amie le temps d'un été. Puis plus rien... La vie suit son cours, il rencontre une autre jeune fille, tombe amoureux, devient papa, est mobilisé à son tour pour la 2ème guerre mondiale. Son couple bat déjà de l'aile, sa femme n'est pas celle qu'il pensait. La vie lui a fait revoir Hennie aussi mais la guerre les a séparés. Il est fait prisonnier, envoyé en Allemagne, croit se rapprocher d'elle mais non. Jusqu'au jour où...

La BD alterne la retranscription de ces lettres et la vie de l'auteur aujourd'hui, sa relation avec le père qui va se reconstruire autour de l'histoire de ce grand-père, autour de cet amour impossible. C'est d'abord une merveilleuse histoire d'amour et une recherche du passé. C'est aussi une histoire entre générations, une relation père / fils, ou même grand-père / père. L'alternance des styles et des histoires m'a gêné au début car je ne comprenais pas le rapport; il y avait des fins trop abruptes dans ces parties. Le style du dessin change aussi mais c'est très bien comme cela avec du marron et du bleu pour les lettres, de la couleur et un style plus moderne pour aujourd'hui. Peu à peu j'ai continué parce que je voulais en savoir plus sur Moïse, sur son destin. Je voulais aussi revoir cette Hennie si touchante. C'est une histoire de guerre aussi, les drames, les massacres, les bombes, les familles brisées et la haine tenace. Cela m'a encore renvoyé à des histoires personnelles, à des personnes disparues de nos vies passées.

Et d'un coté, j'ai envie de lire ce livre de Baptise Beaulieu, de l'autre il me fait peur pour les larmes que je pourrais y verser. Car il y a aussi de quoi en verser, tout autant que quelques sourires pour les joies qui sont aussi dans les pages.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/entreleslignes2.jpg)
