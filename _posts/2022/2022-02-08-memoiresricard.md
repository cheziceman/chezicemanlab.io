---
layout: post
title: Littérature - Carnets d'un moine errant (Mémoires) de Matthieu Ricard (2021)
category: litterature 
tags: 2020s, autobiographie, spiritualité, religion, mémoires, bouddhisme
---

**Voilà un livre que je ne recommanderai pas à tout le monde parce qu'il nécessite de prendre son temps. Je sais que dans notre agitation permanente, c'est peu courant, mais est-ce que ça ne vaut pas le détour?**

Matthieu Ricard, surnommé par certains "L'homme le plus heureux du monde", est ce moine bouddhiste français, proche du Dalaï Lama et du bouddhisme tibétain que l'on voit depuis quelques années parler bonheur, végétarisme, animaux, etc... On sait parfois qu'il est le fils d'un philosophe (Jean-François Revel) et d'une artiste peintre (Yahne Le Toumelin). Une famille aisée, des amis célèbres, une enfance sans problème, des études brillantes menant à un doctorat à l'Institut Pasteur qui auraient pu l'amener à des recherches en génétique. Mais soudain (enfin pas si soudainement...) une envie de partir en Inde, puis de rejoindre un maître dans la région du Darjeeling...Voilà un peu du début de l'ouvrage en accéléré. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/memoiresricard.jpg)

Mais cela serait lui faire injure de résumer les près de 800 pages par une mise en bouche. Ce sont des mémoires (pas toujours rigoureuses dans les détails techniques ou historiques mais qu'importe) et à travers elles, nous suivons cette philosophie de vie qui l'a amené où il est aujourd'hui. Il explique ses interrogations de jeunesse, sur le sens à donner à la vie notamment. Il décrit sa découverte de ce pays, ce qu'il a laissé comme luxe, sa retraite d'ermite, la découverte des méditations. Mais ne croyez pas que c'est une publicité pour le bouddhisme. Il parle des différences culturelles entre orient et occident et de notre approche souvent trop rapide de la spiritualité. Avec d'ailleurs un aparté sur ces faux gourous. 

Il faut donc du temps, se poser pour lire, se perdre parfois dans les méandres du récit pour retrouver ensuite le fil principal. J'ai mis quelques jours et une bonne centaine de page pour arriver à "m'installer dans le récit". Aussi étrangement que cela puisse paraître, cela s'est fait après une période de stress, où j'ai retrouvé un peu de sérénité. Il n'y a pas de fil chronologique réel, ni de thématique claire à chaque fois mais plutôt des suites d'anecdotes réunies autour d'un thème, d'une personnalité, etc...On n'y trouvera pas tout ce qui concerne le Dalaï Lama en un seul endroit, ni même ce qui concerne les grands maîtres du bouddhisme tibétain qu'il a pu côtoyer. On ressent pourtant bien ces passages comme des hommages à ce qu'ils lui ont donné (et donné aux autres).

En fait, j'y vois le prolongement de sa propre religion où l'on comprend qu'il y a des rituels, des prières, des méditations mais que tout cela vise à se rapprocher de l'éveil. Là aussi, c'est une suite d'enseignement, de patience, de rituels sous la forme de la lecture et qui construisent ce tout que sont ces mémoires. On peut décider de tout prendre avec patience, en se perdant parfois. On peut aussi rechercher des éléments précis qui nous intéressent à un instant, aller dans le désordre,... Spirituellement, j'ai trouvé cela intéressant à défaut de parvenir à me captiver comme un roman classique ou une autobiographie, du fait des vocables religieux très nombreux, par exemple, ou des noms qui parfois se ressemblent. Sans doute pas son livre le plus accessible mais un élément essentiel dans son cheminement.

