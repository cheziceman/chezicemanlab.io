---
layout: post
title: BD - Fleur de l'ombre de Kazuo Kamimura (1976)
category: bd 
tags: manga, seinen, féminisme, japon, 1970s
---

**Voilà encore une réédition d'un classique du manga et qui tombe plutôt bien dans notre époque. Il nous parle de Sumire, une jeune femme célibataire dans le Japon des années 70.**

Déjà, être une femme célibataire dans le Japon moderne n'est pas simple. Mais en plus, Sumire est la fille illégitime d'un homme politique. Sa mère, abandonnée par son amant, a sombré dans une sorte de folie. Sumire se jure de ne pas vivre comme sa mère et de ne pas être le jouet des hommes. Mais sa seule arme est sa beauté et son corps. Son emploi dans un grand magasin ne lui permettra pas de sortir de sa condition modeste. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/fleurdelombre0.jpg)

Regroupée en 2 gros volumes de près de 500 pages, cette petite série est bien un seinen. Il y a du sexe, de l'alcool, et il y a quelques liens avec le cinéma japonais de ces années, notamment un certain Oshima. Kazuo Maimura (qui inspira Tarantino par ailleurs) est bien un auteur de son temps et pourtant reste tout à fait moderne dans le Japon d'aujourd'hui. La condition féminine n'a pas tant évolué que cela quand on sait, par exemple, qu'il n'y a que moins de 10% de députées femmes à l'assemblée nationale japonaise. Son dessin est très épuré dans ses personnages tout en ayant beaucoup de détails et de trames dans les décors. Et parfois c'est plus proche de l'aquarelle avec des dégradés et volumes magnifiques.

Alors cette histoire sent le souffre, c'est vrai. C'est même violent et parfois sordide. Il y a ce contrat avec ce vieil homme que passe Sumire, par exemple. Il y a la rencontre avec le musicien aveugle, à la fois amant, père qu'elle n'a pas eu, guide. Et puis un viol... On voit Sumire sensible et froide à la fois, tentant de ne pas succomber à l'amour pour garder ses objectifs et s'émanciper enfin, ou de se blinder face à la violence des hommes. Il y a un esprit de vengeance dans ses actes. Dans le premier tome, on peut l'opposer à sa collègue plus exubérante et moins jolie, qui tente elle aussi de prendre son envol. Les regards les plus cruels et méchants sont souvent ceux des autres femmes, que l'on voit dans des rôles subalternes, servantes serviles. 

Tout le paradoxe est autour de ce statut d'amante, de femme entretenue, le seul qu'on lui laisse finalement. Mais contrairement aux autres femmes de son époque qui courent après le mariage idéal qui les mette à l'abri du besoin, elle se fixe ses propres règles. Et souvent rompt lorsque l'on croit qu'elle est sur le point de succomber. Une histoire très moderne donc qui s'inscrit aussi dans une lutte féministe très japonaise. J'y ai retrouvé par exemple de ce que j'avais lu dans "[La Valse sans fin](https://www.cheziceman.fr/2020/valsesansfin/)", cette romance très rock'n roll dans la même époque. Ou bien encore dans "Homo Japonicus" de la sociologue française Muriel Jolivet. 

Outre les indéniables qualités esthétique des deux ouvrages, voilà qui intéressera ceux qui sont sensibles à ce sujet, sensible au Japon et à sa complexité.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/fleurdelombre1.jpg)