---
layout: post
title: Réflexion - Minimalisme numérique
category: reflexion, geek 
tags:  essai
---

**Le numérique rend bien des services et a pris une place considérable dans nos vies...et dans nos poches. Mais ça devient beaucoup trop imposant, au sens propre et figuré**

Cette impression, je l'ai depuis un moment mais c'est quand je regarde la taille des smartphones dans les mains, et leur omniprésence, que cela saute aux yeux. Je n'ai jamais été suiveur sur les tendances souvent stupides dont on revient très vite. Mais en l'espace de 3 ans, les téléphones ont pris 2,5cm de diagonale en plus, ce qui n'est pas sans conséquence. Pourtant je suis grand, avec des grandes mains et sans doute des grandes poches. Mais je suis aussi de l'avis qu'avait Steve Jobs sur les téléphone : Ca doit pouvoir se manier d'une main. Aujourd'hui, on ne trouve rien en dessous de 6,5" dans les entrées de gamme, rien en dessous de 5,8" sur le reste...et ça donne un pavé de 170mm de haut et 70mmm de large la plupart du temps, ce qui n'est pas sans gêner dans une poche et même un sac. En dehors de l'Iphone SE à 600 Euros, il ne reste qu'un seul modèle en dessous de la barre de 150mm de haut et ce n'est pas une flèche : A peu près équivalent à un milieu de gamme d'il y a 2 ou 3 ans. Sinon, il faut faire dans le reconditionné.

#### Le minimalisme

J'en suis adepte sans excès. Ok, j'ai eu le Sony-Ericson X10 mini, le plus petit smartphone de l'époque et presque de tous les temps. Je regrette encore cet engin génial malgré ses limitations et lenteurs. A l'époque, Sony savait encore faire le job pour les ROMS...parce que Ericson ? Puis je suis passé dans un smartphone baptisé compact à l'époque mais qui était déjà dans les 4,5" et 130mm de haut. Ca faisait alors vraiment tout ce que je voulais. Mais au niveau du prix, c'était astronomique ...quoique quand je regarde ce qui se vend en reconditionné, j'hallucine. La plupart finirons à regarder netflix, faire une vidéo pour tiktok ou jouer à Candy crush. J'ai ensuite réduit la voilure en terme de puissance pour rester dans le raisonnable en taille. 5" tout de même et encore 10mm de plus de pris en hauteur. Ca passe encore mais parfois ça gène dans la poche. Ah oui, je parle de la poche avant car pas question de mettre ça derrière avec la torsion qui peut s'opérer et donc la casse très probable des éléments. J'ai regardé la durée de vie de nos smartphones à madame et moi : au pire, 3 ans, au mieux, 6 ans, record en cours. Car plus on fait grand et fin, plus physiquement, même avec le renfort de métal, ça aura tendance à se tordre, se plier. Et le verre, même du gorilla, ça ne se tord pas très bien, de même qu'un écran LCD. Faire petit, ça parait contraire à une société qui glorifie l'imposant et le clinquant. Comme la voiture, le smartphone devient un signe extérieur de richesse et en changer aussi. Profondément débile... On me disait déjà : Tu fais quoi avec ton mini ? Et bien je m'use les yeux, pardi...Non, je faisais ce que ça me permettait, tout simplement, soit à peu près ce que je vais décrire après, sauf le GPS car trop petit pour voir la carte et lire certains ouvrages ou sites. Mais tous les autres ensuite ont su tout faire.

#### Oui mais pour faire quoi ? 

de quoi ai je besoin sur mon smartphone :
1. Téléphoner
2. Envoyer/recevoir des SMS
3. Envoyer/recevoir des mails
4. Aller surfer sur le web
5. Prendre des photos et faire de petites retouches
6. Ecouter de la musique
7. Lire un Livre en epub
8. Lire une BD
9. Avoir le GPS pour me diriger avec l'info-trafic

Ca vous parait trop encore ? 
Pour les 2 premiers c'est le rôle d'un téléphone.
Pour le 3ème et le 4ème, ça pourrait attendre le retour sur un pc..ou pas?
Pour le 5ème, il y a des appareils photos pour ça.
Pour le 6ème, il y avait des baladeurs pour ça.
Pour le 7ème, il y a des liseuses pour ça.
Pour le 8ème, il y a des tablettes pour ça.
Pour le dernier, il y a des GPS pour ça.

Je sais qu'on va me dire qu'il manque la lecture de vidéos, voir des séries en streaming, etc... Euh, sérieux, vous faites ça sur un truc de 10 à 15cm de diagonale ? Et c'est à moi qu'on dit qu'il faut des bons yeux...Non mais sur une tablette encore ou un pc, je ne dis pas, mais pour vraiment profiter, je veux un vrai écran, un son qui envoie plus que n'importe quel intra pour me plonger dans cet autre monde. Et pas dans les transports en commun où l'on me bouscule. Pour ça j'ai des livres avec des mots qui font des phrases. Vieux con, vous croyez ? On verra dans 10 ans quand tu auras la thune pour du bon son...Bref, chacun a effectivement une utilisation qui lui est propre mais la mienne c'est celle là et je suis certainement entre deux eaux en disant cela. Mais j'entends déjà le commentaire de mon collègue Anatole qui me dira qu'un téléphone ça téléphone et qu'un PC ça...enfin bref, un matos - un usage, si besoin. Ça se discute.

Et donc si on ajoutait tout ces matériels séparément, vous voyez le gâchis électronique quand un seul périphérique peut tout faire? Le minimalisme numérique n'est pas forcément là où l'on pense. A la limite, les livres et BD, tu peux le faire en papier ? Oui mais bon, j'ai d'un coté la bibliothèque et de l'autre j'ai aussi moins à transporter, moins à jeter aussi si je ne trouvais pas preneur en le laissant en don, bref niveau bilan écologique, ça se discute. Le GPS cela a été longtemps un problème pour les mises à jour. Un peu moins maintenant mais tout de même, la durée de vie du précédent GPS ne fut pas si extraordinaire que cela avec sa base de hardware de PocketPC.

![image](https://upload.wikimedia.org/wikipedia/commons/2/2f/BakanovPalekhKhokhloma.jpg)

*Palekh miniature par Ivan Mikhailovich Bakanov (1929). (Wikimedia)*

#### Des Alternatives ? 

Evidemment, après avoir défini ce besoin, j'ai regardé si je pouvais faire ça avec autre chose qu'un Smartphone. J'ai donc regardé ce qu'il y avait dans des gammes de "Feature phone", c'est à dire ce qui existait avant les smartphones, avec ou sans clavier. Autant dire que c'est assez catastrophique avec des dérivés de dérivés de Firefox OS qui sont lents, chers pour ce qu'il y a dedans et en plus bugués. D'ailleurs, les pays émergents ne s'y trompent pas en boudant cela pour passer à des entrées de gamme de pas si mauvaise qualité ou des marques en mal de reconnaissance. En recherchant je suis aussi tombé sur un projet que l'on appelle le Lite Phone. Ils en sont au deuxième modèle et l'objectif avoué n'est pas forcément le minimalisme mais le décrochage numérique. Donc être moins solicité par des notifications, des écrans lumineux etc...Pourquoi pas. Mais quand on voit ce truc en e-ink être aussi limité et vendu à 300 dollars, c'est du foutage de gueule pour bobo californien en mal de sensation. Si au moins l'idée peut être conservée mais réalisée avec un brin de réalisme économique, ça sera bien.

#### Alors c'est foutu ? 

Euh...Pratiquement oui, comme ce monde. OK, j'exagère puisque j'ai trouvé un modèle d'un constructeur chinois connu il y a quelques années qui reste dans une taille raisonnable, avec un processeur correct, suffisamment de mémoire, la dernière version d'Android et tout ça dans une taille d'écran de moins de 5,5" et une hauteur de moins de 142mm. Ouah-ou, l'exploit. OK, niveau qualité, c'est l'entrée de gamme mais avec plus de support qu'un Wiko. Pourtant, après ce modèle, le déluge ou sinon une pomme dorée sur tranche. Les modèles phares reviennent pourtant pour certains à des 5,8" pour des tarifs égaux à un Smic et une profusion d'objectif au dos laissant penser que le smartphone a attraper la maladie qu'avaient les appareils photos il y a une décennie, la pixelite ou l'objectivite. Ou ça se soigne donc je peux me dire qu'un jour on reviendra à la raison, ou alors nous aurons tous changé de garde robe pour avec des grandes poches dans des pantalons et short bouffants ou des sacs à main XXL.

Le marché de l'occasion sinon, ce n'est hélas pas comme les PC. J'ai repris un modèle identique à un de nos smartphones de 4,5" en reconditionné dans une petite boite française. Ça a l'air de tenir la route mais à en lire les commentaires, ce n'est pas une science exacte non plus. Il y a le bon et le mauvais reconditionné, bien plus que pour le PC car il est difficile de visualiser toutes les fragilités d'un terminal tordu, pressé, chauffé, refroidi, etc durant sa vie dans nos mains, poches ou sur nos tables et bureaux. Le Fairphone sinon est un bon concept mais introuvable à des prix raisonnables pour ce que c'est, malgré le support présent. Ce qui m'étonne c'est que j'ai l'impression d'une stagnation en terme de fonctionnalités de ces terminaux et que l'on pousse artificiellement au renouvellement. Le minimalisme, c'est aussi la sobriété, faire durer ce que l'on a, recourir lorsque l'on peut à une rom alternative parfaitement optimisé. Ne serait-ce pas revenir à des sortes de "compatibles" pour des OS genre Lineage ? Les grandes marques de PC parlent de reconditionner les pièces mais pour les smartphones, on attend. Et si on reconditionnait notre manière de consommer?


Bande son : [Mini, Mini, Mini ![video](/images/youtube.png)](https://www.youtube.com/watch?v=3J5hA0X6j9k){:target="_blank"}

##### Commentaires

**Gilles par Mastodon**

>J'avais écrit [ça il y a longtemps](https://www.parigotmanchot.fr/2019/01/12/minimalisme-et-pragmatisme/).  
Côté smartphone, j'ai choisi l'inverse des libristes : je suis à fond chez Google, en échange mon smartphone m'est indispensable ET pratique, ne serait-ce que pour la 2FA (authentification double facteur) que j'essaye d'utiliser au maximum. Pour le reste, j'essaye d'appliquer au max le principe KISS, par exemple pas de Nextcloud mais un Filerun qui fait juste le café mais pas le thé ;)

