---
layout: post
title: Littérature - La Vengeance du Pardon d'Eric-Emmanuel Schmitt (2017)
category: litterature 
tags: littérature, nouvelles, thriller, 2010s, 
---

**Je connaissais Eric-Emmanuel Schmitt auteur de roman, de pièces, de scénarios, et même réalisateur mais je n'avais jamais goûté à ses nouvelles.**

Dans ce recueil, il nous en offre quatre, dans une veine plutôt thriller psychologique. Comme on s'en doute dans le titre du recueil, cela va traiter du pardon. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/vengeancepardon.jpg)

**Les Soeurs Barbarin**

Deux soeurs jumelles qui vivent pendant 80 ans jusqu'à ce que l'une d'elle tombe mystérieusement dans un puit dans la demeure familiale. L'une est la bonté incarnée quand l'autre était jalouse de la première. Et c'est pourtant la seconde qui est morte. Quelques pages pour rentrer dans l'atmosphère, dans la succession de flash-backs, et puis j'ai vu une fin se dessiner dans mon esprit tordu...Qui ne doit pas être loin de celui de M. Schmitt. Joliment écrit et amené.

**Mademoiselle Butterfly**

Allusion au Mrs Butterfly de Puccini? Pas directement. Un père banquier découvre que son fils l'a trahi en montant une arnaque. Un fils illégitime que son père reconnut sur le tard pour faire fortune. Il y a des éléments un peu trop capilotractés mais ça fonctionne. Là aussi, du fait du thème annoncé, on peut deviner la fin.

**La Vengeance du pardon**

Une femme déménage pour aller voir un prisonnier malgré l'opposition de ses soeurs. Cette femme c'est la mère d'une des victimes de ce tueur en série. Vient-elle pour pardonner ? Une sorte de fable autour du pardon des crimes et de la recherche du "pourquoi". Efficace.

**Dessine-moi un avion**

Allusion à St-Exupery ? Un peu puisqu'une petite fille demande à son voisin aviateur retraité de lui dessiner un avion. Le fils de cet homme découvre en même temps qu'il a été neonazi pendant des dizaines d'années. Sur cette histoire, on peut entrevoir le pardon de l'histoire. Mais l'histoire a bien plus de surprises que les précédentes. Une réussite.

Les 4 nouvelles autour du thème du pardon sont toutes très bien, même si ma préférence va à la première. Je trouve presque la 3ème trop évidente mais il faut reconnaître qu'Eric-Emmanuel Schmitt sait y faire pour nous maintenir en haleine, nous perdre parfois sur des personnages annexes ou des scènes d'apparence anodines. Un bon moment entre thriller psychologique et mélodrame.

