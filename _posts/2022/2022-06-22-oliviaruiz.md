---
layout: post
title: Musique - Olivia Ruiz - Le Calme et la tempête (2012)
category: musique
tags: musique, 2010s, chanson, pop, 
---

**Seule rescapée valable de la 1ère Star Academy de TF1 à l'époque, elle avait mis du temps à sortir un premier album (La Femme Chocolat en 2005) couronné de succès. Et j'ai mis du temps à chroniquer une de ses oeuvres...en commençant curieusement par son premier [livre](https://www.cheziceman.fr/2021/commoderuiz/). Erreur réparée avec son 4ème album.**

Entre temps, il y a eu son couple avec Mathias Malzieu (Dyonisos) qui donna aussi lieu à 2 albums. On pourrait dire que cet album est celui de la rupture, de l'envol puisqu'à partir de ce moment, on la voit dans des activités diverses et variées (chant, danse, mode, écriture...). Elle s'était exilée à Cuba peu de temps avant l'enregistrement et il n'y a qu'elle pour dire l'influence que cela a pu avoir sur ses titres. Si j'ai choisi cet album plutôt qu'un autre, c'est parce que s'il n'y a pas de gros hit imparable, j'ai ressenti comme une sorte de synthèse entre ses influences, ses expériences passées.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/oliviaruiz.jpg)

Nous voilà face à 12 titres (14 dans l'édition limitée comme il se doit + 2 remixes) qui vont nous emmener dans l'univers de l'artiste. C'est aussi le premier album où elle signe intégralement paroles et musiques. Il faut se souvenir qu'à l'époque de l'émission de télé, elle s'était distinguée en portant un T-Shirt des Têtes Raides, ce qui n'était pas franchement à la mode, ainsi qu'évidemment avec son timbre de voix si particulier qui exprime à merveille son tempérament. L'album commence donc par "My Lomo & Me" (le Lomo est un type d'appareil photo argentique russe qui eu droit à un retour en grâce à cette époque). Il y a un texte, une richesse dans les arrangements, et c'est un titre que j'aurai bien vu aussi chez [Juliette](https://www.cheziceman.fr/2018/juliettejaimepas/), avec qui elle a d'ailleurs collaboré. La rythmique est importante alors que l'on passe dans la sensualité avec le titre suivant "Volver" en français et espagnol. Rien à voir encore avec "L.A. Melancholy" qui commence par des chants d'oiseaux et des chœurs sur une rythmique plus traînante. Elle parle de Cuba, malgré ce que le titre pourrait laisser penser, et aussi d'une nostalgie amoureuse. Là encore, les arrangements donnent plus de sel au titre. 

La Rythmique a beaucoup d'importance dans cet album, ne venant pas forcément que de percussions. "Le Calme et la tempête" me rappelle plus la période "Femme chocolat" avec une importance du texte (qu'on peut interpréter ...), des ruptures de rythme, du caractère dans la voix. Cuba a sans doute apporter un peu, autant que ses racines espagnoles, à "La Llorona" qu'elle interprète avec son père et son frère. Une manière aussi de lui faire profiter de la lumière. On retrouve plus de sons électroniques sur "Larmes de crocodiles" qui me rappelle cette fois la période "Malzieu". Il en est de même pour "Crazy Christmas" qui est cette fois en anglais, sans me convaincre pour l'accent approximatif. J'aime beaucoup "Mon P'tit chat" (qui a dit évidemment...?) à la fois amusant et intéressant musicalement. "Plus j'aime, plus je pique" est il un résumé d'elle même ? Elle parle de pardon, de pulsions... "Question de pudeur" est bien plus rock et cette fois ça rappelle les *Têtes raides*. "La voleuse de baisers" aurait pu sortir du lot mais je ressens comme un manque pour ce titre aux sonorités rétro. Et on termine par "Ironic Rainbow" tout en sensualité, plus jazz. Là encore je suis moyennement convaincu, surtout pour un titre final.

Pourtant, je lui trouve bien du charme à cet album et elle confirmera encore après en créant et mettant en scène des spectacles autour de thèmes qui lui sont chers. Je m'attends encore à bien des surprises à l'avenir.

[Le single de l'album ![video](/images/youtube.png)](https://www.youtube.com/watch?v=zF8ZZSwSUDA){:target="_blank"}
