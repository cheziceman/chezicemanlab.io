---
layout: post
title: Cinéma - La Dégustation de Ivan Calbérac (2022)
category: cinema
tags: film, cinéma, comédie romantique, 2020s
---

**Lorsque l'auteur d'une pièce à succès transpose son oeuvre au cinéma, on peut s'attendre au pire ou au meilleur. Si en plus il s'agit d'une comédie romantique, l'angoisse saisit les plus réfractaires au genre.**

Mais je suis un très bon client du genre, comme madame. Alors avec Isabelle Carré au casting, plus son acolyte du théâtre Bernard Campan, ça semble quand même du sérieux. Nous avons donc Ivan Calbérac à la réalisation qui reconduit en plus son casting de la version théâtre. Ce n'est pas son coup d'essai avec à son palmarès "L'étudiante et Monsieur Henri" ou "Venise n'est pas en Italie". Cette fois, c'est une romance entre deux personnes de 40-50 ans abîmés par la vie. L'un, Jacques, est un caviste solitaire et bourru, l'autre, Hortense, est une "vieille fille" bigote. Le hasard les fait se rencontrer autour d'une bouteille de vin et ... derrière les apparences, c'est plus compliqué.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/degustation.jpg)

Se greffent quelques personnages secondaires, comme l'ami libraire du caviste, son médecin de famille, la mère d'Hortense, les SDF dont elle s'occupe comme bénévoles, l'apprenti "repris de justesse" de Jacques...Le réalisateur a donc repris les acteurs-trices de sa pièce. Rassurez vous, même si ça se passe dans des vignobles autour de Troyes, l'histoire oublie assez vite le vin pour l'ivresse des mots et des dialogues. Calbérac frappe fort dans ses "punchlines" et autres bons mots. Et le casting maîtrisant parfaitement son sujet, ça fait mouche auprès du public. Ce n'est pas si souvent qu'une comédie romantique se déroule avec des cinquantenaires. 

C'est joliment filmé, sans esbroufe. On y rit beaucoup mais on y verse aussi des larmes quand on va dans le tragique ou dans les inévitables obstacles à la romance. On peste sur les réactions des personnages tout en sachant que le genre impose presqu'une Happy End. Un petit film bien réjouissant et agréable, pour les amateurs du genre, ou ceux qui aiment aussi la version théâtre. L'auteur a eu le bon goût d'élargir un peu le spectre de ses personnages et ça fonctionne très bien aussi. 

[Bande Annonce ![video](/images/youtube.png)](https://www.youtube.com/watch?v=oMXZBQ3CUBI){:target="_blank"}
