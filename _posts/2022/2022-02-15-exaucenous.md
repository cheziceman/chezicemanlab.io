---
layout: post
title: BD - Exauce-nous de Bihel et Makyo (2008)
category: bd
tags: BD, 2000s, thriller, amitié, fantastique
---

**Ah, le pouvoir d'une couverture ou d'un titre. J'avoue que je n'ai pas vu le lien entre ce visage dans l'obscurité, presque inquiétant, et le titre de cette BD mais cela m'a poussé à saisir l'album du présentoir. Pas tout jeune pourtant mais la première planche a continué à m'intriguer, à défaut de comprendre.**

Et pas que moi. Frank aussi, cette sorte de héros de l'histoire qui décide de comprendre qui est "Léonard", l'homme de ménage du théâtre, la mascotte du café de la ville, celui qu'on considère comme un simple d'esprit mais qui n'est peut-être qu'amnésique. Qui est-il ? Et autour de lui, il y a Ernest, le luthier dont le fils a des problèmes avec des trafiquants. Il y a Karim, l'amoureux qu'une rencontre transforme. Et Léo qui regarde avec son innocence ce petit monde, qui ne souhaite que le bien.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/exaucenous1.jpg)

Comme je l'ai dit, cette couverture m'a attiré. Et puis le dessin aussi, la mise en couleur, la mise en scène ou en image finalement qui en montre juste ce qu'il faut. Frank est scénariste mais ça tombe bien, il va "écrire" sur Léo, et l'auteur de l'ouvrage est un peu ce Frank aussi. Il ne lui faut que 100 pages mais j'aurais bien continué à flâner dans ces ruelles d'une ville pas vraiment identifiée. Peu à peu l'atmosphère fantastique s'installe. Il se passe quelque chose autour de Léo. Et je me mets à comprendre le titre de l'album. Le regard de Léo est profond, lumineux. Cette lumière qu'il parvient à rendre aussi à son entourage.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/exaucenous2.jpg)

J'ai pensé à la Ligne verte, ce magnifique film où la aussi, même dans la pire situation, il y avait cette magie, cette lumière chez un personnage que tout le monde regardait comme un simple d'esprit, mais aussi un coupable de quelque chose. Faut-il y croire ou pas pour que cela fonctionne ? Faut-il croire à la magie pour que cette BD nous émerveille aussi ? J'y ai passé un moment de grâce, avec des sourires et des larmes. Et c'est si rare dans le neuvième art (comme dans les autres)!
