---
layout: post
title: Veganisme au quotidien - Un automne français
category: vegan, 
tags: recette, cuisine, veganisme, vegan
---

**Je parle souvent de recettes de tous les pays. Mais en France, malgré notre lien à la viande et à la charcuterie, ou même au fromage, il est possible de manger Vegan avec des produits du terroir. Et en plus, ce sont des plats qui réchauffent...mais pas des plats de Noël. Encore que, autour de cette date ?**

**Cassoulet**

Les puristes prendront du haricot blanc cru qu'ils feront cuire eux-même. Il faut pour cela les mettre la veille à tremper dans l'eau froide. Puis on les fait cuire avec thym et laurier dans une cocotte en les recouvrant d'eau froide pendant 3/4 h. On les égoutte et on refait cuire à l'eau avec du concentré de tomate, de l'ail, du thym et laurier... Ou alors, on achète ça pré-cuit en boite pour les plus pressés, en prenant garde de ne pas avoir n'importe quoi dedans, c'est à dire des graisses animales. Du haricot blanc à la tomate, point. Pour la suite, il faut remplacer saucisses, canard, poitrine par des éléments végans. Pour les saucisses, on trouve maintenant des grosses saucisses végans qui ne sont pas trop mal pour ça. On les fera juste revenir à part pour les dorer un peu. On peut aussi mettre du tempeh et du [seitan](https://fr.wikipedia.org/wiki/Seitan), cette préparation typiquement végane qui imite la viande. Ca peut aussi se faire soi-même avec du gluten pur...En parallèle de la préparation des pois. On rajoutera un peu de chapelure sur le dessus pour servir et ça donnera visuellement à peu près ça : 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/veganfrance1.jpg)

(photo de la recette vegan [ici](https://www.chefclub.tv/fr/recettes/daily/cd1600a6-5da0-4bf8-95fd-b8ae311e4d2f/cassoulet-vegan-cassoulet/))

**Choucroute garnie**

Le plus compliqué, dans tout ça, c'est de trouver un bon choux de choucroute sans la moindre graisse animale. Évidemment, il y a des régions où on trouve ça fait maison, fermenté etc...Mais la plupart du temps, on s'en remet à une boite de conserve, un pot en verre. On y rajoutera quelques grains de genièvre, clou de girofle, du vin blanc ou de la bière et des pommes de terre cuites à l'eau. Ne reste plus que la garniture, selon l'envie et le goût puisqu'il paraît difficile de se passer de "mockmeat" (fausse viande).  Qu'on en me parle pas de choucroute de la mer ou autres inventions marketing douteuse. La vraie, c'est celle avec des produits alsaciens...Bon, ok, je me contredit car pour l'accompagnement, je suis obligé de prendre des productions d'outre-Rhin ou d'outre-Quiévrain.

Pour la cuisson, c'est lent et on va d'abord s'occuper de la choucroute. Selon la provenance, on salera ou on poivrera, en plus du genièvre, clou de girofle, dans une cocotte et on mouillera par le vin ou la bière. On couvre et on laisse mijoter une bonne demi-heure. A part, on aura fait cuire des pommes de terre (pelées ou pas...perso c'est pas) à l'eau et on les rajoute au bout de cette demi-heure. On rajoutera un peu d'huile selon le revêtement de la cocotte pour éviter que la choucroute n'accroche. On rajoute alors ce qui remplacera la poitrine fumée, les saucisses. Par chez moi, c'était avec une Morteau, non végane forcément. Là on oublie donc on peut prendre du Tofumé (le Taïfun est excellent), des saucisses viennoises veganes (Wheaty par exemple), du "jambon" vegan (Wheaty aussi), du [tempeh](https://fr.wikipedia.org/wiki/Tempe_(aliment)). Le tout mijotera encore une demi-heure mais le jambon vegan a tendance à sécher à la cuisson ou à se disloquer, donc on le mettra à la fin. C'est aussi pour ça qu'on évitera la cuisson au four qui est pratiquée aussi.

Il reste aussi la solution de faire soi-même ses saucisses végétales mais je n'ai pas encore tenté le coup pour trouver ce qui se rapprocherait le plus de l'original. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/veganfrance2.jpg)

**Pommes de terre sautées à l'ail**

Rien de plus simple que de faire cuire des tranches de pommes de terre assez fine avec de l'huile, de l'ail et du sel. Alors je sais que d'autres font des cubes, des dés, des grosses tranches épaisses ou même des pommes de terre entière striées de coup de couteau...Mais moi, je préfère la tranche fine (moins de 5mm....si vous voulez une mesure, hé hé).Une large poêle garnie d'huile, feu moyen à fort selon votre instrument de travail, et on met les tranches de pomme de terre avec de l'ail finement haché. On remue et mélange pour que tout ça soit bien saisi, en salant et poivrant selon les goûts. On baisse le feu et on couvre pour la fin de la cuisson. Et on peut accompagner ça aussi de saucisses ou autres, mais ça se suffit à soi-même aussi. Un plat dont je ne connais pas l'origine mais transmis de génération en génération par chez moi. Donc à chercher soit dans le centre et un peu plus au sud, peut être? Les plaisir les plus simples sont souvent les meilleurs. Visuellement, je zape car rien d'extraordinaire non plus, même entre les mains des plus grands.

**Blanquette**

Un plat typique et familial dont l'origine est inconnue. Ça peut se faire avec du Tofu mais je préfère le faire avec des médaillons de [soja texturé](https://vegetalisetoi.com/blog/proteines-soja-texturees/), les larges. Mettre les protéines se soja à réhydrater dans 1 litre de bouillon de légumes pendant 15 minutes. Égoutter et réserver.

Faire chauffer 45 ml d'huile d'olive dans une grande casserole, ajouter 40g farine, 1 litre d'eau, mélanger, ajouter le thym, le romarin, le laurier, saler, poivrer, cuire 20 minutes à feu doux, à couvert. Retirer le thym, le romarin et le laurier, puis ajouter les champignons, les carottes, les champignons et les oignons grelots, mélanger, cuire à couvert pendant 30 minutes.

Pendant ce temps, faire chauffer un peu d'huile d'olive dans une poêle, ajouter les protéines de soja puis les faire dorer quelques minutes. Au bout de 20 minutes de cuisson des légumes, ajouter les protéines de soja dorées, mélanger et laisser mijoter les 20 minutes restantes. Ajouter 1 cuillérée de Maïzena diluée. Mélanger et cuire encore 10 minutes à petit bouillon, sans couvercle. Servir très chaud avec du riz. Bon là, je vous laisse le choix du grain. Et ça donne à peu près ça : 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/veganfrance3.jpg)
Et bien maintenant, bon appétit bien sûr ! 
