---
layout: post
title: BD - La Vie devant toi de Hideki Arai (2019)
category: bd 
tags: manga, seinen, littérature, japon, vieillesse, rédemption, 2010s
---

**Pour une fois, voilà un manga en un tome (en France), qui aborde un sujet majeur au japon : La vieillesse. Il le fait en faisant l'adaptation d'un roman de Taichi Yamada, ce qui va nous emmener dans des péripéties que l'on n'imagine pas.**

*Sôsuke, auxiliaire de vie dans la vingtaine, vient de démissionner de l'EHPAD au sein duquel il travaillait, suite à un "incident" avec une des pensionnaires. Mais grâce à Mlle Shigemitsu, assistante sociale quadragénaire, le jeune homme retrouve très vite un travail : c'est désormais sous le toit d'un vieillard connu pour sa mauvaise humeur, monsieur Yoshizaki, que Sôsuke exercera son métier. Pourtant, lorsqu'il le rencontre, le vieillard lui paraît joyeux et charmant.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/laviedevanttoi1.jpg)

L'ouvrage vaut beaucoup par ses non-dits, ce que je pourrais traduire par ses "silences". Il y en a entre les personnages mais il y en a aussi avec l'histoire par rapport à la société japonaise. On croise ici trois générations différentes : Un jeune homme qui ne sait pas vraiment quoi faire de sa vie, une femme célibataire endurcie sans enfant, un vieil homme seul et sans enfants. Tous les trois sont considérés comme des "anomalies" dans une société bien réglée. Et tous les trois ont un secret à cacher. Je vous rassure, nous ne sommes pas dans un thriller des plus classiques et on peut même avoir l'impression de tourner en rond à un moment dans cette routine des soins. M. Yoshizaki n'est pourtant pas avare de surprise, notamment côté spiritualité dans une approche toute japonaise, entre bouddhiste et shintoïsme. 

C'est évidemment un manga de type Seinen (pour adultes) et pourtant Taichi Yamada n'en avait pratiquement pas lu avant d'accepter cette adaptation. J'aime beaucoup l'approche graphique de Hideki Arai avec ses découpes très dynamiques, ses cases sans dialogues, ses "plans de coupe" qui disent parfois plus que les cases avec dialogues. Je parlais de silences...Le dessin est très précis, mais souvent épuré pour se concentrer sur les personnages. On y voyage dans quelques villes japonaises, dans des restaurants, dans le shinkansen, ...Bref, les amoureux du pays aimeront aussi cet aspect. 

J'ai été surpris par le déroulement de l’œuvre et j'aurai pu m'arrêter à une lecture "premier degré" un peu rapide. Comme je le disais, il y a un aspect très social dans ce manga, que ce soit dans le choc des générations, le problème de la vieillesse mais aussi la place de la femme dans la société japonaise, la place du travail. Un manga qui vaut bien une seconde lecture, ou une lecture patiente, parfois même contemplative. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/laviedevanttoi2.jpg)
