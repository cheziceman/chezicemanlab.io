---
layout: post
title: Cinéma - Les Enfants du Paradis de Marcel Carné (1945)
category: cinema
tags: cinéma, film, cinémathèque idéale, 1940, classique, drame
---

**Encore un classique de plus à rajouter dans ma cinémathèque idéale. Malgré son age, c'est un film que je revois avec plaisir pour bien des raisons.**

Il faudrait parler des conditions de tournage puisqu'il fut tourné durant l'occupation allemande. A tel point que l'on aurait pu faire un film sur ce film. Entre les collaborateurs juifs sous pseudonymes, le tournage aux studios de la Victorine à Nice et au théâtre Dejazet de Paris, les restrictions budgétaires, les bombardements, les changements d'acteurs et de production, il aura fallu 3 ans pour arriver à la fin de ce chef d’œuvre. Sa longueur de 3 heures est peu commune ce qui le tronçonne en deux parties, appelées Époques et donc avec entracte, comme au théâtre. Quoi de plus normal pour un film aussi imprégné par le Théâtre. Et c'est surtout encore une œuvre de Marcel Carné - Jacques Prévert avec Jean-Louis Barrault qui incarne le mime Deburau, l'incarnation même de Pierrot, personnage de la Comedia dell'arte.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/enfantsparadis.jpg)

C'est une histoire d'amour, un drame, un témoignage historique : "*Paris, dans les années 1820. Dans la foule présente sur le boulevard du Crime, on suit Garance (Arletty), une femme libre qui se fait d'abord aborder par un jeune aspirant comédien, Frédérick Lemaître (Pierre Brasseur), qu'elle repousse ironiquement avant de rendre visite à son ami, poète anarchisant et truand Pierre François Lacenaire (Marcel Herrand). Elle rencontre ensuite le mime Baptiste Deburau (Jean-Louis Barrault) qui la sauve d'une injuste accusation de vol en apportant son témoignage muet.À la suite d'une bagarre burlesque entre troupe d'acteurs rivales sur la scène du Théâtre des Funambules, Frédérick et Baptiste sont bientôt engagés en tant que remplaçants et Baptiste permet à Frédérick de trouver une chambre au Grand-relais, pension tenue par Mme Hermine (Jane Marken) et où lui-même réside. Baptiste recroise la nuit même le chemin de Garance, qu'il emmène également chez sa logeuse.*

Le casting est sulfureux pour l'époque puisqu'Arletty fréquentait un peu trop l'ennemi. le très fasciste Robert Le Vigan aurait du incarner le rôle de Lemaître mais il s'enfuit à l'arrivée des alliés. Cela n'empêchera pas le succès du film : 4,7 millions de spectateurs ! Mais pourquoi donc ? Pour l'histoire évidemment, les trios amoureux fonctionnant toujours. Pour la qualité de la mise en scène et de son interprétation, cela va sans dire. Les dialogues de Prévert sont incroyables et beaucoup de scènes sont devenues des classiques. La photographie de Roger Hubert et Marc Fossard est somptueuse pour mettre en valeur les décors, mais aussi les costumes de Mayo. Alors bien sûr c'est ancré dans une certaine image de la France, c'est un film historique en costume dans une époque proche des grands auteurs classiques du 19ème siècle. Il y a du Balzac dans ce Prévert-Carné.

Le charisme de Barrault suffit à lui seul de capter le spectateur. L'opposition entre la gouaille d'Arletty et le mime de Barrault est une trouvaille magnifique, à la fois hommage au muet tout comme aux dialogues ciselés. Il fallait un Brasseur au sommet de son art pour ne pas disparaître face à eux. Il fallait aussi un Carné pour ne pas faire sombrer les seconds rôles en spectateurs, ne pas ennuyer durant 3 heures. Ce n'est justement pas du théâtre filmé comme on peut le craindre avec un tel sujet. Mais la vie est elle même un théâtre, et une poésie. Le film mériterait qu'on le diffuse bien plus souvent mais son format n'aide pas à l'ère de la pub et de la perte d'attention.

[La Bande Annonce ![video](/images/youtube.png)](https://www.youtube.com/watch?v=G1Nz-U9znnY){:target="_blank"}
