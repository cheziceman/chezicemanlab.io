---
layout: post
title: Littérature - La Bâtarde d'Istanbul d'Elif Shafak (2007)
category: litterature 
tags: littérature, roman, turquie, saga, famille, 2000s
---

**C'est sans doute mon premier ouvrage de littérature turque. Pourtant il a été écrit en anglais par une femme turque née à Strasbourg, ayant vécu en Jordanie et en Espagne et qui vit à Istanbul et Londres. Ce roman lui valut aussi des poursuites dans son pays.**

Il faut dire que cette double saga familiale traite du délicat sujet du génocide arménien. Mais pas seulement puisqu'il traite de la société turque autant que de la diaspora arménienne et du conflit de générations, un sujet quasi universel. Double saga puisque l'on suit d'une part les Kazanci, Turcs d'Istanbul : "les femmes sont pimentées, hypocondriaques, aiment l'amour et parlent avec les djinn, tandis que les hommes s'envolent trop tôt - pour l'au-delà ou pour l'Amérique, comme l'oncle Mustafa." Et d'autre part, les Tchakhmakhchian, Arméniens émigrés aux Etats-Unis dans les années 20 : "quel que soit le sexe auquel on appartient, on est très attaché à son identité et à ses traditions." De ces deux familles, il y a Asya d'un côté, fille de père inconnue à Istanbul et Armanouch dites Amy de l'autre, fille d'un arménien et d'une américaine du mid-west. Rien de les destinait à se rencontrer...sauf que le beau-père de l'une est l'oncle de l'autre.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/batardeistanbul.jpg)

Pour rappel, le génocide arménien, c'est 1915-1916 et un exode jusqu'en 1920-23. Nous sommes dans les années 2000 et c'est encore un évènement qui marque autant que la Shoah pour les familles arméniennes. Au point de nourrir une haine féroce des turcs dans cette famille, pour les aînés, bien moins pour Amy qui est pourtant ballottée entre la famille de son père, et sa mère qui met cela au cœur de son divorce. Pour Asya, c'est son envie de liberté, de se sortir du carcan de cette famille de femmes (les hommes meurent prématurément....sauf l'oncle Mustafa envoyé en exil). Même sa mère, rebelle de la sororité, n'est pas comme elle voudrait l'être. Elle se cherche dans cette société d'Istanbul, cette Turquie moderne qui est aussi entre envie de modernité et conservatisme, entre religion et pseudo-laïcité.

Car même la famille d'Asya qui ne pratique pas ouvertement l'Islam, reste attachée à des valeurs liées à la religion, du moins chez la grand-mère et une partie des sœurs. Et en même temps, elles pestent sur d'autres aspects de cette religion. On les voit s'intéresser pour part au soufisme, aux rites des derviches, ce qui reste pour certains une déviance religieuse tandis que pour d'autre c'est une orthodoxie qui trouve ses racines aux origines de l'islam. Mais à l'arrivée d'Amy, c'est aussi un bouleversement. Elle arrive avec ses idées sur les turcs, avec son histoire qu'elle raconte. Face à elle, ce n'est pas le négationnisme attendu mais un manque d'histoire ou plutôt une histoire qui commence en 1923 avec la république turque de Mustafa Kemal. C'est l'histoire d'une Turquie alors tournée vers l'Europe plutôt que l'Asie.

Le livre est passionnant et aide à comprendre cette ville d'Istanbul, carrefour pour toujours, présentée comme un "bateau dont on monte et descend" et qui n'est pas représentative de ce pays lui aussi complexe de par son histoire, son environnement géopolitique, ses migrations et ses peuples. C'est aussi un roman, une saga familiale interculturelle qui se lit facilement sans forcément que chacun réfléchisse au poids à donner à l'histoire. Et pourtant, le(s) secret(s) de famille vont peu à peu se dévoiler jusque dans les dernières pages. 

