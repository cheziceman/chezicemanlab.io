---
layout: post
title: Musique - Girlschool - Vaureal 2022
category: musique 
tags: girlschool, live, concert, rock, metal, hard-rock
---

**Il y a des occasions qui ne se ratent pas ! Et quand un groupe aussi mythique que Girlschool passe à moins de 25km de chez soi, on y va ! En prime, il y a Alcatrazz.**

Girlschool, j'ai déjà parlé du groupe [ici](https://www.cheziceman.fr/2016/girlschool/) avec son meilleur album. Des pionnières dans le monde du Métal et du Hard Rock et il n'y en a déjà pas beaucoup. Alors, même s'il manque Kelly Johnson, décédée d'un cancer en 2007, si l'ami Lemmy Kilminster n'est plus là non plus, elles continuent à écumer les salles avec ce Heavy Hard-Rock'n Roll à l'énergie punk. Depuis deux ans, c'est accompagnées des vétérans d'Alcatrazz qu'elles tournent...ou ont essayé avec ce satané virus. Alcatrazz, ce n'est plus vraiment le groupe d'Heavy Metal qu'on a connu dans les années 80, puisqu'il n'y a plus de Graham Bonnet en tournée, ni Yngwie Malmsteen, mais tout de même Gary Shea et Jimmy Waldo, les ex de [New England](https://www.cheziceman.fr/2022/newengland/). Avec un nouveau batteur et un nouvel album, revoilà nos vétérans aussi en piste dans ce Forum de Vauréal toujours dans son ancienne mouture. Alors nous avons voyagé léger pour supporter la chaleur annoncée...et pouvoir prendre quelques photos sans gros matériel.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal.jpg)

Nous avons juste raté le début du set d'**Alcatrazz** mais avec ce quintet, c'est du sérieux. On a de la cover de Rainbow et Schenker, puisque Doogie White y a officié par le passé. Il n'a rien perdu de sa superbe, vocalement. Le duo de choc Shea et Waldo assure joyeusement sa partie sur la gauche de la scène. Et à la guitare, Joe Stump s'éclate comme l'aurait fait son modèle Malmsteen, quand Mark Benquechea envoie du lourd derrière les futs. J'avoue ma préférence pour les covers de Rainbow pour l'aspect mélodique et le chant, tandis que le groupe pratique aussi un très classique Heavy Metal plus speed. Un set bien carré sur tous les points et qui a déjà chauffé la salle, qui n'en avait pas besoin. A noter l'hommage aux amis disparus : Jimmy Bain, Cozy Powell, John Lord et Ronnie James Dio dans un superbe "Temple of the King".

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_16.jpg)

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_15.jpg)

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_14.jpg)

- Grace of God
- Too Young to Die, Too Drunk to Live
- Wolf to the Moon (Rainbow cover)
- Turn of the Wheel
- Take Me to the Church (Michael Schenker Fest cover)
- Ariel (Rainbow cover)
- Jet to Jet
- Sword of Deliverance
- Too Late for Tears (Rainbow cover)
- God Blessed Video
- Vigilante Man (Michael Schenker’s Temple of Rock cover)
- The Temple of the King (Rainbow cover) 

[Et en live ![video](/images/youtube.png)](https://www.youtube.com/watch?v=VbdcsM8DjSU){:target="_blank"}

La salle se vide pour la pause bar ou toilettes, selon les cas. De quoi voir un peu plus le public en majorité cinquantenaire. Le cheveux est gris, épars pour les hommes mais il y a aussi un public féminin, pour ceux qui en doutaient. Du T-shirt pour afficher sa team Hard-rock évidemment avec pas mal de Motörhead, forcément, mais aussi du Kiss, du Rush, du Thin Lizzy, Children of Bodom etc...La fine fleur du métaleux du coin, dira-t-on. Est-ce la génération ? Mais il y avait moins de smartphones en l'air pour filmer le concert et ça, c'est vraiment appréciable. Bon, en même temps, sans ça, difficile d'avoir des extraits. Mais il y avait quelques photographes et vidéastes dans la petite allée avant la scène. Environ 150 à 200 personnes selon les organisateurs, mais qui crient déjà comme 2000, hé, hé !

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_13.jpg)

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_12.jpg)

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_11.jpg)

Il ne faut que deux gros vans (et une voiture?) pour transporter les deux groupes en tournée alors il y a des concessions de faites, comme le partage de la batterie, et le "Drap housse" (private joke) en fond de scène / backdrop à la gloire de notre quatuor féminin. L'équipe de roadies, là aussi mixte, est au taquet. Aussi voit-on Denise Dufort venir régler les positions des éléments de sa batteries avant l'arrivée du groupe. Les **Girlschools** arrivent à grand coup de riffs. Le temps ne semble pas avoir de prise sur elles car tout de suite, leur énergie rock-punk nous prend aux tripes. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_10.jpg)

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_9.jpg)

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_8.jpg)

Pas de surprises pour un set bien réglé de dates en dates avec à peu près tous les albums. Ça commence très fort avec ces hits du début des années 80. A droite, Tracey Lamb s'éclate entre mimiques, danses et levée de jambe, tout en alignant les lignes de basses tonitruantes. Elles ne sont pas les amies de Lemmy pour rien ! A gauche, c'est Jackie Chambers qui fait plus qu'assurer le remplacement de la regrettée Kelly Johnson avec sa guitare blanche. L'alchimie est là entre les filles avec au centre Kim McCauliffe qui commente entre les titres. On rit à ses petites blagues...comme le "Free Sauna" que nous avons dans cette salle surchauffée. Pour la petite histoire, on tourne à l'eau, au soda sauf Tracey qui a préféré tester une bière numérotée bien française.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_7.jpg)

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_6.jpg)


Pas de temps mort dans ce set. Ah ce "C'mon Let's go", ce "Hunter", ce "Hit and run"...Là encore, la machine à remonter le temps fonctionne et on en oublierait presque la chaleur pour sauter comme des gamin(e)s. La bonne humeur des musiciennes est communicative. Les Girlschool ont "nothing to lose" mais n'ont surtout rien perdu de leur énergie. La salle est conquise depuis bien longtemps lorsque Jackie nous fait chanter sur "take it all away", tendant le micro aux premiers rangs. Elles en profitent aussi pour se ré-accorder elles-mêmes. Il y a aussi ces moments d'émotions comme lorsque l'on parle de Lemmy avant la reprise de "Bomber". Kim en aurait à raconter...

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_5.jpg)

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_4.jpg)

Mais c'est déjà la fin alors qu'on en reprendrait bien pour une heure de plus, notamment pour leur fameuse cover du "Tush" de ZZ Top. Une prochaine fois ? "Emergency" n'a vraiment pas vieilli : Ce titre est terrible d'efficacité. Le groupe s'en va...sauf Denise qui donne de la grosse caisse pour encourager les applaudissements avant le rappel. Kim revient empoigner sa Gibson Les Paul aux reflets roses violacés et c'est reparti pour un titre de plus. Ouah, ce qu'on s'est pris encore ! 44 ans de carrière au compteur, tout de même...

- Demolition Boys
- C'mon Let's Go
- The Hunter
- Hit and Run
- Guilty as Sin
- Action
- Future Flash
- Kick It Down
- Nothing to Lose
- Take It Like a Band
- Take It All Away
- Race With the Devil (The Gun cover)
- Bomber (Motörhead cover)
- Emergency
- Screaming Blue Murder

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_3.jpg)

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_2.jpg)

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/girlschoolvaureal_1.jpg)

Mais ce n'est pas totalement fini puisque nous attendons une bonne grosse demi-heure la sortie du groupe pour dédicacer le ticket. Nous y retrouvons aussi de vieilles connaissances forumiques et enfin les deux groupes arrivent, très sympas et disponibles. C'est évidemment ce qui transparaît déjà dans ce concert, professionnalisme et simplicité et c'est si rare. Nous discutons un peu pendant qu'elles enchaînent les signatures de pochettes de vinyl. Le lendemain, elles sont à l'autre bout de la France, pourtant. Alors, promis, si elles repassent, nous y serons encore ! Girlschool a encore ce petit quelque chose d'unique qui fait leur particularité, et pas seulement parce que c'est un groupe totalement féminin. Forcément, ça rejoint aussi la playlist pour un bon moment, comme pour prolonger le concert.

[Et ça donne ça en live ![video](/images/youtube.png)](https://www.youtube.com/watch?v=GxypWZi2tmE){:target="_blank"}
