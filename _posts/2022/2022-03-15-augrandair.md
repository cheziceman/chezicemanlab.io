---
layout: post
title: BD - Au grand Air d'Afro (2015)
category: bd 
tags: manga, seinen, 2010s, camping, nature, japon, shojo
---

**Décidément, le manga aborde tous les sujets. Et avec cette série en 10 volumes (pour l'instant), nous partons à la découverte du Japon d'une manière très particulière.**

En effet, cette série au titre original Yuru Camp△ nous fait découvrir le Japon par le Camping. Mais quand je dis camping, ce n'est pas vraiment comme en France ou même aux États-unis. Ici, pas de mobile-home, de caravane ou camping-car mais de la tente, à la dure. Et en plus, ce sont des jeunes filles qui sont les héroïnes de cette série avec d'un côté Rin, la campeuse solitaire et expérimentée et de l'autre le trio Nadeshiko, Aoi, Chiaki qui découvre et se passionne pour le sujet dans un club. L'histoire commence évidemment autour de l'emblématique Mont Fuji pour ces citadines qui recherchent le contact de la nature, le calme de ces paysages.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/augrandair1.jpg)

C'est tout le paradoxe d'un japon hyper-urbanisé d'un côté et qui cherche à retrouver une nature quasi vierge de l'autre. Nadeshiko est le personnage central avec sa joie communicative, ses maladresses, son enthousiasme et sa gourmandise. Car en plus de nous faire découvrir des spots de campeurs, on mange beaucoup dans cette série, presque autant que dans le [Gourmet solitaire](https://www.cheziceman.fr/2017/gourmetsolitaire/). C'est juste que la nourriture y est plus basique car il faut pouvoir la préparer en campant. 

Ce qui est surprenant dans cette série, c'est qu'elle fonctionne aussi comme un guide des bonnes astuces de campeur. On y apprend tout sur les tentes, les sacs de couchages, comment faire un feu, etc... Avec des spécificités très japonaises parfois. En fin de chapitre, l'auteur nous parle des lieux qu'il décrit avec des photos car tout existe vraiment. Cela devient alors un guide touristique. Et l'on voit justement que la tente a beaucoup de succès au Japon dans ces lieux, tandis qu'en Europe elle est devenue rare. Sans doute que le coté hyper-urbain, la taille restreinte des logements pousse aussi à trouver complètement autre chose chez les adeptes du camping. Par ailleurs, on visite aussi des auberges et bains d'eau chaude qui sont aussi une spécificité du Japon. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/augrandair3.jpg)

Le casting exclusivement féminin rebutera peut-être des lecteurs mais ça ne m'a pas dérangé. On est à la frontière entre le Shojo et le Seinen. J'ai eu un peu de mal à entrer dans le premier chapitre parce que je m'attendais à autre chose. Et puis petit à petit, je me suis intéressé non seulement à l'aspect technique mais surtout à cette visite et cette immersion dans un autre Japon que celui que l'on voit. On y découvre notamment quelques astuces culinaires intéressantes. Mais je pense que cela passionnera bien plus quelqu'un qui s'intéresse à ce pays. Comme tout Manga, la série est en noir et blanc. Mais pour vraiment rêver un peu plus, il y a la version animée (que l'on trouve assez facilement sur le net) pour avoir tous ces beaux paysages en couleur. Le seul véritable problème, c'est l'essoufflement à partir du 4ème volume. Ça fait quand même plus d'une vingtaine de chapitres...

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/augrandair2.jpg)