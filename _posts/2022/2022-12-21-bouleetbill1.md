---
layout: post
title: BD - Boule et Bill 1 de Jean Roba (1959)
category: bd
tags: bd, jeunesse, enfance, humour, franco-belge
---

**Que j'aime à me replonger dans les bandes dessinées de mon enfance. Et les volumes de Boule et Bill y ont une place importante dans mes souvenirs. Souvenirs imprécis puisque je ne me souviens pas vraiment des tomes ou éditions.**

Alors je me suis rafraîchi la mémoire avec ce premier tome qui reprend en grande partie la première histoire de ce duo, écrit avec Maurice Rosy. Nous sommes bien dans un classique de la BD belge ! De la courte histoire du début, on passe à la BD à sketch sur une page qui alimenta le journal de Spirou pendant de longues années jusqu'à la disparition de Roba. Boule est un petit garçon qui vit dans une petite maison de banlieue avec ses parents et son chien Bill. J'ai toujours été surpris par les prénoms mais bon... L'histoire est inspirée du fils de l'auteur et de son chien. Voilà pourquoi tout semble si plausible et réel et en même temps si drôle dès ce premier tome.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/bouleetbill1.jpg)

La parenté avec Spirou, on la retrouve dans la première histoire des "mini-requins" avec un savant fou un peu ridicule qui crée des bateaux téléguidés munis d'armes bien réelles. Le héros est Bill plus que Boule puisqu'il va déjouer tout le plan machiavélique. On trouve aussi une mini-histoire dans ce tome où Bill est peint en jaune et noir façon... Marsupilami, cet animal emblématique apparu à peines quelques années avant.

Les gags fonctionnent encore avec le grand enfant que je suis devenu. Il y a le personnage de Pouf, l'ami de Boule, plus silencieux avec sa frange qui lui cache les cheveux. Il y a surtout le père plus que la mère de Boule, qu'on retrouvera un peu plus ensuite. On retrouve quelques allusions au colonialisme belge à travers un cousin qui fait livrer un trophée. Malgré cela, la BD n'est pas aussi datée que je pouvais le craindre. Bill est impayable avec un certain flegme de cocker et ce sens du gag où il nous prend même à témoin. Je n'ai plus de souvenir précis de tous les albums que j'ai eu entre les mains mais tout est déjà en place dans celui-ci avec le flic, les amis de Boule, les voisins et cela s'agrandira peu à peu.

Je n'avais pas été trop choqué par les adaptations cinéma de cette série. Ok, ce n'était pas non plus extraordinaire mais ça restait dans l'esprit. Au moins le premier. Le format sketch vieillit plutôt bien et c'est vraiment tout public encore aujourd'hui. Il y a le rythme, un dessin d'apparence simple et coloré. On ne se rend pas compte à quel point Bill est parfaitement "animé" avec ses oreilles et ses mimiques. La chevelure rousse de Boule répond en plus au pelage et ça ne doit pas être un hasard. Indémodable ! Donc à relire sans modération. La mode du cocker est passée et tant mieux pour tous ces pauvres chiens d'élevage. J'en connais de nombreux qui attendent dans des refuges d'ailleurs. Et je cherche dans quel tome on sait comment est arrivé Bill...

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/bouleetbill2.jpg)


