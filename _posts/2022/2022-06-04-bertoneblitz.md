---
layout: post
title: Emotions mécaniques - La Bertone Blitz (1992)
category: automobile
tags: automobile, souvenirs, électrique, environnement, concept-car
---

**J'ai parlé voitures polluantes, bruyantes mais il y en a une qui m'a marquée par sa vision d'avenir. Un concept-car venu d'Italie : La Bertone Blitz**

J'avais découvert ce concept-car lors d'une retranscription du salon de Turin 1992, un salon qui parlait déjà d'environnement. La Carrozzeria Bertone, maison mythique de cette ville, présentait cet étrange roadster futuriste et épuré avec la particularité d'être électrique. Mais les chiffres annoncés étaient élogieux pour l'époque : 0 à 100km/h en 6s, une recharge complète de 4 à 6h pour un véhicule pesant 650kg, avec des batteries plomb acide et une autonomie de 120km environ. Il faut se souvenir que sur le marché, il n'y avait rien, sinon une future Peugeot 106 électrique à l'autonomie similaire voire moindre et aux performances la réservant à une utilisation urbaine. Pour Bertone, on annonçait une vitesse de pointe de 130km/h avec seulement 36ch de puissance.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/bertoneblitz1.jpg)

Un an plus tard, j'étais dans les jardins de Bagatelle dans le bois de Boulogne pour le concours d'élégance Louis Vuitton. Il y avait de belles carrosseries classiques, des princes et comtesses, et quelques concept-cars de l'année passée...dont cette petite voiture. Car elle ne fait que 3m25 de long, est très basse et faite de résine et plastique pour garder son poids contenu. La particularité de ce concours est que ce n'est pas qu'une exposition statique mais qu'on voit rouler à coté de nous ces belles machines qui viennent ensuite sur le podium avec les commentaires qui vont bien. Si en photo elle ne rend pas forcément bien, en vrai à coté de soi, elle faisait de l'effet. En roulant, il y avait ce petit sifflement du moteur électrique, cette capacité d'accélération sur les quelques dizaines de mètres possibles et les reflets métalliques de sa carrosserie grise. Il y en avait une deuxième variante en jaune mais pas présente lors de cet évènement. L'avant était différent selon que l'on mettait cette sorte de bouclier ou pas. Un peu d'esprit Lotus qui n'était pas pour me déplaire.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/bertoneblitz2.jpg)

A quoi ça sert? Cela servait à montrer la capacité ingénierie de l'entreprise Bertone en plus du simple design. Face à des Pininfarina, des Ital Design, des Magna-Steyr, la concurrence était déjà rude. Si rude que la société sera en faillite en 2014 après la mort de Nuccio Bertone en 1997, celui qui donnera ses lettres de noblesse à l'entreprise. La Blitz n'a pas eu de suite et la société s'est perdue dans des projets en Asie qui permirent l'émergence de l'automobile chinoise, notamment. Elle montrait pourtant une voie qui n'a pas été suivie, celle d'une structure revue pour optimiser le poids, ne pas consommer trop et dimensionner tout au juste nécessaire.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/bertoneblitz3.jpg)

Je n'ai hélas pas trouvé de vidéo pour la montrer en mouvement.
