---
layout: post
title: Musique - Juliette Armanet - Brûler le feu (2021)
category: musique 
tags: musique, pop, chanson, 2020s
---

**Le rétro n'est pas qu'une mode dans le jeu vidéo ou le cinéma. Ca fonctionne aussi en musique. Juliette Armanet surfe sur cette tendance, sans rien inventer, essayant juste d'être à la hauteur de ces glorieu(se)s ainé(e)s.**

J'ai découvert l'existence de cette chanteuse lors d'une prestation au Festival de Cannes il y a 3 ans sans être convaincu par sa vision de Michel Legrand. Je la voyais comme une énième artiste parisienne mise en avant par l’intelligentsia du cinéma. Et de la musique...car elle remporte une victoire de la musique en 2018. Un peu plus tard, je l'ai entendu sur du Véronique Sanson et c'était déjà bien mieux, sans atteindre le modèle. Et puis mes oreilles sont tombées sur un extrait de ce deuxième album. Voilà que cela a réveillé des souvenirs...

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/armanetbruler.jpg)

Je disais qu'elle n'a rien inventé mais qu'elle a pris des idées, des styles parmi ses idoles, les sons qu'elle a capté dans ses écoutes récentes ou anciennes et en a fait autre chose. Aussi, cet album est un peu funky, disco, tout en étant très fidèle à la chanson française traditionnelle. J'ai cité Sanson. Je pense aussi à Michel Berger, autre artiste qui a su faire groover la langue française comme son ex-comparse. Elle a souvent parlé de Souchon, Bashung, je n'ai pas vu de référence évidente. Un peu de Barbara dans la voix parfois. Quelques emprunts à Vladimir Cosma, peut-être. Et tout ça nous donne un album qui parle d'amour, qui fait danser, qui fleure bon une nostalgie plutôt heureuse.

Car c'est vrai que je sens l'ombre d'une Sanson sur *Dernier jour du disco* en plus funk, mais pour la voix, c'est plutôt France Gall. *Sauver ma vie* a bien quelques effets pour rendre la voix plus aérienne mais c'est un sacré bon titre aussi. Le piano-voix fonctionne aussi très bien sur l'émotion avec le très beau *Le Rouge aux joues* où la voix est aussi sur un fil. Berger aurait sans doute aimé *Tu me play* où la production reprend des sonorités électro-rétro. Ou bien *Vertigo* qui reprend quelques phrases musicales au modèle. Et Barbara rode dans *Imaginer l'amour* sans atteindre le niveau du modèle. Peut-être trop imité ? Je préfère *Qu'importe* et sa rythmique dansante et sa nostalgie amoureuse. La deuxième moitié de l'album révèle quelques faiblesses tout de même ou des recours à la facilité (*J'te l'donne*). La conclusion en piano-voix sur *Je ne pense qu'à ça* est en crescendo mais j'aurai attendu un titre plus fort pour conclure un joli album. Même la voix plus cassée... Toujours difficile de conclure.

On parle souvent du pouvoir des souvenirs d'enfance pour faire un hit. Et Juliette Armanet a trouvé une très bonne recette qu'elle applique avec talent dans les compositions comme dans l'interprétation. Et même les clips s'y essayent, alors...

[Qu'importe ![video](/images/youtube.png)](https://www.youtube.com/watch?v=ZIVmnpqjHa4){:target="_blank"}
