---
layout: post
title: La Revue de Médias S03E11, Novembre 2022
category: geopolitique 
tags: géopolitique, culture, environnement
---

**Le mois de Novembre serait presque normal après toutes ces chaleurs...2 degrés de plus à peine chez moi. Mais la marche d'un monde de 8 milliards d'individus reste chaotique. Et je suis déjà sur l'évolution de ce feuilleton mensuel...**

*Comme toujours, vous pouvez ouvrir les liens dans d'autres onglets par utilisation de la touche CTRL ou bien appui long sur smartphone. le (EN) pour les articles en anglais, et l’icône coups de cœur ![cc](/images/coeur.png)*


### Géopolitique


* **Afrique** : 
	* Le *Rwanda* joue depuis quelques années un jeu trouble en Afrique centrale. la [RDC le fait savoir.](https://t.co/jxu5wD9MyM) mais les combats se [poursuivent](https://www.rfi.fr/fr/afrique/20221111-rdc-les-combats-se-poursuivent-entre-l-arm%C3%A9e-congolaise-et-le-m23-dans-le-nord-kivu) avec le M23 sur fond d'appropriation des ressources.
	* Une fois encore des pays africains luttent contre le diktat des prix sur les matières premières. Ici le [Cacao](https://www.rfi.fr/fr/afrique/20221110-c%C3%B4te-d-ivoire-et-ghana-lancent-un-ultimatum-aux-multinationales-du-cacao), par exemple. Cette spéculation générale pèse sur les [importations alimentaires](https://www.rfi.fr/fr/%C3%A9conomie/20221111-les-d%C3%A9penses-mondiales-des-importations-alimentaires-bondiront-de-10-en-2022-estime-la-fao).
	* L'*Egypte* accueillait la COP27 mais surtout des [lobbies](https://www.rfi.fr/fr/afrique/20221114-les-lobbys-s-invitent-%C3%A0-la-cop27)...pour un résultat anecdotique.

* **Asie et Pacifique** : 
	* Le *Vietnam* essaie de tirer son épingle du jeu dans le conflit lié aux [composants électroniques(EN)](https://asiatimes.com/2022/11/vietnam-arming-up-to-serve-in-us-chip-war-on-china/) entre USA et Chine. Les [Philippines(EN)](https://asiatimes.com/2022/11/marcos-jr-looks-to-china-for-money-us-for-muscle) de Marcos Jr jouent aussi un double jeu entre argent et [force(EN)](https://asiatimes.com/2022/11/harris-offers-marcos-more-muscle-to-counter-china/). Les rencontres [Chine-USA](https://www.rfi.fr/fr/asie-pacifique/20221122-le-secr%C3%A9taire-am%C3%A9ricain-%C3%A0-la-d%C3%A9fense-a-rencontr%C3%A9-son-homologue-chinois-au-cambodge) se succèdent.
	* Le Caucase (Eurasie) est encore une des régions "chaudes" du monde. L'*Arménie* ne sait plus chez qui trouver des [alliés](https://www.courrierinternational.com/article/accord-sous-influence-americaine-l-armenie-tournera-t-elle-le-dos-a-la-russie). La Russie est encore [médiatrice](https://www.rfi.fr/fr/europe/20221031-haut-karabagh-la-russie-en-m%C3%A9diateur-du-conflit-entre-l-azerba%C3%AFdjan-et-l-arm%C3%A9nie) avec l'Azerbaîdjian. 
	* Les Rohingyas n'ont toujours [nulle part](https://www.courrierinternational.com/article/refugies-les-rohingyas-n-ont-nulle-part-ou-aller) où aller alors que la Birmanie semble s'enfermer encore plus dans une [guerre civile](https://www.mediapart.fr/journal/international/141122/martyrisees-par-la-junte-les-plaines-centrales-de-birmanie-s-engagent-corps-perdu-dans-la-resistanc).
		
* **Amérique du nord** :
	* Une interrogation sur la [marche du monde(EN)](https://t.co/lYgHArIMuJ) de la part de Stephen Walt. Combien de chocs le monde supportera?
	* L'ONU condamne pour la trentième année le [blocus](https://www.courrierinternational.com/article/le-chiffre-du-jour-l-onu-condamne-l-embargo-americain-contre-cuba-pour-la-trentieme-annee-consecutive) de *Cuba* par les USA... Mais ça ne change rien, pas plus que l'élection de Biden. Et le retour de [Trump](https://www.courrierinternational.com/article/etats-unis-trump-grand-perdant-des-elections-de-mi-mandat) n'est plus si sûr, malgré une candidature précoce.
	
* **Amérique du sud et centrale** : 
	* L'information du mois, c'est la [victoire de Lula](https://www.rfi.fr/fr/am%C3%A9riques/20221030-br%C3%A9sil-pr%C3%A9sidentielle-luiz-inacio-lula-da-silva-%C3%A9lu-pr%C3%A9sident-troisieme-fois-jair-bolsonaro) face à Bolsonaro au *Brésil*. Pourtant le pays sort [abîmé](https://www.courrierinternational.com/article/defaite-au-bresil-le-bolsonarisme-continuera-a-vivre) et partagé comme jamais de ces 4 ans.
	L'autre grand pays du continent, l'*Argentine* est toujours embourbée dans ses [problèmes financiers](https://www.courrierinternational.com/article/pouvoir-d-achat-l-argentine-lance-un-plan-prix-justes-contre-l-inflation).
			
* **Europe** : 
	* De par sa position, la *Turquie* est un interlocuteur incontournable dans le conflit russo-ukrainien...et pour les conséquences sur les [céréales](https://www.rfi.fr/fr/europe/20221031-accord-sur-les-c%C3%A9r%C3%A9ales-l-insoluble-question-de-la-s%C3%A9curit%C3%A9-des-navires). Mais c'est aussi un pays exposé au [terrorisme](https://www.rfi.fr/fr/europe/20221113-turquie-explosion-meurtri%C3%A8re-en-plein-c%C5%93ur-d-istanbul), tant interne qu'externe. Ce qui peut devenir aussi un prétexte à des frappes sur les différentes [entités](https://www.rfi.fr/fr/moyen-orient/20221124-l-arm%C3%A9e-turque-poursuit-ses-bombardements-contre-des-positions-en-syrie) du Kurdistan.
	* L'arrivée de l'extrême droite en *Italie* fait craindre l'utilisation de [lois](https://www.liberation.fr/international/europe/en-italie-le-gouvernement-meloni-cible-les-rave-partys-lopposition-craint-une-attaque-plus-large-20221103_DHV2TGDJKFAQFHXZTZKRJYOJQA) pour de mauvais prétextes. Cela marque aussi la politique migratoire mais le [ministre](https://www.rfi.fr/fr/europe/20221109-migrants-l-ocean-viking-en-route-vers-la-france-apr%C3%A8s-une-interdiction-d-accoster-du-gouvernement-italien) de l'intérieur français joue sur des thèmes de l'extrême droite avec la vie d'humains. Ce n'est pas le seul puisqu'en [Suède](https://www.rfi.fr/fr/europe/20221109-su%C3%A8de-le-gouvernement-annonce-une-baisse-des-fonds-allou%C3%A9s-%C3%A0-l-aide-internationale) on réduit l'aide internationale. Le sujet [migratoire](https://www.lepoint.fr/economie/281-millions-de-migrants-dans-le-monde-09-11-2022-2497034_28.php) continue de diviser l'Europe même si on tente des [accords](https://www.rfi.fr/fr/%C3%A9conomie/20221111-les-d%C3%A9penses-mondiales-des-importations-alimentaires-bondiront-de-10-en-2022-estime-la-fao) avec des états.
	* Le conflit *Kosovo-Serbie* continue de se pourrir et ce n'est pas toujours la [faute](https://www.rfi.fr/fr/europe/20221121-apr%C3%A8s-l-%C3%A9chec-des-discussions-serbo-kosovares-%C3%A0-bruxelles-l-ue-bl%C3%A2me-pristina) de la Serbie.
			
* **Moyen-orient** : 
	* En *Israel* les élections ont vu le retour de [Netanyahu](https://www.courrierinternational.com/article/israel-netanyahou-vainqueur-des-legislatives-ou-otage-de-l-extreme-droite) avec l'extrême droite. Cela n'ira donc pas dans le sens de l’accalmie avec une autorité palestinienne déjà discréditée. Et les [colons](https://www.lemonde.fr/international/article/2022/07/25/en-cisjordanie-la-violence-des-colons-israeliens-explose_6136069_3210.html) s'en donnent à cœur joie sous la protection de l'armée israélienne.
	* Le *Liban* continue de subir. Après le départ du président [Aoun](https://www.lemonde.fr/international/article/2022/10/31/au-liban-le-crepuscule-du-general-aoun_6148061_3210.html), c'est toujours l'incertitude politique. Mais c'est aussi une épidémie de [Choléra](https://www.lemonde.fr/planete/article/2022/11/05/dans-le-nord-du-liban-des-infrastructures-defaillantes-aggravent-l-epidemie-de-cholera_6148587_3244.html) qui dure. [L'aide accordée](https://www.rfi.fr/fr/moyen-orient/20221121-le-programme-alimentaire-mondial-va-accorder-5-4-milliards-de-dollars-d-aide-au-liban) tombera-t-elle dans les mains des corrompus ou aidera-t-elle enfin le peuple libanais ? 
	* En *Iran*, on se demande ce qui pourrait précipiter la [chute](https://www.francetvinfo.fr/monde/iran/manifestations/manifestations-en-iran-retour-sur-deux-mois-d-une-revolte-inedite-qui-fait-trembler-la-republique-islamique_5480193.html) de ce régime bicéphale pour une transition modérée.
		
* **Droit des femmes** : 
	* Il y a tellement peu de femmes dans l'industrie du *jeu vidéo*. Alors parler de l'une d'elle est important, pour lui rendre hommage : [Rieko Kodama](https://www.jeuxvideo.com/amp/news/1661091/hommage-a-rieko-kodama-la-grande-dame-de-sega.htm).
	* On ne parle plus d'*Afghanistan*, comme si finalement le sort des femmes importait peu. Pourtant, cela [empire](https://www.francetvinfo.fr/sports/handball/entretien-en-afghanistan-tout-est-interdit-aux-femmes-meme-aller-au-parc-deplore-soraya-karimi-handballeuse-afghane-refugiee-en-france_5484447.html) de jour en jour pour revenir à la situation [passée](https://www.mediapart.fr/journal/international/201122/les-talibans-reviennent-leur-justice-des-annees-1990) de leur précédente domination.
		
### Environnement

* **L'énergie** : 
	* La pénurie supposée de pétrole et de gaz mais pas pour tout le monde. Décidément, la Libye est un chaos très [attractif](https://www.rfi.fr/fr/afrique/20221031-libye-l-italien-eni-et-l-anglais-bp-signent-un-accord-dans-le-gaz). Le [Mozambique](https://www.rfi.fr/fr/afrique/20221113-le-mozambique-annonce-ses-premi%C3%A8res-exportations-de-gaz-naturel-liqu%C3%A9fi%C3%A9) rejoint aussi les exportateurs. Le Nigéria se [renforce](https://www.rfi.fr/fr/afrique/20221124-le-nigeria-inaugure-son-premier-forage-p%C3%A9trolier-en-dehors-du-delta-du-niger). Et le [Qatar](https://www.rfi.fr/fr/moyen-orient/20221121-le-qatar-signe-un-accord-pour-fournir-du-gaz-liqu%C3%A9fi%C3%A9-%C3%A0-la-chine-pendant-27-ans) trouve la Chine comme client...et allié ? 
	* L'augmentation de la part du renouvelable aux USA change beaucoup [les prix des énergies(EN)](https://arstechnica.com/?p=1896112), notamment face au nucléaire. Mais ça n'empêche pas de continuer notre [course folle](https://theconversation.com/comprendre-les-taux-record-atteints-par-les-emissions-de-co-en-2022-194652) vers le réchauffement climatique. Trouver la bonne solution est [complexe](https://asiatimes.com/2022/11/the-renewable-energy-transition-is-failing/) et surtout induit une baisse du besoin (faites le [test](https://nosgestesclimat.fr/), je suis entre 4,6 et 5T/CO2/an) !
	* En *Afrique du sud* c'est le [charbon](https://www.rfi.fr/fr/afrique/20221113-l-afrique-du-sud-a-du-mal-%C3%A0-abandonner-le-charbon-face-%C3%A0-la-hausse-de-la-demande) qui continue à être exploité. Mais c'est aussi [le béton et le ciment(EN)](https://arstechnica.com/?p=1898748) qu'il faudrait revoir pour son impact sur l'environnement.
	
* **Les Transports** :
	* Il est déjà complexe de quantifier l'ensemble des *polluants* du transport routier. Mais pour le [transport aérien](https://theconversation.com/co-nox-vapeur-deau-et-aerosols-comment-bien-comptabiliser-tous-les-effets-de-laviation-sur-le-climat-194124), c'est pire. Pourtant incontestablement la pollution de l'air a [augmenté](https://www.rfi.fr/fr/environnement/20221124-la-pollution-de-l-air-a-tu%C3%A9-en-238-000-personnes-en-europe-en-2020).
	* Il était inévitable que Tesla remette en cause la structure de ses modèles, uniques au niveau batterie dans le secteur automobile. Surtout pour [peser moins](https://www.moniteurautomobile.be/actu-auto/nouveaute/tesla-plateforme-plus-compacte-moins-chere.html). Mais de là à utiliser une batterie à [état solide(EN)](https://arstechnica.com/?p=1894430), il y a un pas qu'Honda franchit.
	* Toujours dans l'automobile, le protocole Green-NCAP est critiquable pour son [impact](https://www.moniteurautomobile.be/actu-auto/environnement/green-ncap-6-voitures-testees-en-octobre-2022.html). A part dire que l'électrique consomme moins, ça sert à quoi? A appauvrir certaines [classes sociales](https://www.liberation.fr/economie/lelectrique-un-reve-automobile-encore-inaccessible-pour-les-plus-modestes-20221116_MZ7HCEPHKFHNXC5E3QPHAAILVY/?redirected=1).
	
* **L'Eau et les déchets** :
	* L'exploitation des [fonds marins](https://www.rfi.fr/fr/environnement/20221031-vers-une-autorisation-de-l-exploitation-mini%C3%A8re-des-fonds-marins) est une source de pollution et de perturbation pour le biotope. Si après quelques hésitations, la France s'y oppose, ce n'est pas le cas de tout le monde.
	* L'Allemagne taxe maintenant les plastiques à [usage unique](https://www.courrierinternational.com/article/environnement-en-allemagne-une-taxe-pour-financer-le-ramassage-des-plastiques-a-usage-unique). On aurait juste aimé qu'elle soutienne un peu plus l'interdiction des pesticides en Europe. Le [glyphosate](https://www.courrierinternational.com/article/pesticides-le-glyphosate-autorise-un-an-de-plus-en-europe) a encore un an de plus !
	* En Tanzanie, on rationne [l'eau](https://www.rfi.fr/fr/afrique/20221104-tanzanie-face-%C3%A0-une-s%C3%A9cheresse-sans-pr%C3%A9c%C3%A9dent-dar-es-salaam-rationne-l-eau) après une sécheresse sans précédent. Et l'appétit pour le Pétrole peut aussi ruiner les efforts de désalinisation, comme à [Corpus Christi(EN)](https://arstechnica.com/?p=1895162), au Texas. Ailleurs, on essaie d'arrêter le [désert](https://www.courrierinternational.com/article/reportage-au-burkina-faso-un-agriculteur-visionnaire-plante-des-milliers-de-baobabs-depuis-1968) avec des arbres et on expérimente positivement dans l'agriculture au [Burkina Faso](https://theconversation.com/photoreportage-au-burkina-faso-un-laboratoire-a-ciel-ouvert-pour-la-transition-agroecologique-193470).
	
* **La Condition animale** :
	* Alors que l'on débat en France de la *Corrida*, cette anomalie, il est bon de se souvenir de ce que c'est [réellement](https://www.francetvinfo.fr/animaux/bien-etre-animal/corrida-l-associationl214publie-une-nouvelle-video-choc-denoncant-une-lente-et-longue-agonie-pour-les-taureaux_5480235.html). Mais le clientélisme reste roi et la France la honte de l'Europe.
	* Le *trafic d'animaux* est un des trafics les plus lucratifs. Il touche souvent les pays les plus pauvres, comme [Madagascar](https://www.rfi.fr/fr/afrique/20221031-madagascar-l-exon%C3%A9ration-de-taxes-pour-l-importation-d-animaux-d-attraction-suscite-la-controverse) par exemple.
	* La pêche est un sujet mondial aujourd'hui avec le pillage des ressources. Et un accord [européen](https://www.rfi.fr/fr/afrique/20221031-p%C3%AAche-apr%C3%A8s-4-ans-de-n%C3%A9gociations-un-accord-est-finalis%C3%A9-avec-l-ue-mais-madagascar-est-insatisfait) et certains pays n'a que peu d'impact.

### Culture

* **J'ai lu** :
	* La BD *Appelés d'Algérie* de Meralli et Deloupy est réussie pour le témoignage sur cette période. Je devrais moi aussi poser des questions sur un oncle mais n'est-ce pas trop tard ? Je suis juste un peu déçu sur le dessin dans l'ensemble. ![cc](/images/coeur.png)
	* Pour rire sur le sujet du travail en entreprise, la série *Open Space* de James est un bon choix...Même si vous l'aurez compris, il s'agit de la vie de bureau. ![cc](/images/coeur.png)
	
* **J'ai vu** : 
	* Le dernier James Gray, *Armageddon Time*, qui reste un film quasi autobiographique sur l'envers du décor du rêve américain dans le New York des années 80. Racisme, surtout et cohabitation entre communautés et classes. Plutôt réussi.	
	* Le *Late Show* d'Alain Chabat et ça m'a laissé songeur. Du Chabat, donc on rit de l'absurdité. De la parodie mais entre les vraies coupures pub et le comique de répétition trop ... répété, ça manquait de surprise.
	* Et pour changer, c'est un peu l'opposé de Soulages avec cette peinture dorée de Rauschenberg.
	
	![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/medias3e11.jpg)
		
* **J'ai écouté** : 
	* le dernier Single [d'Anne Sila](https://www.youtube.com/watch?v=7tnkhnTnJZc) et depuis le temps s'est arrêté, même avec l'heure d'hiver !
	* Sinon, j'hésite entre faire une chronique ou en parler ici pour d'autres albums...à suivre.
	
* **J'ai joué à** : 
	* sur DS à un *Mahjong* de la série "Classics to go" et ça se finit en mode campagne et solo en environ 7h de jeu. Après le truc, c'est de jongler avec le "mélange" pour terminer avant le temps max.
	* sur DS toujours, la version de *Civilization* adaptée à ce format et donc affublé de "Revolution". On retrouvait tous les fondamentaux du premier, voire du deuxième opus avec une ergonomie parfois discutable. J'ai réussi à ne pas être trop drogué par le jeu surtout parce que le système de combat a vieilli.
	
### Science et informatique

* **Le web** : 
	* Le sujet du moment a été la grande migration de [Twitter vers Mastodon](https://www.cheziceman.fr/2022/microblogging/). A relativiser dans le temps et l'ampleur tout de même... Des comptes bannis ont été réactivés et la protection des données part en vrille. Les démissions ne sont pas que chez Twitter puisque chez [Gamekult](https://www.lemonde.fr/pixels/article/2022/11/17/la-redaction-de-gamekult-demissionne-apres-le-rachat-du-site-par-reworld-media_6150369_4408996.html), aussi revendu, ça s'en va.
	* L'Afrique se dote enfin d'un [câble sous-marin](https://www.laprovence.com/actu/en-direct/6960246/2africa-plus-grand-cable-sous-marin-au-monde-se-raccorde-a-marseille.html) qui permet de mieux communiquer par le réseau des réseaux.
	* Les scandales se succèdent sur les [crypto-monnaies](https://www.courrierinternational.com/article/finance-les-cryptomonnaies-en-plein-chaos-binance-renonce-a-racheter-ftx), en espérant qu'on s'en débarrassera avec les NFT.
	* Et en marge, un petit retour sur [l'histoire de ARM(EN)](https://arstechnica.com/?p=1896053).

* **Intelligence artificielle** : 
	* Les constructeurs automobiles Ford et VW se [désengagent](https://www.courrierinternational.com/article/technologie-ford-et-volkswagen-donnent-un-coup-de-frein-a-leurs-projets-de-voiture-autonome) d'une des technologies du domaine de l'IA. Entre scepticisme et autre choix, ce n'est pas pour un débouché rapide.
	* L'intelligence artificielle au service de la science a des limites. On le voit avec les [protéines](https://theconversation.com/lintelligence-artificielle-au-defi-du-design-de-proteines-des-prouesses-et-limites-dalphafold-192748).
	
Et toujours **une page Liberapay**, pour ceux qui veulent me **sponsorer** : [ICI](https://liberapay.com/Chez_Iceman/donate)

Et c'était un novembre pas si pluvieux...mais nouveau.

[L'original révisé ![video](/images/youtube.png)](https://www.youtube.com/watch?v=4_fvXrgAm1A){:target="_blank"}







