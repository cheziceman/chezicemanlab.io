---
layout: post
title: Mon Libre sous ... Android - Edition 2022
category: geek, tuto
tags: android, geek, Geekeries, google, logiciel libre, smartphone, 
---
**C'est la 4ème édition de cette liste de logiciels. Et en trois ans, j'ai fait évoluer les choses, toujours en douceur, toujours un peu plus libre. Je reprends donc la trame des précédents guides.**

Évidemment, tout le monde ne va pas désinstaller toutes les application de Google sur son Smartphone Android, à moins d'installer une ROM alternative comme [LineageOS](https://download.lineageos.org) dans sa version sans applications Google. Et pourtant, je n'utilise pas l'appli Gmail, ni même tous les Google musique, kiosque, .... J'ai même supprimé le playstore mais il est possible de passer uniquement par un marché comme [F-Droid](https://f-droid.org/), ou bien encore, d'installer des APK préalablement vérifiés et/ou sauvegardés, même jusqu'à aller chez Github/Gitlab. J'utilise à la place  [AuroraStore](https://f-droid.org/fr/packages/com.aurora.store/) pour gérer les applications et ça fonctionne bien, malgré MIUI dont il faut désactiver les optimisations. Je n'utilise ni Facebook, ni Twitter et j'ai gardé quelques produits "Xiaomi" ou fournis par la ROM que j'ai particulièrement bridé ou surveillé. Mais des alternatives existent à chaque produit estampillé par la marque du téléphone. Méfiance toutefois si l'utilisation s'accompagne d'une création de compte chez la marque.


### 1- Bureautique et outils

D'abord le mail est géré par [k9mail](https://f-droid.org/en/packages/com.fsck.k9/), outil libre et ouvert à une utilisation de GPG. J'utilise un outil de prise de note simple, [Notes](https://f-droid.org/packages/it.niedermann.owncloud.notes/) qui fonctionne avec un petit espace [Nextcloud](https://f-droid.org/en/packages/com.nextcloud.client/) hébergé chez un prestataire comme Zaclys. J'y rajoute évidemment [DavDroid/DavX](https://f-droid.org/packages/at.bitfire.davdroid) qui est un peu rétif avec MIUI mais finit par fonctionner, pour tout ce qui est synchronisation des carnets d'adresse et agenda. J'ai également l'application [ProtonMail](https://proton.me/), qui est opensource, pour mon compte correspondant. Mon lecteur d'Ebook est [FB-Reader](https://fbreader.org/android), opensource et même GPL lui aussi. Pour mes besoins en calculs scientifique, j'ai la calculatrice de chez [Numworks](https://play.google.com/store/apps/details?id=com.numworks.calculator), la marque française qui a libéré les collégiens et lycéens de l'emprise Casio/TexasInstrument. Et puis l'excellent mais très intrusif clavier Swiftkey a été remplacé par non plus Anysoft Keyboard qui bugue de plus en plus sur les derniers android mais par le plus simple [Openboard](https://f-droid.org/en/packages/org.dslul.openboard.inputmethod.latin/). Enfin pour lire les documents bureautique, j'ai opté pour [SmartOffice](https://play.google.com/store/apps/details?id=com.picsel.tgv.app.smartoffice&gl=US), qui a l'avantage de savoir éditer, de fonctionner avec Box, en attendant peut-être un jour le support LibreOffice et Nextcloud. Bien moins intrusif que la suite Microsoft. J'ai abandonné Office Document Viewer trop limité et lent sur les gros documents, notamment les PDF.

Pour me connecter à mon NAS Synology, je passe par [Total Commander](https://www.ghisler.com/android.htm). Il reste le meilleur gestionnaire de fichier pour les utilisateurs avancés et même "normaux" si on prend le temps de le comprendre mais il commence à être limité sur les dernières versions d'Android, voir buggé. Il faut donc pour l'instant le downgrader en version 3.30. Je verrai pour trouver un remplaçant mais il n'y en a pas avec la gestion du LAN aujourd'hui tout en étant léger. Pour la navigation internet, j'ai un duo de navigateur selon l'utilisation : [Opera](href="https://play.google.com/store/apps/details?id=com.opera.browser&amp;hl=en_US) pour sa rapidité et ses filtres intégrés et [Firefox](https://play.google.com/store/apps/details?id=org.mozilla.fenix) pour continuer à avoir un lien avec le panda roux en attendant que la version normale évolue dans ce sens. Par contre, [ZArchiver](https://play.google.com/store/apps/details?id=ru.zdevs.zarchiver) me convient bien pour les fichiers compressés (surtout les formats bizarres ou les dernières version de RAR avec des mots de passe), pour son confort, mais ce n'est pas libre et je l'utilise rarement. Pour m'informer, j'ai finalement abandonné toute application pour lire directement via le navigateur sur FreshRSS installé chez Zaclys. J'utilisais peu le offline. Pour la sécurité et les publicités, j'ai intégré la solution que propose Sebsauvage avec [DNSFilter](https://sebsauvage.net/wiki/doku.php?id=dnsfilter). Bien plus économe en mémoire que [Blokada](https://blokada.org/), mais aussi moins ergonomique.

### 2- La Vidéo, la Radio/TV, le Ciné

Dans ce domaine, il faut convertir des fichiers parfois et j'utilise maintenant [VideoTranscoder](https://play.google.com/store/apps/details?id=protect.videoeditor&gl=US) qui est une interface masquant FFMpeg.  C'est simple, sobre et opensource. Pour lire les vidéos, en plus du lecteur du constructeur, il y a l'incontournable [VLC](href="https://www.videolan.org/vlc/download-android.html), opensource et libre. [Molotov](https://www.molotov.tv/), c'est quand même bien sympa parfois pour regarder la TV mais pas libre. L'application [Cinetime](https://play.google.com/store/apps/details?id=fr.neamar.cinetime) permet de connaître la programmation des séances ciné près de chez soi, et en plus c'est Opensource. Il n'en est pas de même pour mon [IMDB](https://www.imdb.com/) préféré qui me permet d'avoir une encyclopédie cinéma à bout de doigt mais aussi de savoir quels épisodes de série j'ai vu. J'ai aussi [NewPipe](https://newpipe.net/) pour l'alternative à Youtube, [RadioDroid](https://f-droid.org/en/packages/net.programmierecke.radiodroid2/) pour les radios du monde entier et surtout [AntennaPod](https://antennapod.org) pour les podcasts, tout ça étant libre.

![image](https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/The_Seven_Works_of_Mercy-Caravaggio_%281607%29.jpg/313px-The_Seven_Works_of_Mercy-Caravaggio_%281607%29.jpg)

*Les Sept œuvres de miséricorde - Caravage 1607*

### 3- La Photo et la BD

Pour la gestion des photos, je m'en remets à [SimpleGallery](https://f-droid.org/en/packages/com.simplemobiletools.gallery/) (libre sur fdroid) et si j'ai de la retouche lourde, je dois avouer que [SnapSeed](https://play.google.com/store/apps/details?id=com.niksoftware.snapseed) est devenu incontournable si on n'a pas d'abonnement Adobe. Rien de libre là dedans. Mais en dehors de la Photo, on peut aimer lire ses BD. [Challenger Viewer](https://challengerviewer.wordpress.com/) reste parmi les plus réussis pour transporter l'équivalent de sa bibliothèque papier en version numérique (PDF, CBR ou JPEG). En plus c'est français, sans pub, à défaut d'être libre.

### 4- La Musique

Le lecteur Sony me suffisait mais avec mon passage chez Xiaomi, j'ai cherché un peu avant de prendre...un portage du [lecteur Sony](https://forum.xda-developers.com/t/app-4-2-latest-sony-music-9-4-8-a-0-13-updated-9-march-2021.2728110/) pour tous les androidphones. Il faut aussi Bandcamp pour se connecter sur ce formidable service où les créateurs se sentent plus libres. [Shazam](https://forum.xda-developers.com/t/app-shazam-lite-no-region-restriction.3477858/) est un gadget pas du tout libre mais parfois bien pratique pour ma mémoire défaillante, et toujours grace à XDA on peut s'en sortir avec une version bien allégée. Et puis il y a [AutoTagger](https://play.google.com/store/apps/details?id=com.serg.chuprin.tageditor&gl=US) pour éditer directement les tags sur son téléphone. Pas libre mais si pratique.

### 5- Et le reste...

La [Navigation](https://www.cheziceman.fr/2018/testgps/) se fait par Waze qui reste le plus fiable pour l'estimation des terribles embouteillages parisiens, mais au prix de ses données.  En dehors de ça, je trouve que la consommation de batterie et de données n'est pas si formidable chez les concurrents libres pour s'approcher de la qualité de guidage. Et pourtant je vois de nouveaux bugs sur Waze dans les versions récentes.

### Conclusion

Depuis la dernière fois, j'ai encore progressé en supprimant du superflu. Il y a des rubriques où cela tient vraiment du gadget que j'ai la flemme de réinstaller si j'en ai besoin. On peut donc aisément fonctionner avec la seule rubrique 1-.
Si je fais un bilan sur ce qu'il reste à traiter : 
* Challenger Viewer : Le problème c'est que j'y ai des habitudes. Sans doute possible de faire libre. Librera fonctionne mal dans certains types de fichiers. 
* IMDB : vue la lenteur actuel, je me demande si je ne vais pas utiliser directement le site....Mais en testant, c'est pire pour l'instant malgré le filtrage en place.
* Molotov : aucune concurrence mais ça reste gadget
* Opera : sur un autre terminal plus récent, je passerai sûrement sur le seul Firefox. Sinon Vivaldi.
* Shazam lite : un peu édulcoré c'est déjà bien. Mais comment se passer de cette reconnaissance si pratique.
* SmartOffice : c'est le mieux pour l'instant pour mon besoin et toujours pas de lecteur libre en vue.
* SnapSeed : aucune concurrence en vue dans le monde libre pour ce niveau de prestation.
* Waze : comme je disais, le plus c'est le trafic sinon je serai sur OSMAnd.
* Zarchiver : Il faudrait que les fonctions de Total Commander dans ce domaine soient plus évoluées pour m'en passer. Ou que ZArchiver sache gérer les dossiers LAN. Et là aussi pas d'autre gestionnaire de fichier au niveau....à moins que ? 

Comme toujours, les suggestions de lecteurs font progresser aussi cette liste.

En video : [La liberté n'a pas de prix ![video](/images/youtube.png)](https://www.youtube.com/watch?v=7eXxanfQdD0){:target="_blank"}




