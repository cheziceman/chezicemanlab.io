---
layout: post
title: Pause Photo et Poésie - M.
category: poesie 
tags:  poésie, poême
---

Aussi doux que les caresses de l'enfance

les moments de tendresse à jamais enterrés

Aussi dures que les colères, les offenses

que j'aimerai tant oublier.

.

Je n'ai jamais dit les mots ensoleillés

qui me venaient au cœur,

en ces instants empaillés

que j'aime revoir avec douceur.

.

D'un jet de rêve apaisé

je te tiens par la main autrefois si douce

aujourd'hui d'une vie marquée

par les absences qui s'émoussent.

.

Je peste encore sur tes écarts

les maladresses, les erreurs.

Et puis j'oublie, il est si tard

pour se briser le cœur.

.

Tu seras toujours unique 

comme je le fus pour toi.

Je ne serais jamais amnésique

pour savoir ce que je te dois.



![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/poememaman.jpg)

