---
layout: post
title: BD - Portrait de la France de François Boucq (2017)
category: bd
tags: bd, humour, satire, 2010s, politique
---

**La BD a souvent participé à sa manière à des campagnes électorales. Il y a 5 ans, François Boucq y allait de son portrait de la France. Ca n'a pas vieilli.**

J'ai déjà parlé de cet auteur, avec son héros [Jérôme Moucherot](https://www.cheziceman.fr/2019/perilpieddepoule/). Il est encore convoqué dans ce recueil de planches et dessins sur le thème de la France. C'est une peinture de l'haxagone façon Boucq, donc avec son lot d'humour, d'absurde, et donc de satire. On retrouve le héros au costume léopard car finalement les thèmes ne sont pas toujours très éloigné. Il y a toujours eu l'absurde de la technocratie par exemple.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/franceboucq.jpg)

Autant le dire tout de suite, Boucq n'est pas du tout d’extrême droite. Sa France est malade de Lepenite aigüe, comme le montre la couverture. Mais ce n'est pas non plus une France nostalgique, une France qui aurait été mieux avant. Quoique les années 80 ?  Il parle certes du passé, des générations mais plus souvent pour en montrer des similitudes plutôt que pour opposer. Ce n'est pourtant pas que cela, c'est aussi des sujets de société. La première série de planche sur l'homosexualité est, par exemple, désopilante, nous emmenant dans cet humour absurde qui passe du coq à l'âne, ou de l'hippopotame à l'escargot devrais-je dire. On parle vacances, santé, sport.

On y retrouve des planches réparties par chapitres dans des thèmes avec Moucherot mais aussi des extra-terrestres, des médecins, des Sarkozy-Hollande, des machines improbables, des gros, des musclés, des clins d'œil à sa ville, Lille, sa région, etc...C'est riche en sujet dans ces 150 pages, avec des fois un goût d'inachevé (elle est où la 2ème planche ?) ou des gags plus ou moins réussis. C'est toujours l'écueil des recueils (rime riche). Il n'a pas non plus bossé tout seul sur le sujet (Karim). Il y a des couleurs, du noir et blanc, des bulles de dialogue qui explosent comme la verrue de la couverture. Et en 5 ans, cela n'a pas vraiment changé, ce qui confirme que malgré l'agitation vécue, nous sommes bien dans un retour en arrière (transformé en faux progressisme par centre et droite) sinon un immobilisme.

Un album pas indispensable, mais intéressant pour s'éviter d'acheter quelques miroirs, quelques séances de psy, encore que...

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/franceboucq2.jpg)