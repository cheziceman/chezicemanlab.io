---
layout: post
title: Souvenir de télévision - La Course autour du monde (1976-84)
category: geek, tourisme
tags: télévision, voyage, culture, journalisme, 1980s
---

**En plus des films et séries, il y a des émissions qui vous marquent dans l'enfance et l'adolescence. Souvent la patine du temps les rends meilleures.**

Jacques Antoine en a créé pas mal dans les années 70-80 sur la télévision publique française. On pense souvent aux mythiques *La Tête et les jambes* ou à *la Chasse au Trésor*, mais un peu moins à ce jeu / cette aventure créée pour les télévisions francophones : Antenne 2, la TSR, Télé Radio Canada, RTL Télevision. Je ne me souviens pas de toutes les saisons puisque l'émission commença en 1976. Elle révéla en 1977 un certain Philippe De Dieuleveult et Didier Régnier qui présenta des émissions de...Jacques Antoine. Je me souviens plutôt de la période 81-84.

Le principe était de révéler de jeunes reporters. Il y avait 8 journalistes au départ dotés d'un peu de matériel pour faire leur métier. A l'époque, on était en Super 8 ! Ils choisissaient chacun une destination et faisaient un reportage qui était envoyé à Paris pour un montage selon leurs instructions. Durant l'émission, on présentait le reportage et un jury le notait. Évidemment, il y avait des candidats français, suisses, canadiens, belges ou luxembourgeois. Ils n'avaient droit qu'à deux semaines de suite dans le même pays. Pas vraiment de ligne éditoriale donc et c'était vraiment la surprise, le goût du candidat en plus de sa manière de filmer qui étaient mis en avant.

[Une édition de 1982 ![video](/images/youtube.png)](https://www.youtube.com/watch?v=gPb_rgnRkrw){:target="_blank"}

L'émission avait donc ce parfum d'aventure et de défi propre aux programmes de Jacques Antoine et permettait aussi de repérer de futurs jeunes journalistes pour les chaînes. Mais pour le téléspectateur, c'était une occasion de découvrir des pays, des cultures, de voyager. Pour l'adolescent que j'étais, c'était une part de rêve, un prolongement visuel des romans et livres que je lisais à l'époque. Pourtant je n'ai pas de souvenir marquant d'un reportage, d'un reporter. Je me souviens à peine du plateau, du jury, sinon que je n'étais évidemment pas toujours d'accord avec les notes. Je découvre même maintenant qu'il y a eu des livres autour de cette émission pour en raconter les coulisses, notamment. Difficile à trouver aujourd'hui sinon chez des bouquinistes.

J'ai aussi un vague souvenir de l'émission qui suivit sur un principe similaire, Le Grand Raid, avec Didier Régnier et Noel Mamère à la présentation. On parlait alors de films et pas de reportage, d'ailleurs. Car c'était un sujet un peu plus long que le petit reportage calibré pour le journal télévisé. Aujourd'hui, on parle de "grand format" ou on case ça dans des émissions de ... reportage, des magazines. Les coulisses, la dimension aventure donnait certainement plus envie à cette époque dans un monde qui paraissait paradoxalement moins dangereux dans la fin de la guerre froide. Il faudrait sans doute que j'aille plonger dans les archives de l'INA pour me faire plaisir dans ce bain de nostalgie.

Mais je ne peux m'empêcher de penser à ce qu'est devenue l'information depuis, cette défiance vis à vis des médias, des journalistes. A cette époque, il y avait à la fois la pression de l'information mais aussi le temps du recul, sans même parler d'une envie de découverte de ces jeunes femmes ou hommes. Et s'ils tentaient de trouver un ton, un style, ils faisaient souvent partager des rencontres, des cultures, des sujets légers ou graves selon aussi quelques hasards. Un peu comme la vie, parfois, lorsqu'on la laisse s'aventurer dans ses méandres.

[Une émission complète ... québecoise![video](/images/youtube.png)](https://www.youtube.com/watch?v=DQFzkS7WXlE){:target="_blank"}
