---
layout: post
title: BD - La Brigade Chimérique, ultime renaissance de Lehman, De Caneva et Lou (2021)
category: bd
tags: BD, 2020s, uchronie, superhéros, fantastique
---

**Tout ça, c'est de la faute [d'Alias](https://erdorin.org/la-brigade-chimerique-ultime-renaissance/). Il m'a intrigué avec sa série d'articles sur cette succession de volumes de la Brigade Chimérique, scénarisés par Serge Lehman. Mais autant j'avais du mal sur la première série, autant j'y ai trouvé mon compte sur cette nouvelle version, ou suite.**

Ce que je reprochais aux 6 premiers volumes (environ 50 pages chacun et 2 histoires par volume), c'était un dessin pas toujours maîtrisé rappelant [les Pulps](https://en.wikipedia.org/wiki/Pulp_magazine) sans oser complètement aller dans le style. L'histoire était là avec ses nombreuses références littéraires, parfois ouvertement inventées. Mais ça parle de quoi justement ? Et bien c'est une uchronie qui voit l'apparition de ce que l'on appelle aujourd'hui des super-héros, après la première guerre mondiale (et même pendant). En Europe essentiellement et ils prospèrent dans l'entre-deux guerre avec un certain Mabuse en Allemagne, la Phalange en Espagne, Nous autres et son leader en URSS, etc... Et en France, on a une mystérieuse brigade chimérique. Autour de cette brigade, on retrouve Marie Curie puis sa fille et son mari dans l'Institut du Radium, mais aussi le Nyctalope et son allié anglais. Tous ces héros disparaissent en apparence à l'arrivée de la seconde guerre mondiale, certains partant en exil aux USA, où d'autres héros apparaissent encore avec l'effet de l'atome. La première série était dans les années 30. Celle là se passe à peu près aujourd'hui où l'on redécouvre soudainement la trace de ces "anormalités".

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/ultimechimerique1.jpg)

Nouvelle série, nouveaux héros. Si dans la première, c'était Jean De Séverac et l'intrigante George Spad qui tenaient la barre, cette fois, c'est un certain Charles Dezniak, Dex, un chercheur sur l"hypermonde", le terme qui définit ces transformations de l'humain. Tout commence à Paris avec l'apparition d'un Homme-Rat, puis on retrouve un rescapé des années 30, Jean Lebris dit L'homme Truqué. Ils partent à la recherche de Felifax l'homme-tigre pour se retrouver sur les traces des héros d'antan. Lehman a encore décomposé cette série en épisodes d'une trentaine de pages mais si avant c'était regroupé en tomes de 50 pages, le nouvel éditeur a préféré un seul volume. On a quand même droit à des couvertures pour chaque "chapitre".

Et je trouve que tout ça fonctionne mieux. Déjà le dessin est actuel, sans essayer de singer quelque chose. Là, je sais qu'Alias pense tout le contraire de moi. Nous sommes d'accord par contre pour l'apport de cette série et de ce nouveau volume. J'y retrouve énormément de clin d’œil à la culture européenne du fantastique du début du 20ème siècle. Si les noms de Maurice Leblanc, Paul Féval vous parlent, si la figure de Marie Curie et tout ce qui se disait sur le Radium à l'époque vous fascinent, si vous aimez le cinéma expressionniste allemand, etc...C'est fait pour vous. On retrouve évidemment les gimmicks de Lehman, comme [Metropolis](https://www.cheziceman.fr/2018/metropolisbd/). C'est d'ailleurs le même dessinateur. Ce qui est appréciable dans cette série, outre sa richesse et son imaginaire, c'est d'avoir des rebondissements, des surprises. Lehman rend ainsi un hommage à la littérature en feuilleton. On oublie que beaucoup de grands auteurs ont commencé par être édités ainsi. Dommage donc que les éditeurs n'aient pas osé faire paraître ça dans un autre format, puisque finalement les comic books US le font.

Après deux séries, 600 pages au total, peut-on s'arrêter là ? Il y a marqué Ultime. Mais entre les deux séries, il pourrait y avoir la place pour développer l'Hypermonde, ses héros, à la manière de ce que fait Marvel finalement depuis des décénies. Et puis une adaptation cinéma ? En attendant, il y a le JDR. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/ultimechimerique2.jpg)

