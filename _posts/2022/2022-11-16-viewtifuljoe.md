---
layout: post
title: Souvenir de Gamer - Viewtiful Joe (2003)
category: geek
tags: jeu vidéo, retrogaming, 2000s, geek, beat them all
---

**Alors que l'on croyait le genre promis aux oubliettes avec les Hack'n slash 3D, le Beat'em all trouva un nouveau représentant prêt au combat dans les années 2000. Son nom : Viewtiful Joe.**

Je me souviens des articles de presse qui étaient heureux de voir un tel jeu sortir. C'était l'époque de l'éphémère mode du [Cell Shading](https://en.wikipedia.org/wiki/Cel_shading) sur les consoles PS2 et Gamecube. Mais c'était surtout la réappropriation d'un genre avec les dernières recettes des hits de la baston. Et pour cause puisque l'on avait aux commandes celui qui avait créé "Devil May Cry", Hideki Kamiya. Capcom tenait là une nouvelle licence en commençant aussi à diversifier l'offre sur la Gamecube. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/viewtifuljoe1.jpg)

*Sur la Gamecube*

Mais en lisant ça, je n'étais pas plus emballé que ça, à l'époque. J'avais usé le genre avec les [Renegade](https://www.cheziceman.fr/2017/renegade/), [Double Dragon](https://www.cheziceman.fr/2016/doubledragon/), etc...Mais voilà que 15 ans plus tard, on nous ressortait ça des oubliettes. Je l'ai vu tourner lors d'un salon des jeux vidéos et je trouvais ça sympa visuellement avec cette impression de dessin animé façon superhéros (le costume) et des onomatopées qui apparaissent à l'écran. Il y avait aussi un rythme endiablé qui ne laisse pas de repos au joueur. Mais il faut croire que ce n'était plus ce que je recherchais. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/viewtifuljoe2.jpg)

*Sur la DS*

Et puis je me suis à nouveau penché récemment sur ce petit héros récemment pour voir ce que ça donnait avec 15 ans de plus. Surprise, ça m'a plu. Car effectivement on retrouve la recette originelle, le goût de la baston avec des vagues d'ennemis qui surgissent du décor et trois boutons pour faire tout ça. Mais il y a d'autres boutons qui permettent de ralentir ou de se transformer. Mais surtout, Kamiya a introduit des combos, a su reprendre ce qui avait fait le succès de Devil May Cry, voire de Street Fighter...du même Capcom. Le fan de la marque ne pouvait être dépaysé par cette suite d'appuis sur les différents boutons pour créer des coups incroyables. Mais en plus, le Cell shading n'avait pas que le rôle de gadget. Il donnait vraiment du cachet au jeu, donnant l'impression d'être un personnage de comic book. Les décors sur la Gamecube étaient vraiment dans ce style avec des ennemis complètement délirants.

Par contre, à y rejouer maintenant avec mes réflexes de vieux, je vois mes limites. Et si je passe à peu près bien le premier niveau et ses différentes étapes, c'est mort pour la suite. Déjà, il est considéré comme difficile car derrière une mécanique de jeu simple se cachent des subtilités. Notamment avec ces autres boutons où l'on peut ralentir l'action. Il faut jouer avec la taille de ses sauts et l'orientation en l'air, ce qui ne se maîtrise pas facilement. Et puis on peut même s'accrocher aux murs avec le bon timing. Timing que je n'ai pas évidemment...Alors bon, ne comptez toujours pas sur moi pour un longplay. Reste que le jeu reste totalement jouable maintenant, que cela soit sur le cube Nintendo, sur la PS2 ou bien même sur la version Nintendo DS. Je n'ai pas trouvé l'apport du deuxième écran très pertinent mais la jouabilité y était. 


[Un Longplay ![video](/images/youtube.png)](https://www.youtube.com/watch?v=dLMvXi_-j2E){:target="_blank"}

***Viewtiful Joe de Capcom sur Gamecube, PSP, Playstation 2, Nintendo DS***
