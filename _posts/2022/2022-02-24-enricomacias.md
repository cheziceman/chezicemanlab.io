---
layout: post
title: Musique - Enrico Macias Live à l'Olympia 2003
category: musique
tags: 2000s, musique, live, concert, guitare, variété, chanson, arabo-andalouse
---

**On a tendance à oublier le musicien derrière le chanteur de variété. En près de 60 ans de carrière, le jeune Gaston Ghrenassia est devenu Enrico avec des tubes, des hauts et des bas. Ce concert de 2003 était un des sommets.**

Je ne m'étendrais pas sur le mystère qui entoure sa jeunesse algérienne pendant la guerre d'Algérie et la disparition de son maître Cheikh Raymond. Ni même sur ses choix politiques. J'ai eu parfois l'impression qu'il y avait une part de rédemption dans ses chansons parlant de paix et d'amour. Enrico Macias, c'est un chanteur populaire des années 60 à 80 avec la chaleur de sa voix et son accent. Mais c'est aussi une synthèse entre la musique arabo-andalouse, berbère, le jazz, la chanson française. Dans ce spectacle de 2003, il s'est entouré d'un orchestre qui mèle aussi toutes ces influences, entamant largement un retour aux sources de sa musique qui se clôturera avec un hommage à Raymond Leyris (Cheikh Raymond).

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/enricomacias.jpg)

Il y a beaucoup de nostalgie dans sa musique, une nostalgie d'une Algérie fantasmée ou juifs, arabes et chrétiens vivaient ensemble. Enfant illégitime abandonné, il apprend la musique à l'adolescence et cela ne le quittera jamais. C'est avec "Chanter" qu'il commence son récital mais je lui préfère "Juif Espagnol" qui est une chanson des années 80 où il est déjà plus dans la variété française. C'est aussi l'orientation générale de cette première partie où il n'empoigne la guitare (il y a un DVD de ce concert) qu'au bout de 4 titres. Guitare qui est le centre des attentions comme pour "Oh Guitare, Guitare". S'il chante l'amour et la nostalgie, il sait aussi se faire plus grave comme avec "La Rumeur", "Les Gens du nord" (qu'on peut comparer aux Corrons de Pierre Bachelet) et "Nuit et brouillard". 

J'aime plus la période 60-70 du chanteur/musicien avec des titres comme "Je t'aimerai pour deux" et "Paris tu m'as pris dans tes bras", titre refusé par Aznavour mais qui donne envie de valser sur les quais. Un live, cela permet aussi de redécouvrir autrement un artiste ou des titres, comme "Oranges amères". Et tout le milieu du concert laisse libre court à sa virtuosité de guitariste. On a oublié le merveilleux soliste qu'il est avec cet instrument avec ce jeu à l'oriental si difficile. Les introductions des morceaux sont merveilleuses de technicité et de sonorités. Peu à peu, on entre dans une autre partie de sa vie de musicien jusqu'à revenir aux sources berbères et arabo-andalouses. La bascule se fait avec "Le Violon de mon père" et ensuite il part sur des titres en arabe ou hébreux. Cela commence par le traditionnel "Koum Tara" pour passer au non moins traditionnel "Ygdal". La salle répond avec exaltation à ces chants et musiques, malgré l'age de son public. 

Il n'est pas surprenant qu'il reprenne à son tour le "Ya Rayah" de [Dahmane El Harachi](https://www.cheziceman.fr/2019/dahmaneelhaarrachi/) car c'est un chant devenu universel pour ceux qui ont quitté leur pays et rencontrent la dureté de l'accueil et du travail. Le rappel se fait évidemment sur le "Mendiant de l'amour", parfois devenu trop caricatural mais qui trouve une autre variante avec ce grand orchestre. Un orchestre qui mèle donc les sonorités orientales via le Kanone ou l'Oud et un gros travail de percussion. Vocalement, à 65 ans, le chanteur n'avait pas encore trop perdu de son amplitude vocale, même si les aigus sont moins là. Il a l'expérience pour compenser, contourner les obstacles et donner la parole à son public qui reprend les refrains en choeurs ou hurle des [youyou](https://fr.wikipedia.org/wiki/Youyou_(cri)). 

A plus de 80 ans, malgré la maladie et l'âge, il continue d'être sur scène. Il apporte toujours sa singularité, s'écartant de la caricature qu'il aurait pu devenir. Il reste pour moi un très grand musicien et compositeur qui apporta beaucoup à la chanson française, rencontrant de grands auteurs et d'autres compositeurs et interprètes. Ce magnifique concert a toute sa place dans une discothèque. 

[Extrait du concert ![video](/images/youtube.png)](https://www.youtube.com/watch?v=KKW6XKWje_k){:target="_blank"}

