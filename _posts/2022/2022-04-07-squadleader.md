---
layout: post
title: Wargame du passé - Squad Leader
category: geek,
tags: 1970s, jeu de plateau, wargame, stratégie
---

**Parmi les classiques du jeu de plateau stratégique, et plus précisément du Wargame, ce titre est fondateur de beaucoup de titres actuels, même numérisés. Mais il se mérite.**

S'il a été remplacé en 1985 par son descendant, Advanced Squad Leader (ASL pour les intimes), je trouve que ce premier opus avait l'avantage de rester accessible aux plus motivés et réaliste dans son fonctionnement. Avalon Hill a sans doute eu raison de poursuivre avec de plus en plus de précision à l'époque. Mais l'éditeur s'enfermait définitivement dans un marché de niche qui n'avait d'autre issue que de passer à l'informatique. J'ai de bons souvenirs avec ce jeu, son environnement 2ème guerre mondiale, parce qu'il y avait une certaine liberté à créer des scénarios autour de ces règles, de ces cartes.

Contrairement aux autres wargames, on est à l’échelon tactique, c'est à dire sur des petites unités. On est donc comparable à ce qu'était [Cry Havoc](https://www.cheziceman.fr/2015/cryhavoc/) pour le moyen age, ce qui facilita ma transition. Un peu plus tard, je ferai un dérivé tactique d'un jeu avec figurine en écrivant une règle perso qui mixait un peu de tout ça. Mais la comparaison s'arrête là, les règles de SL étant bien plus riches et complexe. Mon choix de rester à une version "dépassée" était conscient. Il fallait que je puisse expliquer à d'autres joueurs sans passer trop d'heures. Il fallait que j'envisage mes propres variations de règles. Je me souviens encore du détour par le magasin Jeux Descartes à St Michel où les boites de Wargames s'alignaient dans l'indifférence des rôlistes. Elle était plutôt rétro mais bien lourde à transporter.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/squadleader1.jpg)

Quand j'ai ouvert la première fois la boite, je me suis dit, ouah, il y a de quoi faire de sacrés batailles. Il y a des planches de pions avec non pas les habituels signes OTAN pour représenter les types d'unité, mais des symboles au niveau de l'unité. Rien que ça, il faut s'y faire peu à peu mais j'ai encore des souvenirs aujourd'hui de tout ça, alors que je ne joue plus beaucoup aux Wargames. Il y a le fascicule de règles évidemment, les cartes, etc.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/squadleader2.jpg)

Tout déballé sur ma grande table, ça avait effectivement une certaine gueule. Enfin, pour les spécialistes parce que sinon on peut se demander ce que c'est que tout ça. En plus des pions, il y a certains marqueurs en plus. On lui reprochera juste l'absence d'une gestion du "brouillard de guerre", chose qui nécessitera des évolutions plus récentes dans les règles de wargames sur table, et plus faciles en informatique. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/squadleader3.jpg)

La carte reprend évidemment les habituels hexagones. Mais elle est ici "géomorphique". C'est à dire qu'on peut mélanger différents types de terrain en les assemblant. Alors ce n'est pas forcément très glamour à regarder par rapport à des châteaux ou des cartes plus géographiques mais on peut bien identifier les différents terrains et leurs avantages et inconvénients suivant la position offensive ou défensive. C'est au joueur de se plonger dans cet univers pour mettre un nom à ces zones colorées.

Si maintenant le joueur de Wargame est un peu plus familier avec l'aspect moral, l'aspect puissance de tir, les alliances ou "empilement" d'unité, etc, il faut se dire qu'en 1977 c'était un peu nouveau encore à ce niveau de détail. Mais on pousse déjà beaucoup le détail avec les spécificités des unités, la ligne logistique nécessaire pour l'aspect munition. Je dois dire qu'aujourd'hui, j'ai oublié l'essentiel des règles et je serai incapable d'y jouer correctement. Pourtant en jouant à des jeux tactiques et stratégiques, notamment sur ordinateur ou mobile, j'ai retrouvé quelques éléments troublants. C'est un jeu qui a marqué l'histoire et reste donc tout à fait jouable encore aujourd'hui. Juste que l'on préfère le niveau d'au dessus pour les cartes, les FPS et autres ayant plus de succès pour rendre la guerre et son horreur.

Le jeu garde des fans qui ont aussi construit des cartes plus grosses, avec parfois même des éléments de maquettes. Il faut dire qu'empiler les pions est parfois fastidieux, surtout qu'ils étaient plutôt épais pour l'époque. 

Si vous me lisez depuis longtemps, vous pouvez être surpris que j'ai joué à de tels jeux. Il est pourtant intéressant de se confronter à des choix tactiques et de s’apercevoir que l'on peut y oublier les humains représentés par des pions pour vaincre par des sacrifices, des diversions. Et en ce moment, il y a de quoi se poser des questions de chaque coté.
