---
layout: post
title: Cinéma - De l'autre côté du ciel de Yuusuke Hirota (2022)
category: cinema
tags: cinéma, japanimation, dessin animé, japon, 2020s
---

**Décidément, le Studio Ghibli a fait beaucoup de petits dans l'animation japonaise. Ce film du studio 4°C en est la parfaite illustration avec une fable qui s'adresse à tous.**

Adaptation du livre "Poupelle et la ville sans ciel" de Akihiro Nishino, ce film s'est vu affublé d'un curieux titre français. L'histoire : Vivant au milieu de grandes cheminées dont l’épaisse fumée recouvre depuis toujours le ciel de sa ville, Lubicchi aimerait prouver à tous que son père disait vrai et que, par-delà les nuages, il existe des étoiles. Un soir d’Halloween, le petit ramoneur rencontre une étrange créature surnommée L'homme-poubelle, qu'il va nommer Poupelle. Ils décident alors de ne pas respecter l'interdiction des "inquisiteurs" et de découvrir le ciel. Vous l'aurez compris, nous sommes dans une ville où règne une sorte de dystopie. Le tout avec une atmosphère légèrement steampunk. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/autrecoteciel.jpg)

Dès les premières secondes du film, on est saisi par la beauté des décors, leur richesse. Et puis, le dessin n'est pas que typique des mangas, il mêle aussi des inspirations plus européennes. Pourtant Yusuke Hirota ne nous a pas habitué à travailler dans ce secteur. Il est plutôt dans les effets visuels et l'image de synthèse. Pour cette première réalisation, on peut dire qu'il a mis le paquet. C'est fluide, extrêmement riche en détails et sans beaucoup de recours à l'animation 3D, même si on se doute que tous les moyens modernes ont été mis en œuvre pour un tel résultat. Mais le dessin n'est pas le seul à être riche de détails car l'histoire aurait pu laisser place à plus d'un film, tant cet univers est fascinant, ainsi que les personnages secondaires. L'histoire justement...Elle parle évidemment d'écologie, d'environnement avec cette pollution qui emprisonne cette ville comme une île. Mais cela parle aussi du Japon, de ce côté insulaire du pays, un peu de son histoire aussi, même récente et cette tentation de se protéger à outrance. Et puis on parle d'obscurantisme, de droit à la différence...

En fait, comme beaucoup de films d'animation japonais, il y a une double lecture. On a un très beau film avec des personnages attachants pour les enfants, Poupelle, évidemment qui est ici interprété par Philippe Katherine en français qui colle très bien au personnage. Et puis il y a un film plus adulte par ses thèmes multiples, ses clins d’œil à la culture japonaise aussi. Un film qui est palpitant, émouvant avec des moments où l'on verse des larmes, drôle, touchant. Au rayon des regrets, une scène d'introduction façon roller-coaster un peu trop longue à mon goût, même si les séquences "jeux vidéos" sont une super idée. Le petit discours "les hommes ça ne pleure pas" est un peu dépassé mais je n'ai pas non plus lu le livre d'origine et en même temps, ça se dit encore, hum. Pourtant, il y a des personnages féminins forts, notamment celle qui sculpte et soude tout en grillant quelques ennemis au lance-flamme...

Tout ça nous donne 1h40 de bonheur et de ravissement pour les yeux, mais aussi les oreilles avec une bonne bande son, sa chanson finale comme il se doit. On a même un superbe numéro de danse avec là aussi quelques allusions très japonaises. Il date en réalité de 2020 et aurait même du sortir pour Halloween ou Noël pour être raccord. Mais voilà, tout a été chamboulé. Alors ce n'est pas une raison pour bouder notre plaisir pour un nouveau petit chef d’œuvre du pays du soleil levant. Et je sens qu'au deuxième visionnage, je découvrirai encore d'autres détails.

[Bande-annonce ![video](/images/youtube.png)](https://www.youtube.com/watch?v=EsONuxrIpG8){:target="_blank"}
