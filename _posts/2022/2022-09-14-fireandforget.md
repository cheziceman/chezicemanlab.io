---
layout: post
title: Souvenir de Gamer - Fire and Forget (1988)
category: geek
tags: geek,jeu video,retrogaming,1980s
---

**Parmi les jeux qui marquèrent mes années Amstrad CPC, il y a ce jeu de course un peu étrange, presque post-apocalyptique.**

L'éditeur français Titus y recycle un peu son fameux Crazy Cars en l'adaptant au goût du jour. Car le principe de la course de voiture mélé au jeu de tir, ce n'est pas une invention française. Le jeu ressemble étrangement à Roadblasters d'Atari. Titus s'inspirait beaucoup des hits d'arcade de toute façon. Roadblasters est sorti en 1987...et sera adapté en 1988. Et à revoir l'un, on peut trouver que Fire and Forget n'a pas pris beaucoup de lattitude pour la copie. Je pense aussi à Chase HQ de Taito en 88 qui reprend la recette avec un flic aux commandes. Et il y a évidemment Battle Outrun de Sega qui s'y mettra aussi...après la bataille.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/fireandforget.jpg)

*L'égypte, ça fait rêver*

Outre le fait d'avoir une voiture à conduire très vite pour arriver au checkpoint, il faut ici tirer sur des ennemis et gérer son carburant. Une jolie carte du monde nous fait voyager pour justifier le changement de décor. Et je vous passe le scénario de guerre mondiale qui justifie tout cela. Typiquement fin des années 80. On a donc une sorte de SUV avant l'heure, avec un canon sur le toit. Il faut tirer sur des chars ou les éviter, viser des hélicoptères ou avions, récupérer du carburant par bidons entiers. La jauge de fuel fond comme neige au soleil avec un tel véhicule. Et puis pour viser en conduisant, ça tient un peu du miracle. On apprend vite à faire des petits à-coups pour s'écarter au dernier moment. Et tout n'étant pas aléatoire, on apprend les séquences au fur et à mesure. 

Comment toujours à cette époque, le jeu sera adapté sur toutes les plateformes. Je l'ai vu tourner sur l'Amiga, mais on l'a retrouvé sur Atari, ZXSpectrum, C64, PC (beurk en CGA). Certaines utilisaient le clignotement de bandes pour l'impression de vitesse quand d'autres se contentaient d'utiliser le grossissement des sprites. Le jeu sera suivi d'un 2 où cette fois la voiture volait. A croire que Retour vers le futur 2 (1990) a inspiré les créateurs pour cette suite de 1990. Elle ne révolutionnait pas le jeu sinon et on peut prendre l'un comme l'autre aujourd'hui. 

Je n'ai pas rejoué à ce jeu mais je me souviens très bien de la maniabilité mais aussi de la répétitivité. C'est encore un jeu de scoring où non seulement il faut aller loin mais où l'on essaye de faire plus. J'ai vu quelques mondes différents à l'époque mais sans aller au bout. Je suis toujours étonné du temps passé par rapport au temps qu'il fallait pour terminer tous les mondes. Il ne m'a pas manqué grand chose apparemment sur les environnements, puisque j'ai souvenir de 4 d'entre eux.

Voilà ce que je ne suis jamais arrivé à faire : Terminer le jeu. 
[Le Longplay ![video](/images/youtube.png)](https://www.youtube.com/watch?v=55bS0vqFa_k){:target="_blank"}
