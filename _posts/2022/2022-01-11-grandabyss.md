---
layout: post
title: BD - Grand Hôtel Abîme de Marcos Prior et David Rubin (2016)
category: bd 
tags: comics, bd, social, capitalisme, lutte, dystopie, 2010s
---

**Voilà un comics assez étonnant sur le thème pourtant rabâché de la dystopie. Il l'est par son format, son graphisme autant que par son discours, infiniment social et actuel.**

Son format est en effet à l'italienne, donc dans l'horizontal. Cela le distingue rapidement sur un stand, mais en plus de cela, cela met en valeur le personnage sur la couverture. Petit détail qui a son importance, le livre est sorti en version espagnole en 2016 puis a été traduit en 2019 en anglais en Grand Abyss Hotel (et cette version l'a popularisé). La version française est chez Rackham sous le titre "Grand Hôtel Abîme".

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/grandabysshotel0.jpg)

Son graphisme saute aussi aux yeux dès la couverture. Classé dans la catégorie comics, on pourrait avoir l'impression d'un super-héros un peu déglingué façon Watchmen. Il y a une impression d'urgence, de flammes et ça se confirme dès les premières planches. Chaque page a été travaillée dans le moindre détail par David Rubin qu'on connaît aussi pour des séries comme Rumble, Beowulf, ... Il s'agit donc bien d'une BD espagnole mais dans un format plutôt US. La mise en page fait appel largement à l'infographie mais d'une manière très intelligente. Il y a le plus souvent une planche de fond, sur le prologue et le premier chapitre, puis des superpositions de vignettes et de dialogue. C'est un peu déroutant au début mais on s'y fait. Cela nécessite aussi une lecture plus attentive, jusqu'à chercher les détails, les nombreux clins d’œil à notre monde, à des références.

Et puis donc, il y a l'histoire et le discours. Comme je l'ai dit, il s'agit d'une dystopie. Nous sommes dans un pays en crise où la population se révolte contre l'austérité, la pauvreté. Le prologue rappelle des morceaux de discours, qui vont de Margaret Thatcher à des dirigeants plus récents, européens notamment. Tout est bon pour monter les uns contre les autres et faire passer la pilule par la peur. C'est un peu ce qui ressort. Mais ce monde dystopique est aussi aux mains de multinationales. Le sponsoring est partout et même le parlement se voir affublé d'un grand M d'un célèbre fast-food, tandis que les boucliers des C.R.S. sont ornés de logos de cartes bancaires. Le débat télévisuel est orienté et préfère dénoncer la violence des insurgés plutôt que de parler des causes. Tiens, ça ne vous rappelle rien ? Pile ce qui se passa en France pendant les Gilets Jaunes 2 ans après la parution de l'ouvrage.

C'est donc un comics éminemment politique mais qui pourrait passer pour simplement anti-libéral. Si on peut penser à la série "Black Mirror" parfois, ou à l'incendie de Rome, le foisonnement graphique peut gêner la compréhension de l'histoire. A tel point qu'il faut revenir dans une autre lecture afin de comprendre, sinon découvrir d'autres pistes. Par exemple, l'aspect "effet de groupe" ou le panurgisme de la foule est aussi traité. On peut le penser lié à ce capitalisme, mais l'est-il vraiment ? D'une certaine manière, tout le monde en prend pour son grade, du survivaliste au fasciste. Une œuvre de moins de 100 pages mais qui est complexe à appréhender, donc passionnante.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/grandabysshotel1.jpg)

