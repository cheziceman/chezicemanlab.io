---
layout: post
title: BD et Cinéma - Le Sommet des Dieux de Patrick Imbert (2021)
category: bd, cinema, 
tags: manga, seinen, alpinisme, film, cinéma, himalaya
---
**Le film de montagne est un genre oublié. Mais quand on le prend sous la forme de l'adaptation d'un manga, lui même adaptation d'un roman, cela renouvelle complètement le genre.**

Modeste randonneur, je n'ai rien d'un alpiniste mais la montagne, ça me gagne. J'ai vu des tonnes de films d'ascension quand j'étais plus jeunes, vibré aux exploits de ceux qui bravaient l'Everest, l'Annapurna, le Cervin, etc...Et d'un autre coté, le regretté Jiro Taniguchi fait partie de mes mangakas préférés. Sauf que curieusement, je n'avais pas abordé ses ouvrages consacrés à la montagne, comme cette adaptation d'un roman de Baku Yumemakura. La sortie de ce film m'a permis de rattraper cet oubli, ou cette erreur.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/sommetdieux.jpg)

C'est donc une production franco-luxembourgeoise qui s'emploie à adapter ce manga. Ca tombe bien, Taniguchi est proche de la ligne claire franco-belge. Mais son dessin hyper-détaillé en devenait déjà bluffant. Mis en image qui bougent, c'est encore plus frappant. Le début du film nous fait nous demander si ce sont des images réelles ou un dessin. Mais revenons un instant à l'histoire. Tous les films de montagne et d'ascension parlent du défi d'un homme (ou plus rarement d'une femme) face à un sommet. La fin, c'est le succès ou l'échec, souvent la mort. Et ici, nous suivons un journaliste spécialisé, Fukamachi Makoto, qui va enquêter sur un appareil photo qui serait la seule preuve que l'alpiniste anglais George Mallory a vaincu l'Everest en 1924 avant de disparaître. Cet appareil serait en possession d'un alpiniste japonais, Habu Jôji, qui ne donne plus signe de vie depuis des années. Orphelin, il a une passion pour la haute montagne qui l'a conduit à des excès. La mort d'un jeune compagnon de cordée l'a pourtant beaucoup affecté. Fukamachi trouvera-t-il Habu et comprendra-t-il ce qui le pousse vers le sommet de l'Everest ?

Le film ou le manga nous emmène donc dans les Alpes comme dans le massif de l'Himalaya, de Katmandu à Tokyo, dans ce qui ressemble aux années 80-90. La réalisation a essayé de rester fidèle au dessin de Taniguchi, notamment dans la palette de couleur qui reste sobre, mettant en valeur les alpinistes face à une montagne majoritairement blanche. Cela donne des plans magnifiques où l'on ressent vraiment l'immensité et la petitesse de l'alpiniste face à cela. Mais le livre/le film s'interroge beaucoup sur cette vanité de l'alpinisme, l'ego qui pousse certains à des aventures en solitaires, à chercher un exploit comme ne pas prendre d'oxygène ou enchaîner les sommets dans le mois de temps possible. Le journaliste ne cherche à priori que de belles images de montagne, à capter parfois l'exploit et le danger. Mais n'est-il pas aussi lui-même dans une recherche d'ego, le sien ou celui de ces hommes ? 

La beauté plastique du film est plus en avant que l'histoire, et cette opposition entre nos deux héros, qui étaient au coeur du manga (en 5 volumes). Forcément, le film pousse à lire...Comme si on recherchait ce qui manque pour comprendre un peu plus ces histoires personnelles. Et puis je me suis remémoré ces autres films vus il y a des années, ces émissions du dimanche sur ces aventuriers et ces aventurières (comme Catherine Destivelle, par exemple). Le sujet peut rebuter, c'est vrai mais il est plus large que la montagne. Il interroge sur l'humain et ses buts.

[Bande Annonce ![video](/images/youtube.png)](https://www.youtube.com/watch?v=sM_KmxpEaJU){:target="_blank"}
