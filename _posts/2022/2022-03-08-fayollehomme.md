---
layout: post
title: BD - L'homme en pièces de Marion Fayolle (2011)
category: bd
tags: bd, illustration, poésie, 2010s
---

**Pour un peu, j'aurais cru à un livre pour enfant, ou une réédition d'une ancienne bande dessinée, en regardant la couverture sur le présentoir. Mais non, c'est un livre assez singulier mais pour adulte.**

Enfin quand je dis pour adulte, ne croyez pas que c'est quelque chose de sexuel. A part qu'il y a des hommes, des femmes, parfois nus mais c'est le relationnel qui intéresse l'autrice. Il s'agit presque d'un livre d'illustration plutôt qu'une véritable BD. Il n'y a aucune bulle, juste une succession de dessins dans ce style si particulier qui est un peu naïf, démodé et finalement terriblement attachant.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/fayollehomme1.jpg)

Marion Fayolle observe l'humain et décompose cela en scénettes, avec un titre en haut de page et parfois des enchaînements. Cela donne des moments très poétiques, touchants, comme ces planches sur la solitude d'une femme dont on découvre que l'homme silencieux à sa table n'est qu'un décor. Cela donne aussi des moments plus surréalistes, des transformations des corps, des objets. Il y a de l'amour, de la violence, de la tendresse. Ce sont des moments de vie à travers le prisme de ces illustrations. Et parfois elles s’enchaînent entre plusieurs pages, chacune pouvant être prise indépendamment mais ne donnant sa pleine mesure qu'en lien avec les autres.

Quand je disais que ce n'est pas un livre qui s'adresse à tous, c'est qu'il faut prendre le temps d'entrer dans ces planches, de se laisser aller à la poésie, se confronter aux quelques mots du titre. Il faut du rêve et un regard sur soi, ses congénères humains. Marion Fayolle a trouvé son style dans d'autres albums également avec des thèmes. Celui de l'humain me paraissait le plus large.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/fayollehomme2.jpg)