---
layout: post
title: Littérature - Flashback Acide de Philippe Manoeuvre (2021)
category: litterature
tags: musique, 2020s, rock, autobiographie, littérature
---

**Je n'aime pas ce personnage, figure autoproclamée du rock en France, mais j'étais curieux de jeter un oeil sur son flashback sur ses années drogues.**

Pourquoi je n'aime pas Philman ? Très simple, c'est le type qui conchiait AC/DC et Queen jusqu'à ce que ces groupes deviennent des standards. Il est resté en boucle sur les Stones et n'a jamais été foutu de reconnaître les groupes rocks en devenir. Ah ça pour nous fourguer les ridicules BB Brunes comme du rock, y'avait du monde. Bref, c'est le type même du faux "rock critic", lécheur de burnes, fort avec les faibles et faible avec les forts. Et je dois avouer que pour ça, je n'ai pas été déçu par son petit livret.

Parler des Scorpions quand ils sont au sommet mais oublier tout ce qu'il y a avant, c'est typiquement Manoeuvre. Gorky Park en prend évidemment pour son grade, avec la condescendance qu'il se doit vis à vis des russes lors de la fameuse tournée des Scorps (il ratera le Moscow Peace Festival, évidemment) qui fit peut-être tomber plus vite le mur. Ok, c'était une opportunité de producteur comme tant d'autres groupes de cette période. Je vous passe les jeux de mots dont il est coutumier, mais au moins, il a un style. Et sous sa plume, Tonton Zeze n'est qu'un "DJ"..."Pardonne mais n'oublie pas, dit-il", hum. Et comme on s'y attend, on a la citation de toutes ses rencontres célèbres comme des faits d'armes. Juste les "légendes" (même ceux qu'il avait snobés à leurs débuts dans des chroniques assassines), pas les autres évidemment. Il trouve même le moyen de citer Kiss (en 2009 tout de même), alors que bon, il en rigolait bien au début.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/philmanacid.jpg)

Alors si je n'aime pas ce type, pourquoi chroniquer ainsi ce petit truc ? Parce que je lui trouve quand même de l'intérêt, vous allez voir. Déjà parce qu'il se déconstruit lui même sa statue sans s'en apercevoir. Le but, ce n'est pas d'aligner les faits d'armes, c'est de parler de l'enfer de l'alcool et la drogue, non ? J'avais parlé de [Nikki Sixx](https://www.cheziceman.fr/2007/sixxamheroindiaries/), un pape du genre (qu'il cite) et son bouquin était bien plus éloquent dans l'autodestruction. Manoeuvre à coté, c'est du fifrelin, le canada dry du junky. La période avec Virginie Despentes est explicitée, bof. Ils deviennent sobres ensemble, même si les champignons... De vagues descriptions de trip, parce qu'il faut encore se souvenir de ça après. Et ça part sur des digressions sur Bowie, une réinvention de l'histoire du punk au seul profit de son ami Marc Zermati, créateur de l'open market ...Un gros bazar dans sa tête pour oublier autant de choses autour du glam rock, des mods et du garage, et on se doute que c'est un amalgame de textes écrits à différentes périodes, sans doute pas dans le meilleur état cognitif. Il n'a toujours pas encaissé les critiques autour de "ses" babyrockers qui éclipsèrent alors tous les groupes qui tentaient réellement de faire vivre le rock en France : Mission impossible ? Sans doute sa manière à lui de faire enfin du constructif et de tenter de retrouver une jeunesse perdue. En d'autre termes, la crise de la cinquantaine que son petit groupe.

On a la vague impression, à la lecture de ce livret, que son métier, c'était de profiter du "Sex, drog & Rock'n roll" des autres finalement. Être dans l'entourage des groupes qui marchaient, les flatter pour continuer sa propre descente aux enfers. N'attendez pas de regard critique autre que "la drogue et l'alcool c'est mal". Ou "ouais, mais ça fait parti du deal, mec". Le constat de la déliquescence du Rock sans se dire lui même qu'il y a largement participé en jouant le jeu de la flatterie et du commercial (le parcours de ses poulains est édifiant). Le témoignage est brouillon mais on peut y pécher quelques anecdotes, si on ne les a pas entendu dans ses interviews qu'il ressasse en boucle. Un livre qui se retourne sur un passé peu glorieux auprès des gloires du rock. J'en ai connu des plus intéressants mais moins médiatiques...Même la couverture est moche. Alors l'escroc Phil, ça fait quel effet une critique aussi assassine que les tiennes ? 

#### Commentaires

**Ewen par Mail**

>Un homme qui a vécu, vit et vivra par procuration. Un ego, aussi. Au fond, il souhaite être le défricheur du Rock alors qu'il se comporte comme un fossoyeur. A une échelle moindre, c'est le gars qui t'interpelle de cette façon "quoi, toi, t'écoutes ce groupe ?!? Mais c'est ma génération, ça !!!", comme si naître à une époque apportait la légitimité ou non d'écouter une certaine musique.

**Sima78 par Mail**

>Je n’ai jamais apprécié Manœuvre, je suis complètement passé à côté de la sortie de son livre, tu m’as convaincu que cela ne me manquera pas. Par curiosité je suis allé voir ce qui se disait par ailleurs et c’est étonnant (faudrait-il dire "compréhensible" ?) le peu de commentaires constructifs, quand il y en a. J’ai également trouvé quelques extraits peu convaincants, dont un très parlant : "Comme moi, Bowie sentait que la façade rock se lézardait et que nos jours, nous étions bien d'accord, étaient désormais comptés." qui image très bien la modestie du personnage. Je pense qui si j’avais eu la chance d’échanger avec Bowie sur les questions du Rock j’aurais plutôt dit : Comme Bowie, je sentais … ou Bowie sentait que (…), et j’étais d’accord avec lui. Et le "nos jours" où il s’identifie en tant que Rock-Star… Ho garçon ! Tu n’es que critique animateur !

