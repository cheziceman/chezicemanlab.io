---
layout: post
title: BD - Malgré tout de Jordi Lafebre (2021)	
category: bd 
tags: 2020s, bd, romance, amour, 
---

**Ce ne sont pas les histoires d'amour qui manquent en littérature comme en bande-dessinée. Mais il y en a qui sont sublimées par la qualité de l'histoire, du dessin.**

La couverture ne cache rien du sujet : Un couple amoureux sous la pluie. Des mouettes : On est près de la mer. Une architecture classique plutôt du sud de l'Europe. Des vêtements plutôt des années 60-70? Un style expressif et dynamique. C'est tout ce que l'on sait au départ avant d'ouvrir et de lire les premières pages

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/malgretout1.jpg)

Et puis nous voilà à rencontrer ce même couple bien plus âgé dans les premières pages. Elle est ancienne maire d'une ville. Il est libraire sur le point de fermer son officine. Ils s'aiment et s'embrassent. Et pourtant on apprend qu'elle, Ana, est mariée à un certain Giuseppe. Lui, Zepo, n'a aucune attache à part 3 voisines qui aiment sa librairie comme son charme. L'histoire semble compliquée entre eux, un peu platonique. Mais on passe alors au chapitre suivant...Numéroté 20 !

On croit à un flashback mais petit à petit on découvre que tout va fonctionner à l'envers, à rebours. Nous remontons le temps pour comprendre ce qui les a amenés tous les deux à vivre une histoire d'amour à distance, avec si peu de contact à part un coup de téléphone nocturne, parfois et quelques lettres perdues. Il y a l'amour d'Ana pour Giuseppe, pour sa fille, sa passion pour sa ville dont elle est maire presque toute sa vie d'adulte. Il y a Zepo qui passe sa vie à écrire sa thèse qui parle de mécanique quantique et de temps, tout en parcourant la terre en bateau, en ayant des amours éphémères dans chaque port. Au fond, pour chacun, il n'y a qu'un seul amour passion, celui qui est né 40 ans plus tôt. 

Que voilà une belle histoire, très bien servie par ce dessin à la fois moderne mais classique dans sa forme "ligne claire", dynamique par ses expressions et ses mouvements, chatoyant par ses couleurs. La découpe en chapitre nous donne peu à peu les éléments, les liens entre les personnages, malgré ce récit à rebours. Comme la thèse de Zepo finalement où l'amour que l'on voit au début du livre est celui de la fin, mais n'existe que parce qu'il y a eu un début, que l'on découvrira à la fin. Une très belle comédie romantique, finalement mais sans Meg Ryan, ou Audrey Tautou. Évidemment, j'ai "adoré" parcourir ces pages. J'ai même pris mon temps pour le savourer encore plus lentement quand j'ai compris où cela pourrait m'emmener. Je conseille mais évidemment, il faut être sensible à ce genre pour l'apprécier, bien que je pense que cela pourrait ouvrir quelque chose de sensible chez le plus blasé ?

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2021/malgretout2.jpg)