---
layout: post
title: BD - Lugosi, grandeur et décadence de l'immortel Dracula de Koren Shadmi (2021)
category: bd, cinema, 
tags: biographie, bd, 2020s, cinéma, 
---

**Lugosi, c'est pour Bela Lugosi, acteur mythique mais souvent oublié malgré son rôle emblématique du comte Dracula. Il fallait bien une biographie pour en parler, et c'est tellement mieux de parler cinéma en BD.**

J'ai fait connaissance avec Bela Lugosi en trois étapes. Tout d'abord, j'ai lu il y a très longtemps une biographie de Boris Karlov, l'acteur qui créa le monstre de Frankenstein...Et qui accessoirement fut le rival à l'époque de Dracula, incarné par Bela Lugosi. Puis j'ai lu une de ces belles anthologies des films classiques, celle consacrée au Cinéma Fantastique, de Patrick Brion. Et puis enfin, j'ai vu le personnage à l'écran par l'entremise de Tim Burton dans son "Ed Wood", avec Martin Landau dans la peau de l'acteur. Alors quand j'ai vu cette jolie couverture barrée du nom de l'acteur, je n'ai pas résisté à ouvrir le volume et me dire que c'était pour moi.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/belalugosi1.jpg)

Bela Lugosi est hongrois et ne s'est jamais départi de ce fort accent hongrois, toute sa carrière. Très jeune, il quitte sa famille pour le théâtre, verse dans le militantisme communiste à la sortie de la seconde guerre mondiale, dans l'éphémère république populaire hongroise avant que la dictature en place ne menace sa vie. Dommage, il commençait à percer. Il part à Vienne sans le sou, avec sa femme de l'époque puis vers les USA où il recommence tout à zéro. Et c'est là qu'il débarque à Broadway et a la chance d'incarner le comte Dracula dans cette adaptation venue de Londres, du livre de Bram Stoker. Cela l’amène à Hollywood où un certain Carl Laemle Jr, jeune directeur d'Universal parie sur ce cinéma de genre. Les ligues de vertu auront une première fois raison du genre, avant qu'il ne revienne à la mode après la seconde guerre mondiale.

Oui mais...Bela Lugosi, outre l'accent qui le limite à des rôles exotiques, est un flambeur, un vantard, un séducteur-manipulateur. Il fait faillite de nombreuses fois. Sa vie amoureuse est houleuse avec ses multiples infidélités. Et c'est tout cela que Koren Shadmi nous raconte dans ces 180 pages. Le style reste assez épuré mais avec suffisamment de détails pour être à la fois lisible par un jeune lecteur que par un vieux passionné comme moi. L'auteur-dessinateur a puisé dans les biographies et anecdotes disponibles pour que cela reste passionnant. Il faut bien l'avouer, Bela Lugosi est pénible, détestable souvent, surtout avec ses épouses, et pourtant, il en devient attachant avec ses failles. Acteur incontestablement talentueux, il aura le malheur de ne pas être toujours à l'endroit où il aurait du être.

Alors, si vous aimez Dracula, les Biographies, l'Histoire du Cinéma, ça vous plaira. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/belalugosi2.jpg)