---
layout: post
title: BD - Le Complexe du Chimpanze de Richard Marazano et Jean-Michel Ponzio (2007)
category: bd 
tags: science-fiction, espace, 2000s, bd
---

**L'espace fascine toujours et encore, avec ses voyages intergalactiques. Mais avant cela, il y a la Lune, maintenant Mars. Et si nous étions confrontés à de nouvelles limites que nous n'envisagions pas ?**

Réunie dans une intégrale avec quelques bonus, les trois tomes de cette série nous emmènent dans un incroyable voyage. Mais avant ce voyage, il y a un évènement incompréhensible : 

*Une capsule spatiale tombe dans l'océan Indien à proximité d'un bâtiment de l'US Navy. Repêchée et ouverte, elle se révèle habitée par deux hommes qui prétendent être Neil Armstrong et Buzz Aldrin, les deux célèbres astronautes appartenant à la mission Apollo XI ayant marché sur la lune le 21 juillet 1969.Le problème? Nous sommes en 2035, et les deux astronautes en question sont revenus sur terre où ils sont morts depuis longtemps.*

L'héroïne, c'est Hélène, une femme astronaute dont le rêve est d'aller sur Mars. Juste avant cet évènement, la NASA a annulé le programme, faute de budget. Mais voilà qui relance tout, puisque c'est vers la Lune qu'elle doit aller avec ce matériel. Hélène est aussi maman de la petite Sofia qui ne supporte plus les absences de sa mère. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/complexechimpanze1.jpg)

Trois tomes et c'est bien suffisant pour ce récit qui fleure bon la mécanique quantique. Il faut accepter en tant que lecteur, de mettre en pratique le [Principe d'incertitude d'Heisenberg](https://fr.wikipedia.org/wiki/Principe_d'incertitude). Je te laisse lire l'article, lecteur et si tu es prêt, alors tu peux embarquer dans cette aventure. La mise en scène est assez grandiose, surtout dès que l'on part dans l'espace. J'ai tout de même un bémol sur l'aspect graphique qui paraît presque "digitaliser" des photographies. Au point de figer beaucoup trop les attitudes des visages et de ressembler parfois à un mauvais roman photo. La mise en couleur fonctionne particulièrement bien pourtant pour rendre cette atmosphère.

Étant plutôt client de ce genre de thématique quantique, j'ai accepté d'embarquer. J'ai vite compris ce qui pourrait se passer par rapport à la mise en pratique de ce principe, mais il y a justement de l'incertitude quant à l'avenir de cette mission, au devenir de la petite Sofia, à l'aspect mental de la chose aussi. Alors que nous commençons juste à esquisser de vraies missions au long cours, l'anticipation de 15 ans (28 ans à l'époque du premier tome) rend tout cela relativement crédible. Il faut conditionner le corps, l'esprit pour des mois, des années. Ce qui ne fonctionne pas, c'est l'aspect musculaire , l'aspect gravité, etc, mais bon, ça reste de la science...fiction aussi. C'est aussi une sorte de Gravity (le film) en plus grand avec les mêmes incohérences cinématographiques. De quoi faire rêver le lecteur qui le voudra bien.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/complexechimpanze2.jpg)

