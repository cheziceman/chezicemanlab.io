---
layout: post
title: Pause Vidéo et Poésie - 15 secondes
category: poesie 
tags: video, poésie, poème
---

**Juste une vidéo à regarder ou écouter, un texte qui dure le même temps à lire. 15 secondes cette fois.**

Face à l'océan

Les vagues, le vent, le firmament

La vie juste devant

Pour remonter dans le temps

[La Vidéo ](https://videos.pair2jeux.tube/w/j1mAWJP2vWmAWC8uii1Pra){:target="_blank"}
