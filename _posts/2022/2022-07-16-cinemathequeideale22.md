---
layout: post
title: Ma cinémathèque idéale
category: cinema,
tags: cinéma, cinémathèque idéale, imdb, liste
---

**J'ai vu un collègue [blogueur](https://lord.re/visionnages/topimdb/) partir de la liste IMDB des [250 meilleurs films](https://www.imdb.com/chart/top) et essayer de tout voir. J'ai depuis quelques années alimenté une liste de film avec le tag "cinémathèque idéale". Et comme je tiens aussi ma liste sur IMDB, je peux regarder toutes mes meilleurs notes.**

Ok, je suis cinéphile depuis mon passage à l'age adulte, quand j'ai pu aller plus au cinéma et j'aime beaucoup le noir et blanc, les vieux films classiques même si parfois la remise dans le contexte actuel est cruelle. D'ailleurs, la liste d'IMDB mérite quelques bémols sur des films qui n'ont pas encore assez de recul du public. Les films français classiques sont sous-cotés, voir enlevés du top 250 alors qu'ils y ont droit.

Je pars de la liste d'aujourd'hui en 2022 pour mon propre classement et il n'y en a pas vraiment 250 mais pas si loin. L'ordre n'est pas celui de mes préférences car je regroupe par note mais l'aspect aléatoire permet d'y piocher au hasard. Et j'ai fait le choix de ne pas mettre les films d'animation à part.

#### Notés 10

***Comédies***
* [Les Aventures de Rabbi Jacob](https://www.cheziceman.fr/2019/rabbijacob/) de Gérard Oury (1973) | IMDB 7,4 |

***Comédies dramatiques***

* Modern Times / Les temps modernes de Charlie Chaplin (1936) | IMDB 8,5 **TOP46** |
* [Mr. Smith Goes to Washington / M. Smith au sénat](https://www.cheziceman.fr/2017/mrsmithausenat/) de F. Kapra (1939) | IMDB 8,1 **TOP190** |
* The Great Dictator / Le Dictateur de Charlie Chaplin (1940) | IMDB 8,4 **TOP63** |

***Drames***

* The Remains of the Day / Les Vestiges du jour de James Ivory (1993) | IMDB 7,8 |
*	[Hana-bi](https://www.cheziceman.fr/2013/hanabi/) de Takeshi Kitano (1997) | IMDB 7,7 |
*	[The Green Mile / La Ligne verte](https://www.cheziceman.fr/2022/ligneverte/) de Frank Darabont (1999) | IMDB 8,6 **TOP26** |
*	[Sunrise: A Song of Two Humans / L'Aurore](https://www.cheziceman.fr/2013/laurore-murnau/) de FW Murnau (1927) | IMDB 8,1 |
*	[Freaks](https://www.cheziceman.fr/2012/freaks/) de Tod Browning (1932) | IMDB 7,9 |
*	Citizen Kane de Orson Welles (1941) | IMDB 8,3 **TOP93** |
*	[Les Enfants du paradis](https://www.cheziceman.fr/2022/enfantsparadis/) de Marcel Carné (1948) | IMDB 8,3 **TOP95** |
*	[Hadaka no shima / L'île nue](https://www.cheziceman.fr/2012/lilenue/) de Kaneto Shindo (1960) | IMDB 8,1 **TOP200** |
* The Elephant Man de David Lynch (1980) | IMDB 8,2 **TOP155** |
*	[Hotaru no haka / Le Tombeau des lucioles](https://www.cheziceman.fr/2018/tombeaudeslucioles/) de Isao Takahata (1988) | IMDB 8,5 **TOP49** |
	
***Films musicaux***
	
*	[Singin' in the Rain / Chantons sous la pluie](https://www.cheziceman.fr/2016/chantonssouslapluie/) de Stanley Donnen (1952) | IMDB 8,1 **TOP88** |
*	[The Band Wagon / Tous en scène](https://www.cheziceman.fr/2008/bandwagon/) de Vincente Minelli (1953) | IMDB 7,5 
	
***Science-Fiction***
* A Clockwork Orange / Orange mécanique de Stanley Kubrick (1971) | IMDB 8,3 **TOP102** |

***Thriller / Policier***

*	Kind Hearts and Coronets / Noblesse oblige de Robert Hamer (1949) | IMDB 8,0 |
*	The Third Man / Le Troisième Homme de Carol Reed (1949) | IMDB 8,1 **TOP180** |
*	Rear Window / Fenêtre sur cour d'Alfred Hitchcock (1954) | IMDB 8,5 **TOP51** |
*	The Night of the Hunter / La Nuit du chasseur de Charles Laughton (1955) | IMDB 8,0 |
*	The Birds / Les Oiseaux d'Alfred Hitchcock (1963) | IMDB 7,7 |
	

#### Notés 9

***Aventure***
*	Aladdin de Clements et Musker (1992) | IMDB 8,1 **TOP195** |
* Up / Là-haut de Docter et Peterson (2009) | 8,3 **TOP112** |
*	Toy Story 2 de John Lasseter (1995) | IMDB 7,3 **TOP74** |
* [Kari-gurashi no Arietti / Arrietty](https://www.cheziceman.fr/2011/cecilecorbel-arrietty/), le petit monde des chapardeurs de Hiromasa Yonebayashi (2010) | IMDB 7,6 |
*	Gladiator de Ridley Scott (2000) | IMDB 8,5 **TOP37** |
* Finding Nemo / Nemo de Stanton et Unkrich (2003) | IMDB 8,2 **TOP152** |
*	Kingsman: The Secret Service de Matthew Vaughn (2014) | IMDB 7,7 |
*	Hauru no ugoku shiro / Le Chateau Ambulant de Hayao Miyazaki (2004) | IMDB 8,2 **TOP164** |
* To Have and Have Not / Le Port de l'angoisse de Howard Hawks (1944) | IMDB 7,8 |
* Shichinin no samurai / Les 7 samurai de Akira Kurosawa (1954) | IMDB 8,7 **TOP20** |
* The Bridge on the River Kwai / Le Pont de la rivière Kwai de David Lean (1957) | IMDB 8,2 **TOP165** |
*	[Cartouche](https://www.cheziceman.fr/2018/catouche/) de Philippe de Broca (1962) | IMDB 6,5 |
*	Lawrence of Arabia / Lawrence d'Arabie de David Lean (1962) | IMDB 8,3 **TOP95** |
*	C'era una volta il West / Il était une fois dans l'Ouest de Sergio Leone (1968) | IMDB 8,5 **TOP46** | 
*	E.T. the Extra-Terrestrial de Steven Spielberg (1982) | IMDB 7,9 |
* Dances with Wolves / Dance avec les loups de Kevin Costner (1990) | IMDB 8,1 **TOP250** |

***Comédies***
* Groundhog Day / Un jour sans fin d'Harold Ramis (1993) | IMDB 8,1 **TOP222** |
* Shrek de Adamson et Jenson (2001) | IMDB 7,9 |
*	Tanguy d'Etienne Chatiliez (2001) | IMDB 6,4 |
* Arsenic and Old Lace / Arsenic et vieilles dentelles de Frank Capra (1943) | IMDB 7,9 |
*	Ratatouille de Brad Bird (2007) | IMDB 8,1 **TOP223** |
* Bringing up Baby / L'impossible monsieur Bébé de Howard Hawks (1938) | IMDB 7,8 |
* Some Like it Hot / Certains l'aiment chaud de Billy Wilder (1959) | IMDB 8,2 **TOP130** |
*	La guerre des boutons d'Yves Robert (1962) | IMDB 7,4 |
*	Dr. Strangelove / Dr Folamour de Stanley Kubrick (1964) - | 8,4 **TOP69** |
*	Les tontons flingueurs de George Lautner (1963) | IMDB 7,7 |
*	La Grande Vadrouille de Gerard Oury (1966) | IMDB 7,9 |
* Mary and Max de Adam Elliot (2009) | IMDB 8,1 **TOP205** |

***Comédies Dramatiques***
* Ed Wood de Tim Burton (1994) | IMDB 7,9 |
* The Big Lebowski des frères Coen (1998) | IMDB 8,1 **TOP200**|
* La vita è bella / La Vie est belle de Roberto Begnini (1997) | IMDB 8,6 **TOP25** |
*	The Truman Show de Peter Weir (1998) | IMDB 8,2 **TOP144** |
*	City Lights / Les lumières de la ville de Charlie Chaplin (1931) | IMDB 8,5 **TOP51** |
* Ma vie de Courgette de Claude Barras (2016) | IMDB 7,7 |
*	Stranger Than Fiction / L'incroyable destin d'Harold Creek (2006) | IMDB 7,6 |
* Limelight / Les Feux de la rampe de Charlie Chaplin (1952) | IMDB 8,0 |
*	La vache et le prisonnier d'Henri Verneuil (1959) | IMDB 7,1 |
*	Gisaengchung / Parasite de Bong Joon Ho (2019) | IMDB 8,5 **TOP35** |
* Les chatouilles d'Andrea Bescond et Eric Metayer (2018) | IMDB 7,1 |
*	Do the Right Thing de Spike Lee (1989) | IMDB 7,9 |

***Drames***
* Yume / Rêves d'Akira Kurosawa (1990) | IMDB 7,7 |
*	[The Father](https://www.cheziceman.fr/2021/thefather/) de Florian Zeller (2020) | IMDB 8,3 **TOP131** |
*	Hachi: A Dog's Tale / Hatchi de Lasse Hälstrom (2009) | IMDB 8,1 **TOP225** |
*	[Thelma & Louise](https://www.cheziceman.fr/2020/thelmaetlouise/) de Ridley Scott (1991) | IMDB 7,5 |
*	Schindler's List / La Liste de Schindler de Steven Spielberg (1993) | IMDB 9 **TOP6** |
* The Shawshank Redemption / Les évadés de Frank Darabont (1994) | IMDB 9,3 **TOP1** |
*	Saving Private Ryan / Il faut sauver le soldat Ryan de Steven Spielberg (1998) | IMDB 8,6 **TOP23** |
* The Thin Red Line / La Ligne rouge de Terrence Mallick (1998) | IMDB 7,6 |
* Polisse de Maïwenn (2011) | IMDB 7,3 |
* Breaking the Waves de Lars Von Trier (1996) | IMDB 7,8 |
*	The Sixth Sense / Le 6ème sens de M. Night Shyamalan (1999) | IMDB 8,1 **TOP141** |
* Dancer in the Dark de Lars Von Trier (2000) | IMDB 8,0 |
*	Requiem for a Dream de Darren Aronofsky (2000) | IMDB 8,3 **TOP84** |
*	Kikujirô no natsu / L'été de Kikujiro de Takeshi Kitano (1999) | IMDB 7,7 |
* Les Bas-fonds de Jean Renoir (1936) | IMDB 7,5 |
*	Le jour se lève de Marcel Carné (1939) | IMDB 7,7 |
*	The Hours de Stephen Daldry (2002) | IMDB 7,5 |
* La grande illusion de Jean Renoir (1937) | IMDB 8,1 |
*	Whale Rider / Paï l'élu d'un peuple nouveau (2002) | IMDB 7,5 |
* Le Quai des brumes de Marcel Carné (1938) | IMDB 7,8 |
* Rebecca de Alfred Hitchcock (1940) | IMDB 8,1 **TOP230** |
*	Bambi de Halgar, Armstrong et Hand (1942) | IMDB 7,3 |
*	[It's a Wonderful Life / La Vie est belle](https://www.cheziceman.fr/2016/lavieestbelle/) de Frank Capra (1946) | IMDB 8,7 **TOP21** |
*	All About Eve / Eve de Joseph L. Mankiewicz (1950) | IMDB 8,3 **TOP132** |
* 12 Angry Men / 12 hommes en colère de Sidney Lumet (1957) | IMDB 9 **TOP5** |
* Det sjunde inseglet / Le 7ème sceau d'Ingmar Bergman (1957) | IMDB 8,1 **TOP199** |
* Bab el hadid / Gare centrale de Youssef Chahine (1958) | IMDB 7,5 |
*	Morte a Venezia / Mort à Venise de Luchino Visconti (1971) | IMDB 7,4 |
*	One Flew Over the Cuckoo's Nest / Vol au dessus d'un nid de coucou de Milos Forman (1975) | IMDB 8,7 **TOP18** |
* Rocky de John Avildsen (1976) | IMDB 8,1 **TOP209** |
* Manbiki kazoku / Une Affaire de Famille d'Hirokazu Koreeda (2018) | IMDB 7,9 |
*	Little Lord Fauntleroy / Le Petit lord Fountleroy de Jack Gold (1980) | IMDB 7,4 |
*	The Shining de Stanley Kubrick (1980) | IMDB 8,4 **TOP61** |
*	Das Boot de Wolgang Petersen (1981) | IMDB 8,4 **TOP78** |
* La Strada de Federico Fellini (1954) | IMDB 8,0 |
* WarGames de John Badham (1983) | IMDB 7,1 |
*	Ran d'Akira Kurosawa (1985) | IMDB 8,2 **TOP139** |
* The Godfather / Le Parrain de Francis Ford Copolla (1972) | IMDB 9,2 **TOP2** |

***Documentaires***
* Inside Job de Charles Ferguson (2010) | IMDB 8,2 **TOP130** |
*	Bowling for Columbine de Michael Moore (2002) | IMDB 8,0 |
*	Royahaye dame sobh / Des rêves sans étoiles de Mehrdad Oskouei (2009) | IMDB 7,8 |

***Fantastique***
*	Batman Returns / Batman le défi de Tim Burton (1992) | IMDB 7,1 |
* [Beetlejuice](https://www.cheziceman.fr/2019/beetlejuice/) de Tim Burton (1988) | IMDB 7,5 |
*	The Lord of the Rings: The Fellowship of the Ring / Le Seigneur des anneaux, la communauté de l'anneau de Peter Jackson (2001) | IMDB 8,9 **TOP9** |
*	Tenkû no shiro Rapyuta / Le Chateau dans le ciel d'Hayao Miyazaki (1986) | IMDB 8,0 |
*	Nosferatu, eine Symphonie des Grauens / Nosferatu le vampire de FW Murnau (1922) | IMDB 7,9 |
* Sen to Chihiro no kamikakushi / Le Voyage de Chihiro de Hayao Miyazaki (2001) | IMDB 8,6 **TOP31** |
*	Edward Scissorhands / Edward au mains d'argent de Tim Burton (1990) | IMDB 7,9 |
	
***Films Musicaux***
*	The Commitments d'Alan Parker (1991) | IMDB 7,6 |
* Moulin Rouge! de Baz Luhrman (2001) | IMDB 7,6 |
*	The Wizard of Oz / Le Magicien d'Oz de Fleming, Cukor et Leroy (1939) | IMDB 8,1 **TOP218** |
*	An American in Paris / Un américain à Paris de Vincente Minelli (1951)| IMDB 7,2 |
*	Gentlemen Prefer Blondes / Les Hommes préfèrent les blondes de Howard Hawks (1953) | IMDB 7,1 |
* A Star Is Born / Une étoile est née de George Cukor (1954) | IMDB 7,5 |
*	Party Girl / Traquenard de Nicholas Ray (1958) | IMDB 7,0 |
*	Mary Poppins de Robert Stevenson (1964) | IMDB 7,8 |
* Hair de Milos Forman (1979) | IMDB 7,5 |
* This Is Spinal Tap de Rob Reiner (1984) | IMDB 7,9 |
* Brigadoon de Vincente Minelli (1954) | IMDB 6,8 |
* [Victor/Victoria](https://www.cheziceman.fr/2012/victorvictoria/) de Blake Edwards (1982) | IMDB 7,6 |

***Romance***
*	You've Got Mail / Vous avez un mess@ge de Nora Ephron (1998) | IMDB 6,7 |
* [Le fabuleux destin d'Amélie Poulain](https://www.cheziceman.fr/2016/yanntiersenamelie/) de Jean-Pierre Jeunet (2001) | IMDB 8,3 **TOP101** 
* Love Actually de Richard Curtis (2003) | IMDB 7,6 |
*	The Philadelphia Story / Indiscrétions de George Cukor (1940) | IMDB 7,9 |
*	To Be or Not to Be / Jeux dangereux de Ernst Lubitsch (1942) | IMDB 8,2 **TOP235** |
*	Doctor Zhivago / Docteur Jivago de David Lean (1965) | IMDB 8,0 |
*	When Harry Met Sally.../ Quand Harry rencontre Sally de Rob Reiner (1989) | IMDB 7,7 |

***Science-Fiction***
* Terminator 2: Judgment Day de James Cameron (1991) | IMDB 8,6 **TOP29** |
*	Gattaca / Bienvenue à Gattaca de Andrew Niccol (1998) | IMDB 7,8 |
*	Cube de Vincenzo Natali (1997) | IMDB 7,2 |
* [Metropolis](https://www.cheziceman.fr/2013/metropolis-lang/) de Fritz Lang (1927) | IMDB 8,3 **TOP120** |
*	[Avalon](https://www.cheziceman.fr/2020/avalon-kawai/) de Mamoru Oshii (2001) | IMDB 6,4 |
*	Back to the Future / Retour vers le futur de Robert Zemeckis (1985) | IMDB 8,6 **TOP30** |
* Brazil de Terry Gilliam (1985) | IMDB 7,9 |
* WALL·E de Andrew Stanton (2008) | IMDB 8,4 **TOP57** |
* 2001, l'odyssée de l'Espace de Stanley Kubrick (1968) | IMDB 8,3 **TOP89** |

***Thriller / Policier***
*	The Silence of the Lambs / Le Silence des agneaux de Jonathan Demme (1991) | IMDB 8,6 **TOP22** |
*	Léon de Luc Besson (1994) | IMDB 8,6 **TOP33** |
* Fargo des frères Coen (1996) | IMDB 8,1 **TOP170** |
*	[Margin Call](https://www.cheziceman.fr/2012/margincall/) de JC Chandor (2012) | IMDB 7,1 |
* M - Eine Stadt sucht einen Mörder / M Le Maudit de Fritz Lang (1931) | IMDB 8,3 **TOP97** |
* The Others / Les Autres de Alejandro Amenabar (2001) | IMDB 7,6 |
* Suspicion / Soupçons de Alfred Hitchock (1941) | IMDB 7,1 |
* L'assassin habite... au 21 de HG Clouzot (1942) | IMDB 7,3 |
*	Le corbeau de HG Clouzot (1943) | IMDB 7,9 |
*	Psycho / Psychose d'Alfred Hitchcock (1960) | IMDB 8,5 **TOP32** |
* Spellbound / La Maison du Dr Edwards (1945) | IMDB 7,5 |
*	The Big Sleep / Le Grand Sommeil de Howard Hawks (1946) | IMDB 7,9 |
* Notorious / Les Enchainés d'Alfred Hitchcock (1946) | IMDB 7,9 |
*	The Postman Always Rings Twice / Le Facteur sonne toujours deux fois de Tay Garnett (1946) | IMDB 7,5 |
*	Monsieur Verdoux de Charlie Chaplin (1947) | IMDB 7,8 |
*	The Lady from Shanghai / La Dame de Shanghai de Orson Welles (1947) | IMDB 7,5 |
*	Vertigo / Sueurs froides d'Alfred Hitchcock (1958) | IMDB 8,3 **TOP100** |
*	North by Northwest / La Mort aux trousses d'Alfred Hitchcock (1959) | IMDB 8,3 **TOP98** |
*	Le trou de Jacques Becker (1960) | IMDB 8,5 **TOP52** |
* In the Heat of the Night / Dans la chaleur de la nuit de Norman Jewison (1967) | IMDB 8,0 |
*	The Night of the Generals / La Nuit des généraux d'Anatole Litvak (1967) | IMDB 7,2 |

On voit quand même pas mal de films qui sont dans le top IMDB (70 ici plus ceux notés moins...au dernier pointage il m'en reste une petite soixantaine). Je trouve dommage que certaines catégories de films soient souvent dévalorisées par certains individus peu ouverts d'esprit. Mais bon, c'est le jeu et IMDB a quand même un goût très occidental, voire nord-américain... Il n'y a pas 250 films ici non plus mais seulement 166, si j'ose dire. Par contre, si je fais le compte par décennie dans ce top, ça donne ça : 

* 1920s ■■
* 1930s ■■■■■■■■■■■
* 1940s ■■■■■■■■■■■■■■■■■■■■■
* 1950s ■■■■■■■■■■■■■■■■■■■■■
* 1960s ■■■■■■■■■■■■■■■
* 1970s ■■■■■■■
* 1980s ■■■■■■■■■■■■■■
* 1990s ■■■■■■■■■■■■■■■■■■■■■■■■■■■■
* 2000s ■■■■■■■■■■■■■■■■■■■■■■■■
* 2010s ■■■■■■■■
* 2020s ■

Il est clair que je suis autour de deux époques : L'âge d'or d'Hollywood et les films de mon époque. Finalement assez prévisible pour un cinéphile. J'ai toujours trouvé que les films des années 70 vieillissent mal mais ce n'est pas partagé par beaucoup de critiques.


Pour les réalisateurs fétiches, on trouve : 
* **[Alfred Hitchcock](https://www.cheziceman.fr/2019/hitchcock-truffaut/)** : 8 films
* **[Charlie Chaplin](https://www.cheziceman.fr/2018/charliechaplin/)** : 5 films
* **Tim Burton** : 4 films
* **Howard Hawks** : 4 films
* **Stanley Kubrick** : 4 films
* Steven Spielberg : 3 films
* Hayao Miyazaki : 3 films
* Akira Kurosawa : 3 films
* David Lean : 3 films
* Marcel Carné : 3 films
* Vincente Minelli : 3 films 
* George Cukor : 3 films

Là encore, beaucoup de classiques et même du très vieux, seulement 2 "modernes". Mais sans ces classiques, les modernes n'existeraient pas. Ils sont tellement copiés.

Et vous ne serez donc pas surpris de prochaines chroniques de la cinémathèque idéale. J'ai de la marge...Il y aura peut-être de nouvelles entrées, d'ailleurs. Me voilà quand même embêté pour terminer en musique.


[Cinéma ![video](/images/youtube.png)](https://www.youtube.com/watch?v=kxI2g1I9s5Q){:target="_blank"}
