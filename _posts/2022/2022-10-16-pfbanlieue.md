---
layout: post
title: Portfolio - Banlieue en automne
category: photo
tags: photographie, film, argentique, banlieue, ville, streetphotography
---

**Je voulais me remettre à la photo argentique et j'ai donc remis en fonctionnement le vieux reflex et son 50mm 1,7**

![banlieue](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfbanlieue1.jpg)

![banlieue2](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfbanlieue2.jpg)

![Banlieue3](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfbanlieue3.jpg)

![banlieue4](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfbanlieue4.jpg)

![Banlieue5](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfbanlieue5.jpg)

![Banlieue6](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfbanlieue6.jpg)

![Banlieue7](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfbanlieue7.jpg)

![Banlieue8](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfbanlieue8.jpg)