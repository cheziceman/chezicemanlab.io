---
layout: post
title: BD - Le Chat du Rabbin de Joann Sfar (2002-)
category: bd 
tags: bd, religion, 2000s, 
---

**Au fil des tomes (11 à ce jour), cette série est devenue un classique. Pourtant ce n'était pas gagné de parler d'abord de la religion juive à travers le personnage d'un chat**

*"Au début du XXe siècle, le chat d’un rabbin d’Alger raconte sa vie et ses dialogues avec son maître. En effet, ce chat parle depuis qu’il a dévoré le perroquet de la maison et a tendance à dire ce qu'il pense sans inhibition. Il remet tout en question, autant les apprentissages du rabbin que les fondements mêmes du judaïsme. Craignant la mauvaise influence que son chat parlant pourrait exercer sur sa fille Zlabya, le rabbin les sépare. Le chat demande alors au rabbin de lui enseigner la Torah, le Talmud, la Michna et la Guemara."*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/chatdurabbin1.jpg)

Le chat apporte un détachement par rapport aux humains qui permet un jugement plus facile de toutes les situations rencontrées. Il est à la fois néophyte et connaisseur et s'interroge donc sur toutes les contradictions de la religion juive mais aussi du comportement humain. Nous sommes dans l'Algérie coloniale aussi avec cette forme d'Apartheid entre les colons et les autres, qu'ils soient juifs, musulmans, arabes, berbères, ... Le dessin de Sfar a du style, son style, avec une belle mise en couleur pour donner cette atmosphère orientale, méridionale. Il y a parfois ce coté naïf qui le rend très accessible aux plus jeunes mais ce sens du détail qui le rend intéressant pour les plus âgés.

Peu à peu, la série se détache de la simple retranscription de l'Algérie de l'époque ou de la simple religion juive pour parler simplement des humains, de leurs croyances et superstitions, de leurs différences et similitudes qui pourtant parviennent à les diviser. Le chat communique avec ses congénères animaux, perd pourtant la parole avec les humains dans le tome 2. Ce n'est pas grave, il continue de nous parler à nous. Et justement, je trouve que la série prend vraiment de l'épaisseur dès le tome 3. 

Série en cours, donc mais on peut piocher dedans à volonté. C'est ce que j'ai fait il y a d'abord quelques années sur le premier tome avant de laisser la série de coté et d'y revenir un peu par hasard pour reprendre le fil. Forcément, ça s’essouffle parfois, il peut y avoir des maladresses ou des planches moins efficaces mais j'aimerai bien avoir de telles discussions avec mes chats...Quoi que parfois.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/chatdurabbin2.jpg)

