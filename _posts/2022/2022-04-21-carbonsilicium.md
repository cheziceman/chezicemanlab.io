---
layout: post
title: BD - Carbone et Silicium de Mathieu Bablet (2020)
category: bd
tags: science-fiction, cybernétique, cyberpunk, anticipation, philosophie, 2020s
---

**La Bande-dessinée a cette capacité de distraire mais aussi de faire réfléchir. Cet ouvrage aborde ainsi l'Humanité dans un futur proche en la confrontant à l'intelligence artificielle et ce qu'elle sous-entend.**

Carbon et Silicium sont deux robots, ou plutôt deux intelligences artificielles que l'on a placé dans le corps de robots à notre image. Leur capacité d'autonomie, d'évolution est sans pareil mais ils ont été gardés dans une bulle par les chercheurs qui les ont créés. Le but de Noriko, la scientifique la plus impliquée dans le projet, est de les faire devenir des assistants parfaits pour une population humaine vieillissante. Mais en développant cette intelligence, elle et ses collègues leur ont donné la capacité à explorer le monde virtuel et ses données, et donc à capter toute l'agitation de l'humanité. Une humanité qu'ils ont du mal à comprendre et qu'ils voudraient explorer jusqu'à l'infini. Un jour, ils s'échappent tous deux et se retrouvent séparés.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/carbonsilicium.jpg)

Il y a beaucoup à dire sur ce récit qui va se dérouler sur plusieurs siècles. Car évidemment, si ces androïdes ont eu une durée de vie fixée, ils peuvent se réimplanter dans d'autres "corps". Le récit interroge des sujets comme l'intelligence humaine et ce qu'elle crée de néfaste. Se pose la question par exemple de notre violence envers nos congénères, de notre destruction des autres espèces, de notre environnement, tout comme notre envie de nous entourer d'animaux. L’interaction sociale est un des sujets majeur car on voit l'envie de ces deux intelligences de rencontrer d'autres humains, alors que justement l'humanité préfère devenir virtuelle, se murer, rester dans des mondes qui sont filmés, recréés synthétiquement. Mais c'est aussi une humanité qui verse dans le transhumanisme, jusque dans une prolongation de la vie plutôt extrême comme lorsque Noriko vit ses derniers instants. Au point que la différence entre humains et androïdes devient difficile au fil de l'évolution.

J'ai été dérouté au début par le dessin, les personnages à l'allure déformée, molle, presque désincarnée. Nos deux robots sont faits à notre image dans une caricature hypersexualisée. Mais ces corps ont l'air de se désagréger comme le monde autour d'eux. J'avais aimé la couverture qui me rappelait étrangement l'androïde de Metropolis tout en s'inspirant de créations japonaises, comme l'univers de [Shirow](https://fr.wikipedia.org/wiki/Masamune_Shirow). C'est sans doute moins complexe à comprendre que notre auteur japonais mais cela amène autant de questions. Les scènes où les intelligences sortent de leur enveloppe pour explorer le monde virtuel sont assez étranges parfois, pas toujours lisibles. Mais les planches qui montrent le monde extérieur fourmillent de détails, magnifiés par les choix de couleurs. La lecture n'est pourtant pas aisée, avec quelques histoire parallèles, un sentiment d'inachevé parfois et beaucoup de longueurs sur la seconde partie. Reste pourtant un ouvrage sur lequel on aime revenir, qui fait des choix radicaux et surtout qui nous interroge sur nous mêmes, humains.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/carbonsilicium2.jpg)

*voir aussi la [chronique d'Alias](https://erdorin.org/carbone-silicium/).*