---
layout: post
title: Littérature - La Ferme des animaux de George Orwell (1945)
category: litterature
tags: roman, fable, dystopie, animalisme, dictature
---

**Des animaux qui chassent les humains dans une ferme. Voilà qui ressemble à de l'animalisme. Mais cette fable d'Orwell (1984) peut avoir plusieurs lectures...et existe d'ailleurs sous différentes formes.**

Orwell n'était nullement vegan ou animaliste dans le sens où on l'entend aujourd'hui. Il utilisa cette forme de fable dans ce court roman pour émettre une critique de ce qui se passait en URSS. Après les années Lénine et l'espoir de lutte des classes et de revanche du prolétariat, le régime était passé sous le joug de Staline avec son lot de purges encore plus violentes et de déportations. Orwell reprend donc en condensé le cheminement de la révolution de 1917 jusqu'aux dérives d'un régime.

Dans sa fable, les cochons sont vus comme les plus intelligents des animaux et ceux qui mènent la rébellion. Ils écrivent un chant révolutionnaire pour galvaniser les troupes (équivalent à l'Internationale) et démontrent que les animaux n'ont pas besoin des humains alors que les humains ont besoin d'eux pour les exploiter. Ils parviennent à leurs fins mais bien vite une hiérarchie s'installe dans les différents animaux de la ferme. Des leaders cochons, ceux qui permettent de produire la nourriture sont sous leurs ordres et puis on trouve aussi un service d'ordre avec les chiens de la ferme. Chez les cochons, on retrouve une scission après la mort du leader initial qui correspond à la lutte entre Staline et Trotsky. On peut s'étonner de la crédulité des autres animaux face aux boniments de Napoléon (Staline) contre Boule de neige (Trotsky)

J'avais tenté de théoriser les relations humaines dans un groupe dans un [billet](https://www.cheziceman.fr/2021/theoriensemble/) et il en va aussi de cela ici. Les meneurs oublient rapidement les idéaux initiaux, même s'ils sont écrits. Le temps fait son œuvre et on se débarrasse des gêneurs, on utilise les plus crédules, ou ceux que l'on peut corrompre. La question que ne se pose pas Orwell, c'est est-ce que cela aurait pu fonctionner sans cette dérive. Sa fable est une sorte de pamphlet anti-stalinien mais on peut l'interpréter plus largement contre le totalitarisme. La dictature du prolétariat, ou ici des animaux, ne contient-elle que les racines du totalitarisme ? Il y a en effet un refus radical des humains qui montre ses limites dans l'autarcie mise en place. Et ce fut aussi le cas dans le communisme initial qui avait besoin de transition. Pourtant, il y avait d'autres éléments de progrès et de liberté, trop souvent oubliés.

Je ne vais pourtant pas disserter sur ce court roman car cela revient à disserter sur Marx ou sur d'autres théoriciens du collectivisme, ou des critiques du capitalisme. Plus qu'à un système, je m'attache plus au comportement humain qui délite le système. Et dans cette "Ferme des animaux", j'y vois aussi une hiérarchisation par rapport à des idées très humaines, comme la supériorité supposée de l'intellectuel par rapport au manuel ou encore des ressorts du patriarcat pour asseoir un pouvoir. Il n'y a pas de figure féminine forte dans ce livre...fruit d'une époque aussi. Le coté dystopique est bien moins présent par rapport à d'autres œuvres.

Je parlais de différentes formes pour le lire. Si le roman est court et se lit bien, il y a aussi une BD sortie récemment assez fidèle malgré quelques changements de nom qui peuvent perturber. A chacun de choisir (il y en a d'autres) mais surtout de réfléchir au delà de la volonté initiale de l'auteur sur ces relations dominants-dominés qui empêchent cette petite planète bleue de tourner rond.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/fermeanimaux.jpg)


