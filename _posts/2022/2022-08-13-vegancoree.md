---
layout: post
title: Veganisme au quotidien - Printemps coréen
category: vegan
tags: recettes,vegan,vegetarien,végétarisme, corée
---

**Et bien pour cette salve de recettes, partons du coté de la Corée (sud et nord, même si je doute qu'on mange à sa faim au nord). Aucun des liens n'est sponsorisé et le plus dur c'est souvent de trouver les ingrédients. Donc j'ai mis quelques boutiques que j'utilise aussi.**

*On commence par une recette devenue plus courante dans beaucoup de restaurants/traiteurs asiatiques (souvent tenus par des chinois). J'ai découvert ça à Seoul mais il y a autant de variantes que ce que l'on a dans son frigo.*

**Bibimbap**

Avant toute chose, il vous faudra faire du riz, de préférence dans le cuiseur de riz, sinon cuit à l'asiatique et donc un riz qui vient d'Asie du sud-est, voire du Japon/Corée. N'oubliez pas de le rincer au moins 3 fois avant la cuisson. Pour le reste, c'est un peu comme on veut mais il faut au moins ça :
* Du Kimchi (voir recette plus loin)
* Des algues pimentées comme [celles-ci](https://online.k-mart.fr/products/k-mart-seaweed-prepared-with-spices-130g?_pos=8&_sid=2180c8c47&_ss=r) ou [celles-là](https://yuns.fr/recette-haricot-bokkeum/). 
* De l'huile de sésame
* Des grains de sésame
* [Gochujang](https://fr.wikipedia.org/wiki/Gochujang) ou pâte de piment amer
* Un oeuf...arg c'est plus vegan là. A la limite, si on tue pas les poussins mâles ? 

Et puis après quelques idées : 
* Epinards
* Champignons Shitake ce qui remplace l'habituelle viande des bibimbap carnivores.
* Concombre
* Germes de soja
* Salade
* Oignons légèrement frits

Le principe est que l'on met le riz chaud au fond et que l'on a fait revenir évidemment quelques ingrédients pour qu'ils soient chauds. On met un filet d'huile de sésame, une cuillère de Gochujang. Et on termine par l’œuf au dessus avec les graines de sésame. Deux écoles : L’œuf cuit au plat ou l'oeuf cru qui cuira avec le reste lorsque l'on mélangera. Car si les coréens utilisent des baguettes métalliques pour manger, ici ça se mélange avec une cuillère et ça se mange avec. Le riz doit être un peu croustillant au fond en général car il a cuit dans le bol. A noter sur la photo le bol en pierre qui permet de conserver la chaleur. Mais bon, on fait comme on peut.

![bibimbap](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/Korean.food-Bibimbap-02.jpg/640px-Korean.food-Bibimbap-02.jpg)

*Un exemple parmi d'autres (Wikipedia)*


*Plus difficile à faire à cause des ingrédients, c'est encore un plat populaire que l'on peut trouver dans la rue. Attention c'est épicé. Les Tteok sont des grosses pâtes de riz épaisses et cylindriques.*

**Tteokbokki ou Topokki**

Sauce pour 250g de Topokki

* 1 paquet de [Tteok](https://www.asiamarche.fr/epicerie-asiatique-en-ligne/2181-pate-tteok-rice-cake-pour-topokki-600g-matamun-3701471708408.html)
* 1 ou 2 cuillères de Gochujang.
* Carottes coupées en petits dés
* 2 cuillères de sucre
* 1 cuillères de sauce soja
* un peu de poudre de piment
* Équivalent d'une gousse d'ail
* 1 verre d’eau
* Deux à trois cuillerées de sauce tomate.

Mettre les carottes dans une casserole et recouvrir à peine d'eau puis laisser bouillir puis mijoter jusqu'à feu doux. Rajouter le reste des ingrédients. Mélanger et faire chauffer. Pendant ce temps, plonger les pâtes Tteok dans l'eau bouillante dans 10 à 15 minutes. Réunir le tout pour servir chaud avec de la ciboule.

![tteok](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Tteokbokki.JPG/640px-Tteokbokki.JPG)

*On voit les pâtes sans sauce sur le coté droit (Wikipedia)*


*L'élément de base de la cuisine coréenne, c'est le fameux Kimchi. Mais la plupart du temps on le trouve avec de l'Anchois. Je vous en propose une variante vegane à faire fermenter soi-même.*

**Kimchi vegan**

- 2kg de chou chinois
- 100 à 160g de gros sel
- 500g de carottes
- 2 poireaux
- 3 gousses d'ail
- 3 à 4 cm de gingembre
- 2 CS de piment
- 1 CS de sucre

Découper le chou en lanières puis le mettre dans un grand récipient qui se ferme bien, genre gros pot de cornichon molossol...ou ancien pot de Kimchi du commerce, ha ha. Ajouter le gros sel et mélanger. fermer et laisser une nuit. Rincer le chou et égoutter le lendemain. Éplucher le gingembre. Râper les carottes et émincer les poireaux. Écraser l'ail et mélanger avec le piment et le sucre. Mélanger le tout avec le chou égoutté. Conserver dans des bocaux hermétiques.

![kimchi](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Korean_cuisine-Kimchi-08.jpg/640px-Korean_cuisine-Kimchi-08.jpg)

*Avec ou sans anchois, ça ne se voit pas (Wikipedia)*


*Et puis une recette que l'on trouve dans l'Asie du nord, même si c'est originaire de Chine. J'ai découvert ça dans des boutiques japonaises mais j'en ai vu aussi en Corée du sud.*

**Mabo Tofu / Mabodofu / Mapa Dubu** 

* 1 bloc de tofu
* 1 bloc de tofu soyeux
* 1 CS d'huile de sésame et du piment / poivre du sichuan broyé ou un sachet de mélange
* 1 CS d'huile de tournesol
* De la salade ou des poireaux
* 2 CS de fécule

Faire d'abord revenir la salade dans l'huile puis rajouter le mélange d'huile. Mettre les cubes de tofu pour les colorer. Dans un bol, mettre la fécule et la délayer dans un volume d'eau froide. rajouter le mélange eau=fécule et mélanger pour que l'ensemble prenne. Servir avec du riz blanc.

![mabo](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Mapodoufu.jpg/640px-Mapodoufu.jpg)

*image Wikipedia par [Toddfast](https://en.wikipedia.org/wiki/User:Toddfast)*

Avec tout ça, il y a de quoi avoir faim ou avoir envie de découvrir la gastronomie coréenne. Il y a beaucoup de grillades par contre, mais on trouve des variantes végétariennes dans les bonnes enseignes, comme par exemple l'excellent restaurant Biguine à Paris dans le quartier de la station Pyramides.
