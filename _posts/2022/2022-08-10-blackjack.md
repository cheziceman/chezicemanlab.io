---
layout: post
title: BD - BlackJack de Osamu Tezuka (1973-1983)
category: bd
tags: manga, japon, médecine, 1970s, 
---

**Encore un classique du maître du Manga, Osamu Tezuka, qui peut donner l'impression d'avoir vieilli. Et pourtant, c'est un témoignage de ce Japon qui s'était relevé de la guerre et des interrogations sur l'avenir du pays. Tout ça à travers un médecin rebelle.**

Cette longue série est sortie juste avant les [3 adolfs](https://www.cheziceman.fr/2012/3adolfs/). On retrouve un trait très caractéristique, au point que certains personnages font penser à ceux de [Astro Boy](https://www.cheziceman.fr/2020/astroboyantho/). Mais Tezuka va sur un sujet inédit : "Black Jack est un médecin de l'ombre. Il doit son visage bicolore au don de peau fait dans son enfance par un camarade de classe métis le jour où un accident l'a laissé défiguré. Ayant suivi des études de médecine pour honorer son sauveur, il refuse cependant de passer les examens finaux pour protester contre la corruption et le conservatisme qui plombent la médecine japonaise. En marge de la société, il n'hésite pas à venir en aide à quiconque, indépendamment de la moralité de la victime, qui demande du secours. Il peut tout aussi bien exiger des sommes astronomiques de ses patients que les aider sans rien demander en retour, tout dépendant de leurs possibilités financières et de leur mentalité."

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/blackjack1.jpg)

Kaze a eu la bonne idée de rééditer tout cela en une nouvelle édition à la couverture attirante. Le seul problème, c'est de les trouver... Ce sont donc des chapitres de quelques dizaines de pages qui se succèdent, sans vraiment de liens les uns avec les autres. On peut donc passer d'un patient exposé aux radiations d'un essai nucléaire dans le pacifique, à un apprenti restaurateur de sushi qui perd l'usage de ses bras. On y découvre peu à peu la personnalité et les origines de notre héros si peu bavard. Il a une assistante plutôt curieuse, la petite Pinoko. Elle a le physique d'une petite fille mais l'âge réel d'une jeune femme, donc amoureuse de son patron. Notre Black Jack rappelle un peu Albator (1969) avec ses cicatrices, son mutisme et son mystère. Lui aussi part rendre à sa manière la justice. On est dans un graphisme typique des mangas des années 70.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/blackjack2.jpg)

J'avais un peu de mal avec ce dessin au début et le décalage avec le sérieux et même la précision qu'il peut y avoir pendant les opérations. L'opposition dessin enfantin / histoire adulte surprend. Mais peu à peu, je me suis pris au jeu et j'ai dévoré les différents volumes que j'ai pu trouver. Il y a aussi l'envie d'en savoir plus sur notre personnage, comme fil rouge. On est dans une médecine-fiction, surtout replacé dans le contexte des années 70. Mais on retrouve des thèmes récurrents dans le japon d’aujourd’hui, comme par exemple la robotisation. Et ça, pour le créateur de Astro Boy, c'est marquant. Voir par exemple cet hôpital automatisé dans l'histoire d'U-18 dans le premier volume. On sent poindre alors une critique sociale du Japon et des dérives du pays. Ou encore la poignante histoire de Triton l'orque. Il y a aussi l'idée que la médecine et la science ne peuvent pas tout faire, que cela peut créer des monstruosités. D'autres [mangas](https://www.cheziceman.fr/2018/lastheroeinuyashiki/) plus récents abordent à nouveau ces thèmes.

Le manga a été adapté à l'écran en série et en OAV. Puis il y a même eu un film avec de vrais acteurs. Difficile à trouver, il faut se rabattre sur les bibliothèques si les personnes responsables ont le bon goût de se l'être fourni. Parce que certains prix d'occasion dépassent l'entendement.