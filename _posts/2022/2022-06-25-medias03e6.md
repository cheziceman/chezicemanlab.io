---
layout: post
title: La Revue de Médias S03E06, Juin 2022
category: geopolitique 
tags: géopolitique, culture, environnement
---

**Je reviens enfin à ma vie passée, sans élections, sans rien, même si ça laissera des traces. On en parle la semaine prochaine. En attendant, il s'est passé beaucoup de choses mais j'essaye de ne pas trop en mettre.**

*Comme toujours, vous pouvez ouvrir les liens dans d'autres onglets par utilisation de la touche CTRL ou bien appui long sur smartphone. le (EN) pour les articles en anglais, et l'icone coups de coeur ![cc](/images/coeur.png)*

### Géopolitique

* **Afrique** : 
	* La litanie des attaques continue en Afrique de l'ouest, où la [France](https://www.courrierinternational.com/article/perte-d-influence-en-afrique-de-l-ouest-un-pre-carre-francais-en-peau-de-chagrin) n'a déjà plus son mot à dire. Le *Burkina Faso* est encore [touché](https://www.rfi.fr/fr/afrique/20220521-burkina-faso-les-forces-arm%C3%A9es-repoussent-une-attaque-de-la-base-de-bourzanga). Les [déplacements de population](https://www.rfi.fr/fr/afrique/20220528-plus-de-72-000-personnes-d%C3%A9plac%C3%A9es-en-raison-des-combats-dans-l-est-de-la-rdc) en *RDC* se poursuivent sur fond de combats.
	* L'urgence alimentaire déjà importante dans la corne de l'Afrique, [s'étend maintenant](https://www.rfi.fr/fr/afrique/20220603-tchad-le-gouvernement-d%C3%A9clare-l-urgence-alimentaire) sur le continent entier avec le *Tchad*. Au point que les [représentants du continent](https://www.rfi.fr/fr/afrique/20220603-macky-sall-rencontre-son-vladimir-poutine-pour-alerter-sur-l-impact-de-la-guerre-en-ukraine) essaient de [peser(EN)](https://responsiblestatecraft.org/2022/06/06/the-war-in-ukraine-through-an-african-lens/) dans le conflit Russo-Ukrainien.
	* Le *Rwanda* a été au centre de l'actualité pour son accueil de réfugiés en provenance du Royaume-Uni. Mais ce scandale ne masque pas les relations compliqués avec ses [voisins](https://www.rfi.fr/fr/afrique/20220616-rdc-tous-les-accords-sign%C3%A9s-avec-le-rwanda-suspendus-apr%C3%A8s-un-conseil-de-d%C3%A9fense) de la région des lacs.
	
* **Asie** : 
	* Bien que l'*Australie* soit en océanie, le [changement de régime](https://www.rfi.fr/fr/asie-pacifique/20220521-australie-les-travaillistes-d-albanese-d%C3%A9logent-les-conservateurs-du-pouvoir) du pays aura des influences en Asie-Pacifique. Le [Quad](https://www.rfi.fr/fr/asie-pacifique/20220524-l-ordre-international-au-menu-du-quad-l-ombre-de-la-chine-a-plan%C3%A9-sur-la-r%C3%A9union-de-tokyo) formé avec Japon, USA et Inde en sera-t-il changé ? Face à cela, la *Chine* joue son jeu avec les [mêmes armes](https://www.rfi.fr/fr/asie-pacifique/20220526-dans-le-pacifique-sud-p%C3%A9kin-vante-son-accord-r%C3%A9gional-de-libre-%C3%A9change-et-de-s%C3%A9curit%C3%A9). Pendant ce temps, de [gigantesques manœuvres navales(EN)](https://asiatimes.com/2022/06/worlds-largest-joint-naval-exercise-a-message-to-china/) ont lieu pour faire "peur" à la Chine...qui pourrait trouver un allié aux [Philippines(EN)](https://asiatimes.com/2022/06/marcos-to-tap-chinas-assistance-for-economic-recovery/) ? 
	* De la crise COVID on passe à la crise alimentaire qui a aussi des [conséquences](https://www.courrierinternational.com/une/la-une-du-jour-la-crise-alimentaire-bombe-a-retardement-en-asie) en Asie. Il y a bien sûr l’aggravation et l'instabilité politique qui en découle au [Sri Lanka](https://www.rfi.fr/fr/asie-pacifique/20220525-sri-lanka-les-p%C3%A9nuries-qui-frappent-les-plus-d%C3%A9munis-s-aggravent-encore). 
	* L'Asie compte encore une nouvelle puissance [spatiale](https://www.rfi.fr/fr/asie-pacifique/20220621-s%C3%A9oul-lance-sa-premi%C3%A8re-fus%C3%A9e-spatiale-%C3%A0-partir-de-technologies-sud-cor%C3%A9ennes) avec la *Corée du sud* cette fois.
	
* **Amérique du nord** :
	* Il faut rappeler toute l'histoire d'*Haïti* et la [dette injuste](https://www.rfi.fr/fr/am%C3%A9riques/20220525-ha%C3%AFti-l-%C3%A9crasante-dette-pay%C3%A9e-%C3%A0-la-france-au-moment-de-l-ind%C3%A9pendance) qui accentua le chaos de ce pays.
	* Et le parti républicain des *USA* qui continue sa longue dérive vers son [extrême](https://www.courrierinternational.com/article/etats-unis-le-parti-republicain-du-texas-glisse-vers-l-extreme-droite). Ca me rappelle quelque chose...Et la politique internationale du gouvernement Biden reste toujours sous l'influence des [Néo-conservateurs(EN)](https://asiatimes.com/2022/06/neocon-think-tanks-are-driving-bidens-ukraine-policy/).
	
* **Amérique du sud et centrale** : 
	* Des élections ont lieu en *Colombie* avec enfin une représentativité pour la [communauté noire](https://www.courrierinternational.com/article/temoignages-francia-marquez-candidate-a-la-vice-presidence-au-nom-des-ignorees-de-colombie). La situation reste explosive notamment avec les [cartels](https://www.monde-diplomatique.fr/2022/05/GOMEZ/64619) et anciens FARC. Mais c'est encore un pays basculant [à Gauche](https://www.courrierinternational.com/article/politique-le-triomphe-inedit-de-gustavo-petro-premier-president-de-gauche-en-colombie).
	* Le *Venezuela* se rapproche de l'Iran pour un [accord](https://www.france24.com/fr/moyen-orient/20220611-l-iran-et-le-venezuela-vis%C3%A9s-par-des-sanctions-am%C3%A9ricaines-concluent-un-accord-de-20-ans). Les ennemis de mes ennemis sont mes amis...
	* En *Equateur*, comme d'autres pays du continent, le ton monte entre le gouvernement et les [amérindiens](https://www.courrierinternational.com/article/bras-de-fer-manifestations-en-equateur-le-ton-monte-entre-les-amerindiens-et-le-gouvernement).
		
* **Europe** : 
	* La *Grèce* et la *Turquie* se regardent toujours en chien de faïence mais avec [les USA](https://www.courrierinternational.com/article/visite-l-idylle-greco-americaine-affichee-a-washington-deplait-a-ankara) comme arbitre. Mais la Turquie se projette plus sur le moyen-orient ces dernières semaines avec la [Palestine](https://www.rfi.fr/fr/moyen-orient/20220524-visite-historique-du-ministre-turc-des-affaires-%C3%A9trang%C3%A8res-dans-les-territoires-palestiniens), la [Syrie](https://www.rfi.fr/fr/moyen-orient/20220524-la-turquie-projette-une-quatri%C3%A8me-op%C3%A9ration-en-syrie) et l'[Arabie Saoudite](https://www.courrierinternational.com/article/visite-en-se-rendant-en-turquie-mbs-scelle-le-rapprochement-entre-riyad-et-ankara)
	* Le système de santé Français n'est pas le seul touché en Europe. Même causes (libéralisme économique, coupes sombres dans le service public...), mêmes effets avec [l'Allemagne](https://www.courrierinternational.com/article/soins-sans-les-travailleurs-etrangers-le-systeme-de-sante-allemand-serait-au-bord-de-l-effondrement).
	* Les sanctions vis à vis de la *Russie* [pèsent bien peu](https://asiatimes.com/2022/06/how-the-west-miscalculated-its-ability-to-punish-russia/) dans la réalité sur l'économie russe, faute d'anticipation. Et l'union avec l'Ukraine commence à se [fendiller](https://www.courrierinternational.com/article/vu-de-tchequie-l-occident-embarrasse-par-l-obstination-de-l-ukraine) devant le jusqu'au boutisme de son dirigeant.
		
* **France** : 
	* Un rappel des positions de la France [face à l'OTAN](https://blog.mondediplo.net/otan-reprends-ton-vol) et ce qui pourrait en advenir.


* **Moyen-orient** : 
	* Le *Liban* est toujours dans une terrible situation, au point que la [population](https://www.rfi.fr/fr/moyen-orient/20220526-face-%C3%A0-la-pire-crise-%C3%A9conomique-depuis-150-ans-les-libanais-cherchent-%C3%A0-quitter-leur-pays) continue de quitter le pays. Les législatives ont donné des [enseignements](https://theconversation.com/les-principaux-enseignements-des-elections-legislatives-libanaises-183560). Et Israël de profiter de la situation pour profiter de [gisements frontaliers](https://www.rfi.fr/fr/moyen-orient/20220605-le-liban-s-insurge-de-l-exploitation-du-champ-gazier-de-karish-par-isra%C3%ABl).
	* Au *Yemen*, une [trêve](https://theconversation.com/treve-au-yemen-lespoir-est-il-permis-183063) est signée mais pour combien de temps et quelle issue ? 
	* En *Israel*, c'est la montée des extrêmes aussi coté juif. On en a eu une dramatique [illustration](https://www.courrierinternational.com/article/opinion-marche-des-drapeaux-a-jerusalem-l-embrasement-est-pour-demain) qui fait craindre l'explosion. Et de là au retour de Netanyahu ? Et même les biens de l'église [chrétienne orthodoxe](https://www.rfi.fr/fr/moyen-orient/20220610-isra%C3%ABl-la-justice-confirme-la-vente-de-biens-immobiliers-de-l-%C3%A9glise-%C3%A0-des-colons-juifs) sont visés.
	
* **Droit des femmes** : 
	* Et le droit à l'avortement encore [attaqué aux USA](https://fr.news.yahoo.com/%C3%A9tats-unis-loi-bannissant-lavortement-005436251.html), avant le coup de grace de la cour suprême !
	
### Environnement

* **L'énergie** : 
	* La *transition énergétique* en Afrique, on en parle [trop peu](https://www.rfi.fr/fr/afrique/20220527-la-transition-%C3%A9nerg%C3%A9tique-de-l-afrique-pr%C3%A9occupe-%C3%A0-six-mois-de-la-cop27) alors que c'est critique en période de renchérissement des ressources.
	* On en a parlé pendant les législatives mais le *Nucléaire* a besoin de refroidissement, donc d'eau pas trop chaude. Les périodes de [canicule](https://www.courrierinternational.com/article/vu-du-royaume-uni-l-avenir-du-nucleaire-en-france-menace-par-les-pannes-de-centrales) n'aident pas mais c'est aussi un problème de conception en fonction de la localisation.
	* Et du côté de l'Hydrogène, on progresse encore pour rendre cela [renouvelable](https://theconversation.com/une-nouvelle-famille-de-materiaux-pour-la-production-solaire-dhydrogene-renouvelable-181313).
	
* **Les Transports** :
	* Le transport spatiale mais aussi les communications pourraient être touchés par la pollution engendrée par [nos débris(EN)](https://arstechnica.com/?p=1856018) autour de la planète. D'autant que de nouvelles [puissances spatiales(EN)](https://arstechnica.com/?p=1856980) apparaissent.
	* L'*hydrogène* progresse encore et toujours avec cette fois des [cartouches portables](https://www.moniteurautomobile.be/actu-auto/innovation/toyota-developpe-cartouches-hydrogene-portables.html).
	* Fin des *moteurs thermiques* en 2035 en [Europe](https://fr.news.yahoo.com/eurod%C3%A9put%C3%A9s-signent-mort-voitures-%C3%A0-040234173.html)...sauf pour les modèles à moins de 1000 exemplaires. Forcément, Italiens et Allemands perdaient en prestige.
	
* **L'Eau** :
	* Les liens entre extinction de la biodiversité et produits [chimiques](https://theconversation.com/produits-phytopharmaceutiques-et-biodiversite-les-liaisons-dangereuses-182815) sont avérés.
	* L'eau, c'est la pollution mais aussi sa montée comme au [Vanuatu](https://www.rfi.fr/fr/asie-pacifique/20220528-l-archipel-du-vanuatu-d%C3%A9clare-l-%C3%A9tat-d-urgence-climatique).
	* L'eau devient rare, même là où elle ne devrait pas. [L'Amérique du sud](https://www.courrierinternational.com/article/penurie-riche-en-eau-douce-lamerique-latine-pourtant-de-plus-en-plus-soif) en est un exemple.
	
* **La Condition animale** :
	* Le *véganisme*, c'est vieux...comme les [philosophes antiques](https://www.courrierinternational.com/article/histoire-les-philosophes-vegans-de-l-antiquite). Mais le marché de la [fausse viande](https://www.courrierinternational.com/article/alimentation-le-marche-de-la-fausse-viande-n-est-pas-dans-son-assiette) n'est pas à la hauteur des espérances.
	
### Culture

* **J'ai lu** : 
	* La BD *Rouge Himba* de Solenn Bardet et Simon Hureau, ou la retranscription en image du retour de notre Himba française parmi les "siens". De quoi en apprendre plus sur ce peuple issu des Héréros qui furent les victimes du premier génocide connu en Namibie. Passionnant mais riche. ![cc](/images/coeur.png)
	* J'ai relu les premiers tomes des [Chroniques de la Lune noire](https://restez-curieux.ovh/2022/05/10/les-chroniques-de-la-lune-noire/) et j'ai globalement le même avis qu'à l'époque de sa sortie : Brillant graphiquement mais l'histoire a de grosses lacunes et parfois on ne comprend rien des transitions. En plus certains personnages se ressemblent trop. Bref, un classique qui vieillit et qu'on a trop étendu en tomes.
	* J'ai lu le premier tome de *Tanguy Et Laverdure* de Charlier et Uderzo car si j'en avais lu après celui là, je n'en connaissais pas les débuts. Déjà très mature dès le débat avec du détail, de l'aventure, des rebondissements et même des morts. La cible était bien adolescente.
	
* **J'ai vu** : 
	* Le film *Les Promesses* avec Isabelle Hupert et Reda Kateb est une bonne illustration de la politique en local et dans les hautes sphères. Traîtrise, passion, conservation du pouvoir, égos, ...le film est assez réussi et Kateb est très bon.
	
	* Et au musée Jacquemart-André on peut voir cette oeuvre de Canaletto :
	
	![peinture](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/medias03e6.jpg)
		
* **J'ai écouté** : 
	* Des albums que je prends le temps de chroniquer. Mais surtout les concerts du *Hellfest* grace à Arte. A commencer par Megadeth (pas top le son au début) et MSG avec un Michael Schenker en pleine forme pour des démo de tapping et des titres de UFO évidemment ! A suivre... et en replay youtube/artetv sans modération.
	
* **J'ai joué à** : 
	* (brièvement) Le jeu de Ghibli *Ni No Kuni Cross Worlds* est magnifique visuellement avec des cinématiques splendides. Reste que la mécanique n'est pas très novatrice et même très à l'ancienne pour un JRPG où l'on intervient peu ce qui donne l'impression d'un dessin animé interactif.
	* (brièvement) à *Star Wars Knight Of The Old Republic* sur Android et qu'est-ce que c'est mal foutu pour les contrôles et l'interface. Ca a vieilli mais l'histoire et l'environnement Star Wars font le reste.
	
### Science et informatique

* **J'ai testé** : 
	* Un nouveau ancien player sur lequel je suis revenu, parce qu'il y a des gens qui bossent sur XDADev pour ne pas que des applis restent liées à une marque de smartphone. Et je suis revenu dessus à la place de Pulsar.

* **Le web** : 
	* Que se passe-t-il ? Techniquement pas grand chose à part le vaporware des NFT, Blockchain, Web 3.0.
		
* **Intelligence artificielle** : 
	* L'intelligence artificielle a souvent progressé dans le militaire. C'est encore le cas pour contrer les missiles [Hypersoniques](https://asiatimes.com/2022/06/china-claims-new-ai-can-intercept-hypersonics/).
	* Tesla est sur la sellette avec un nombre important d'accidents dûs à [l'Autopilot(EN)](https://arstechnica.com/?p=1860975).
	

Et toujours **une page Liberapay**, pour ceux qui veulent me **sponsorer** : [ICI](https://liberapay.com/Chez_Iceman/donate)

Terminons en musique : 

[et quelle musique ![video](/images/youtube.png)](https://www.youtube.com/watch?v=PF6mk2Sq4yY){:target="_blank"}

A ce sujet, ça vous dit que j'intègre un plugin pour afficher les vidéos sur la page ou je laisse comme ça? dans l'esprit, j'ai toujours privilégié la légèreté de chargement et le monde du libre. Mais de manière ciblée peut-être?





