---
layout: post
title: Musique - Sharon Van Etten - We've been going about this all wrong (2022)
category: musique
tags: musique, 
---
**J'ai découvert cette artiste bien tardivement à la radio française. C'est vrai que le registre pop-folk est un peu encombré, surtout aux USA. Mais il faut croire que cet album me parlait plus que les 5 précédents.**

Il est vrai que cette native du New Jersey a commencé sa carrière musicale à la fin des années 2000...elle n'avait pas 30 ans. La folk est un registre qui exige un peu de maturité. 13 ans de carrière plus tard, la voilà avec un nouvel opus après un déménagement à Los Angeles. C'est donc nourrie de cette période de confinement et de ce changement de vie qu'elle nous revient. Pour une fois, je me suis penché sur l'édition Deluxe, car il y a un titre radio qui a capté mon attention et qui ne se trouve que sur ce support de 14 titres. Pourtant malgré les singles, j'avais écouté la version normale mais j'étais passé un peu à coté. C'est un style qu'on croit rabâché, éculé au point de sombrer dans la banalité. Pour sortir du lot, il faut non seulement un bon titre, mais aussi la voix qu'il faut avec les bons arrangements. Et c'est là que cet opus fait souvent mouche pour moi.

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/sharonvanetten.jpg)


Le bien nommé *Darkness Fades* ouvre l'album en douceur. Une voix étherée, le son de la guitare et une basse discrète. Puis le morceau monte progressivement en intensité avec percussions et synthétiseur. Il y aura parfois un coté [Enya](https://www.cheziceman.fr/2009/enya-shepherdmoons/) dans cet album  avec les très beaux aigus de Sharon Van Etten et quelques accents Cranberries parfois pour les phases plus graves. Le tempo de l'album est globalement lent avec une production assurée en partie par la chanteuse avec Daniel Knowles. Les synthétiseurs sont souvent présents pour une conotation très électro-pop avec ce fond de folk. C'est le cas par exemple dans le très bon *I'll Try* où l'on a droit aux graves profonds comme aux aigus les plus planants. Mais on peut aussi revenir aux fondamentaux avec *Anything*, un des bons singles de l'album : Guitare, Basse, Voix...et un tambourin quand même. Elle a maintenant une excellente de son home studio pour une production très efficace, sans excès d'effets.

Parfois les morceaux s'étirent comme le très beau *Born* qui  est un peu le pendant de l'introduction de l'album avec un crescendo du plus bel effet jusqu'à l'arrivée des cordes, de l'orgue et des percussions. Je serai quand même curieux de voir ce que cela donne en live pour ce morceau magnifique. La qualification "Indie Rock" de l'album me paraît exagéré, malgré les accents rock de *Headspace*. Là aussi c'est avec le renfort des synthétiseurs avec cette voix très aigu que ça fonctionne le mieux. Même si c'est bien produit, je suis moins fan du plus banal *Come Back* ou même de *Darkish*, exercice totalement solo de la chanteuse. Je lui préfère le plus rythmé *Mistakes* qui a un coté un peu rétro de pop-rock US très efficace. Là encore, un bon single. On plonge ensuite dans la douceur de *Far Away*, avec cet accent mélancolique présent dans tout l'album. 

Parmi les titres bonus de la version deluxe, il y a déjà l'excellent *Never gonna change* où la voix semble parfois au bord de la rupture. Un beau moment d'émotion qu'on peut trouver trop répétitif. On a aussi le plus electro-pop et un peu 80s *Porta* qui reste efficace dans son refrain. Je trouve le timing mal choisi pour le très ethéré *Used to it*. Comment terminer autrement que par *When I die* qui reste un morceau très représentatif de l'album. Un album plutôt pop-folk qui n'est pas forcément original mais qu'on prend plaisir à réécouter en boucle. Et comme en plus il a le bon goût d'être disponible sur Bandcamp...

La Video :  [Mistakes![video](/images/youtube.png)](https://www.youtube.com/watch?v=n9MgJ_VTpb0){:target="_blank"}
