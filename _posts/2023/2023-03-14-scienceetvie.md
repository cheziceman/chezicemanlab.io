---
layout: post
title: Presse du Passé - Science et Vie (1913-2003)
category: geek 
tags: presse, science, 1910s, vulgarisation
---

**Voilà un magazine qui a beaucoup fait pour la vulgarisation scientifique pendant près de 90 ans. Car depuis que son éditeur l'a revendu, la publication n'est plus vraiment celle que l'on a connu.**

Je ne sais pas vraiment à quel age mes parents m'ont mis ce magazine entre les mains. J'étais adolescent, c'est sûr et au collège, sans doute. Je sais que c'est plus vraisemblablement dans la fin des années 80, début des années 90 que j'en fus un lecteur plus assidu. A l'époque, il n'y avait pas de déclinaison junior, tout juste la déclinaison "Micro" pour l'informatique qui prenait plus de place depuis les années 80. En relisant des vieux numéros, j'ai d'ailleurs vu que la photo prenait aussi une place non négligeable dedans, avant que de véritables magazines sur la photo ne prennent le relais, comme le célèbre "Chasseur d'Images". S&V pour les intimes est une vénérable institution qui donnait accès à la connaissance scientifique pour le grand public, sans aller trop dans les détails techniques comme La Recherche (1970-2020) ou Pour la Science (depuis 1977). Il avait un seul concurrent pendant longtemps, Science et Avenir (maison fondée en 1947).

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/scienceetvie1.jpg)

*Une couverture d'une formule qui me fut familière*

Ce que j'appréciais dans ce magazine, c'était d'aborder tous les sujets, justement et dans un petit format plutôt pratique, sans un excès d'image à l'époque (ça changera). Il y a eu de nombreux numéros de prospective sur le futur, évidemment. Il est amusant de les relire maintenant car les prédictions étaient soient trop optimistes, soient très pessimistes avec les merveilles du numérique, de l'informatique d’aujourd’hui. Une chose est sûre, l'aspect spatial ou aéronautique n'a pas été à la hauteur des rêves et espérances de nos jeunes années. "La voiture volante" démocratisée attendra encore quelques décennies, au moins. Mais lorsque je regarde les titres des années 90, je vois bien les préoccupations de l'époque. Il y avait pas mal de choses sur l'aéronautique avec l'habituel numéro du salon du Bourget, des préoccupations sur l'énergie, l'industrie, la nature. Et puis bien sûr l'espace, ses mystères, les conquêtes à venir. Je n'ai pas souvenir d'un numéro ou d'un article en particulier. C'est un tout, quelque chose qui nourrissait la curiosité pour de nombreux sujets. 

C'était le bon coté de la vulgarisation avec cette notion de donner envie d'en savoir plus à une époque où les sources d'information étaient plus en bibliothèque qu'à la télévision et certainement pas dans un internet qui n'existait pas encore, du moins pour le grand public. Le mauvais coté, c'est qu'à force de se disperser dans diverses formules, de vulgariser, le titre se perdra un peu trop et mettra l'infographie et l'image plus en avant que le fond. A l'heure où Internet commença à être une source d'information, le combat était déjà perdu dans ce domaine. Seuls les titres les plus pointus dans la science sont restés dans leur niche. Aujourd'hui, après les rachats, les journalistes sont partis en grande partie pour fonder epsilon. Je n'en ai plus ouvert un numéro depuis 15 ans au moins. J'ai feuilleté un epsilon du début sans m'y retrouver non plus. Là aussi, j'ai l'impression que l'on fait fausse route, à moins que ce soit surtout moi qui ait vieilli et attende autre chose.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/scienceetvie2.jpg)

*les années 30 et la conquête de la stratosphère*

Il y a de très vieux numéros lisibles sur le site [Retronews](https://www.retronews.fr/titre-de-presse/science-et-la-vie), dont certains s'appellent encore "La Science et la vie", le nom originel. On y trouve par exemple un numéro de 1935 qui parle d'un scaphandre stratosphérique qui fait penser aux premières combinaisons de cosmonautes/astronautes. On voit toutefois le chemin parcouru mais aussi le prosélytisme de certains secteurs qui a pu avoir cours tout au long de la carrière de ce magazine. La science a fait des progrès mais elle a su surtout se remettre en cause. Elle continuera à le faire. Je ne sais pas vraiment ce qui aujourd'hui remplacerait le rôle d'un tel magazine auprès du grand public moins friand de presse papier. Wikipedia a de bonnes choses mais peut aussi sombrer dans la médiocrité de par son fonctionnement. La vulgarisation peut aussi être sous influence, comme toujours et l'on devient méfiant. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/scienceetvie3.jpg)

*On a fait quelques progrès depuis ce numéro de 1990*

La science d'aujourd'hui me semble parfois reculer dans les esprits, alors que paradoxalement nous n'avons jamais eu autant de sources d'informations. Science & Vie et quelques autres ont du forger la curiosité de bien des lecteurs depuis l'origine, créer des vocations aussi. Aujourd'hui, le sujet principal me paraît autour du climat et des solutions que l'humain trouvera à ses propres fautes. Et tout n'a pas été fait non plus pour se mettre à la hauteur des enjeux. 
