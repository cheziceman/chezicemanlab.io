---
layout: post
title: Pause Photo et Poésie - En dehors de sa peau
category: poesie 
tags: poème, vie, mort,
---

Je me griffe et me déchire.

Des lambeaux entre mes doigts,

des larmes qui s'étirent,

des grains que je broie.

Me voilà à me regarder mort,

vieille guenille sur le sol

comme un manteau que l'on sort

avec ses souvenirs d'école.

Je me vois ridé, vidé, desséché.

Me voilà en quête d'une autre peau,

une autre vie avec souvenirs et péchés

pour retrouver un nouveau tempo.


![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/poemepeau.jpg)


