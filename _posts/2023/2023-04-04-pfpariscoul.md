---
layout: post
title: Portfolio - Paris en couleurs
category: photo
tags: photographie, paris, noir et blanc, 
---

**Un portfolio consacré à Paris toujours mais en couleurs cette fois.**

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfpariscoul01.jpg)

*C'est le quartier des tours de Paris et on ne retient souvent que le quartier asiatique. Il y a pourtant un aspect très graphique, géométrique quand on s'y promène. Et la dalle commerciale d'Olympiades est bien déserte aujourd'hui*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfpariscoul02.jpg)

*Toujours en flânant, on peut tomber sur de petites merveilles graphiques comme cette grande fresque de grapheurs. Je ne sais plus où elle se situait mais elle aura certainement mué en autre chose.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfpariscoul03.jpg)

*Paris c'est aussi la fête comme l'été à la Foire du Trône. Du bruit, de la couleur, des odeurs qu'une simple photo ne peut retranscrire.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfpariscoul04.jpg "image")

*On peut alors monter dans des manèges au dessus des toits pour des chutes vertigineuses ou pas. Ou simplement se prendre pour un oiseau.*


![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfpariscoul05.jpg)

*Toujours dans Olympiades, on peut prendre le temps de regarder cet empilement de formes, de vies aussi qui s'affichent ou pas sur les balcons et aux fenêtres.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfpariscoul06.jpg)
*Puis les parisiens s'enfoncent à nouveau dans leur métro, sous les halles ou ailleurs.*
