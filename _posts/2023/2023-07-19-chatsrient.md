---
layout: post
title: Littérature - Les chats ne rient pas de Kosuke Mukai (2020)
category: litterature
tags: Littérature, roman, japon, chat, 2020s
---

**La valeur des livres est souvent bien différente du nombre de pages ou de l'originalité d'un sujet. Il y a parfois des histoires simples qui nous touchent, même à travers une traduction.**

Ici, c'est du japonais, toujours par le spécialiste du genre, les Éditions Picquier. Pourtant il n'y a pas trop de dépaysement ou de sujet trop japonais. C'est aussi un premier roman, d'un scénariste de film...Son histoire tient dans les 160 pages de cette ouvrage : "Il y a d’abord un chat de gouttière au pelage d’un roux doré, qui aime dormir pelotonné en U devant le poêle. Il est vieux et ses jours sont comptés. Pour réconforter ce chat en fin de vie, se forme un étrange ménage à trois composé d’une jeune et prometteuse réalisatrice de cinéma, de son mari journaliste et de son ex-compagnon, scénariste désenchanté et trop porté sur la boisson. Une intimité imprévue se crée entre eux à la faveur de leur amour commun pour ce chat. ..."

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/chatsrient.jpg)

Je trouve que le résumé de l'éditeur en dit presque trop...j'ai coupé la fin. Bon, il est évident que l'auteur a mis beaucoup de sa propre histoire, ne serait-ce que dans Hayakawa le personnage principal, le narrateur. Mais le personnage principal est en fait Son, le chat...Son ne veut pas dire fils en japonais mais il y a quelque chose autour de cela avec Renko, la compagne de Hayakawa quand ils recueillent ce chat. Et aussi vis à vis de Miyawa, le mari de Renko lorsqu'elle appelle notre scénariste pour lui dire qu'il n'en a plus pour longtemps. Le sujet n'est pourtant pas la maladie ou le deuil, d'un humain ou d'un animal. Il est déjà plus universel dans le sens où cela parle de la place que peut prendre un animal dans notre vie, ce témoin qui sait tout, ne dit rien mais cristallise pourtant bien des choses. 

Cette lente agonie du chat est l'occasion de revenir sur des épisodes de la vie du couple du narrateur avec la belle Renko, devenue réalisatrice à succès. Un retour aussi sur sa propre vie, ses réussites ou ses échecs, ses lâchetés. Son savait tout ça et donnait souvent son avis à sa manière, mais ça, le narrateur ne s'en aperçoit que tardivement. Quant au mari, lui aussi il réalise que sa place n'est pas celle qu'il imaginait. Il réalise à travers la relation de Renko et de Son que sa vie n'était pas aussi parfaite qu'il le pensait et qu'il doit changer. Son est donc avare de mot, souvent de gestes mais sa présence fait tout. C'est un jeu subtil dans ce trio qui passe lui aussi un acte de sa vie.

La traduction rend totalement cette subtilité, ce fragile équilibre dans la vie de chacun ou cette absence d'équilibre. Étant amateur de chats, je m'imaginais sans doute autre chose mais finalement j'y ai trouvé mon compte. Contrairement aux chiens, il y a une relation très spéciale avec ces animaux qui s'expriment finalement peu par la voix, nous rendent parfois esclaves mais dégagent aussi une forme de sagesse ou restent simplement des témoins de nos vies. Voilà aussi ce que ce roman rend parfaitement. Son n'est pas comme le chat de la jolie couverture mais on se l'imagine aisément, jusqu'à se remémorer ses propres moments avec notre félin adoré.



