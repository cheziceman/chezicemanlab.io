---
layout: post
title: Littérature - L'Oiseau de mauvais augure de Camilla Läckberg (2010)
category: litterature 
tags: littérature, policier, thriller, roman, suède, 2010s
---

**La littérature policière nordique est toujours à la mode. Et parmi les autrices en vogue, il y a la prolifique Camilla Läckberg. Alors j'ai pris au hasard dans ce que j'ai trouvé, même si c'est le 4ème épisode pour ces personnages de Patrick Hedström et Erica Falck.**

Je n'avais donc pas d'idée de ce qui m'attendait, pas droit à la présentation des personnages du premier tome, etc. Pourquoi celui là? Le titre, la couverture énigmatique, et une histoire qui paraissait justement totalement banale : Une femme retrouvée morte d'un supposé accident de la route en état d'ébriété. Mais Patrick Hedström a une intuition ... Ce qui sort de la banalité, c'est qu'il se tourne une émission de télé-réalité, genre les Anges en Suède, ou plutôt à Tanumshede, la ville où se situe l'histoire.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/lackbergoiseau.jpg)

Je m'attendais à un page-turner habituel du genre et j'ai été surpris. Car l'autrice nous emmène dans la vie de ce village, de son commissariat et surtout de ses personnages. Il y a notre couple de héros qui va se marier mais il y a aussi le commissaire incompétent, la sœur d'Erica traumatisée par un précédent épisode, la nouvelle arrivée dans le commissariat, Hannah, et son compagnon Lars, etc ...Il faut attendre un gros tiers pour que ça se décante, et ça démarre vraiment à la moitié du livre jusqu'à l'emballement final. J'avais entrevue l'hypothèse sur le/la/les coupables vers cette moitié sans avoir la moindre compréhension de cette machination. 

Camilla Läckberg nous balade pas mal dans cette succession de scènes de chaque personnage. Chacun a ses failles, ses doutes, sa part d'ombre. Je me dis même qu'elle prépare déjà ses personnages pour une autre histoire. Et puis le narrateur n'est pas du tout omniscient, au point qu'elle nous cache même des éléments cruciaux pour nous faire saliver et nous pousser à lire la suite. C'est terriblement frustrant, mais ça marche avec moi, pauvre lecteur. Après tout, j'use aussi parfois de cela dans mes modestes billets... 

Finalement, après ce démarrage laborieux (mais volontaire), j'ai pris du plaisir à la lecture, stimulé par ce premier accident qui se révèle un meurtre mais aussi par ce fil rouge de la télé-réalité. Au passage, le genre en prend pour son grade, mais l'humain téléspectateur aussi. Notre coté voyeuriste n'est-il pas lui même présent dans la lecture d'un bon polar? Je ne pense pas, en tout cas, que ce soit le meilleur Läckberg mais pourquoi pas rempiler sur un autre pour voir la différence. Le dépaysement par rapport à cette région de Suède proche de la frontière norvégienne fonctionne aussi très bien pour un lecteur éloigné de cet univers. Alors pensez, lire cela quand c'était la canicule...
