---
layout: post
title: Portfolio - Concerts vol.1
category: photo
tags: photographie, paris, noir et blanc, 
---

**Quelques unes des photos de concerts en tant que spectateur non accrédité.**

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfconcert01.jpg)

*Cette photo d'un lointain concert de Kiss avait été reprise dans un article de Wired online, après une des déclarations provocatrices de Gene Simmons.*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfconcert02.jpg)

*Judas Priest, c'était un duo de guitare diabolique avec un Rod Halford dont la voix déclinait déjà. KK Downing venait de quitter le groupe*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfconcert03.jpg)

*Un guitar Hero avec de l'humour, voilà ce qui qualifie Rick Nielsen de Cheap Trick et ses guitares improbables.*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfconcert04.jpg)

*Peut-être un des derniers concerts du groupe Process avec sa chanteuse Liz.*


![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfconcert05.jpg)

*Et dans un registre plus folk, la grande Joan Baez nous enchantait toujours avec voix et guitare à plus de 70 ans.*
