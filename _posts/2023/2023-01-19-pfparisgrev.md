---
layout: post
title: Portfolio - Paris en grève
category: photo
tags: photographie, paris, noir et blanc, 
---

**Finalement, j'ouvre cette rubrique en ce jour symbolique. Un Paris qui paraît lointain ou pas quand tout se fige, se bloque, se congestionne.**

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparisgreve01.jpg)

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparisgreve02.jpg)

*Dans ces moments, tout le monde cherche des informations, se retrouve bloqué...mais c'est parfois simplement le cas parce que la SNCF ne sait pas prévenir de la voie d'arrivée d'un train plus de 5 minutes avant ! (Photo prise au 50mm sur un Canon Elan7)*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparisgreve03.jpg)

*Alors on essaie de trouver d'autres moyens de transport. D'un coté on arrive en voiture, de l'autre on essaie de partir en bus. (Photo prise au 50mm sur un Canon Elan7)*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparisgreve04.jpg)

*La Place de Clichy voit tous les moyens se rejoindre. Et la nuit tombe sur la chaussée humide dans un concert de klaxons. (Photo prise au 50mm sur un Canon Elan7)*


![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparisgreve05.jpg)

*La grève dure jusqu'au lendemain matin et tout le monde attend son métro. (Photo prise au 50mm sur un Canon Elan7)*
