---
layout: post
title: Série du passé - Mariés, deux enfants (1987-1997)
category: serie 
tags: 1980s, série, comédie, SOAP
---

**Une série qui marqua ceux qui la regardèrent dans les années 90, au point que le personnage principal est devenu culte : Al Bundy. Un héros qu'on aime détester.**

J'avoue que j'ai eu du mal lorsque j'ai vu le premier épisode de cette série. D'habitude, le héros est quelqu'un à qui on s'identifie, ou que l'on trouve rassurant, un modèle parfois. Et là, c'était tout l'inverse. Al Bundy est le chef de famille, vendeur de chaussures, obsédé sexuel, mysogine, ... j'en passe. Il est marié à Peggy, bimbo rousse passionnée de télé-achat et autres émissions du même genre et ne fait rien d'autre. Elle a deux enfants, Kelly, la blonde écervelée et Bud, le plus intelligent de la famille. Je vous passe les cas sociaux du coté de la famille de Peggy. Forcément, il y a de quoi faire pour un bon soap opera comme les américains en ont le secret.

A travers la famille Bundy, on a une caricature de la famille américaine, un opposé même de la famille modèle que l'on voit sur les autres séries. C'est souvent trash, avec beaucoup de dialogues autour du sexe, que cela soit dans la quête de Bud de "devenir un homme" ou dans les lubbies de Al, les petits copains de Kelly, vue comme la fille facile. La série est ouvertement mysogine dans sa caricature et pourtant on s’aperçoit que Peggy, derrière son insouciance, est loin d'être stupide, menant aussi son Al par le bout du nez. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/marriedchildren.jpg)

Cela fonctionne aussi grâce à un casting réussi et la présence d'un public lors du tournage. Ed O'Neil reste marqué par ce rôle, alors que l'on a pu voir Katey Sagal dans des films et séries diverses. Christina Applegate sortira aussi de ce rôle réducteur alors que la filmographie de David Faustino est moins fournie. Du fait de la présence du public, il y a un coté très théatral dans cette série et on ne fait pas dans la dentelle. Petit à petit elle fit évoluer ses personnages vers plus de caricature encore et parfois ce fut dommageable (pour Kelly par exemple)


Finalement, le téléspectateur se sentait (à priori) plus intelligent que les Bundy et riait de leurs mésaventures. On pourrait dire aujourd'hui que l'on fait pire sans même se soucier du scénario avec certaines télé-réalités, habilement "scriptées" et montées. Au moins là, c'était écrit et joué. La série dura 10 ans sans atteindre de sommets d'audience. Elle s'exporta plutôt bien puisque c'est aussi une manière de se moquer de nos amis américains. 

[Le Générique ![video](/images/youtube.png)](https://www.youtube.com/watch?v=p8t5cOjlEPU){:target="_blank"}
