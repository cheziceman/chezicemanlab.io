---
layout: post
title: Souvenir de gamer - Saint Dragon (1989)
category: geek, 
tags: geek, retrogaming, jeu vidéo, 1980s, 
---

**L'homme a toujours rêvé de voler. Le Gamer aussi mais si en plus il est fans de Fantasy, il veut voler sur ... Un Dragon !**

Aussi, les jeux qui ont mis un joueur à chevaucher un dragon se sont succéder. Il y a le très bon Panzer Dragoon de Sega mais je m'intéresse à un shoot plus classique mais pourtant très intéressant : Saint Dragon. Cette fois, on n'est pas sur un dragon mais on est dans un dragon cybernétique, une sorte de vaisseau spatial improbable en forme de dragon. C'est Jaleco qui a commis ce jeu en 1989 (Panzer Dragoon c'est 95) et cela arrive deux ans après un hit comme le R-Type d'Irem ou un an après Silkworm de Tecmo. Une époque phare pour ce genre du Shoot'em up latéral. Une époque où le genre trustait les machines de salle d'arcade avec les beat'em all. Mais alors pourquoi ce jeu plutôt qu'un autre ? 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/saintdragon.jpg)

Tout simplement parce que notre dragon a une tête à gérer mais aussi une queue et c'est toute la particularité du gameplay. En utilisant le placement de la queue du dragon dans son déplacement, on trouve des techniques surtout quand les tirs se multiplient. La queue est une arme mais aussi un élément à caser dans la profusion d'ennemis et de tirs. Le graphisme est finalement assez proche des R-Type de l'époque mais avec des sprites plus gros. Il y a aussi beaucoup d'éléments au sol comme ... Silkworm par exemple mais aussi au plafond. Du classique dans les shoots horizontaux finalement. Ce qui rendait ce jeu particulier c'était la fluidité de l'ensemble, le look de ce vaisseau / robot et le design de l'ensemble qui fonctionnait vraiment très bien. 

Evidemment, je n'ai jamais été très bon sur ce jeu, d'autant que les sprites étant gros, il y a des passages à ne pas rater pour glisser notre dragon où il faut. Le principe reste de connaître peu à peu les différentes vagues d'ennemis et de scorer en augmentant les options de tirs grace aux capsules que l'on récupère. Là dessus, aussi, c'est une mécanique très classique. Le jeu est évidemment moins spectaculaire que le dragon 3D de Sega mais je lui trouve aujourd'hui encore un certain charme. Mais avoir un dragon est d'un commun avec ces sorciers qui en élèvent secrètement...

[Le traditionnel Longplay ![video](/images/youtube.png)](https://www.youtube.com/watch?v=0doYiySTflc){:target="_blank"}

***Sorti par NMK et Jaleco en 1989 sur Arcade, Amiga, Amstrad CPC, Atari ST, Commodore 64, PC-Engine, ZX Spectrum, MSX***