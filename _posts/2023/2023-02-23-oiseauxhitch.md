---
layout: post
title: Cinema - Les oiseaux d’Alfred Hitchcock (1963)
category: cinema
tags: cinémathèque idéale, classique, hitchcock, catastrophe, angoisse, 1960s
---

**Dans les films de genre, Alfred Hitchcock tient une large place, notamment pour le suspens et l'angoisse. Mais quand les films de monstre envahissaient les écrans avec des araignées et fourmis géantes, avec des effets spéciaux qui ont vieilli ensuite, Hitch nous faisait peur avec d'autres animaux bien plus réels...**

Si ce film est tout à fait regardable aujourd'hui, c'est justement qu'il ne fait pas appel à des marionnettes, de l'animatronics, du stop-motion et toutes ces techniques souvent remplacées par de l'image de synthèse. Non, il n'y a que de vrais oiseaux, dressés ou juste filmés avec la dextérité d'un maître du cinéma. Et surtout une histoire comme seul notre réalisateur anglais sait tirer la substantifique moelle. C'est une nouvelle de Daphné du Maurier, très remaniée tout de même. 

"Melanie Daniels (Tippi Hedren), une belle et riche jeune femme de San Francisco, rencontre l'avocat Mitch Brenner (Rod Taylor) chez un marchand d'oiseaux. Brenner fait semblant de la prendre pour une employée et lui demande un couple d'oiseaux, des « inséparables » qu'il veut offrir à sa jeune sœur, Cathy. Le lendemain matin, Melanie se rend à l'appartement de Mitch avec un couple d'inséparables. Elle apprend qu'il est parti pour le week-end à Bodega Bay. Elle décide d'aller là-bas en voiture, avec les oiseaux. Melanie loue un bateau et traverse la baie pour déposer discrètement la cage à la ferme des Brenner. Mitch l'aperçoit de loin. Il prend sa voiture pour la retrouver au port. Alors que Melanie aborde le quai, un goéland fonce sur elle." 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/oiseaux.jpg)

François Truffaut a largement décortiqué ce film avec son maître dans un célèbre livre d'entretien dont j'ai parlé ici. Mais faut-il se contenter de cela quand ce film montre qu'avec peu on peut réaliser beaucoup. L'émotion, que l'on connaisse ou pas l'histoire avant de le voir, monte crescendo. La petite bourgade portuaire paraît charmante mais bascule peu à peu dans un cauchemar que l'on pense apporté par cette "étrangère". Hitch se délecte de torturer cette poupée blonde (dans le film et dans la réalité semble-t-il selon les dires de Melanie Griffith la fille de Tippi Hedren), comme de nous torturer en créant une angoisse à travers le comportement et la présence d'oiseaux. Un comportement d'abord anormal, comme une révolte face aux humains qui les exploitent. Mais quelle est la cause de cela ? La présence des oiseaux est grandissante sans être d'abord menaçante et pourtant nous regardons ces innocents volatiles autrement. Il y a cette scène mythique ou l’héroïne fait face à ces centaines d'oiseaux immobiles. 

Pas d'effets spéciaux ou presque donc, mais de la patience et du dressage, ce qui n'est pas courant pour certaines espèces d'oiseaux. Il faut de l'expertise pour arriver à un tel résultat, par le cadrage, le montage et la musique aussi. Je n'ai pas vu ce film tant de fois que cela, mais il m'a marqué durablement, sans me faire faire des cauchemars. J'ai continué aussi à m'occuper d'oiseaux dans la volière familiales (recueillis et sauvés des griffes d'autres animaux...) ou à les observer avec bienveillance. Mais ce film reste inspirant pour bien d'autres œuvres où la nature se rebelle face à l'homme. Il y a eu récemment la série Zoo, par exemple. Le film relança complètement le genre aussi mais personne n'a atteint cette perfection. Évidemment, il faut aussi aimer se faire peur mais si j'aime le cinéma, c'est pour les émotions qu'il procure, pas seulement l'extase d'une prouesse technique. 

[La Bande Annonce ![video](/images/youtube.png)](https://www.youtube.com/watch?v=kL6FhxkILVg){:target="_blank"}
