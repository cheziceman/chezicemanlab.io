---
layout: post
title: Portfolio - Paris et son architecture
category: photo
tags: photographie, paris, noir et blanc, 
---

**Un portfolio consacré à Paris et son architecture. Mais pas de manière touristique, évidemment.**

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparisarchi1.jpg)

*Montmartre et ses escaliers, ses cabarets mythiques où débutèrent poètes et chanteurs. J'aimais m'y balader avec mon premier numérique.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparisarchi2.jpg)

*A l'opposé, il y a La Défense, où un simple appareil jetable argentique peut donner ce résultat.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparisarchi3.jpg)

*Autour de l'Opéra de Paris, des statues viennent rappeler la beauté de l'art de la danse.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparisarchi4.jpg)

*Qu'il est loin Le Bon Marché qui inspira Zola. Les barrières restent, les escaliers ont changé.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparisarchi5.jpg)

*Toujours à La Défense, on semble vouloir défier le ciel en bâtissant toujours plus haut. Mais Icare s'y brûla.*
