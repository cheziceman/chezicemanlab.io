---
layout: post
title: Musique - The Struts - Strange Days (2020)
category: musique
tags: musique, rock, hard-rock, 2020s
---

**Pour un peu, j'aurai raté cet album, le troisième, d'un groupe qui avait eu un gros succès il y a déjà 8 ans. Entre Rock, pop et Hard-rock, j'avais été déçu par le second opus du combo de Derby.**

Et si je m'étais arrêté au premier titre de cet album, j'aurai pu passer à coté. Car la balade qui donne son nom à l'album est assez banale au final, même si on peut aimer. Ce n'est pas vraiment ce qui représente l'album surtout. Je lui préfère le titre plus rythmé *All Dressed up*. Etonnant d'entendre aussi la reprise de **Kiss** *Do You love me*, pas vraiment novatrice, si tôt dans l'album. Luke Spiller a peut-être du talent vocalement mais ce n'est pas Paul Stanley. Le groupe n'est jamais aussi efficace qu'avec un gros riff, une mélodie pop et une bonne rythmique. C'est le cas sur *I hate how much I want you* produit, comme l'album par John Levine (Dua Lippa, Drake !...). C'est efficace par l'énergie qui se dégage.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/strutstrange.jpg)

L'énergie, il y en a encore qui déborde dans le très rageur *Wild Child*. La légende dit que l'album a été enregistré en 10 jours, ce qui fait finalement un titre par jour. De quoi, en effet, garder l'énergie dévastatrice du groupe qui éclabousse quelques soli de guitare. Le groupe revendique de nombreuses influences dans cet album, comme Phil Collen et Joe Elliot (Def Leppard)...Soit, mais alors dans la période originelle. Peu à peu l'album fait son effet avec des titres comme *Cool* qui pourtant refroidit un peu les ardeurs rock pour un son plus groovy. Hasard ou pas, après la fraîcheur, c'est la chaleur plus bluesy de *Burn it down* qui remplit cette deuxième partie. Spiller a la voix qu'il faut pour ça et les anglais s'y entendent toujours pour ce style. Il y a même quelques accents "Rolling Stone" dans ce titre.

Avec *Another Hit Of Showmanship*, on revient sur une voix plus "blanche" et une pop banale. Le titre faible de l'album pour moi. On s'endormirai...non, il y a *Can't Sleep*, plus sautillant dans un style très Rock'n Roll des années 70. De quoi crier le réveil à gros coup de riffs. Les sonorités de guitare sont elles aussi bien rétro, donc forcément tendance ! Je trouve même un petit coté garage avec l'aspect bricolé de la pochette et le son parfois un peu "sale". Mais si on a commencé par une balade, on termine aussi en douceur. J'aime bien terminer en apothéose mais là, c'est plus funky et jazzy. Pas forcément ma tasse de thé... Earl Grey, of course. Bilan, un album plutôt sympa dans sa globalité mais qui n'atteint pas la qualité du premier opus. J'ai entendu quelques autres reprises du groupe et je parie qu'un jour Luke Spiller voudra voler de ses propres ailes. 

En attendant, c'est un groupe à continuer de suivre, et à écouter en streaming sur [Bandcamp](https://thestruts.bandcamp.com/album/strange-days).

[Un des singles ![video](/images/youtube.png)](https://www.youtube.com/watch?v=UJ1jxUaVMb4){:target="_blank"}
