---
layout: post
title: Portfolio - Photo de rue à Paris
category: photo
tags: photographie, paris, noir et blanc, 
---

**Un portfolio consacré à Paris pour de la photo de rue avec le point commun que les visages ne sont pas tous visibles.**

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparistreet1.jpg)

*Cette photo fut prise Place de l'étoile. Inutile de montrer un visage quand on peut imaginer toute l'histoire autour.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparistreet2.jpg)

*C'est du côté de la station Châtelet que j'ai pris cette photo qui semble ancienne. Je ne sais plus trop pourquoi j'avais pris l'argentique ce jour là mais je trouve qu'elle fonctionne très bien avec ce contraste fort.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparistreet3.jpg)

*Quel choc quand un matin je vis cet homme au sol en ouvrant les volets. Je fus inquiet de savoir s'il respirait mais tout allait bien (c'était le printemps). Jamais il ne revint là ou dans le quartier mais là aussi, il y a des histoires qui viennent à l'esprit.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparistreet4.jpg)

*C'est à Pigalle que je suis tombé sur cet ange sorti de nulle part, sur le terre-plein central. Il semblait chercher son chemin et le paradis n'était peut-être pas si loin.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparistreet5.jpg)

*Comme la première, c'est sur les Champs-Élysées que je pris cette photo. Les piétons et touristes contournaient cette femme, détournant souvent le regard. Elle restait ainsi la tête baissée et silencieuse sous le soleil d'été.*