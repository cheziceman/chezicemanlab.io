---
layout: post
title: Littérature - Les cigognes sont immortelles d'Alain Mabanckou (2018)
category: litterature
tags: roman, littérature, afrique, congo, colonisation, décolonisation, histoire
---

**Pour ma première incursion dans l'oeuvre de l'écrivain franco-congolais Alain Mabanckou, j'ai choisi son dernier roman, une œuvre à la fois historique et en partie autobiographique. Nous voilà projeter dans le Congo Brazzaville des années 70.**

Le début du roman est à hauteur d'enfant : "Michel, un jeune Congolais de Pointe-Noire, enfant ou préadolescent de 12-13 ans, du collège des Trois-Glorieuses, se prépare à passer son certificat d'études primaires, lorsqu'un coup d'État militaire vient perturber sa vie et sa conception du monde, et de la vérité." L'histoire de la décolonisation du Congo va faire grandir ce jeune-homme et nous en apprendre aussi beaucoup sur la situation de l'époque. On y parle ainsi des luttes entre le nord et le sud du pays, la rivalité avec le Congo Belge, ou d'autres pays voisins. On y rappelle l'influence considérable de l'URSS dans les mouvements de libération des anciennes colonies mais aussi l'influence d'une certaine radio américaine ou de l'armée française. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/cigognesmabanckou.jpg)

Alain Mabanckou parle indiscutablement de sa propre enfance à travers le personnage de Michel. Il prend un ton presque naïf, souvent avec humour, croquant les personnages de la famille de Michel, les voisins, les cousins. Cette période de la décolonisation, après l'indépendance de 1960, montre déjà l'instabilité d'un pays sous ces multiples influences et partagé par le pouvoir blanc précédent. C'est l'arrivée prochaine de Denis Sassou-Nguesso que l'on nous compte, les différentes juntes, groupes, ethnies, langues. C'est un pays que l'on sait riche de ses ressources mais exploités par d'autres que ses habitants qui pourtant hésitent entre capitalisme et communisme. C'est pourtant une période faite d'espoirs mais de grandes désillusions par rapport à la politique.

Les chapitres s’enchaînent presque sans pauses avec le très volubile Michel, tantôt à la recherche de son chien, ou essayant de comprendre dans quel camp se ranger. Et après cette période d'espoir mais d'instabilité, succédera celle d'un pouvoir sans partage du colonel Sassou-Nguesso qui est d'ailleurs encore chef de l'état. On connaît peu les vicissitudes politiques du Congo et c'est donc particulièrement intéressant d'en apprendre sur ce pays. Ce faisant, Alain Mabanckou parle aussi d'autres pays voisins, et ainsi nous aide à comprendre un peu mieux l'Afrique de l'ouest d'aujourd'hui. En utilisant un adolescent, il aide aussi le plus grand nombre à appréhender des problématiques d'ordinaire complexes. A lire pour ceux qui aiment comprendre et découvrir.
