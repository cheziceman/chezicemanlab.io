---
layout: post
title: Cinéma - C'est Magnifique de Clovis Cornillac (2021)
category: cinema 
tags: Cinéma, Poésie, Fantastique, Romance, 2020s
---

**Sans faire de bruit, Clovis Cornillac se fait une carrière de réalisateur plus qu'intéressante. Et quand il s'attaque au sujet de "La Famille" à travers une comédie romantique, il le fait à sa manière.**

J'avais déjà apprécié sa première réalisation, "Un peu, Beaucoup, Aveuglément", dans un registre de comédie romantique. Ici, c'est la même équipe au scénario, à savoir Lilou Fogli (sa femme à la ville) et Tristan Schulman. C'est aussi sur une forme de comédie romantique que l'on part, et pourtant, il s'agit en réalité d'une comédie dramatique, à moins que cela soit un conte ? Parfois le mélange des genres est compliqué mais ici, cela se passe au mieux : 

"*Pierre Feuillebois a la quarantaine. Ce passionné d'abeilles et hibiscus a toujours vécu « coupé du monde », protégé par ses parents. Après le décès de ces derniers, il se rend compte que ce n'étaient pas ses parents biologiques et qu'il n'existe pas au regard de la loi. En quête de réponse sur ses origines, il rencontre la belle Anna Lorenzi.*"

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/cmagnifique.jpg)

Présenté comme ça, ou pourrait croire à une banale histoire d'amour. Mais Anna Lorenzi (Alice Pol) nous apparaît comme une jeune femme colérique, en proie avec les services sociaux. Pour quelle raison ? Et lorsque les parents de Pierre (Clovis Cornillac) meurent, le voilà qui hérite d'argent, d'une maison de ville à Lyon, mais sans avoir les codes de ce monde moderne. Il part avec dans sa valise un disque de Dario Moreno (C'est Magnifique...) et du miel d'Hibiscus de sa production. Il a la chance de faire la rencontre de Daria (Gilles Privat), une voisine travestie au grand coeur qui va le prendre sous son aile. Et tout ce petit monde de tourner autour d'un bistrot, celui de Roger (Roger...Cornillac). Car d'un coté son but est de savoir qui il est, qui est sa mère. Mais il faut aider Anna Lorenzi à reprendre sa fille placée en foyer. Le thème central est donc bien la famille.

Mais quand je parle de conte, c'est dans la manière de filmer et cela commence dès la première minute avec la mort des parents de Pierre, filmée de manière un peu onirique. Cela devrait être un cauchemar mais Pierre est un naïf, plein de générosité et de bonté, ne pensant que nature, fleurs et arbres ou insectes, ses amis depuis son enfance. Autant dire que l'on ne peut que l'aimer et non se moquer. On rit forcément du ridicule de certaines situations mais sans rire du personnage si attachant campé par Cornillac. Alice Pol est encore une fois parfaite dans son rôle de jeune femme un peu perdue mais elle aussi avec le coeur sur la main. Comme dans un conte, ils étaient faits pour se rencontrer. Mais il fallait y rajouter un jeu sur les couleurs. Avec le choc des révélations sur ses origines, Pierre change de couleur...Comme s'il fanait ? 

Les couleurs sont d'ailleurs magnifiées par un traitement que n'aurait pas renié Jean-Pierre Jeunet. Tout ça en filmant magnifiquement le Lyon de son enfance, Cornillac n'oublie pas d'aller dans les Alpes, à Sixt-Fer-à-cheval pour ceux qui connaissent. Merci à Thierry Pouget pour ce travail d'orfèvre. Mais Cornillac ne s'arrête pas à la partie conte, allant du coté du Fantastique, façon "Passe Muraille", celui de Marcel Aymé. Il n'essaye pas de copier le film de Jean Boyer avec Bourvil mais y apporte autant de poésie. Ce n'est d'ailleurs pas le seul moment où elle est présente. On la retrouve aussi dans le regard de Mme Fontaine (Myriam Boyer, la mère de Clovis Cornillac à la ville), personnage central. 

Tout cela nous donne un très joli film, à la fois drôle, touchant, tragique et poétique. Comme souvent dans ce genre, la critique se fit assassine, le public étant un peu plus au rendez-vous. Il est maintenant temps de lui donner la seconde vie qu'il mérite. La phrase qui conclut le film donne toute l'essence de l'histoire et donc envie de le voir et le revoir pour en saisir mieux quelques messages. Et tout ça avec la chanson de Jehro en tête, encore un artiste qui fait discrètement son chemin. Si parfois la filmo de l'acteur Clovis Cornillac paraît alimentaire, il n'en est rien de celle du réalisateur qui avec sa petite équipe familiale trace son chemin. Et le prochain n'aura rien à voir...

[Bande Annonce ![video](/images/youtube.png)](https://www.youtube.com/watch?v=z-MlOwkqhDY){:target="_blank"}
