---
layout: post
title: Portfolio - Le Vietnam en couleurs
category: photo
tags: photographie, vietnam, couleur
---

**Un pays magique, plein de surprises et encore un peu préservé du tourisme de masse à l'époque de ces photos**

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfvietnam01.jpg)

*Ces deux enfants vivent sur une maison flottante dans la baie d'Halong. Pas de jardin mais il y a pourtant de quoi s'amuser.*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfvietnam02.jpg)

*Les barques rondes sont une spécificité de cette région du centre Vietnam. Mais chut, je ne dirai pas son nom...*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfvietnam03.jpg)

*Les maisons colorées sont une manière de se différencier et rien à voir avec Instagram et le tourisme.*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfvietnam04.jpg)

*Le Delta du mekong est un lieu de culture et d'échange, comme ici pour le fruit du dragon.*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfvietnam05.jpg)

*La moto n'est pas le moyen le plus simple dans le delta alors la barque à la godille c'est mieux entre potes.*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfvietnam06.jpg)

*Les marchands vivent sur leurs bateaux avec leur famille.*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfvietnam07.jpg "Image")

*Dans les grandes villes du delta, on a construit sur pilotis avec ce que l'on avait. Cela n'empêche pas de cultiver des fleurs.*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfvietnam08.jpg)

*Et dans le centre vietnam, les couleurs sont comme une mosaïque infinie.*