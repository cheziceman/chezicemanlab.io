---
layout: post
title: Pause Vidéo et Poésie - 17 secondes
category: poesie
tags: poésie, son, vidéo, poême
---

**Juste une vidéo à regarder ou écouter, un texte qui dure le même temps à lire. 17 secondes cette fois.**

Je m'arrête une fois encore,

juste pour écouter l'eau.

Un bonheur arraché à la mort

pour quelques gouttes de beau.

[La Vidéo](https://videos.pair2jeux.tube/w/6ng2JxfZSmQv8UCRBg347J){:target="_blank"}
