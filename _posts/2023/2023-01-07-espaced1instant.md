---
layout: post
title: BD - L'Espace d'un instant de Niki Smith (2022)
category: bd 
tags: bd, adolescence, amitié, 2020s, USA
---

**Il y a des histoires qui paraissent simples et émeuvent le/la lecteur/trice. Une sorte de moment de grace...au titre bien porté ici : L'espace d'un instant.**

C'est l'histoire de Manuel, un adolescent américain d'origine hispanique, vivant dans le Midwest. L'histoire débute alors qu'il reprend les cours après un "incident" dont il a été témoin et qui visait sa prof d'arts plastiques. Cela l'a profondément affecté au point que parfois il n'est plus dans la réalité, l'espace d'un instant, comme perdu dans ce traumatisme. Il a alors besoin d'un "point d'ancrage" et c'est à travers la photo qu'il le trouve sur son vieux smartphone cabossé. Mais voilà qu'il fait la rencontre de Sebastian et Caysha, deux camarades de cette classe.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/espaced1instant1.jpg)

C'est d'abord une histoire d'amitié avec ses deux camarades, surtout Sebastian, dont la famille est dans l'élevage et qui doit s'occuper de Daisy, un veau qu'il a engagé dans un concours. Et puis c'est donc aussi sur la santé, les traumatismes qui subsistent et qui sont durs à vivre pour ceux qui les ont mais aussi pour leur entourage. La mère de Manuel est infirmière et se tue au travail tout en s'inquiétant pour son fils. Le fil rouge de l'histoire c'est cet instant traumatique dont sa prof d'art plastique est le centre. Manuel se réfugie dans la photo, a un don pour cela, le coup d’œil qu'il faut. 

Tout cela est très joliment dessiné par Niki Smith, qui est aussi au scénario. Il y a une inspiration manga dans le dessin également, question de génération. Elle a mis de sa propre enfance dans ce récit graphique, nous gratifiant de très jolis paysages à la palette graphique. Le récit est touchant, à fleur de peau comme son héros. C'est tendre, sensible malgré la gravité des faits et le parcours chaotique de notre héros. Il y a également un sens du découpage très photographique et ça tombe plutôt bien. Tout cela nous donne un ouvrage qui touchera à la fois l'adolescent et l'adulte avec ses différents niveaux de lecture. La gravité ne rend pourtant pas cela trop triste et l'on sourit avec parfois des souvenirs d'enfance pour ceux qui ont eu la chance d'aller en dehors des villes.

Car si le sujet principal est un traumatisme, on peut aussi élargir le sujet et penser à quel point un traumatsme visuel et psychologique peut affecter le corps d'un enfant ou d'un adulte. Je pense évidemment à tous les traumatismes d'enfance mais aussi aux viols ou aux visions de violences comme celles liées aux guerres. Ici, l'environnement de notre héros est plutôt bienveillant avec des camarades compréhensifs qui ne le marginalisent pas. Il y a aussi en filigrane le sujet des minorités ethniques aux USA mais Niki Smith lisse sans doute un peu trop les choses pour que ça n'interfère pas avec son sujet principal. Qu'importe finalement si le résultat nous touche. 

Et vous aurez compris que le titre américain "The Golden Hour" est plus subtil car il rappelle ces moments de la journée chers au photographe quand les paysages sont magnifiés par le soleil couchant ou levant, révélant aussi les ombres comme les reliefs. Cela embellit aussi le décor, même le plus insignifiant… au point de motiver l'observateur à suivre le rythme naturel des choses....un peu comme dans cette ferme du Midwest. Ce temps même que la mère du héros ne peut maîtriser...Le temps est aussi au coeur de l'oeuvre d'un photographe qui essaye de capturer l'instant, figeant les mouvements. Notre héros aimerait qu'un instant n'ait pas lieu, celui de son traumatisme.

Une belle découverte inattendue à l'actif de cet éditeur spécialisé dans ce type de récit, aujourd'hui.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/espaced1instant2.jpg)

