---
layout: post
title: Souvenir de Gamer - Puzzle Quest (2007-2021)
category: geek 
tags: retrogaming, jeu vidéo, puzzle game, 2000s
---

**Voilà un jeu que j'ai découvert tardivement puis laissé de côté avant d'y revenir récemment. Étrange mélange entre puzzle game, jeu de stratégie, jeu de rôle. Et en plus ça nous venait d'Australie.**

On ne compte plus les versions et ports de ce jeu depuis 2007. Mais je vais surtout parler de la version Nintendo DS puisque le jeu est bien plus pratique à jouer avec un stylet ou en tactile... Mais aussi bien plus moche, même pour l'époque et la console. Le principe, c'est que l'on choisit un personnage par classe (4 au choix parmi Druide, Guerrier, Chevalier ou Magicien) puis que l'on va combattre des ennemis au fur et à mesure des déplacements sur une carte et des quêtes, de manière à apprendre des sorts, à améliorer ses capacités, etc....Bon ça, c'est comme un jeu de rôle. Sauf que les combats se règlent à travers un jeu de type "Match 3", c'est à dire un damier avec des éléments à regrouper par 3 pour les éliminer...Un genre que l'on a vu dans Bejeweled / Jewels ou encore Zoo Keeper.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/puzzlequest1.jpg) 

*La vénérable version DS*

Et là, le système devient intéressant puisque les joueurs jouent à tour de rôle. Il y a 7 éléments : Bleu, Jaune, Rouge, Vert pour représenter Terre, Eau, Feu, Air dans le désordre ce qui va donner accès à des sorts en cumulant les captures. Ensuite il y a l'or pour pouvoir acheter des choses après le combat et l'XP en violet pour faire progresser le niveau de son personnage. Enfin, il y a des têtes de mort pour frapper l'adversaire et lui enlever des points de vie. Elles peuvent même devenir rouge pour être plus puissantes. Et quand on fait 4 alignés, on rejoue. Ça paraît compliqué ? C'est facile d'accès mais on se retrouve à jouer stratégique.

Pour faire baisser les points de vie de l'adversaire, il faut soit aligner les têtes de morts, soit utiliser un sort et pour utiliser un sort, réunir des éléments de couleur. Mais l'adversaire fera pareil...Donc on doit à la fois empêcher l'adversaire d'avoir certaines couleurs et réunir les siennes. Mais on peut aussi faire le choix de réunir de l'XP ou de l'or dans un combat. Bref, on doit trouver le bon équilibre, se dire qu'en supprimant des cases, d'autres apparaissent et peuvent donner des alignements possibles pour l'adversaire. Perso, j'aime bien le druide, un équilibre entre la magie et le combat avec la possibilité de se soigner. Chacun ses goûts, comme dans un RPG. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/puzzlequest2.jpg)

*Plus joli sur la XBOX 360*

Et je parlais de quête...Il y a en effet une histoire puisque ça débute dans la ville de Bartonia et ...Euh, on s'en fout un peu de l'histoire car c'est vraiment pas terrible. On comprend vite qu'au fil des missions, on découvre de nouveaux lieux, de nouveaux ennemis avec une certaine banalité et répétitivité. Des ennemis nous empêchent de progresser librement ce qui revient aux combats aléatoires habituels pour "leveler". Clairement, l'histoire ne passionne pas et il n'y a que l'envie de progresser en niveau qui maintient l'attention. En plus, on choisit la difficulté dans les combats. Dans le mode facile, l'intelligence artificielle est conne comme un orc... Mais en mode difficile, il faut s'accrocher pour progresser. Ah oui, on ne meurt jamais donc on a le droit de recommencer indéfiniment.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/puzzlequest3.jpg)

*Et finalement, la Switch y a droit aussi*

Il faut compter une bonne quinzaine d'heures pour la quête principale. On peut aussi conquérir des villes (un combat comme un autre) ou capturer des ennemis (on doit alors jouer seul pour enlever tous les éléments du plateau de jeu). Bref, pour qui aime le Match3, il y a de quoi faire mais j'avoue qu'on s'en lasse un peu. Il y a bien à se méfier des sorts de certains ennemis mais quand même, c'est répétitif et il n'y a pas l'aspect score des autres puzzle games. Mais comme il y a un mode multijoueur et que c'est en ligne pour pas mal de plateforme, il y a de quoi renouveler le jeu. Ce fut un hit à sa sortie à cause de tout cela. Pas dur de trouver des longplay pour comprendre un peu la mécanique. Pour la présentation, le boulot a été bien fait par Kensey.

[Présentation en français ![video](/images/youtube.png)](https://www.youtube.com/watch?v=GmA8NtTTPcw){:target="_blank"}

Après tout ça, il y a eu donc des versions avec l'univers Marvel ou Magic. Le dernier opus, baptisé ....3, ne m'emballe pas plus que ça, même si ça se joue sur smartphone. La grille est limitée. Il y a de nouvelles classes mais bon, c'est pareil. Et c'est du Free2play avec tous les défauts et les qualités du genre. Donc je préfère reprendre la version originelle qui avait déjà tout compris. A chacun de voir mais en 15 ans, la mécanique fonctionne toujours.

***Par Infinity Plus Two sur PC Windows, Nintendo DS, XBOX 360, PS2, PS3, Android, iOS, PSP, Nintendo Wii, Nintendo Switch, ...à suivre.***

Et quelques copies d'écran personnelles des fins de différentes quêtes pour terminer.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/puzzlequest4.jpg)
![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/puzzlequest5.jpg)
