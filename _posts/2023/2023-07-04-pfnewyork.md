---
layout: post
title: Portfolio - New-York
category: photo
tags: photographie, paris, noir et blanc, 
---

**Une ville-monde qui paraît inépuisable dans les sujets de photo.**

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfnewyork01.jpg)

*J'ai toujours été fasciné par ce bâtiment, le Flat Iron, mais n'est il pas aussi beau entouré par les arbres du Madison Square park?*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfnewyork02.jpg)

*Je ne pouvais échapper au symbolique taxi jaune, même si cette grosse Crown Victoria a dispatu, comme ces vieux bus recyclés.*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfnewyork03.jpg)

*Ville fascinante où la mer et la plage sont à portée de métro comme ici à Coney Island, près de Little Odessa.*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfnewyork04.jpg)

*C'est aussi le siège, décrépi, des Nations unies, qui tentent toujours de faire taire les armes.*

![Image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfnewyork05.jpg)

*Et la ville ne dort jamais, tandis que ces magasins du passé me hantent toujours.*