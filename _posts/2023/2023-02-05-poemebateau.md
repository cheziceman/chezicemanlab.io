---
layout: post
title: Pause Photo et Poésie - Voguons
category: poesie 
tags: Poème, poésie
---

Sur quel esquif affronter

ces vagues vertes et bleues

qui dressent leurs cimes effrontées

pour m'engloutir et me dire adieu ?

.

Voguons sur cette immensité déchaînée

à en perdre le nord dans les australes.

Fuyons le tumulte terrien des étés

pour nager entre les rorquals.

.

Et soudain le calme, le vent qui s'éteint.

Même le pétrel a quitté ma compagnie

pour s’acoquiner aux embruns.

Je n'ai plus que l'esprit du banni.

.

J'attends un signal, une voile, un courant.

Ou que le gîte reprenne sa berceuse,

et me fasse découvrir quelque continent,

pour céder à nouveau à cette illustre amoureuse.


![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/poemebateau.jpg)

