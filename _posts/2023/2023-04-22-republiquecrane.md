---
layout: post
title: BD - La République du Crâne de Brugeas et Toulhoat (2022)
category: bd 
tags: bd, aventure, pirate, esclavage, 2020s
---
**La piraterie est une époque qui s'étale sur près de 2 siècles. Autant dire qu'elle a son lot de mythes, de personnages et donc de fictions. Voilà un album qui m'a fait revenir à mon adolescence tout en gardant un côté militant et très adulte.**

J'ai en mémoire des films classiques comme **Le Corsaire Rouge**, adaptation d'un livre de Fenimore Cooper que j'avais aussi lu dans mon adolescence. J'ai aussi en mémoire le plus méconnu **A L'Abordage** avec Maureen O'Hara et Errol Flynn. Si je parle de ce film, c'est que justement il y a Une pirate dans cette histoire. Et à la manière d'une sorte d'uchronie, l'ouvrage crée un lien entre la piraterie et l'esclavage. Pourtant, en se penchant sur la réalité de l'histoire de la piraterie, il y avait vraiment un lien entre ces rebelles et parias de la Marine et les esclaves venus d'Afrique jusque dans les Caraïbes ou le sud de ce qui n'était pas encore les USA. Les premiers se libéraient du joug de capitaines qui les exploitaient sans vergogne, pendant que les autres se libéraient du joug d'esclavagistes et propriétaires terriens. Il y a même eu quelques pirates célèbres qui étaient "mulatre".

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/republiquecrane1.jpg)

Pourtant le héros de l'histoire n'est pas une héroïne mais un certain Olivier, marin français venu de Vannes et qui fut autrefois Corsaire. Il côtoie un certain Sylla, pirate charismatique et meneur d'hommes né. Mais un jour, ils partent à l'assaut d'un navire portugais, l'Esperanza et tombent sur des esclaves qui ont réussi à se libérer, menés par leur reine, une certaine Maryam. Nous les suivons dans leurs assauts de navires marchands, dans leur fuite et leurs combats face aux terribles anglais...jusque vers les côtes africaines. C'est donc une belles BD d'aventure comme on les aime avec quelques héroïnes belles et rebelles. Mais il y a aussi un message autour de cette histoire méconnue, cette lutte des classes et cette révolte des sans-grades de la marine face à l'aristocratie et la bourgeoisie.

Tout ça est dans un style coloré, moderne, dynamique. Cela donne envie de prendre les flots, de se revoir justement les classiques et de replonger dans les lectures de jeunesse. Je ne vous parle même pas de mon jeu de chevet, Sid Meier's Pirates… Et vous devinerez peut-être ce que le titre de l'ouvrage sous-entend avec ce que je vous ai déjà dévoilé. L'histoire réelle réserve encore des mystères alors autant rêver un peu grace à ce genre d'aventures. La vie dans ses navires n'était pas rose pourtant et les jambes de bois, crochets ou œil de verre n'étaient pas que pour le folklore du cinéma. Il y a donc son lot de violence, de sang ou même de scènes un peu sexy, sur fond de rhum et d’embruns. Plus de 200 pages tout de même et je n'ai pas regretté d'embarquer.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/republiquecrane2.jpg)
