---
layout: post
title: BD - Punk Rock Jesus de Sean Murphy (2012)
category: BD 
tags: 2010s, bd, comics, religion, punk, rock
---

**Mais pourquoi j'ai mis 10 ans avant de découvrir ce Comic Book?! Pourquoi 9 ans depuis sa sortie en France ? Pourquoi Jesus ne serait pas punk ?**

Sean Murphy nous embarque dans une anticipation (2019) de la société états-unienne avec l'idée suivante : "la société de télé-réalité Ophis lance le projet J2 (Jesus #2), destiné à réaliser le clonage de Jésus-Christ à partir de cellules du Saint Suaire. Grâce à l'expertise scientifique du Dr. Epstein et à la surveillance de Thomas McKael, le premier clonage humain réussit.Une jeune femme vierge, Gwen, donne ainsi naissance en direct à la télévision à un petit garçon, Chris." Clonage, religion, téléréalité... C'est déjà pas mal pour un début mais finalement presque du déjà vu. Et si on rajoute quelques créationnistes complotistes ? Non, toujours pas. Alors il nous faut de bons héros, à commencer par le mystérieux Thomas McKael, le garde du corps ancien terroriste de l'IRA.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/punkrockjesus1.jpg)

Mais ce qui fonctionne d'entrée, c'est la claque visuelle que l'on prend avec le dessin monochrome de Murphy. Du noir et blanc vif et acéré, hyper-détaillé et avec du mouvement, un sens du découpage hors-pair. Ici pas de Batman ou autre super héros mais McKael a une sacré classe pour une montagne de muscles avec sa bécane sortie de l'enfer... de la guerre. Même le Docteur Epstein, la généticienne qui veut sauver le monde du réchauffement climatique a de la gueule. Mais pourtant, le héros, ça devrait être ce Chris, le clone supposé de Jésus. Ou Dieu ? Lequel déjà parce qu'avec les extrèmistes de la nouvelle Amérique chrétienne, il est bien barré, autant qu'avec ceux des autres religions qui interviennent.

Dans ce futur proche déjà au passé, la télé-réalité fascine le monde. Un monde ultra violent, en proie aux multinationales, au mercantilisme... Bref, le notre, quoi. Les éxtrèmistes religieux qui attaquent les institutions, ça ne vous rappelle rien ? Un visionnaire notre Murphy, si on ajoute que quelques savants s'essaieraient au clonage humain dans le secret d'un laboratoire. 6 chapitres réunis dans une belle intégrale avec un peu de bonus pour faire joli et on en redemanderait presque. Tout ça est entrecoupé par des bandes sons accompagnatrices, pas forcément toutes punk, d'ailleurs. Je n'ai pas tenté l'expérience mais pas de temps mort dans ce monde d'anticipation pas tout à fait post-apocalyptique.

L'apocalypse serait-elle en cours, finalement? Je vous laisse trouver les multiples interprétations des choix de l'auteur, les nombreux clins d’œils à l'univers SF, cinématographique ou au monde des super-héros. Un sacré bon album, si j'osais...Mais le sacré, hein, bon.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/punkrockjesus2.jpg)

