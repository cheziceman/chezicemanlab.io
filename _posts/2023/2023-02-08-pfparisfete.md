---
layout: post
title: Portfolio - Paris en fête
category: photo
tags: photographie, paris, noir et blanc, 
---

**S'il y aura aussi de la couleur, je continue en noir et blanc avec Paris. Car les occasions de fêter et se réjouir sont nombreuses.**

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparisfete1.jpg)

*L'été, Paris fait la fête...sur une plage artificielle. Ce fut ma dernière pellicule avec mon premier Reflex.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparisfete2.jpg)

*Il faisait déjà chaud pour les parisiens et ces douches furent l'occasion de se rafraichir...et d'une belle photo ou parents et enfants s'amusent*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparisfete3.jpg)

*J'ai profité de la fin de l'argentique pour récupérer un Elan7 de Canon et partir à l'aventure dans Paris pendant les fêtes de Noël, il y a quelques années de cela.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparisfete4.jpg)

*Je voulais capter toujours en argentique, la fascination des enfants pour les vitrines du Boulevard Haussmann.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparisfete5.jpg)

*Et il y a certainement un peu de mon regard ou de ma part d'enfance dans ses enfants qui regardent les vitrines animées. Toujours avec l'Elan7 et un 50mm sur de la pellicule Fuji Neopan 100.*
