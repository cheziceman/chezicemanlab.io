---
layout: post
title: BD - Dans mes Yeux de Bastien Vivès (2009)
category: bd 
tags: bd, romance, subjective, 2000s
---

**Une couverture d'un rouge flambloyant, des premières cases sans dialogue, sur une jeune femme aux cheveux aussi flambloyant que la couverture. J'étais intrigué… Sans même faire attention à l'auteur qui a une oeuvre polémique.**

En fait Bastien Vivès nous fait vivre une romance de l'intérieur, du coté de l'homme amoureux de cette étudiante en littérature. Et ainsi, il nous met à la place que l'on a pu vivre (je vous le souhaite) en tant qu'homme / femme. Il y a cette envie de connaître l'autre, d'être avec elle. Mais il y a aussi ce sentiment d'être exclu(e) lorsqu'elle est avec ses amis, sa famille. Il y a ces partages de passions, ces premiers rendez-vous et les maladresses de chaque coté. Il y a cet amour qui transforme sans doute la réalité, nous fait focaliser sur l'être aimé et oublier le reste. Il y a ces moments si doux et inoubliables.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/dansmesyeux1.jpg)

Bastien Vivès a fait le choix de l'encre et de crayons de couleurs. On est assez loin justement de ses autres œuvres, plus "électroniques". Cela trouve un lien aussi avec des éléments du récit. Cela nous met aussi dans ce sentiment d'être hors du temps lorsque l'on est amoureux. On ne sait même pas si c'est une part de l'auteur ou une fiction totale puisque l'on ne sait rien de cet homme (ça on en est sûr). Il y a d'habiles procédés pour voir que parfois il perd le fil d'une situation, il n'écoute plus rien, ne regarde qu'elle. Il y a cette manière de rendre les baisers et autres gestes tendres. Et puis si l'on se demande qui est l'homme qui regarde, on se demande aussi qui est au fond cette étudiante qui semble peu à peu blessée, fragile. Après, n'attendez pas de rebondissement, d'intrigue complexe. Ce n'est pas le but de l'auteur.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/dansmesyeux2.jpg)

Parfois, ce sentiment d'être dans l'intimité d'une relation peut gêner mais Bastien Vivès sait ne pas aller trop loin. Une œuvre rare, décalée, émouvante et l'amour fait toujours de bonnes histoires. Je m'en serais voulu de ne pas avoir connu cet album...et l'amour, ce sentiment que l'on veut éternel et que la vie tente de nous faire oublier par des tours et détours. Mais c'est justement une BD fugace comme peut l'être l'amour passion. Le rouge de la couverture lui va finalement si bien et nous ramène ainsi à nos propres passions. Et que sommes-nous alors dans les yeux de l'autre ? Une question oubliée ou qu'il ne faut pas poser ? Vous aurez peut-être la réponse ici. 

*Pour le reste de l'oeuvre de l'auteur, c'est un autre problème. J'en ai connu d'autres, en littérature ou en musique par exemple, qui ont pris des chemins délicats ou criticables, après de bons ouvrages.*
