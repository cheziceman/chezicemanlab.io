---
layout: post
title: Veganisme au quotidien - Quelques nouveautés agro-alimentaires
category: vegan 
tags: vegan, veganisme, végétarisme, végétarien, agro-alimentaire
---

**Comme j'aime bien tester les produits végans que je trouve en magasin, je fais parfois des articles pour en parler. Articles évidemment non sponsorisés. Je ne vais parler que de ce que l'on trouve facilement dans les commerces, pas ce qui est réservé à une ou deux boutiques parisiennes.**

#### Fromages à tartiner

Commençons par ces fromages à tartiner, c'est à dire un truc pas vraiment fromage mais plutôt laitier avec de petites aromatisations au passage. Evidemment, en vegan, plus de lait mais c'est la texture qui compte. Je suis fan des produits Bute Island mais on ne les trouve que dans des boutiques spécialisées. Alors j'ai découvert le Tartare végétal en 2021, et puis maintenant le Boursin Végétal en 2022. Le premier appartient à la marque Savencia, ex Bongrain, 2ème groupe laitier français. Le deuxième appartient aux fromageries Bel, la marque de Vache qui rit, Babybel, etc. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/produitvegan22a.jpg)

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/produitvegan22b.jpg)

Regardons d'abord les compositions : 
* Tartare : Lait d’amande 63% (eau, purée d’amande 15%), huile de colza, farine de riz, eau, ail & fines herbes (2,7%), inuline (fibres de chicorée), jus de citron concentré, sel, arômes naturels d’ail et de persil, poivre.
* Boursin : Eau, huile de tournesol (19%), farine de riz, farine de tournesol, fibres de bambou (3.5%) , fécule de pomme de terre et protéines de pomme de terre, ail 1%, sel, jus de citron concentré, arômes naturels, fines herbes 0,3% (persil 0,2%, ciboulette 0,1%), poivre blanc. Peut contenir des traces de sésame. 
Avantage Tartare pour les ingrédients, à priori pour les herbes plus présentes.

En informations nutritionnelles :
* Tartare : 1032Kj pour 100g, 18% de matière grasse, 15% de glucides dont 0,7 de sucre soit 20% de moins que la version normale pour les calories, 17% de moins de matière grasse...MAIS 13% de glucides en plus.
* Boursin : 1074Kj pour 100g, 22% de matière grasse, 13% de glucides, soit 15% de moins que la version normale pour les calories, 9% en moins en matière grasse...MAIS 10% de glucides en plus. 
Toujours un léger avantage à Tartare.

Au goût : Le Boursin n'est pas très proche de l'original et on ressent cet aspect sucré. Pas vraiment mauvais mais pas vraiment bon, il s'oublie rapidement. Le Tartare, bien que différent de l'original, est assez bon avec un fort gout de fines herbes. Mais on ressent aussi ce petit goût d'amande qui peut étonner. Intéressant mais pas incroyable.

Pour le prix, on est à 2 euros contre 1,6 Euros pour le tartare végétal face à son original. Et 2,10 euros contre 1,8 euros pour le Boursin.

#### "Faux" fromages

Dans cette catégories, on ne trouve pour l'instant que des rapés, des tranches de fromages genre Ceddar, Mozzarella, Emmental, donc ce que l'on peut mettre sur des pâtes, des pizzas, dans des burgers. La grande nouvelle, c'est que les produits Violife se trouvent dans la grande distribution. C'était effectivement une des meilleures marques sur le marché, mieux par exemple que ce que fait le belge Tartex. Violife est une marque grecque. Je vais prendre l'exemple de la tranche saveur Emmental.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/produitvegan22c.jpg)

Pour la composition : EAU, HUILE DE NOIX DE COCO (23%), AMIDON MODIFIÉ*, AMIDON, SEL DE MER, CONCENTRÉ DE JUS DE POMME, SAVEUR EMMENTAL, EXTRAIT D'OLIVE, COLORANT : B-CAROTÈNE, VITAMINE B12.
Rien d'extraordinaire donc.
Pour les informations nutritionnelles : 1226Kj pour 100g, 23% de matière grasse, 22% de glucides dont 0,7% de sucres. 2% de sel. A comparer au 1500Kj d'un emmental normal, des 30% de matière grasse, 0,5% de sucre et 0,7% de sel. Pour résumer pour tous les produits fromagers vegan, on gagne en calorie et gras ce que l'on perd en sucre et sel.

Les produits violife ont surtout l'avantage d'un comportement à la cuisson très proche des fromages véritables. D'autres marquent fondent mal ou n'ont pas la texture ou le goût. 

Pour le prix : On est à 2,21 euros en grande distribution (et 3 euros en boutique), contre 1,8 Euros pour un produit traditionnel. 

#### Les Desserts "lactés"

La bataille de la mousse au chocolat a bien lieu. Et cette fois, vous avez le choix entre La Mousse Végétale Andros et la Danette végétale Mousse au Chocolat. Andros, c'est aussi Mamie Nova mais ils ont préféré utiliser leur marque connotée fruits donc végétal. Danette, c'est le géant Gervais-Danone, le 3ème groupe laitier mondial.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/produitvegan22d.jpg)

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/produitvegan22e.jpg)

Regardons d'abord les compositions : 
* Andros : Lait de coco allégé 31,8% (eau, extrait de noix de coco), eau, chocolat 16.2% (sucre, pâte de cacao, beurre de cacao, cacao maigre en poudre, émulsifiant: lécithine de SOJA, arôme naturel de vanille), sucre, eau de coco, amidon de pomme de terre, amidon de tapioca, protéines de pomme de terre, émulsifiant: mono et diglycérides d’acides gras (colza), épaississant: carraghénanes, sel.
* Danette : LAIT de coco 60% (eau (35%), CREME de coco (25%)) eau (14,6%) chocolat en poudre (10,9%) (sucre, cacao en poudre) fibre soluble de maïs sucre(4,1%) protéines de pois chocolat noir en poudre (1%) (poudre de cacao,pâte de cacao, sucre) épaississants (amidon transformé de tapioca (E1442), agar agar (E406)) émulsifiants (mono-diglycérides d'acides gras (E472b)) sel.
Bon, ce n'est pas généial tout ça mais on retrouve des ingrédients très proches. Que de transformation.

En informations nutritionnelles :
* Andros : 671Kj pour 100g, 8% de matière grasse et 19% de glucides dont 16% de sucres. 
* Danette : 635Kj pour 100g, 8% de matière grasse et 14% de glucides dont 12% de sucres.
A priori, un peu meilleur pour Danette

Au goût : C'est la texture qui fait la différence. Les pots d'Andros sont plus grands mais aussi plus aérés ce qui donne une texture plus légère et mousseuse. Le goût est aussi moins écœurant et paradoxalement moins sucré en apparence. 

Pour le prix : La Danette est à 3,25 Euros soit le double du produit normal. L'Andros est à 2,8 Euros soit le double du produit du même groupe chez Mamie Nova.

#### Produits surgelés

J'ai découvert au hasard un nouveau produit Picard dans leur gamme de "Pizzas Créations Créatives". S'il y a toujours eu des pizzas végétariennes, se posait le problème du fromage (voir au dessus). Et bien Picard a tenté une solution originale sur la Pizza Créative N°21. Sur l'étiquette : houmous basilic, asperge, amande. On a donc un produit qui n'essaye pas de singer mais remplace carrément les ingrédients par autre chose. L'allure est plutôt bonne par rapport à des pizzas surgelées standards et ça ne sera pas pire pour les ingrédients, c'est sûr. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/produitvegan22f.jpg)

Les Ingrédients : Pâte 50%, garniture 25,9% à base de courgette grillée, d’asperge verte, d’épinard et d’amande, sauce 24,1% à base de pois chiche et de basilic ; précuite, surgelée. 
Les Informations nutritionnelles : 847KJ pour 100g, 6,6% de matières grasses, 28% de glucides, 1% de sel.

Je confirme qu'au goût c'est sucré, un peu trop mais c'est quand même très bon avec les asperges. Il ne faut pas s'attendre à en avoir beaucoup mais cela donne au moins des idées pour en faire une soi-même. Pour la pâte, on est dans la moyenne haute des pizzas surgelés avec suffisamment de moelleux et pas d'aspect carton. Un produit vraiment intéressant pour offrir une alternative vegane en dernière minute par exemple. Sauf que le produit a été retiré du catalogue quasiment un an avant de réapparaître ce mois-ci.

Pour le prix : 4,3 €...4,5€ aujourd'hui c'était raisonnable, les autres pizzas de la marque étant à moins de 5 euros. C'est 2 fois le prix d'une pizza surgelée de supermarché.

En conclusion, on peut dire que dans la gamme de produits transformés, on gagne en graisses et calories à passer sur des alternatives veganes. Mais dommage que cela soit compensé par trop de sucre. Reste que la créativité des recettes est aussi intéressante pour aller au delà de l'imitation. Et puis pour être vegan, il faut être riche....ou malin et faire soi-même.

*A lire aussi : Ce qu'on devrait savoir sur les [fromages végétaux](https://theconversation.com/veganisme-tout-ce-quil-faut-savoir-sur-les-fromages-vegetaux-185986)....Mais il ne faut pas oublier ce que l'on ne sait pas sur les vrais fromages et la souffrance animale qui en découle.*
