---
layout: post
title: Photographie - Stanley Greene (1949 - 2017)
category: geek
tags:  photographie, journalisme
---


**Il découvrit sa vocation tardivement. Il obtint aussi la notoriété sur le tard et est un des rares à avoir touché à différents genres de cet art : La photographie.**

J'avoue que j'ai découvert ce photographe sur le tard, à travers Polka Magazine, et surtout après son décès en 2017. Et puis plus récemment, j'ai lu une biographie [en BD](https://www.editions-delcourt.fr/bd/series/serie-stanley-greene-une-vie-vif/album-stanley-greene-une-vie-vif), pas forcément des plus réussies, mais qui a le mérite de donner un panorama d'une vie bien remplie. De là, l'envie d'en parler ici à travers un article. 

*J'ai réduit la résolution et rajouté le nom en grand car il y a un problème de droit, forcément...Ne gagnant pas d'argent là dessus, j'espère qu'on ne m'en voudra pas*

![portrait](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Stanley_Greene_in_Uz%C3%A8s%2C_France%2C_2008.png/345px-Stanley_Greene_in_Uz%C3%A8s%2C_France%2C_2008.png)

Il faut croire qu'il est tombé dedans quand il était petit. Son père, militant syndicaliste et black listé pendant le maccarthisme, ne trouve guère de rôles au cinéma. Déjà un métier d'image. Mais le jeune Stanley se cherche dans le New York des années 60-70. Il devient militant des Black Panther, fait de la peinture. Et c'est le hasard qui le fait rencontrer le photographe Eugene Smith. A l'époque, il verse plutôt dans la drogue, ce qui le rapproche de ce fantasque photographe lui aussi engagé. L'approche de Smith est radical et il repère vite en Stanley Greene quelqu'un qui a l'oeil. Il ne lui reste qu'à apprendre la technique. Et comme dit Smith, qu'importe l'appareil, ce n'est qu'un outil. 

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/stangreene1.jpg)

*Ses débuts dans le rock*

Le voilà photographe des groupes de la scène New-Yorkaise de l'époque, une scène rock, les prémices du punk, etc...La drogue aussi et encore. Il se retrouve un peu par hasard à Paris et par rencontres et petites amies entame une deuxième carrière de photographe de mode. Mais ce milieu le fait vomir par son utilisation des jeunes filles. Il ne se sent pas à l'aise dans ce rôle, même si ça le fait vivre dans ce début des années 80. Le voilà aussi par hasard à Berlin lorsque le mur tombe. Il découvre Berlin Est et une de ses photos fait la une. Le voilà prêt pour une troisième carrière : Photojournaliste.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/stangreene2.jpg)

*La photo qui le fit connaître*

C'est sans doute la plus tumultueuse mais aussi, pour moi et d'autres, la plus intéressante. Il couvre ainsi la tentative de coup d'état contre Boris Eltsine, alors impitoyable avec ses ennemis mais avec des alliés occidentaux qui se servaient allègrement. Il couvre ensuite tous les conflits des années 90 : Irak, Liban, Somalie, Croatie, Rwanda, Tchétchénie. Difficile de prendre seulement quelques clichés parmi les siens pour résumer toute l'émotion et l'horreur de la guerre. C'est un homme de rencontre, notamment avec des femmes fortes, des guerrières, des photographes engagées, dont certaines le payent de leur vie. Lui dit qu'il a eu de la chance.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/stangreene3.jpg)

*Au coeur des combats*

Effectivement, il y a une part de chance pour tomber au bon moment au bon endroit ou même pour ne pas être dans le véhicule pris pour cible par une roquette ou qui passera sur une mine. On se souvient par exemple des clichés de Gerda Taro pendant la guerre d'Espagne, qui furent parfois attribués à son compagnon Robert Capa. Elle y laissa la vie. La vie de Stanley Greene est aussi faite de ces moments tragiques. Parfois il a choisi de ne pas montrer directement l'horreur de la guerre en laissant le spectateur que nous sommes imaginer, construire une histoire, comme dans le cliché ci-dessous.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/stangreene4.jpg)

*Toute l'horreur de la guerre*

Il y aura maintenant, périodiquement ou pas, des expositions qui feront appel à ces photos que l'on aimerait voir rester dans le passé. Mais non, il y a toujours les mêmes scènes qui se répètent, des armes toujours plus destructrices dans tous les points du globe pour des prétextes que nous ne pouvons comprendre. La Tchétchénie est une dictature mais qui sait ce qu'il arrivera en Ukraine, au Haut-Karabakh ou encore dans le Soudan, le Tigré, la Somalie ou le Yemen... Stanley Greene avait permis l'émergence d'autres photographes avec l'Agence Noor, même si il était aussi un électron libre. Il n'était pas un gestionnaire mais un passionné. Il était porté par l'actualité ou la curiosité. Il n'a pas voulu tomber dans des traitements qui l'auraient trop contraint et mourra dans le dénuement à Paris. Sa mémoire est à travers ses clichés et quelques amis qui lui apportèrent une notoriété tardive.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/stangreene5.jpg)

*Une de ses dernières photos primées en 2007.*

En dehors de cette BD, il y a aussi le *N°118 de Photo-Poche*, et différents livres de photos au long de sa carrière que l'on peut toujours trouver comme *Open Wound* ou *Western Front*.
