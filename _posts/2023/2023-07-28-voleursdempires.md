---
layout: post
title: BD - Voleurs d'Empires de Dufaux et Jamar (1993)
category: bd 
tags:  bd, histoire, fantastique, 1990s, empire, mort
---

**J'ai arrêté depuis longtemps de compter les BD scénarisées par Jean Dufaux que j'ai pu lire. J'avais un souvenir lointain du premier tome de cette série de 7. Une couverture noire, une chevelure féminine rousse flambloyante faisant face à un maréchal d'empire squelette...**

Vous avez compris que nous sommes dans du fantastique historique. Mais ce n'est pas de Napoléon 1er que l'on parle ici, malgré les apparences, mais de son descendant, le très controversé Napoléon III...et la guerre de 1870-1871 contre les prussiens. Notre prolifique scénariste belge nous emmène donc sur un chemin balisé par l'histoire de France mais dans une histoire de pacte avec la mort. Du classique me direz-vous, mais dans un tel contexte c'est plus original. Il s'est ici associé au non moins belge Martin Jamar, le natif de Liège ayant un style classique parfait pour ce type de récit. Il faut du décor parisien, des uniformes, des chevaux, des calèches, donc autant de détails qui ont besoin d'un trait fin et de couleurs précises.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/voleursdempires1.jpg)

Ce qui m'a fait reprendre le fil de ce récit, c'est une intégrale massive regroupant les 7 tomes avec une couverture toute aussi frappante où cette mort impériale nous regarde fixement. C'est une nouvelle édition d'ailleurs car la première date déjà de 20 ans, date du dernier tome. Mais pourquoi en parler alors ? Il y a déjà le mystère bien caché de cette femme rousse qui a passé un pacte diabolique, vit avec des rats dans une pension éloignée de Paris, cachant ses origines prestigieuses. On tient bien 3 tomes comme cela. Il y a ce contexte historique qui est rappelé dans une ou deux planches par tome. C'est donc une période très troublée en France après de multiples changement de régimes entre république, monarchie et empire. On y croise des noms comme Gambetta, Thiers, MacMahon, Hugo, Courbet...Louise Michel. Car évidemment, cela s'achève dans la Commune de Paris. Nos personnages ne sont pas vraiment des héros mais des témoins ou des jouets d'un destin qui leur échappe le plus souvent. Il y a Nicolas le noble orphelin ténébreux. Il y a Julien, l'aristocrate imbus de lui-même. Il y a Anais, la provinciale naïve. Il y a Madeleine, l'aristocrate manipulatrice et un peu nymphomane....Et la Mort qui vient roder autour de cela, dans cette pension étrange. 

Il y a son lot de rebondissements et de cliffhangers pour nous tenir en haleine. Dufaux sait faire dans une atmosphère qui fait appel aux classiques de la littérature de cette fin 19ème siècle. Nos héros sont faillibles, traîtres parfois, et il n'est pas aisé de deviner s'ils survivront à cette machination diabolique ou mortelle. Outre l'ambiance, il y a donc une envie d'aller plus loin dans cet univers, de comprendre les tenants et aboutissants. Et cela aurait pu aussi déboucher sur des suites, des spin-off. Alors bien sûr le style est très ancré dans son époque, très franco-belge classique mais parfaitement réalisé. Quelle saga au final avec un élément central qui ne se dévoile pas tout à fait à la fin. On sent qu'il y a eu beaucoup de travail en amont aussi bien dans la cohérence graphique que pour faire tenir tous les fils de cette intrigue. Bien que l'idée de base reste classique, on peut aussi y ressentir des liens avec les débuts de la littérature fantastique de cette époque. Je craignais que l'ensemble n'ait vieilli mais ça tient tout à fait la route de nos jours car ce n'est pas trop barré ou alambiqué comme ce qui fut écrit dans les années 80.

On pourra évidemment creuser le sujet, notamment celui de cette guerre souvent oubliée et de ses multiples conséquences, cette récupération bourgeoise et aristocratique de la révolution de la fin du 18ème siècle. Dufaux s'en sert habilement pour son récit dans un mélange des genres que j'ai trouvé séduisant.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/voleursdempires2.jpg)



