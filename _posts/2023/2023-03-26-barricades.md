---
layout: post
title: BD - Barricades de Charlotte Bousquet et Jaypee (2018)
category: bd
tags: transgenre, adolescence, bd, 2010s, sexualité
---

**La couverture bleue de cet ouvrage masquait plutôt bien le sujet. Le titre aussi, faisant penser plutôt à des manifestations. Et pourtant c'est bien de tout autre chose dont parle cette BD, si ce n'est qu'il s'agit bien de la jeune fille en couverture.**

Charlotte Bousquet avait entâmé une série comme scénariste avec l'IVG. C'est le deuxième tome de la série "Secret pour secret" et c'est bien un sujet délicat aussi. Son héroïne, Sam, est au lycée. On apprend très vite qu'elle a du changer d'établissement. On pense au harcèlement. Elle n'a pas confiance en elle, encore moins en ses camarades de classe. Et pourtant, elle fait la rencontre d'un groupe de rock du lycée qui lui propose de chanter avec eux. La voilà qui tente de s'épanouir mais .… le secret bien gardé la rattrape.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/barricades1.jpg)

Charlotte Bousquet est philosophe, romancière, en plus d'être scénariste ici. Elle trouve en Jaypee la personne qui saura mettre des images sur les mots et phrases. C'est à l'aide de pastel gras que nous entrons dans cet univers d'adolescents et jeunes adultes. Parfois c'est très épuré pour mettre en valeur les personnages, parfois c'est plus détaillé avec des couleurs vives, souvent sombres. Il faut dire que l'humeur de notre héroïne est sombre aussi. Elle se scarifie les avant-bras et pourtant ses parents sont présents, aimants. Seule la grand-mère paternelle est hostile à cette mystérieuse situation. Je vous laisse quelques indices tout de même... Cela tient à la sexualité, à l'ouverture d'esprit aussi de notre société. 

J'ai lu quelques commentaires de jeunes lecteurs/lectrices qui trouve cela déprimant, trop "hardcore". Est-ce parce que je suis plus agé que je trouve cela juste et réussi. On peut juste reprocher une fin attendue mais optimiste. C'est un mal adolescent qui est décrit ici, même s'il a une connotation plus rare que la simple estime de soi ou la recherche de sa place dans la société. Le récit est effectivement émouvant. On devine quand même assez rapidement le secret de cette histoire. Mais comment réagirait-on en temps que parent, que camarade, que prof, que chef d'établissement ? Au delà du simple harcèlement avec l'arme fatale des réseaux sociaux, il y a ici une bonne base de discussion plus que philosophique mais simplement sociétale. Et si vous êtes allé jusq'au bout de cet article, vous avez donc droit à un second indice : On parle de genre !

![Image non trouvée : https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/barricades2.jpg](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/barricades2.jpg "Image non trouvée : https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/barricades2.jpg")


