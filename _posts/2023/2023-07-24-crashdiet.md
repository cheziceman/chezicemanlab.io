---
layout: post
title: Musique - Crahsdïet - Automaton (2022)
category: musique
tags: musique, hard-rock, sleaze, 2020s
---

**Après un premier album tonitruant en 2005 (Rest in Sleaze) et le suicide de leur chanteur Dave Leppard en 2006, les suédois de Crashdïet auraient pu disparaître. Mais le trio qui forme l'ossature du groupe a tenu bon jusqu'à ce 6ème album.**

On retrouve donc toujours Martin Sweet à la guitare, Peter London à la basse et Eric Young à la batterie. Vous l'aurez compris, ce sont des pseudos (pourtant Martin Hosselton, ça va, non ?). Mais au chant, c'est un certain Gabriel Keyes qui officie depuis 2018, soit le 4ème chanteur. Le groupe avait déjà montré de bonnes dispositions avec "Generation Wild" en 2010 et Simon Cruz au chant. C'est bien Sweet qui semble le leader, déjà coupable d'un side-project avec "Sweet Creature" et d'un album solo de cover. En bon groupe de Sleaze donc dérivé du glam metal, on retrouve depuis le début de nettes influences Mötley Crüe, Def Leppard, Hanoï Rocks, et j'en passe. C'est un des groupes du renouveau d'un style qui s'était un peu essoufflé.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/crashdiet.jpg)

A la première écoute de ce nouvel album, j'ai trouvé ça très accrocheur, presque trop avec ces "Oh Oh", ces chœurs qu'on aime reprendre pour les refrains. On y perd en énergie punk ce que l'on gagne pour l'aspect mélodique. Presque pop, mais c'est aussi toute la force du hard-rock nordique dans du Sleaze bien efficace. Et ça envoie du lourd niveau son en plus : Grosse basse et rythmique appuyée. Après la courte intro, "Together Whatever" est bien à l'image de l'album : Fédérateur et prenant. On rajoute un peu de claviers pour "Shine On" qui ne ralentit pas le rythme (bien marqué). Le chanteur assure avec une production au cordeau. On imagine ce que ça doit donner en live ! C'est toujours aussi bon sur "No Man's Land" qui a la force d'avoir une bonne mélodie mêlée à l'énergie et la puissance du groupe. Il faut noter encore une fois les arpèges soignés en fond, sur beaucoup des morceaux. La production ne se contente pas de pousser les potards à fond !

Il faut bien baisser le tempo comme sur "Darker Minds", toujours très efficace mélodiquement, à défaut d'un peu d'originalité. "Dead Crusade" a une grosse intro à coup de riffs et de batterie agressive. Là encore du chœur qui fait mouche et on se voit bien dans une fosse surchauffée à scander les paroles. Comme le groupe a pas mal d'humour dans ses vidéos, il s'entend bien avec les américains de **Steel Panther**. On retrouve donc Michael Starr au chant en duo sur le très efficace "Powerline". J'avoue préférer Keyes à Starr mais le clip vaut le détour... Petite allusion au monde des films d'horreur avec "Resurrection of the Damned" ? Pas vraiment quand on écoute les paroles de ce groupe qui revit vraiment dans cet album après cette pause COVID. "Sick of singing, The dead man's blues"...Dave, on pense toujours à toi. 

Quel plaisir que d'entendre "We Die hard" et son refrain. Je l'aurai curieusement bien vu dans le dernier Scorpions pour la thématique. "No matter where we start, The road is in our hearts". C'est bien la vie qu'ils ont choisie. Toujours du bon solo coté Martin Sweet, sans en faire trop dans un style qui prête pourtant à le faire. Cet album a vraiment de la puissance qui est surmultipliée par une écoute à haut volume, comme avec "Shell Shock", dans la même veine que Dead Crusade. Il n'y a vraiment pas de moment faible dans cet opus qui reste homogène, même avec "Unbroken". On termine quand même sur une balade pour un peu de douceur...C'est touchant même si les limites de Keyes apparaissent plus et qu'on aligne quelques poncifs du genre. J'aurais fait un autre choix de titre final mais bon... Un vraiment bon album au final qui montre que ce groupe en a encore sous le pied et acquis ses lettres de noblesse du genre, à coté de ses modèles. Glam will never die !

[Le deuxième single![video](/images/youtube.png)](https://www.youtube.com/watch?v=Hm-zVoLJxac){:target="_blank"}
