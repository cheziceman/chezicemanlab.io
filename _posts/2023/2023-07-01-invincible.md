---
layout: post
title: BD et Séries - Invincible de Kirkman, Walker et Ottley (2003)
category: BD, serie
tags: comics, superhéros, 2010s, série
---

**N'étant plus très fan de comics, je recherche un peu plus l'originalité dans ce registre. Et c'est par l'émission de radio Blockbusters que j'ai entendu parlé de cette série de chez Image Comics, un éditeur qui a le mérite d'être aussi prolifique qu'original.**

Le scénariste Robert Kirkman part pourtant d'une idée assez banale : Un adolescent se découvre des super-pouvoirs, tout en ayant son petit job, sortant avec sa petite amie, etc...Mouais, on a vu ça chez Spiderman. Sauf que son père est aussi un super-héros, venu d'une autre planète et qui a épousé une humaine. Ok, ça c'est Superman. Il fait la rencontre d'autres super-héros qui font équipe. Bon, là on a les XMen, les Avengers, la Justice League, etc...Alors elle est où l'originalité ? Tout simplement dans le traitement de l'histoire qui parle un peu plus de la psychologie de ses personnages, des héros qui ne sont pas tous aussi gentils ou méchants qu'on peut le croire. Ainsi le père de notre héros Mark Grayson/Invincible, le fameux Omni-Man, n'est pas que le père protecteur et surpuissant...Mais ça, on le sait au bout de la première intégrale. La série étant terminée, elle a été éditée aussi en gros volumes de 350 pages ce qui donne accès à des bonus. Il y a aussi de l'autodérision, de la punchline mais aussi de gros clins d’œil à de nombreuses autres séries.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/invincible1.jpg)

Visuellement, le rendu peut surprendre. C'est très "old school" par ses couleurs plutôt criardes mais en même temps avec un dessin moderne, souvent épuré, même s'il y a parfois des doubles pages très détaillées. On est dans un dessin plus européen qu'américain. Mais ce n'est pas ce qui m'a convaincu de suivre cette longue série (15 ans environ). C'est plutôt l'humour avec des intermèdes proches du gag. Évidemment, on a comme d'habitude une galerie de méchants mutants et d'extra-terrestres parfois un peu ridicules ou franchement flippants. Mais il y a aussi toutes les failles de personnages et la vie réelle de cette adolescent ou jeune adulte partagé entre sa vie normale et ses missions de super-héros. Je ne vous révélerai pas ce qu'il advient à sa mère mais c'est un personnage important dans la série, même si elle n'a aucun pouvoir. Sur le lot d'autres héros, il y a évidemment de quoi faire. Et puis on a aussi le classique triangle amoureux.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/invincible2.jpg)

Il y a évidemment cette tendance à glisser des suites et intrigues parallèles un peu partout. On tire en longueur à partir de la 4ème saison mais ça réussit à fonctionner quand même. Je suis moins fans des épisodes avec la grosse baston générale façon Avengers. Par contre, il y a des épisodes plutôt surprenant, comme celui sur les violences conjugales, par exemple. Alors pourquoi passer à cette série plutôt que l'univers Marvel ou DC ? Justement parce que ça ne se prend pas autant au sérieux, qu'il y a de vraies nouveautés dans le traitement, comme le sang qui gicle, les onomatopées qui prennent dans la place dans les cases. Et qu'on ne se lasse pas de trouver les liens et moqueries envers d'autres héros. Les histoire se suivent évidemment avec quelques fils rouges mais finissent souvent en cliffhanger avec aussi un bon lot de gros rebondissements et de mystères qui prennent du temps à se démêler, ce qui pousse à lire et lire encore, ou même à relire.

Sinon, il y a eu une adaptation en animé. Il y en a qui aiment mais je préfère le dessin du comics. Cela dit, l'histoire semble fidèlement adaptée, donc c'est au choix. 

[En Série ![video](/images/youtube.png)](https://www.youtube.com/watch?v=-bfAVpuko5o){:target="_blank"}
