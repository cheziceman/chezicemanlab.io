---
layout: post
title: Portfolio - Panorama de Paris
category: photo
tags: photographie, paris, noir et blanc, 
---

**Un portfolio consacré à Paris en noir et blanc dans toute sa variété.**

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparispano01.jpg)

*On commence par un Paris et ses toits où l'on voit aussi bien les immeubles hausmanniens que la tour Montparnasse, comme un monolithe. (photo prise au 50mm sur Canon Elan7)*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparispano02.jpg)

*Paris, c'est le tourisme, l'Opéra évidemment qui réunit des personnes de tous horizons.*

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2023/pfparispano03.jpg)

*Mais la nuit, la ville dévoile aussi d'autres charmes, des contrastes. (photo prise au 50mm sur Canon Elan7)*