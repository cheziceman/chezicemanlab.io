---
layout: post
title: Cinéma - Le Terminal de Steven Spielberg (2004)
category: cinema 
tags: film, cinéma, 2000s, spielberg, comédie romantique
---

**Encore un film qui me tient à coeur et pourtant, il est rarement cité quand on parle des films de Spielberg. Je l'ai vu de nombreuses fois et j'ai l'impression qu'il est comme le bon vin...**

Le sujet du film est basé sur une histoire vraie : Celle de "Sir Alfred" [Merhan Karimi Nasseri](https://fr.wikipedia.org/wiki/Mehran_Karimi_Nasseri), un ressortissant iranien qui passa 16 ans coincé à Roissy pour des problèmes administratifs. Vraiment pas de chance de rester à Roissy...Spielberg est plus clément, situant l'action à New-York et transformant la quête de notre héros, Viktor Navorsky (Tom Hanks), en tout autre chose (je laisse le suspens). Il vient d'un pays imaginaire, la Krakhozie, proche de la Russie. Un coup d'état rend son visa caduque et le voilà retenu dans la zone de transit pour une durée indéterminée, avec quelques tickets restaurants, une carte téléphonique et sa valise...Et aussi une mystérieuse boite de cacahuètes.

![image](https://filedn.eu/llqi9IBxlYouGRXYG2xlROb/img/2022/theterminal.jpg "image")

Avec un tel sujet, on pense plus à un drame. Mais de la comédie dramatique, on passe aussi à la comédie romantique lorsque Viktor rencontre une hôtesse de l'air, Amélia (Catherine Zeta Jones), aux amours compliqués. Coup de foudre? Il y a aussi la relation entre le livreur de repas et la douanière dont Franck se trouve à jouer les entremetteurs. Et ainsi il tisse des relations avec de nombreux personnages pour vivre sa vie dans le terminal. Tout irait bien s'il n'y avait Frank Dixon (Stanley Tucci), le responsable de la sécurité pour lui mettre des battons dans les roues. C'est donc aussi un combat à distance entre ces deux hommes pour obtenir un peu plus que la liberté de circuler, ... Un peu d'humanité.

Le film fonctionne non seulement grâce à la mise en scène de Spielberg, à l'aise sur tous les terrains. Mais il faut tout le talent de Tom Hanks pour incarner ce monsieur tout-le-monde. Il s'inspire notamment de ses aînés du muet (Chaplin, Keaton) dans ses gestes et sa démarche. De cette fausse maladresse, il y oppose la dextérité à faire des travaux manuels, l'intelligence pour se sortir de toutes les situations, comme pour trouver de la nourriture, ou sauver un homme... Pourtant, Hanks ne tire pas la couverture et laisse parfaitement la place au génial Stanley Tucci, détestable à souhait, ou à la charismatique Catherine Zeta-Jones qui ne se contente pas de sa beauté mais nous émeut dans ce rôle. Il y a aussi Mr Gupta (Kumar Pallana) ou le policier Thurman (Barry Shabaka Henley) qui restent emblématiques, Zoê Saldana (la douanière) trouvant ici un rôle déjà conséquent avant Avatar.

Spielberg soigne son film avec un décor très léché, des scènes très graphiques (Gupta face à l'Avion), et de pures scènes de comédie classique américaine. On sent qu'il a été bercé par ces grandes comédies des années 40. S'ajoute à cela la partition de John Williams qui est particulièrement inspiré. Il en fut récompensé par de nombreux prix. Il mêle le jazz à une partition épuré avec quelques accents que l'on imaginerait plus chez son collègue Hisaishi. 

On s'attend évidemment à un film qui finit bien. Mais il ne finit pas comme on peut l'attendre d'une comédie romantique ou même dramatique. On y sourit, on y rit, mais on y pleure aussi, signe des grands films. Voilà pourquoi au fil des ans, je vois les notes remonter dans les sites spécialisés et que je prends toujours du plaisir à le revoir.

[La Bande Annonce en V.O. ![video](/images/youtube.png)](https://www.youtube.com/watch?v=hjydAG1lG_8){:target="_blank"}
