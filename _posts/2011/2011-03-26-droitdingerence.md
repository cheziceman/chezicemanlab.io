---
layout: post
title: Géopolitique - Du droit d'ingérence
category: geopolitique
tags: géopolitique, libye, 
---

**L'intervention de la France et de ses "alliés" en Libye montre encore en action le désormais fameux droit d'ingérence. Un droit souvent à sens unique et aux choix souvent étranges.**

Le terme a été créé par le philosophe Jean-François Revel (source wikipedia) en 1979 et représente le droit d'intervention d'un pays dans les affaires internes d'un autre pays sous mandat d'une organisation supranationale.De manière plus claire, c'est en se basant sur l'argument humanitaire qu'avec le concours des nations-unies, des états interviennent militairement dans un autre étât. Le terme a été popularisé par Ronny Brauman* et Bernard Kouchner avec MSF mais ces deux personnages ont depuis montré leur coté sombre.

Voyons maintenant quelques exemples :

* La Somalie où il n'y avait plus réellement d'état au moment de l'intervention internationale en 1992 est bien une intervention à but humanitaire et un fiasco militaire. Elle s'est faite sous mandat de l'ONU.

* La Libye en 1986 a été bombardée par les Etats-unis sans aucun mandat, uniquement en représailles d'attentat supposément commandités par la Lybie (des preuves ont été apportées par la suite). Ici il ne s'agit pas d'intervenir dans les affaires internes du pays mais de viser un dirigeant.
* Le Kosovo : Alors que la résolution de l'ONU n'exprimait que l'envoi d'une force de maintien de la paix dans cette province de la Yougoslavie objet de violences interethniques entre Serbes et Albanais, l'administration sous mandat de l'ONU a aboutit à une déclaration d'indépendance du Kosovo, toujours pas reconnu depuis par les instances internationales. Un certain Kouchner sera nommé administrateur par l'ONU.
* La Tchétchénie réclame son indépendance et est l'objet d'une guerre civile depuis la perestroika. De part et d'autre, des exactions, attentats et massacres ont été commis sans qu'aucune intervention de l'ONU n'ait été suggérée, la Russie étant membre du conseil de sécurité.
* Les révoltes au Tibet ont été sévèrement et militairement réprimées par l'armée chinoise, l'autonomie ou l'indépendance de cette région n'étant par reconnue. Des déplacements de population ont également permis de mettre en minorité l'ethnie tibétaine. La Chine est également membre du conseil de sécurité.
* Au Rwanda, des violences ethniques mettent à feu et à sang le pays. Il faudra attendre des semaines avant que l'ONU lance une résolution permettant l'envoi d'une force d'interposition. On estime les victimes à 800 000.
* En Birmanie, une révolte est sévèrement réprimée par la junte au pouvoir. Aucune résolution de l'ONU n'est prise. La Junte continue de vivre de l'exploitation du pétrole par des sociétés occidentales.
* Au Libéria, la guerre civile fait rage et met à feu et à sang le pays :  150 000 morts et près d'un million de déplacés. L'ONU s'appuie sur la CEDEAO, mais sans résultat. 3 ans après le début du conflit, une résolution sur l'embargo des armes est prise. Une force d'observation se met en place après des accords de paix en 93. La guerre reprendra dans les années 2000 et ce n'est qu'à la défaite de Charles Taylor (présenté au TPI) que le pays connaîtra une paix encore fragile.
* Au Darfour, la famine et les massacres font rage dans l'indifférence générale : 300 000 Morts. Il faudra une médiatisation et le soutien de quelques stars pour que l'ONU s'intéresse au problème et mandate des troupes.
* En Irak, après avoir trouvé l'excuse du massacre d'enfant dans un hôpital pour intervenir au koweit, les Etats-Unis inventent des armes de destruction massive pour intervenir sans mandat de l'ONU. Aucune action n'avait été entreprise lors du gazage, pourtant bien réel, des populations Kurdes. La "libération" de l'Irak a causé la mort de plus de 150 000 civils.
* En Palestine, entre luttes internes palestiniennes et occupation israélienne,  des civils meurent chaque jour de part et d'autre. Aucune force de l'ONU n'a jamais été proposée. L'ONU ne fait que dénoncer passivement la situation.

Ces quelques exemples montrent que les pays les plus puissants de la planète peuvent se permettre de régler de manière violente leur problèmes internes tout en s'arrogeant le droit d'intervenir ailleurs, surtout lorsqu'ils ont des intérêts à défendre. Hubert Védrine a récemment rappelé, de manière fort juste, son opposition à ce concept où il voit un prolongement du colonialisme. C'est effectivement ce que l'on remarque encore aujourd'hui par le traitement à deux vitesses des diverses révolutions touchant le monde arabe. On traite ainsi différemment le Yemen, Bahrein, la Syrie et la Libye, les liens avec les membres permanents du conseil de sécurité de l'ONU n'étant pas les même. La Chine et la Russie ont souvent une position anti-ingérence, leurs problèmes internes étant en contradiction avec cette notion.

L'intervention militaire en Libye est donc le dernier exemple de cette ingérence à deux vitesse qui arrive bien tard pour sauver les insurgés. Elle fait également des victimes parmi les populations civiles restées dans les territoires loyalistes. Devait-on pour autant laisser un dictateur reprendre le pouvoir et stabiliser la situation dans ce pays au prix du sang? En ne répondant pas de manière globale et homogène partout dans le monde où ces problèmes se posent, on ne fait qu'accentuer le ressentiment vis à vis de l'ONU et du monde occidental.  Ainsi, Laurent Gbagbo joue de cet argument pour se maintenir au pouvoir et discréditer l'ONU en Afrique. Il ne sera certainement pas le dernier et en croyant régler un problème, nous risquons bien, nous occidentaux, d'en créer bien plus à ignorer ne seraient-ce que les maux profonds du continent africain.

* : Ronny Brauman s'est fait entendre pour s'opposer à l'intervention en Libye

